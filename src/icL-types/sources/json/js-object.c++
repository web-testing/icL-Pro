#include "js-object.h++"

#include "js-array.h++"

#include <QJsonArray>
#include <QJsonObject>



jsObject::jsObject() = default;

QJsonObject jsObject::toJson() const {
    QJsonObject ret;

    for (auto it = begin(); it != end(); it++) {
        switch (it.value().type()) {
        case icType::Bool:
            ret.insert(it.key().getData(), it.value().toBool());
            break;

        case icType::Int:
            ret.insert(it.key().getData(), it.value().toInt());
            break;

        case icType::Double:
            ret.insert(it.key().getData(), it.value().toDouble());
            break;

        case icType::String:
            ret.insert(it.key().getData(), it.value().toString().getData());
            break;

        case icType::JsObject:
            ret.insert(it.key().getData(), it.value().toJsObject().toJson());
            break;

        case icType::JsArray:
            ret.insert(it.key().getData(), it.value().toArray().toJson());
            break;

        default:;
            // nothing to do
        }
    }

    return ret;
}

jsObject & jsObject::fromJson(const QJsonObject & object) {
    for (auto it = object.begin(); it != object.end(); it++) {
        switch (it.value().type()) {
        case QJsonValue::Bool:
            insert(it.key(), it.value().toBool());
            break;

        case QJsonValue::Double:
            insert(it.key(), it.value().toDouble());
            break;

        case QJsonValue::String:
            insert(it.key(), icString{it.value().toString()});
            break;

        case QJsonValue::Array:
            insert(it.key(), jsArray{}.fromJson(it.value().toArray()));
            break;

        case QJsonValue::Object:
            insert(it.key(), jsObject{}.fromJson(it.value().toObject()));
            break;

        default:
            insert(it.key(), icVariant{});
        }
    }

    return *this;
}
