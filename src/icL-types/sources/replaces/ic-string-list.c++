#include "ic-string-list.h++"

#include "ic-set.h++"

#include <ic-regex.h++>

#include <QRegularExpression>

#include <algorithm>



icStringList::icStringList() = default;

icStringList::icStringList(const icString & value) {
    append(value);
}

icStringList::icStringList(const QList<QString> & list) {
    d.append(list);
}

icStringList::icStringList(const icString & value1, const icString & value2) {
    append(value1);
    append(value2);
}

icStringList::icStringList(const icString & value, const icStringList & list) {
    append(value);
    append(list);
}

icStringList::icStringList(const icStringList & list, const icString & value) {
    append(list);
    append(value);
}

icStringList::icStringList(
  const icStringList & list1, const icStringList & list2) {
    append(list1);
    append(list2);
}

bool icStringList::isEmpty() const {
    return d.isEmpty();
}

int icStringList::length() const {
    return d.length();
}

bool icStringList::contains(const icString & str, bool caseSensitive) const {
    return d.contains(
      str, caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive);
}

int icStringList::count(const icString & str) const {
    return d.count(str);
}

icString icStringList::first() const {
    return d.first();
}

int icStringList::indexOf(const icString & str, int start) const {
    return d.indexOf(str, start);
}

int icStringList::indexOf(const icRegEx & str, int start) const {
    return d.indexOf(str, start);
}

icString icStringList::join(const icString & delimiter) const {
    return d.join(delimiter);
}

icString icStringList::join(const icChar & delimiter) const {
    return d.join(delimiter);
}

int icStringList::lastIndexOf(const icString & str, int start) const {
    return d.lastIndexOf(str, start);
}

int icStringList::lastIndexOf(const icRegEx & str, int start) const {
    return d.lastIndexOf(str, start);
}

icSet<icString> icStringList::toSet() const {
    icSet<icString> ret;

    for (auto & str : d) {
        ret.insert(str);
    }

    return ret;
}

icStringList & icStringList::append(const icString & str) {
    d.append(str);
    return *this;
}

icStringList & icStringList::append(const icStringList & list) {
    for (auto & str : list) {
        d.append(str);
    }
    return *this;
}

icStringList & icStringList::insert(int index, const icString & str) {
    d.insert(index, str);
    return *this;
}

icStringList & icStringList::move(int from, int to) {
    d.move(from, to);
    return *this;
}

icStringList & icStringList::prepend(const icString & str) {
    d.prepend(str);
    return *this;
}

icStringList & icStringList::removeAll(const icString & str) {
    d.removeAll(str);
    return *this;
}

icStringList & icStringList::removeAt(int index) {
    d.removeAt(index);
    return *this;
}

icStringList & icStringList::removeDuplicates() {
    d.removeDuplicates();
    return *this;
}

icStringList & icStringList::removeFirst() {
    d.removeFirst();
    return *this;
}

icStringList & icStringList::removeLast() {
    d.removeLast();
    return *this;
}

icStringList & icStringList::removeOne(const icString & str) {
    d.removeOne(str);
    return *this;
}

icStringList & icStringList::replaceInStrings(
  const icString & before, const icString & after) {
    d.replaceInStrings(before, after);
    return *this;
}

icStringList & icStringList::sort(bool caseSensitive) {
    d.sort(caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive);
    return *this;
}

icStringList icStringList::filter(
  const icString & str, bool caseSensitive) const {
    return d.filter(
      str, caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive);
}

icStringList icStringList::filter(const icRegEx & rx) const {
    return d.filter(rx);
}

icStringList icStringList::mid(int pos, int n) const {
    return d.mid(pos, n);
}

icStringList & icStringList::operator<<(const icString & value) {
    append(value);
    return *this;
}

icString icStringList::operator[](int i) const {
    return d[i];
}

bool icStringList::operator==(const icStringList & other) const {
    return d == other.d;
}
