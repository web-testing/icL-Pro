#include "ic-file.h++"

#include "ic-string.h++"

icFile::icFile() = default;

icFile::icFile(const char * const path)
    : d(path) {
    d.open(QFile::ReadOnly);
    d.seek(0);
}

icFile::icFile(const icString & path)
    : d(path) {
    d.open(QFile::ReadOnly);
}

bool icFile::isReadable() const {
    return d.isReadable();
}

icString icFile::getFileName() const {
    return d.fileName();
}

bool icFile::operator==(const icFile & other) const {
    return d.fileName() == other.d.fileName();
}
