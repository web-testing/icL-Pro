#include "ic-regex.h++"

#include "ic-string-list.h++"
#include "ic-string.h++"

#include <QRegularExpression>



icRegEx::icRegEx() = default;

icRegEx::icRegEx(const icString & pattern, uint flags)
    : d(pattern, castFlags(flags)) {}

icRegEx::icRegEx(const icString & pattern, const icString & mods)
    : d(pattern, castFlags(mods)) {}

const icString icRegEx::pattern() const {
    return d.pattern();
}

void icRegEx::setPattern(const icString & pattern) {
    d.setPattern(pattern);
}

uint icRegEx::patternOptions() const {
    return castFlagsBack(d.patternOptions());
}

void icRegEx::setPatternOptions(uint flags) {
    d.setPatternOptions(castFlags(flags));
}

icRegExMatch icRegEx::match(const icString & str) const {
    return {d.match(str)};
}

icStringList icRegEx::namedCaptureGroups() const {
    return d.namedCaptureGroups();
}

icRegEx::operator QRegularExpression() {
    return d;
}

icRegEx::operator const QRegularExpression() const {
    return d;
}

icRegEx icRegEx::fromWildcard(const icString & in) {
    return {QRegularExpression::wildcardToRegularExpression(in)};
}

bool icRegEx::operator==(const icRegEx & other) const {
    return d == other.d;
}

QFlags<QRegularExpression::PatternOption> icRegEx::castFlags(uint flags) {
    QFlags<QRegularExpression::PatternOption> ret;


    if (flags & InvertedGreedinessOption)
        ret = ret | QRegularExpression::InvertedGreedinessOption;
    if (flags & CaseInsensitiveOption)
        ret = ret | QRegularExpression::CaseInsensitiveOption;
    if (flags & MultilineOption)
        ret = ret | QRegularExpression::MultilineOption;
    if (flags & DotMatchesEverythingOption)
        ret = ret | QRegularExpression::DotMatchesEverythingOption;
    if (flags & UseUnicodePropertiesOption)
        ret = ret | QRegularExpression::UseUnicodePropertiesOption;
    if (flags & ExtendedPatternSyntaxOption)
        ret = ret | QRegularExpression::ExtendedPatternSyntaxOption;

    return ret;
}

uint icRegEx::castFlagsBack(uint flags) {
    uint ret = 0x0;

    if (flags & QRegularExpression::InvertedGreedinessOption) {
        ret |= InvertedGreedinessOption;
    }
    else if (flags & QRegularExpression::CaseInsensitiveOption) {
        ret |= CaseInsensitiveOption;
    }
    else if (flags & QRegularExpression::MultilineOption) {
        ret |= MultilineOption;
    }
    else if (flags & QRegularExpression::DotMatchesEverythingOption) {
        ret |= DotMatchesEverythingOption;
    }
    else if (flags & QRegularExpression::UseUnicodePropertiesOption) {
        ret |= UseUnicodePropertiesOption;
    }
    else if (flags & QRegularExpression::ExtendedPatternSyntaxOption) {
        ret |= ExtendedPatternSyntaxOption;
    }

    return ret;
}

QFlags<QRegularExpression::PatternOption> icRegEx::castFlags(
  const icString & mods) {
    uint flags = 0;

    if (mods.contains('f')) {
        flags = flags | InvertedGreedinessOption;
    }
    else if (mods.contains('i')) {
        flags = flags | CaseInsensitiveOption;
    }
    else if (mods.contains('m')) {
        flags = flags | MultilineOption;
    }
    else if (mods.contains('s')) {
        flags = flags | DotMatchesEverythingOption;
    }
    else if (mods.contains('u')) {
        flags = flags | UseUnicodePropertiesOption;
    }
    else if (mods.contains('x')) {
        flags = flags | ExtendedPatternSyntaxOption;
    }

    return castFlags(flags);
}

icRegEx operator""_rx(const char * const pattern, unsigned long) {
    return {icString{pattern}};
}

icRegExMatch::icRegExMatch(const QRegularExpressionMatch & match)
    : d(match) {}

bool icRegExMatch::hasMatch() {
    return d.hasMatch();
}

icString icRegExMatch::captured(int id) const {
    return d.captured(id);
}

icString icRegExMatch::captured(const icString & name) const {
    return d.captured(name);
}

icStringList icRegExMatch::capturedTexts() const {
    return d.capturedTexts();
}
