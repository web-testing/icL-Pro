#include "ic-text-stream.h++"

#include "ic-char.h++"
#include "ic-file.h++"
#include "ic-string.h++"

#include <iostream>



icTextStream::icTextStream(icFile * stream) {
    assert(stream->d.isOpen());
    assert(stream->d.isReadable());
    d.open(stream->d.fileName().toStdString(), std::ifstream::binary);
    d.seekg(0, d.beg);
}

icString icTextStream::readAll() {
    icChar   ch;
    icString ret;

    while (atEnd()) {
        *this >> ch;
        ret.append(ch);
    }

    return ret;
}

bool icTextStream::atEnd() {
    return d.peek() == EOF || parsingFailed;
}

int64_t icTextStream::pos() {
    return d.tellg();
}

void icTextStream::open(icFile * stream) {
    d.open(stream->d.fileName().toStdString(), std::ifstream::binary);
    d.seekg(0, d.beg);
}

void icTextStream::open(std::shared_ptr<icFile> stream) {
    d.open(stream->d.fileName().toStdString(), std::ifstream::binary);
    d.seekg(0, d.beg);
}

bool icTextStream::seekBy(int position) {
    d.seekg(pos() + position, d.beg);
    return true;
}

bool icTextStream::seekTo(int position) {
    d.seekg(position, d.beg);
    return true;
}

icTextStream & icTextStream::operator>>(icChar & ch) {
    char byte1 = 0, byte2 = 0, byte3 = 0, byte4 = 0;

    d.get(byte1);

    char zeroPos   = 0;
    char byte1copy = byte1;

    while ((byte1copy & 0b10000000) != 0) {
        zeroPos++;
        byte1copy <<= 1;
    }

    switch (zeroPos) {
    case 0:
        ch = byte1;
        break;

    case 2:
        getAndCheck(byte2);
        break;

    case 3:
        getAndCheck(byte2);
        getAndCheck(byte3);
        break;

    case 4:
        getAndCheck(byte2);
        getAndCheck(byte3);
        getAndCheck(byte4);
        break;

    default:
        printError(byte1);
    }

    if (!parsingFailed) {
        int utf8code = ((byte1 << zeroPos) & 0xFF) >> zeroPos;

        if (byte2 != 0) {
            utf8code <<= 6;
            utf8code |= byte2 & 0x3F;
        }
        if (byte3 != 0) {
            utf8code <<= 6;
            utf8code |= byte3 & 0x3F;
        }
        if (byte4 != 0) {
            utf8code <<= 6;
            utf8code |= byte4 & 0x3F;
        }

        ch = utf8code;
    }

    return *this;
}

void icTextStream::getAndCheck(char & byte) {
    if (!parsingFailed) {
        d.get(byte);
        checkByte(byte);
    }
}

void icTextStream::checkByte(char byte) {
    if (byte != 0 && (byte & 0b11000000) != 0b10000000) {
        printError(byte);
    }
}

void icTextStream::printError(char byte) {
    parsingFailed = true;

    std::cout << std::hex << "Decoding error: uexpected byte " << byte
              << " at position " << d.tellg() << std::endl;
}
