#include "ic-math.h++"

#include <math.h>



constexpr double pi = 3.14159265358979323846;

double icPow(double v, double pow) {
    return std::pow(v, pow);
}

double icSqrt(double v) {
    return std::sqrt(v);
}

double icAcos(double v) {
    return std::acos(v);
}

double icAsin(double v) {
    return std::asin(v);
}

double icAtan(double v) {
    return std::atan(v);
}

int icCeil(double v) {
    return int(std::ceil(v));
}

double icCos(double v) {
    return std::cos(v);
}

double icDegreesToRadians(double v) {
    return v * pi / 180.;
}

double icExp(double v) {
    return std::exp(v);
}

int icFloor(double v) {
    return int(std::floor(v));
}

double icLn(double v) {
    return std::log(v);
}

double icRadiansToDegrees(double v) {
    return v * 180. / pi;
}

int icRound(double v) {
    return int(std::round(v));
}

double icSin(double v) {
    return std::sin(v);
}

double icTan(double v) {
    return std::tan(v);
}
