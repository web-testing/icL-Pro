#include "ic-datetime.h++"

#include <ic-string.h++>

#include <QTimeZone>

#include <utility>



icDateTime::icDateTime(QDateTime dt)
    : d(std::move(dt)) {}

bool icDateTime::isValid() const {
    return d.isValid();
}

int icDateTime::year() const {
    return d.date().year();
}

int icDateTime::month() const {
    return d.date().month();
}

int icDateTime::day() const {
    return d.date().day();
}

int icDateTime::hour() const {
    return d.time().hour();
}

int icDateTime::minute() const {
    return d.time().minute();
}

int icDateTime::second() const {
    return d.time().second();
}

int icDateTime::daysTo(const icDateTime & other) const {
    return int(d.daysTo(other.d));
}

int icDateTime::secsTo(const icDateTime & other) const {
    return int(d.secsTo(other.d));
}

icDateTime & icDateTime::addDays(int days) {
    d = d.addDays(days);
    return *this;
}

icDateTime & icDateTime::addMonths(int months) {
    d = d.addMonths(months);
    return *this;
}

icDateTime & icDateTime::addSecs(int secs) {
    d = d.addSecs(secs);
    return *this;
}

icDateTime & icDateTime::addYears(int years) {
    d = d.addYears(years);
    return *this;
}

icDateTime & icDateTime::setDate(int year, int month, int day) {
    d.setDate({year, month, day});
    return *this;
}

icDateTime & icDateTime::setHMS(int hour, int minute, int second) {
    d.setTime({hour, minute, second});
    return *this;
}

icDateTime & icDateTime::toTimeZone(int seconds) {
    d.toTimeZone(QTimeZone{seconds});
    return *this;
}

icDateTime & icDateTime::toUTC() {
    d.toUTC();
    return *this;
}

icString icDateTime::toString() const {
    return d.toString();
}

icString icDateTime::toString(const icString & format) const {
    return d.toString(format);
}

int icDateTime::toSecsSinceEpoch() const {
    return int(d.toSecsSinceEpoch());
}

icDateTime icDateTime::fromString(const icString & datetime) {
    return QDateTime::fromString(datetime);
}

icDateTime icDateTime::fromString(
  const icString & datetime, const icString & format) {
    return QDateTime::fromString(datetime, format);
}

icDateTime icDateTime::fromSecsSinceEpoch(int seconds) {
    return QDateTime::fromSecsSinceEpoch(seconds);
}

icDateTime icDateTime::currentDateTime() {
    return QDateTime::currentDateTime();
}

icDateTime icDateTime::currentDateTimeUtc() {
    return QDateTime::currentDateTimeUtc();
}

int icDateTime::currentSecsSinceEpoch() {
    return int(QDateTime::currentSecsSinceEpoch());
}

uint64_t icDateTime::currentMSecsSinceEpoch() {
    return uint64_t(QDateTime::currentMSecsSinceEpoch());
}

bool icDateTime::operator==(const icDateTime & other) const {
    return d == other.d;
}
