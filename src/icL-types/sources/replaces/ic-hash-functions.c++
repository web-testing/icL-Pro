#include "ic-hash-functions.h++"

#include "ic-list.h++"
#include "ic-variant.h++"

#include <ic-char.h++>

#include <QHash>



uint qHash(icL::ce::Role role, uint seed) {
    return qHash(int(role), seed);
}

uint qHash(icL::memory::Type type, uint seed) {
    return qHash(int(type), seed);
}

uint qHash(
  const icPair<icL::memory::Type, icL::memory::Type> & pair, uint seed) {
    return qHash(pair.first(), seed) ^ qHash(pair.second(), seed);
}

bool operator<(const icL::memory::Type left, const icL::memory::Type right) {
    return int(left) < int(right);
}

uint qHash(const icList<icVariant> & list, uint seed) {
    uint ret = 0x0;

    for (auto & var : list) {
        ret = ret ^ qHash(var, seed);
    }
    return ret;
}

uint qHash(const icVariant & var, uint seed) {
    switch (var.type()) {
    case icType::Void:
        return 0;

    case icType::Bool:
        return qHash(var.toBool(), seed);

    case icType::Int:
        return qHash(var.toInt(), seed);

    case icType::Double:
        return qHash(var.toDouble(), seed);

    case icType::String:
        return qHash(var.toString(), seed);

    case icType::StringList:
        return qHash(var.toStringList(), seed);

    default:
        return 0;
    }
}

uint qHash(const icString & str, uint seed) {
    return qHash(str.getData(), seed);
}

uint qHash(
  const icPair<icList<icL::memory::Type>, icList<icL::memory::Type>> & pair,
  uint                                                                 seed) {
    return qHash(pair.first(), seed) ^ qHash(pair.second(), seed);
}

uint qHash(icL::cp::ResultType type, uint seed) {
    return qHash(int(type), seed);
}

uint qHash(const icChar & ch, uint seed) {
    return qHash(QChar(ch), seed);
}

uint qHash(const icType & type, uint seed) {
    return qHash(int(type), seed);
}

uint qHash(const icList<icL::memory::Type> & list, uint seed) {
    uint ret = 0x0;

    for (auto & var : list) {
        ret = ret ^ qHash(var, seed);
    }
    return ret;
}
