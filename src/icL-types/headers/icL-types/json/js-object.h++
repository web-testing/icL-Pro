#ifndef types_jsObject
#define types_jsObject

#include "../replaces/ic-object.h++"
#include "../replaces/ic-string.h++"



class icVariant;

class jsObject : public icObject<icString, icVariant>
{
public:
    jsObject();

    class QJsonObject toJson() const;

    jsObject & fromJson(const QJsonObject & object);
};

#endif  // types_jsObject
