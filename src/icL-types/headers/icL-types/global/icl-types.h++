#ifndef types_Global
#define types_Global

#include "global.h++"

#if defined(ICLTYPES_LIBRARY)
#/**/ define icL_SHARED icL_LIB_EXPORT
#else
#/**/ define icL_SHARED icl_LIB_IMPORT
#endif

#endif  // types_Global
