#ifndef icL_Global
#define icL_Global

#ifdef OS_WIN
#/**/ define icL_LIB_EXPORT __declspec(dllexport)
#/**/ define icL_LIB_IMPORT __declspec(dllimport)
#else
#/**/ define icL_LIB_EXPORT __attribute__((visibility("default")))
#/**/ define icl_LIB_IMPORT __attribute__((visibility("default")))
#endif

#endif  // icL_Global
