#ifndef types_icHashFunctions
#define types_icHashFunctions

#include "ic-list.h++"
#include "ic-pair.h++"
#include "ic-types-enum.h++"

#include <icL-ce-base/main/ce.h++>

#include <icL-cp/result/type.h++>


uint qHash(enum icL::ce::Role role, uint seed = 0);
uint qHash(icL::memory::Type type, uint seed = 0);
uint qHash(
  const icPair<icL::memory::Type, icL::memory::Type> & pair, uint seed = 0);
uint qHash(const icList<icVariant> & list, uint seed = 0);
uint qHash(const icList<icL::memory::Type> & list, uint seed = 0);
uint qHash(const icVariant & var, uint seed = 0);
uint qHash(const icString & str, uint seed = 0);
uint qHash(
  const icPair<icList<icL::memory::Type>, icList<icL::memory::Type>> & pair,
  uint seed = 0);
uint qHash(enum icL::cp::ResultType type, uint seed = 0);
uint qHash(const class icChar & ch, uint seed = 0);
uint qHash(const icType & type, uint seed = 0);

#endif  // types_icHashFunctions
