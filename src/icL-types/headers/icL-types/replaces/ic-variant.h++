#ifndef types_Variant
#define types_Variant

#include "ic-types-enum.h++"

#include <icL-types/global/icl-types.h++>

#include <any>



class icString;
class icDateTime;
class icStringList;
class icRegEx;
class jsArray;
class jsObject;

namespace icL {

namespace il {
struct Cookie;
struct DB;
struct Query;
struct Element;
struct Elements;
struct File;
struct Tab;
struct TargetData;
struct Window;
struct Session;
struct Position;
struct LambdaTarget;
struct JsLambda;
struct JsFile;
struct Document;
}  // namespace il

namespace memory {
struct Set;
struct Object;
struct PackedValue;
}  // namespace memory

}  // namespace icL

class icL_SHARED icVariant
{

    std::any d;
    icType   m_type = icType::Void;

public:
    icVariant();
    icVariant(const bool value);
    icVariant(const int & value);
    icVariant(const double & value);
    icVariant(const icString & string);
    icVariant(const icDateTime & dt);
    icVariant(const icStringList & list);
    icVariant(const icRegEx & re);
    icVariant(const icL::memory::PackedValue & packet);
    icVariant(const jsArray & array);
    icVariant(const jsObject & object);

    static icVariant makeVoid();

    icType type() const;

    bool isBool() const;
    bool isInt() const;
    bool isDouble() const;
    bool isString() const;
    bool isCookie() const;
    bool isDB() const;
    bool isQuery() const;
    bool isElement() const;
    bool isElements() const;
    bool isDocument() const;
    bool isSession() const;
    bool isWindow() const;
    bool isTab() const;
    bool isDateTime() const;
    bool isStringList() const;
    bool isRegEx() const;
    bool isObject() const;
    bool isJsObject() const;
    bool isSet() const;
    bool isPacked() const;
    bool isFile() const;
    bool isJsArray() const;
    bool isPosition() const;
    bool isLambda() const;
    bool isJsLambda() const;
    bool isJsFile() const;

    bool isNull() const;  // rename after to isVoid
    bool isVoid() const;
    bool isValid() const;

    template <typename T>
    static icVariant fromValueTemplate(const T & value, icType type);

    static icVariant fromValue(const bool & value);
    static icVariant fromValue(const int & value);
    static icVariant fromValue(const double & value);
    static icVariant fromValue(const icString & string);
    static icVariant fromValue(const icL::il::Cookie & cookie);
    static icVariant fromValue(const icL::il::DB & db);
    static icVariant fromValue(const icL::il::Query & query);
    static icVariant fromValue(const icL::il::Element & element);
    static icVariant fromValue(const icL::il::Elements & elements);
    static icVariant fromValue(const icL::il::Document & document);
    static icVariant fromValue(const icL::il::Session & session);
    static icVariant fromValue(const icL::il::Window & window);
    static icVariant fromValue(const icL::il::Tab & tab);
    static icVariant fromValue(const icDateTime & dt);
    static icVariant fromValue(const icStringList & list);
    static icVariant fromValue(const icRegEx & re);
    static icVariant fromValue(const icL::memory::Object & obj);
    static icVariant fromValue(const icL::memory::Set & set);
    static icVariant fromValue(const icL::memory::PackedValue & packet);
    static icVariant fromValue(const icL::il::File & file);
    static icVariant fromValue(const jsArray & array);
    static icVariant fromValue(const jsObject & object);
    static icVariant fromValue(const icL::il::Position & postion);
    static icVariant fromValue(const icL::il::LambdaTarget & lamda);
    static icVariant fromValue(const icL::il::JsLambda & lambda);
    static icVariant fromValue(const icL::il::JsFile & file);

    const bool &                     toBool() const;
    const int &                      toInt() const;
    const double &                   toDouble() const;
    const icString &                 toString() const;
    const icL::il::Cookie &          toCookie() const;
    const icL::il::DB &              toDB() const;
    const icL::il::Query &           toQuery() const;
    const icL::il::Element &         toElement() const;
    const icL::il::Elements &        toElements() const;
    const icL::il::Document &        toDocument() const;
    const icL::il::Session &         toSession() const;
    const icL::il::Window &          toWindow() const;
    const icL::il::Tab &             toTab() const;
    const icL::il::TargetData        toTarget() const;
    const icDateTime &               toDateTime() const;
    const icStringList &             toStringList() const;
    const icRegEx &                  toRegEx() const;
    const icL::memory::Object &      toObject() const;
    const icL::memory::Set &         toSet() const;
    const icL::memory::PackedValue & toPacked() const;
    const icL::il::File &            toFile() const;
    const jsArray &                  toArray() const;
    const jsObject &                 toJsObject() const;
    const icL::il::Position &        toPosition() const;
    const icL::il::LambdaTarget &    toLambda() const;
    const icL::il::JsLambda &        toJsLambda() const;
    const icL::il::JsFile &          toJsFile() const;

    icVariant & clear();

    bool operator==(const icVariant & other) const;

    operator bool() const;
    operator int() const;
    operator double() const;
    operator icString() const;
    operator icDateTime() const;
    operator icStringList() const;
    operator icRegEx() const;
};

#endif  // types_Variant
