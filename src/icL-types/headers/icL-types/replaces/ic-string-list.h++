#ifndef types_StringList
#define types_StringList

#include "ic-char.h++"
#include "ic-list.h++"
#include "ic-string.h++"



template <typename>
class icSet;

class icStringList
{
    QStringList d;

public:
    icStringList();
    icStringList(const icString & value);
    icStringList(const QList<QString> & list);
    icStringList(const icString & value1, const icString & value2);
    icStringList(const icString & value, const icStringList & list);
    icStringList(const icStringList & list, const icString & value);
    icStringList(const icStringList & list1, const icStringList & list2);

    using Iterator      = QStringList::Iterator;
    using ConstIterator = QStringList::ConstIterator;

    // properties

    bool isEmpty() const;
    int  length() const;

    // immutable members

    bool contains(const icString & str, bool caseSensitive = true) const;

    int count(const icString & str) const;

    icString first() const;

    int indexOf(const icString & str, int start = 0) const;
    int indexOf(const icRegEx & str, int start = 0) const;

    icString join(const icString & delimiter) const;
    icString join(const icChar & delimiter) const;

    int lastIndexOf(const icString & str, int start = 0) const;
    int lastIndexOf(const icRegEx & str, int start = 0) const;

    // cast functions

    icSet<icString> toSet() const;

    // mutable members

    icStringList & append(const icString & str);
    icStringList & append(const icStringList & list);

    icStringList & insert(int index, const icString & str);

    icStringList & move(int from, int to);

    icStringList & prepend(const icString & str);

    icStringList & removeAll(const icString & str);
    icStringList & removeAt(int index);
    icStringList & removeDuplicates();
    icStringList & removeFirst();
    icStringList & removeLast();
    icStringList & removeOne(const icString & str);
    icStringList & replaceInStrings(
      const icString & before, const icString & after);
    icStringList & sort(bool caseSensitive = true);

    // generating new lists

    icStringList filter(const icString & str, bool caseSensitive = true) const;
    icStringList filter(const icRegEx & rx) const;

    icStringList mid(int pos, int n) const;

    // operators

    icStringList & operator<<(const icString & value);
    icString       operator[](int i) const;

    bool operator==(const icStringList & other) const;

    // iterators

    auto begin() {
        return d.begin();
    }

    auto end() {
        return d.end();
    }

    auto begin() const {
        return d.begin();
    }

    auto end() const {
        return d.end();
    }
};

#endif  // types_StringList
