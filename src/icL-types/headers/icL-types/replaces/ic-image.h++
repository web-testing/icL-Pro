#ifndef ICIMAGE_H
#define ICIMAGE_H



class icString;

class icImage
{
public:
    icImage();

    static void saveData(const icString & base64, const icString & path);
};

#endif  // ICIMAGE_H
