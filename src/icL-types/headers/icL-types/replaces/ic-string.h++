#ifndef types_String
#define types_String

#include <QString>



class icChar;
class icRegEx;
class icStringList;

class icString
{
    QString d;

public:
    icString() = default;
    icString(const char * cString);
    icString(const icChar & ch);
    icString(QString str);

    operator QString() const;

    // properties

    bool isEmpty() const;
    int  length() const;

    // immutable members

    icChar at(int i) const;

    bool beginsWith(const icString & str) const;

    bool contains(char ch) const;
    bool contains(icChar ch) const;
    bool contains(const char * substr) const;
    bool contains(const icString & substr) const;

    bool compare(const icString & other, bool cs = true) const;

    int count(const icString & str) const;
    int count(const icRegEx & regex) const;

    bool endsWith(const icString & str) const;

    int indexOf(const icChar & ch, int start = 0) const;
    int indexOf(const icString & str, int start = 0) const;
    int indexOf(const icRegEx & rx, int start = 0) const;

    int lastIndexOf(const icChar & ch, int start = -1) const;
    int lastIndexOf(const icString & str, int start = -1) const;
    int lastIndexOf(const icRegEx & rx, int start = -1) const;

    const QString & getData() const;

    // casting members

    int toInt() const;
    int toInt(bool & ok) const;

    double toDouble() const;
    double toDouble(bool & ok) const;

    std::string toString() const;

    // mutable members

    icString & append(const icString & str);

    icString & clear();

    icString & insert(int pos, const icString & str);
    icString & insert(int pos, const icChar & ch);

    icString & prepend(const icString & str);

    icString & replace(int pos, int n, const icString & after);
    icString & replace(const icString & before, const icString & after);
    icString & replace(const char * before, const icString & after);
    icString & replace(const char * before, const char * after);
    icString & replace(const icRegEx & before, const icString & after);

    icString & remove(int pos, int n = -1);
    icString & remove(const icString & what, bool cs = true);
    icString & remove(const icRegEx & regex);

    icString & toLower();
    icString & toUpper();

    /**
     * @brief fixLast replaces the last symbol with `]` of it's equal to `,`
     */
    icString & fixLast();

    // new strings generating members

    icString clone() const;

    icString toLower() const;
    icString toUpper() const;

    icString left(int n) const;

    icString leftJustified(
      int width, const icString & fill, bool truncate = false) const;

    icString mid(int pos, int n) const;

    icString right(int n) const;

    icString rightJustified(
      int width, const icString & fill, bool truncate = false) const;

    icStringList split(
      const icString & delimiter, bool keepEmptyParts = true,
      bool caseSensitive = true) const;
    icStringList split(
      const icRegEx & delimiter, bool keepEmptyParts = true) const;

    icString trimmed();

    // operator members

    icString & operator=(const icString & other);

    bool operator==(const icString & other) const;
    bool operator!=(const icString & other) const;

    bool operator<(const icString & other) const;
    bool operator>(const icString & other) const;
    bool operator<=(const icString & other) const;
    bool operator>=(const icString & other) const;

    icString & operator%(const char * other);
    icString & operator%(const icString & other);
    icString & operator%(const icChar & other);
    icString & operator%(const char & other);

    icString operator%(const char * other) const;
    icString operator%(const icString & other) const;
    icString operator%(const icChar & other) const;
    icString operator%(const char & other) const;

    icString operator+(const icString & other) const;

    icString & operator+=(const icString & other);
    icString & operator+=(const icChar & other);
    icString & operator+=(char other);

    icChar operator[](int index) const;

    // static members

    static icString number(int i);
    static icString number(double i);
};

// non-member operators

icString operator+(const char * s1, const icString & s2);
icString operator+(const icString s1, const char * s2);
icString operator%(const char * s1, const icString & s2);
icString operator%(char s1, const icString & s2);
icString operator""_str(const char * str, size_t size);

#endif  // types_String
