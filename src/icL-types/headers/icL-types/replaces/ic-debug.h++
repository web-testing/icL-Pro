#ifndef types_ICDebug
#define types_ICDebug

#include "ic-hash-functions.h++"

class ICDebug
{
public:
    static ICDebug & instance();

    ICDebug & operator<<(const icString & str);
};

#define icDebug ICDebug::instance
#define icWarning ICDebug::instance

#endif  // types_ICDebug
