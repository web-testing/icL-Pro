#ifndef type_TypesEnum
#define type_TypesEnum

enum class icType {
    Initial,
    Void,
    Bool,
    Int,
    Double,
    String,
    Cookie,
    DB,
    Query,
    Element,
    Elements,
    Document,
    Session,
    Window,
    Tab,
    DateTime,
    StringList,
    RegEx,
    Object,
    Set,
    Packed,
    File,
    JsArray,
    JsObject,
    Position,
    Lamda,
    JsLambda,
    JsFile
};

#endif  // type_TypesEnum
