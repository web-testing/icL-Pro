#ifndef types_icFile
#define types_icFile

#include <QFile>



class icString;

class icFile
{
    QFile d;

public:
    icFile();
    icFile(const char * path);
    icFile(const icString & path);

    bool isReadable() const;

    icString getFileName() const;

    bool operator==(const icFile & other) const;

    friend class icTextStream;
};

#endif  // types_icFile
