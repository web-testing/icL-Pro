#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>

#include <icL-shared/main/engine.h++>

#include <QDir>



int main() {
    using namespace icL;

    shared::Engine engine;

    engine.finalize();

    engine.il()->vms->init(QDir::homePath() + "/~/test/main.icL");
    engine.il()->vms->run();

    return 0;
}
