#ifndef cp_CP
#define cp_CP

#include "../flyer/flyer.h++"
#include "../result/structs.h++"

#include <icL-il/auxiliary/source-of-code.h++>
#include <icL-il/main/cp.h++>
#include <icL-il/structures/code-fragment.h++>



namespace icL {

namespace ce {
class Keyword;
}

namespace cp {

/**
 * @brief The CP class represents a command processor
 */
class CP
    : public il::CP
    , public il::Node
{
    /// \brief flyer is the tokenizer engine
    Flyer flyer;

    /// \brief m_lastResult is the last key as string
    FlyResult m_lastToken;

    /// \brief code is a fragment of shared source of code
    il::CodeFragment code;

    /// \brief repeatToken forces to repeat the current token
    bool repeatToken = false;

    // padding
    long : 56;

public:
    CP(il::InterLevel * il, const il::CodeFragment & code);

    /**
     * @brief parseNext parses the next logical symbol
     * @return the pointer to created block
     */
    ce::CE * parseNext();

    /**
     * @brief lastKey returns the last catched key
     * @return the last catched key as string
     */
    icString lastKey();

    /**
     * @brief getFilePath gets the path of current file
     * @return path to current file
     */
    icString getFilePath();

    /**
     * @brief getFilePathLineChar gets the path to current file with line nr
     * @return the path to current file with line & char number
     */
    icString getFilePathLineChar();

    /**
     * @brief toCodeF cast to code fragment
     * @param result is the parsing result
     * @return a code fragment based on parsing result
     */
    il::CodeFragment toCodeF(const FlyResult & result);

    /**
     * @brief reset resets flayers position and range
     * @param code is the code to execute
     * @note used in keep alive layers
     */
    void reset(const il::CodeFragment & code);

    /**
     * @brief repeat forces to repeat the last token
     */
    void repeat();

protected:
    ce::CE * withModifiers(
      ce::Keyword * keyword,
      icStringList (icL::*getter)() = &icL::getModifiers);
    ce::CE * withModifiersWithNotSupport(ce::Keyword * keyword);

private:
    // common

    ce::CE * pack_local(const FlyResult & result);
    ce::CE * pack_global(const FlyResult & result);
    ce::CE * pack_function(const FlyResult & result);
    ce::CE * pack_property(const FlyResult & result);
    ce::CE * pack_method(const FlyResult & result);
    ce::CE * pack_key(const FlyResult & result);
    ce::CE * pack_identifier(const FlyResult & result);

    // keywords

    ce::CE * pack_assert(const FlyResult & result);
    ce::CE * pack_any(const FlyResult & result);
    ce::CE * pack_case(const FlyResult & result);
    ce::CE * pack_do(const FlyResult & result);
    ce::CE * pack_else(const FlyResult & result);
    ce::CE * pack_emit(const FlyResult & result);
    ce::CE * pack_emitter(const FlyResult & result);
    ce::CE * pack_exists(const FlyResult & result);
    ce::CE * pack_filter(const FlyResult & result);
    ce::CE * pack_for(const FlyResult & result);
    ce::CE * pack_if(const FlyResult & result);
    ce::CE * pack_jammer(const FlyResult & result);
    ce::CE * pack_listen(const FlyResult & result);
    ce::CE * pack_notify(const FlyResult & result);
    ce::CE * pack_now(const FlyResult & result);
    ce::CE * pack_range(const FlyResult & result);
    ce::CE * pack_slot(const FlyResult & result);
    ce::CE * pack_switch(const FlyResult & result);
    ce::CE * pack_wait(const FlyResult & result);
    ce::CE * pack_while(const FlyResult & result);

    // singletons and commands

    ce::CE * pack_Alert(const FlyResult & result);
    ce::CE * pack_By(const FlyResult & result);
    ce::CE * pack_Cookies(const FlyResult & result);
    ce::CE * pack_Datetime(const FlyResult & result);
    ce::CE * pack_DB(const FlyResult & result);
    ce::CE * pack_DBManager(const FlyResult & result);
    ce::CE * pack_Document(const FlyResult & result);
    ce::CE * pack_DSV(const FlyResult & result);
    ce::CE * pack_File(const FlyResult & result);
    ce::CE * pack_Files(const FlyResult & result);
    ce::CE * pack_icL(const FlyResult & result);
    ce::CE * pack_Import(const FlyResult & result);
    ce::CE * pack_Key(const FlyResult & result);
    ce::CE * pack_Log(const FlyResult & result);
    ce::CE * pack_Make(const FlyResult & result);
    ce::CE * pack_Math(const FlyResult & result);
    ce::CE * pack_Mouse(const FlyResult & result);
    ce::CE * pack_Move(const FlyResult & result);
    ce::CE * pack_Request(const FlyResult & result);
    ce::CE * pack_Query(const FlyResult & result);
    ce::CE * pack_Session(const FlyResult & result);
    ce::CE * pack_Sessions(const FlyResult & result);
    ce::CE * pack_Signal(const FlyResult & result);
    ce::CE * pack_Stack(const FlyResult & result);
    ce::CE * pack_Stacks(const FlyResult & result);
    ce::CE * pack_State(const FlyResult & result);
    ce::CE * pack_Tab(const FlyResult & result);
    ce::CE * pack_Tabs(const FlyResult & result);
    ce::CE * pack_Types(const FlyResult & result);
    ce::CE * pack_View(const FlyResult & result);
    ce::CE * pack_Window(const FlyResult & result);
    ce::CE * pack_Windows(const FlyResult & result);

    // types

    ce::CE * pack_bool(const FlyResult & result);
    ce::CE * pack_cookie(const FlyResult & result);
    ce::CE * pack_database(const FlyResult & result);
    ce::CE * pack_datetime(const FlyResult & result);
    ce::CE * pack_document(const FlyResult & result);
    ce::CE * pack_double(const FlyResult & result);
    ce::CE * pack_element(const FlyResult & result);
    ce::CE * pack_file(const FlyResult & result);
    ce::CE * pack_handler(const FlyResult & result);
    ce::CE * pack_int(const FlyResult & result);
    ce::CE * pack_lambda_icl(const FlyResult & result);
    ce::CE * pack_lambda_js(const FlyResult & result);
    ce::CE * pack_lambda_sql(const FlyResult & result);
    ce::CE * pack_list(const FlyResult & result);
    ce::CE * pack_listener(const FlyResult & result);
    ce::CE * pack_object(const FlyResult & result);
    ce::CE * pack_query(const FlyResult & result);
    ce::CE * pack_set(const FlyResult & result);
    ce::CE * pack_session(const FlyResult & result);
    ce::CE * pack_string(const FlyResult & result);
    ce::CE * pack_tab(const FlyResult & result);
    ce::CE * pack_void(const FlyResult & result);
    ce::CE * pack_window(const FlyResult & result);
    ce::CE * pack_system_type(const FlyResult & result);

    // other literal

    ce::CE * pack_bool_literal(const FlyResult & result);
    ce::CE * pack_int_literal(const FlyResult & result);
    ce::CE * pack_double_literal(const FlyResult & result);
    ce::CE * pack_string_literal(const FlyResult & result);
    ce::CE * pack_js_value(const FlyResult & result);
    ce::CE * pack_js_file(const FlyResult & result);
    ce::CE * pack_js_code(const FlyResult & result);
    ce::CE * pack_js_literal(const FlyResult & result);
    ce::CE * pack_element_literal(const FlyResult & result);
    ce::CE * pack_css_literal(const FlyResult & result);
    ce::CE * pack_xpath_literal(const FlyResult & result);
    ce::CE * pack_link_literal(const FlyResult & result);
    ce::CE * pack_links_literal(const FlyResult & result);
    ce::CE * pack_tag_literal(const FlyResult & result);
    ce::CE * pack_tags_literal(const FlyResult & result);
    ce::CE * pack_input_literal(const FlyResult & result);
    ce::CE * pack_field_literal(const FlyResult & result);
    ce::CE * pack_regex_literal(const FlyResult & result);

    // operators

    ce::CE * pack_And(const FlyResult & result);
    ce::CE * pack_Bg(const FlyResult & result);
    ce::CE * pack_BgEq(const FlyResult & result);
    ce::CE * pack_BgEqSm(const FlyResult & result);
    ce::CE * pack_BgSm(const FlyResult & result);
    ce::CE * pack_Bs(const FlyResult & result);
    ce::CE * pack_Cl(const FlyResult & result);
    ce::CE * pack_ClAs(const FlyResult & result);
    ce::CE * pack_ClCl(const FlyResult & result);
    ce::CE * pack_ClSt(const FlyResult & result);
    ce::CE * pack_ClEq(const FlyResult & result);
    ce::CE * pack_Eq(const FlyResult & result);
    ce::CE * pack_EqEq(const FlyResult & result);
    ce::CE * pack_Mn(const FlyResult & result);
    ce::CE * pack_Nt(const FlyResult & result);
    ce::CE * pack_NtEq(const FlyResult & result);
    ce::CE * pack_NtSm(const FlyResult & result);
    ce::CE * pack_NtSt(const FlyResult & result);
    ce::CE * pack_Or(const FlyResult & result);
    ce::CE * pack_Or2(const FlyResult & result);
    ce::CE * pack_Pc(const FlyResult & result);
    ce::CE * pack_Pl(const FlyResult & result);
    ce::CE * pack_Sm(const FlyResult & result);
    ce::CE * pack_SmBg(const FlyResult & result);
    ce::CE * pack_SmEq(const FlyResult & result);
    ce::CE * pack_SmEqBg(const FlyResult & result);
    ce::CE * pack_SmSm(const FlyResult & result);
    ce::CE * pack_SmSt(const FlyResult & result);
    ce::CE * pack_Sl(const FlyResult & result);
    ce::CE * pack_SlAp(const FlyResult & result);
    ce::CE * pack_St(const FlyResult & result);
    ce::CE * pack_StSt(const FlyResult & result);
    ce::CE * pack_Tl(const FlyResult & result);
    ce::CE * pack_RunCx(const FlyResult & result);
    ce::CE * pack_LimitedCx(const FlyResult & result);
    ce::CE * pack_ValueCx(const FlyResult & result);
    ce::CE * pack_comma(const FlyResult & result);
    ce::CE * pack_CommandEnd(const FlyResult & result);
    ce::CE * pack_unexpected(const FlyResult & result);

    // CP interface
public:
    void newSignal(const icString & name) override;
    int  getSignal(const icString & name) override;

    icList<il::CodeFragment> splitCommands(
      const il::CodeFragment & code) override;

    icString lastTokenPosition() override;
};

}  // namespace cp
}  // namespace icL

#endif  // cp_CP
