#ifndef cp_ResultType
#define cp_ResultType

namespace icL::cp {

/**
 * @brief The ResultType enum represents a result type of a icL fly
 */
enum class ResultType {
    Comment,   ///< it's a comment
    Local,     ///< @-prefixed variable
    Global,    ///< #-prefixed variable
    Function,  ///< it's a function pointer
    Property,  ///< it's a object property
    Method,    ///< it's a object method
    Key,       ///< it's keyword or typename or command
    // Literals
    Double,   ///< it's a double literal
    Element,  ///< it's an element literal
    Int,      ///< it's an integer literal
    JsValue,  ///< it's a js:value{..} literal
    JsCode,   ///< it's a js{..} literal
    JsFile,   ///< it's a js:file[..] literal
    RegEx,    ///< it's a regular expression literal
    String,   ///< it's a string literal
    // Operators
    And,     ///< it's the `&` operator
    Bg,      ///< it's the `>` operator
    BgEq,    ///< it's the `>=` operator
    BgEqSm,  ///< it's the `>=<` operator
    BgSm,    ///< it's the `><` operator
    Bs,      ///< it's the `\` operator
    Cl,      ///< it's the `:` operator
    ClAs,    ///< it's the `:?` operator
    ClCl,    ///< it's the `::` operator
    ClSt,    ///< it's the `:*` operator
    ClEq,    ///< it's the `:=` operator
    Eq,      ///< it's the `=` operator
    EqEq,    ///< it's the `==` operator
    Mn,      ///< it's the `-` operator
    Nt,      ///< it's the `!` operator
    NtEq,    ///< it's the `!=` operator
    NtSm,    ///< it's the `!<` operator
    NtSt,    ///< it's the `!*` operator
    Or,      ///< it's the `|` operator
    Or2,     ///< it's the `^` operator
    Pc,      ///< it's the `%` operator
    Pl,      ///< it's the `+` operator
    Sm,      ///< it's the `<` operator
    SmBg,    ///< it's the `<>` operator
    SmEq,    ///< it's the `<=` operator
    SmEqBg,  ///< it's the `<=>` operator
    SmSm,    ///< it's the `<<` operator
    SmSt,    ///< it's the `<*` operator
    Sl,      ///< it's the `/` operator
    SlAp,    ///< it's the `/'` operator
    St,      ///< it's the `*` operator
    StSt,    ///< it's the `**` operator
    Tl,      ///< it's the `~` operator
    // Contexts
    RunOp,      ///< it's the run context open bracket
    RunCl,      ///< it's the run context close bracket
    LimitedOp,  ///< it's the limited context open bracket
    LimitedCl,  ///< it's the limited context close bracket
    ValueOp,    ///< it's the value context open bracket
    ValueCl,    ///< it's the value context close bracket
    Comma,      ///< is used in value and limited context as delimiter
    // Command End
    CommandEnd  ///< it's the `;` operator
};

}  // namespace icL::cp

#endif  // cp_ResultType
