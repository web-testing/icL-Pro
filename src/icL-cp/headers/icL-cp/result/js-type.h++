#ifndef ce_ResultType
#define ce_ResultType

namespace icL::cp {

/**
 * @brief The JsResultType enum represents a result type of a JS fly
 */
enum class JsResultType {
    Comment,     ///< it's a comment
    Identifier,  ///< it's a identifier or keyword
    String,      ///< it's a string
    RoundOp,     ///< it's a round open bracket
    RoundCl,     ///< it's a round close bracket
    SquareOp,    ///< it's a square open bracket
    SquareCl,    ///< it's a square close bracket
    CurlyOp,     ///< it's a curly open bracket
    CurlyCl,     ///< it's a curly close bracket
    CommandEnd,  ///< it's the ';' operator
    Token        ///< it means any uncatched token
};

}  // namespace icL::cp

#endif  // ce_ResultType
