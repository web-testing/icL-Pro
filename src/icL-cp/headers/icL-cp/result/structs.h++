#ifndef cp_FlyResult
#define cp_FlyResult

#include "js-type.h++"
#include "type.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/structures/position.h++>



namespace icL::cp {


/**
 * @brief The BaseFlyResult struct represents a result of a any fly
 */
struct BaseFlyResult
{
    /// \brief key is the catched string
    icString key;

    /// \brief begin is the begin of token
    il::Position begin;

    /// \brief end is the end of token
    il::Position end;

    /// \brief data is a container for addition data of each case
    icObject<icString, icVariant> data;
};

/**
 * @brief The FlyResult struct represents a result of a icL fly
 */
struct FlyResult : public BaseFlyResult
{
    /// \brief type is the type of result
    ResultType type = ResultType::CommandEnd;
};

struct JsFlyResult : public BaseFlyResult
{
    /// \brief type is the type of result
    JsResultType type = JsResultType::CommandEnd;
};


}  // namespace icL::cp

#endif  // cp_FlyResult
