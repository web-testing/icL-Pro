#ifndef cp_Flyer_icL
#define cp_Flyer_icL

#include "../result/type.h++"
#include "i-flyer.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/structures/position.h++>
#include <icL-il/structures/target-data.h++>



namespace icL::cp {

/**
 * @brief The Flyer class represents an flying cursor
 */
class icL
    : public service::INode
    , virtual public IFlyer
{
public:
    icL() = default;

    /**
     * @brief flyNext flies the next token
     * @return the result of fly
     */
    FlyResult flyNext(
      bool packContexts = true, bool skipErrors = false) override;

    /**
     * @brief finishContext finish a context parsing
     * @param result is the fly result to evaluate
     */
    void finishContext(FlyResult & result);

    /**
     * @brief getModifiers gets modifiers as list
     * @return the modifiers of keyword
     */
    icStringList getModifiers();

    /**
     * @brief getModifiersWithNotSupport gets modifiers with `!` support
     * @return the list of modifiers (`! == not`)
     */
    icStringList getModifiersWithNotSupport();

    /// `js...`
    void flyJs(FlyResult & result);

    /// `method:modifier[selector]`
    void flyElement(FlyResult & result);

private:
    /**
     * @brief flyToNext flies to next not-white character
     * @return the next non-white character or 0x0 at end of source
     */
    icChar flyToNext();

    /**
     * @brief flyName extracts a name of a method/property/variable/function
     * @return the extracted name
     */
    icString flyName();

    /**
     * @brief flyLiteral flyies a bracket literal
     * @param result will add to data `lit-begin`, `lit-end` & `lit-str`
     */
    void flyLiteral(FlyResult & result);

    // Common tokens

    /// fly comments
    void flyComment(FlyResult & result);

    /// `@local`
    void flyLocal(FlyResult & result);

    /// `#global`
    void flyGlobal(FlyResult & result);

    /// `$function`
    void flyFunction(FlyResult & result);

    /// `'property`
    void flyProperty(FlyResult & result);

    /// `.method`
    void flyMethod(FlyResult & result);

    /// `key`
    void flyKey(FlyResult & result);

    // Literals

    /// `d+.d+`
    void flyDouble(FlyResult & result);

    /// `d+`
    void flyInt(FlyResult & result);

    /// `js:value{..}`
    void flyJsValue(FlyResult & result);

    /// `js@target{..}` or `js{..}`
    void flyJsCode(FlyResult & result, const il::TargetData & target);

    /// `js:file[..]`
    void flyJsFile(FlyResult & result);

    /// `d+` & `d+.d+`
    void flyNumber(FlyResult & result);

    /// `/xREGEXx/`
    void flyRegEx(FlyResult & result);

    /// `"string"`
    void flyString(FlyResult & result);

    // Operators

    /// `&`
    void flyAnd(FlyResult & result);

    /// `>` & `>=` & `>=<` & `><`
    void flyBg(FlyResult & result);

    /// `>=`
    void flyBgEq(FlyResult & result);

    /// `>=<`
    void flyBgEqSm(FlyResult & result);

    /// `><`
    void flyBgSm(FlyResult & result);

    /// `\`
    void flyBs(FlyResult & result);

    /// `:` & `:?` & `::` & `:*`
    void flyCl(FlyResult & result);

    /// `:?`
    void flyClAs(FlyResult & result);

    /// `::`
    void flyClCl(FlyResult & result);

    /// `:*`
    void flyClSt(FlyResult & result);

    /// `:=`
    void flyClEq(FlyResult & result);

    /// `=` & `==`
    void flyEq(FlyResult & result);

    /// `==`
    void flyEqEq(FlyResult & result);

    /// `-`
    void flyMn(FlyResult & result);

    /// `!` & `!=` & `!<` & `!*`
    void flyNt(FlyResult & result);

    /// `!=`
    void flyNtEq(FlyResult & result);

    /// `!<`
    void flyNtSm(FlyResult & result);

    /// `!*`
    void flyNtSt(FlyResult & result);

    /// `|`
    void flyOr(FlyResult & result);

    /// `^`
    void flyOr2(FlyResult & result);

    /// `%`
    void flyPc(FlyResult & result);

    /// `+`
    void flyPl(FlyResult & result);

    /// `<` & `<>` & `<=` & `<=>` & `<<` & `<*`
    void flySm(FlyResult & result);

    /// `<>`
    void flySmBg(FlyResult & result);

    /// `<=`
    void flySmEq(FlyResult & result);

    /// `<=>`
    void flySmEqBg(FlyResult & result);

    /// `<<`
    void flySmSm(FlyResult & result);

    /// `<*`
    void flySmSt(FlyResult & result);

    /// `/` & `/'`
    void flySl(FlyResult & result);

    /// `/'`
    void flySlAp(FlyResult & result);

    /// `*` & `**`
    void flySt(FlyResult & result);

    /// `**`
    void flyStSt(FlyResult & result);

    /// `~`
    void flyTl(FlyResult & result);

    // Contexts

    /// `{`
    void flyRunOp(FlyResult & result);

    /// `}`
    void flyRunCl(FlyResult & result);

    /// `[`
    void flyLimitedOp(FlyResult & result);

    /// `]`
    void flyLimitedCl(FlyResult & result);

    /// `(`
    void flyValueOp(FlyResult & result);

    /// `)`
    void flyValueCl(FlyResult & result);

    /// `,`
    void flyComma(FlyResult & result);

    // End of commands

    /// `;`
    void flyCommandEnd(FlyResult & result);
};

}  // namespace icL::cp

#endif  // cp_Flyer_icL
