#ifndef cp_Flyer
#define cp_Flyer

#include "icL.h++"
#include "js.h++"


namespace icL::cp {

class Flyer
    : public icL
    , public JS
    , public il::Node
{
    std::unique_ptr<il::SourceOfCode> m_source;

public:
    Flyer(il::InterLevel * il, const il::CodeFragment & code);

    /**
     * @brief seek moves the text cursor
     * @param pos is the needed postition
     */
    void seek(const il::Position & pos);

    /**
     * @brief getSource gets the source of code
     * @return the source of code
     */
    icString getFilePath();

    /**
     * @brief reset resets flayers position and range
     * @param code is the code to execute
     * @note used in keep alive layers
     */
    void reset(const il::CodeFragment & code);

    // IFlyer interface
public:
    std::unique_ptr<il::SourceOfCode> & source() override;
    icString issue(const il::Position & pos) const override;
    icString issueWithPath(const il::Position & pos) const override;

    // INode interface
protected:
    il::InterLevel * _il() override;
};

}  // namespace icL::cp

#endif  // cp_Flyer
