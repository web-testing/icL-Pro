#include "source-server.h++"

#include <source-of-code.h++>



namespace icL::cp {

SourceServer::SourceServer(il::InterLevel * il)
    : Node(il) {}

std::shared_ptr<il::SourceOfCode> SourceServer::getSource(
  const icString & path) {
    if (sources.contains(path)) {
        return sources[path];
    }

    auto ptr = std::make_shared<SourceOfCode>(
      il, std::make_shared<icFile>(path), il::Position{0, 0, 1, 1},
      il::Position{-1, -1, -1, -1});

    sources[path] = ptr;
    return std::move(ptr);
}

void SourceServer::closeSource(const icString & path) {
    sources.remove(path);
}

}  // namespace icL::cp
