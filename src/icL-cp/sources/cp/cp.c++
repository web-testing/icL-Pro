#include "cp.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/lambda-target.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-keyword/advanced/assert.h++>
#include <icL-ce-keyword/advanced/emit.h++>
#include <icL-ce-keyword/advanced/emitter.h++>
#include <icL-ce-keyword/advanced/jammer.h++>
#include <icL-ce-keyword/advanced/listen.h++>
#include <icL-ce-keyword/advanced/now.h++>
#include <icL-ce-keyword/advanced/slot.h++>
#include <icL-ce-keyword/advanced/wait.h++>
#include <icL-ce-keyword/control/any.h++>
#include <icL-ce-keyword/control/case.h++>
#include <icL-ce-keyword/control/else.h++>
#include <icL-ce-keyword/control/exists.h++>
#include <icL-ce-keyword/control/if-proxy.h++>
#include <icL-ce-keyword/control/switch.h++>
#include <icL-ce-keyword/loop/do.h++>
#include <icL-ce-keyword/loop/filter.h++>
#include <icL-ce-keyword/loop/for-proxy.h++>
#include <icL-ce-keyword/loop/range.h++>
#include <icL-ce-keyword/loop/while.h++>
#include <icL-ce-literal/const/bool.h++>
#include <icL-ce-literal/const/double.h++>
#include <icL-ce-literal/const/element.h++>
#include <icL-ce-literal/const/int.h++>
#include <icL-ce-literal/const/regex.h++>
#include <icL-ce-literal/const/string.h++>
#include <icL-ce-literal/functional-js/js-file.h++>
#include <icL-ce-literal/functional-js/js-run.h++>
#include <icL-ce-literal/functional/function.h++>
#include <icL-ce-literal/functional/method.h++>
#include <icL-ce-literal/functional/property.h++>
#include <icL-ce-literal/static/identifier.h++>
#include <icL-ce-literal/static/type.h++>
#include <icL-ce-operators-ALU/arithmetical/addition.h++>
#include <icL-ce-operators-ALU/arithmetical/division.h++>
#include <icL-ce-operators-ALU/arithmetical/exponentiation.h++>
#include <icL-ce-operators-ALU/arithmetical/multiplication.h++>
#include <icL-ce-operators-ALU/arithmetical/remainder.h++>
#include <icL-ce-operators-ALU/arithmetical/root.h++>
#include <icL-ce-operators-ALU/arithmetical/subtraction.h++>
#include <icL-ce-operators-ALU/logic/biconditional.h++>
#include <icL-ce-operators-ALU/logic/conjunction.h++>
#include <icL-ce-operators-ALU/logic/disjunction.h++>
#include <icL-ce-operators-ALU/logic/exclusive-disjunction.h++>
#include <icL-ce-operators-ALU/logic/negation.h++>
#include <icL-ce-operators-ALU/logic/selective-select.h++>
#include <icL-ce-operators-ALU/system/assign.h++>
#include <icL-ce-operators-ALU/system/comma.h++>
#include <icL-ce-operators-ALU/system/limited-context.h++>
#include <icL-ce-operators-ALU/system/overload.h++>
#include <icL-ce-operators-ALU/system/run-context.h++>
#include <icL-ce-operators-ALU/system/value-context.h++>
#include <icL-ce-operators-advanced/cast/can-cast.h++>
#include <icL-ce-operators-advanced/cast/cast.h++>
#include <icL-ce-operators-advanced/cast/error-less-cast.h++>
#include <icL-ce-operators-advanced/cast/type-of.h++>
#include <icL-ce-operators-advanced/compare/bigger-equal-smaller.h++>
#include <icL-ce-operators-advanced/compare/bigger-equal.h++>
#include <icL-ce-operators-advanced/compare/bigger-smaller.h++>
#include <icL-ce-operators-advanced/compare/bigger.h++>
#include <icL-ce-operators-advanced/compare/equality.h++>
#include <icL-ce-operators-advanced/compare/inequality.h++>
#include <icL-ce-operators-advanced/compare/smaller-bigger.h++>
#include <icL-ce-operators-advanced/compare/smaller-equal-bigger.h++>
#include <icL-ce-operators-advanced/compare/smaller-equal.h++>
#include <icL-ce-operators-advanced/compare/smaller.h++>
#include <icL-ce-operators-advanced/including/not-contains-template.h++>
#include <icL-ce-operators-advanced/including/not-contains.h++>
#include <icL-ce-value-base/lambda/lambda-js-value.h++>
#include <icL-ce-value-browser/document/cookies.h++>
#include <icL-ce-value-browser/document/document.h++>
#include <icL-ce-value-browser/enum/key.h++>
#include <icL-ce-value-browser/enum/mouse.h++>
#include <icL-ce-value-browser/enum/move.h++>
#include <icL-ce-value-browser/settings/icl.h++>
#include <icL-ce-value-browser/window/alert.h++>
#include <icL-ce-value-browser/window/session.h++>
#include <icL-ce-value-browser/window/sessions.h++>
#include <icL-ce-value-browser/window/tab.h++>
#include <icL-ce-value-browser/window/tabs.h++>
#include <icL-ce-value-browser/window/window.h++>
#include <icL-ce-value-browser/window/windows.h++>
#include <icL-ce-value-system/file.h++>
#include <icL-ce-value-system/files.h++>
#include <icL-ce-value-system/import.h++>
#include <icL-ce-value-system/log.h++>
#include <icL-ce-value-system/make.h++>
#include <icL-ce-value-system/math.h++>
#include <icL-ce-value-system/request.h++>
#include <icL-ce-value-system/signal.h++>
#include <icL-ce-value-system/stack.h++>
#include <icL-ce-value-system/stacks.h++>
#include <icL-ce-value-system/state.h++>
#include <icL-ce-value-system/types.h++>

#include <icL-memory/state/memory.h++>

//#include <iostream>



namespace icL::cp {

CP::CP(il::InterLevel * il, const il::CodeFragment & code)
    : il::Node(il)
    , flyer(il, code)
    , code(code) {}

ce::CE * CP::parseNext() {
    static const icObject<ResultType, ce::CE * (CP::*)(const FlyResult &)>
              proxies{{ResultType::Local, &CP::pack_local},
              {ResultType::Global, &CP::pack_global},
              {ResultType::Function, &CP::pack_function},
              {ResultType::Property, &CP::pack_property},
              {ResultType::Method, &CP::pack_method},
              {ResultType::Key, &CP::pack_key},
              {ResultType::Double, &CP::pack_double_literal},
              {ResultType::Element, &CP::pack_element_literal},
              {ResultType::Int, &CP::pack_int_literal},
              //              {ResultType::Js, &CP::pack_js_literal},
              {ResultType::RegEx, &CP::pack_regex_literal},
              {ResultType::String, &CP::pack_string_literal},
              {ResultType::And, &CP::pack_And},
              {ResultType::Bg, &CP::pack_Bg},
              {ResultType::BgEq, &CP::pack_BgEq},
              {ResultType::BgEqSm, &CP::pack_BgEqSm},
              {ResultType::BgSm, &CP::pack_BgSm},
              {ResultType::Bs, &CP::pack_Bs},
              {ResultType::Cl, &CP::pack_Cl},
              {ResultType::ClAs, &CP::pack_ClAs},
              {ResultType::ClCl, &CP::pack_ClCl},
              {ResultType::ClSt, &CP::pack_ClSt},
              {ResultType::ClEq, &CP::pack_ClEq},
              {ResultType::Eq, &CP::pack_Eq},
              {ResultType::EqEq, &CP::pack_EqEq},
              {ResultType::Mn, &CP::pack_Mn},
              {ResultType::Nt, &CP::pack_Nt},
              {ResultType::NtEq, &CP::pack_NtEq},
              {ResultType::NtSm, &CP::pack_NtSm},
              {ResultType::NtSt, &CP::pack_NtSt},
              {ResultType::Or, &CP::pack_Or},
              {ResultType::Or2, &CP::pack_Or2},
              {ResultType::Pc, &CP::pack_Pc},
              {ResultType::Pl, &CP::pack_Pl},
              {ResultType::Sm, &CP::pack_Sm},
              {ResultType::SmBg, &CP::pack_SmBg},
              {ResultType::SmEq, &CP::pack_SmEq},
              {ResultType::SmEqBg, &CP::pack_SmEqBg},
              {ResultType::SmSm, &CP::pack_SmSm},
              {ResultType::SmSt, &CP::pack_SmSt},
              {ResultType::Sl, &CP::pack_Sl},
              {ResultType::SlAp, &CP::pack_SlAp},
              {ResultType::St, &CP::pack_St},
              {ResultType::StSt, &CP::pack_StSt},
              {ResultType::Tl, &CP::pack_Tl},
              {ResultType::RunOp, &CP::pack_RunCx},
              {ResultType::LimitedOp, &CP::pack_LimitedCx},
              {ResultType::ValueOp, &CP::pack_ValueCx},
              {ResultType::Comma, &CP::pack_comma},
              {ResultType::CommandEnd, &CP::pack_CommandEnd},
              {ResultType::RunCl, &CP::pack_unexpected},
              {ResultType::LimitedCl, &CP::pack_unexpected},
              {ResultType::ValueCl, &CP::pack_unexpected}};
    FlyResult token;

    if (repeatToken) {
        flyer.seek(m_lastToken.end);
        token       = m_lastToken;
        repeatToken = false;
    }
    else {
        do {
            token = flyer.flyNext();
            //            std::cout << token.begin.byte << " - " <<
            //            token.end.byte << " - "
            //                      << token.key.toString() << std::endl;
        } while (token.type == ResultType::Comment);
    }

    m_lastToken = token;
    auto proxy  = proxies.find(token.type);

    auto result = (this->**proxy)(token);
    if (result != nullptr)
        result->link(token, code);
    return result;
}

icString CP::lastKey() {
    icObject<ResultType, icString> keysTypes{
      {ResultType::Comment, "comment"},
      {ResultType::Local, "local variable"},
      {ResultType::Global, "global variable"},
      {ResultType::Property, "property"},
      {ResultType::Method, "method"},
      {ResultType::Key, "keyword"},
      {ResultType::Double, "double literal"},
      {ResultType::Element, "element literal"},
      {ResultType::Int, "integer literal"},
      {ResultType::JsCode, "JavaScript literal"},
      {ResultType::JsFile, "JavaScript literal"},
      {ResultType::JsValue, "JavaScript literal"},
      {ResultType::RegEx, "regular expression literal"},
      {ResultType::String, "string literal"},
      {ResultType::RunOp, "run context"},
      {ResultType::LimitedOp, "limited context"},
      {ResultType::ValueOp, "value context"},
      {ResultType::CommandEnd, "command delimiter"}};

    icString keyType = keysTypes.value(m_lastToken.type, "operator");
    icString token   = m_lastToken.key;

    int first = token.indexOf('\n');
    int last  = token.lastIndexOf('\n');

    if (first >= 0) {
        token.replace(first, last - first + 1, "..");
    }

    return keyType % ": " % token;
}

icString CP::getFilePath() {
    return flyer.getFilePath().remove(il->vms->getRootDir());
}

icString CP::getFilePathLineChar() {
    return flyer.getFilePath().remove(il->vms->getRootDir()) % ':' %
           icString::number(flyer.source()->getPosition().line) % ':' %
           icString::number(flyer.source()->getPosition().relative) % ':';
}

il::CodeFragment CP::toCodeF(const FlyResult & result) {
    il::CodeFragment code;

    code.source = this->code.source;
    code.begin  = result.data["begin"];
    code.end    = result.data["end"];

    if (result.data.contains("name")) {
        code.name = result.data["name"];
    }

    return code;
}

void CP::reset(const il::CodeFragment & code) {
    this->code.source = code.source;
    flyer.reset(code);
}

void CP::repeat() {
    repeatToken = true;
}

ce::CE * CP::withModifiers(
  ce::Keyword * keyword, icStringList (icL::*getter)()) {
    keyword->giveModifiers(((&flyer)->*getter)());
    return keyword;
}

ce::CE * CP::withModifiersWithNotSupport(ce::Keyword * keyword) {
    return withModifiers(keyword, &icL::getModifiersWithNotSupport);
}

ce::CE * CP::pack_local(const FlyResult & result) {
    return service::Factory::fromValue(
      il, il->mem->stackIt().getContainerForVariable(result.data["name"]),
      result.data["name"]);
}

ce::CE * CP::pack_global(const FlyResult & result) {
    return service::Factory::fromValue(
      il, il->mem->stateIt().state(), result.data["name"]);
}

ce::CE * CP::pack_function(const FlyResult & result) {
    return new ce::Function{il, result.data["name"]};
}

ce::CE * CP::pack_property(const FlyResult & result) {
    ce::Prefix prefix = ce::Prefix::None;

    if (result.data.contains("prefix")) {
        static icObject<icString, ce::Prefix> prefixes{
          {"attr", ce::Prefix::Attr},
          {"css", ce::Prefix::CSS},
          {"prop", ce::Prefix::Prop}};

        prefix = *(prefixes.find(result.data["prefix"]));
    }

    return new ce::Property{il, prefix, result.data["name"]};
}

ce::CE * CP::pack_method(const FlyResult & result) {
    return new ce::Method{il, result.data["name"]};
}

ce::CE * CP::pack_key(const FlyResult & result) {
    icObject<icString, ce::CE * (CP::*)(const FlyResult &)> keys{
      {"any", &CP::pack_any},
      {"assert", &CP::pack_assert},
      {"case", &CP::pack_case},
      {"do", &CP::pack_do},
      {"else", &CP::pack_else},
      {"emit", &CP::pack_emit},
      {"emitter", &CP::pack_emitter},
      {"exists", &CP::pack_exists},
      {"filter", &CP::pack_filter},
      {"for", &CP::pack_for},
      {"if", &CP::pack_if},
      {"listen", &CP::pack_listen},
      {"notify", &CP::pack_notify},
      {"now", &CP::pack_now},
      {"range", &CP::pack_range},
      {"slot", &CP::pack_slot},
      {"switch", &CP::pack_switch},
      {"wait", &CP::pack_wait},
      {"while", &CP::pack_while},
      {"Alert", &CP::pack_Alert},
      {"By", &CP::pack_By},
      {"Cookies", &CP::pack_Cookies},
      {"Datetime", &CP::pack_Datetime},
      {"DB", &CP::pack_DB},
      {"DBManager", &CP::pack_DBManager},
      {"Document", &CP::pack_Document},
      {"DSV", &CP::pack_DSV},
      {"File", &CP::pack_File},
      {"Files", &CP::pack_Files},
      {"icL", &CP::pack_icL},
      {"Import", &CP::pack_Import},
      {"Key", &CP::pack_Key},
      {"Log", &CP::pack_Log},
      {"Make", &CP::pack_Make},
      {"Math", &CP::pack_Math},
      {"Mouse", &CP::pack_Mouse},
      {"Move", &CP::pack_Move},
      {"Request", &CP::pack_Request},
      {"Query", &CP::pack_Query},
      {"Session", &CP::pack_Session},
      {"Sessions", &CP::pack_Sessions},
      {"Signal", &CP::pack_Signal},
      {"Stack", &CP::pack_Stack},
      {"Stacks", &CP::pack_Stacks},
      {"State", &CP::pack_State},
      {"Tab", &CP::pack_Tab},
      {"Tabs", &CP::pack_Tabs},
      {"Types", &CP::pack_Types},
      {"View", &CP::pack_View},
      {"Window", &CP::pack_Window},
      {"Windows", &CP::pack_Windows},
      {"bool", &CP::pack_bool},
      {"cookie", &CP::pack_cookie},
      {"database", &CP::pack_database},
      {"datetime", &CP::pack_datetime},
      {"document", &CP::pack_document},
      {"double", &CP::pack_double},
      {"element", &CP::pack_element},
      {"file", &CP::pack_file},
      {"handler", &CP::pack_handler},
      {"int", &CP::pack_int},
      {"jammer", &CP::pack_jammer},
      {"lambda-icl", &CP::pack_lambda_icl},
      {"lambda-js", &CP::pack_lambda_js},
      {"lambda-sql", &CP::pack_lambda_sql},
      {"list", &CP::pack_list},
      {"listener", &CP::pack_listener},
      {"object", &CP::pack_object},
      {"query", &CP::pack_query},
      {"set", &CP::pack_set},
      {"session", &CP::pack_session},
      {"string", &CP::pack_string},
      {"tab", &CP::pack_tab},
      {"void", &CP::pack_void},
      {"window", &CP::pack_window},
      {"alert", &CP::pack_system_type},
      {"tabs", &CP::pack_system_type},
      {"windows", &CP::pack_system_type},
      {"sessions", &CP::pack_system_type},
      {"true", &CP::pack_bool_literal},
      {"false", &CP::pack_bool_literal},
      {"js", &CP::pack_js_literal},
      {"css", &CP::pack_css_literal},
      {"xpath", &CP::pack_xpath_literal},
      {"link", &CP::pack_link_literal},
      {"links", &CP::pack_links_literal},
      {"tag", &CP::pack_tag_literal},
      {"tags", &CP::pack_tags_literal},
      {"input", &CP::pack_input_literal},
      {"field", &CP::pack_field_literal}};

    auto it = keys.find(result.key);

    if (it != keys.end()) {
        return (this->*it.value())(result);
    }

    return pack_identifier(result);
}

ce::CE * CP::pack_identifier(const FlyResult & result) {
    return new ce::Identifier(il, result.key);
}

ce::CE * CP::pack_assert(const FlyResult & /*result*/) {
    return new ce::Assert{il};
}

ce::CE * CP::pack_any(const FlyResult & /*result*/) {
    return new ce::Any{il};
}

ce::CE * CP::pack_case(const FlyResult & /*result*/) {
    return new ce::Case(il);
}

ce::CE * CP::pack_do(const FlyResult & /*result*/) {
    return new ce::Do{il};
}

ce::CE * CP::pack_else(const FlyResult & /*result*/) {
    return new ce::Else(il);
}

ce::CE * CP::pack_emit(const FlyResult & /*result*/) {
    return withModifiers(new ce::Emit{il});
}

ce::CE * CP::pack_emitter(const FlyResult & /*result*/) {
    return withModifiers(new ce::Emitter{il});
}

ce::CE * CP::pack_exists(const FlyResult & /*result*/) {
    return new ce::Exists{il};
}

ce::CE * CP::pack_filter(const FlyResult & /*result*/) {
    return withModifiers(new ce::Filter{il});
}

ce::CE * CP::pack_for(const FlyResult & /*result*/) {
    return withModifiers(new ce::ForProxy{il});
}

ce::CE * CP::pack_if(const FlyResult & /*result*/) {
    return withModifiersWithNotSupport(new ce::IfProxy(il));
}

ce::CE * CP::pack_jammer(const FlyResult & /*result*/) {
    return withModifiers(new ce::Jammer{il});
}

ce::CE * CP::pack_listen(const FlyResult & result) {}

ce::CE * CP::pack_notify(const FlyResult & result) {}

ce::CE * CP::pack_now(const FlyResult & /*result*/) {
    return new ce::Now{il};
}

ce::CE * CP::pack_range(const FlyResult & /*result*/) {
    return withModifiers(new ce::Range{il});
}

ce::CE * CP::pack_slot(const FlyResult & /*result*/) {
    return withModifiers(new ce::Slot{il});
}

ce::CE * CP::pack_switch(const FlyResult & /*result*/) {
    return new ce::Switch(il);
}

ce::CE * CP::pack_wait(const FlyResult & /*result*/) {
    return withModifiers(new ce::Wait{il});
}

ce::CE * CP::pack_while(const FlyResult & /*result*/) {
    return withModifiersWithNotSupport(new ce::While{il});
}

ce::CE * CP::pack_Alert(const FlyResult & /*result*/) {
    return new ce::Alert{il};
}

ce::CE * CP::pack_By(const FlyResult & result) {}

ce::CE * CP::pack_Cookies(const FlyResult & /*result*/) {
    return new ce::Cookies{il};
}

ce::CE * CP::pack_Datetime(const FlyResult & result) {}

ce::CE * CP::pack_DB(const FlyResult & result) {}

ce::CE * CP::pack_DBManager(const FlyResult & result) {}

ce::CE * CP::pack_Document(const FlyResult & /*result*/) {
    return new ce::Document{il};
}

ce::CE * CP::pack_DSV(const FlyResult & result) {}

ce::CE * CP::pack_File(const FlyResult & /*result*/) {
    return new ce::File{il};
}

ce::CE * CP::pack_Files(const FlyResult & /*result*/) {
    return new ce::Files{il};
}

ce::CE * CP::pack_icL(const FlyResult & /*result*/) {
    return new ce::icL{il};
}

ce::CE * CP::pack_Import(const FlyResult & /*result*/) {
    return new ce::Import{il};
}

ce::CE * CP::pack_Key(const FlyResult & /*result*/) {
    return new ce::Key{il};
}

ce::CE * CP::pack_Log(const FlyResult & /*result*/) {
    return new ce::Log{il};
}

ce::CE * CP::pack_Make(const FlyResult & /*result*/) {
    return new ce::Make{il};
}

ce::CE * CP::pack_Math(const FlyResult & /*result*/) {
    return new ce::Math{il};
}

ce::CE * CP::pack_Mouse(const FlyResult & /*result*/) {
    return new ce::Mouse{il};
}

ce::CE * CP::pack_Move(const FlyResult & /*result*/) {
    return new ce::Move{il};
}

ce::CE * CP::pack_Request(const FlyResult & /*result*/) {
    return new ce::Request{il};
}

ce::CE * CP::pack_Query(const FlyResult & result) {}

ce::CE * CP::pack_Session(const FlyResult & /*result*/) {
    return new ce::Session{il};
}

ce::CE * CP::pack_Sessions(const FlyResult & /*result*/) {
    return new ce::Sessions{il};
}

ce::CE * CP::pack_Signal(const FlyResult & /*result*/) {
    return new ce::Signal{il};
}

ce::CE * CP::pack_Stack(const FlyResult & /*result*/) {
    return new ce::Stack{il, il->mem->stackIt().stack()};
}

ce::CE * CP::pack_Stacks(const FlyResult & /*result*/) {
    return new ce::Stacks{il};
}

ce::CE * CP::pack_State(const FlyResult & /*result*/) {
    return new ce::State{il};
}

ce::CE * CP::pack_Tab(const FlyResult & /*result*/) {
    return new ce::Tab{il};
}

ce::CE * CP::pack_Tabs(const FlyResult & /*result*/) {
    return new ce::Tabs{il};
}

ce::CE * CP::pack_Types(const FlyResult & /*result*/) {
    return new ce::Types{il};
}

ce::CE * CP::pack_View(const FlyResult & result) {}

ce::CE * CP::pack_Window(const FlyResult & /*result*/) {
    return new ce::Window{il};
}

ce::CE * CP::pack_Windows(const FlyResult & /*result*/) {
    return new ce::Windows{il};
}

ce::CE * CP::pack_bool(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::BoolValue};
}

ce::CE * CP::pack_cookie(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::CookieValue};
}

ce::CE * CP::pack_database(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::DatabaseValue};
}

ce::CE * CP::pack_datetime(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::DatetimeValue};
}

ce::CE * CP::pack_document(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::DocumentValue};
}

ce::CE * CP::pack_double(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::DoubleValue};
}

ce::CE * CP::pack_element(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::ElementValue};
}

ce::CE * CP::pack_file(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::FileValue};
}

ce::CE * CP::pack_handler(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::HandlerValue};
}

ce::CE * CP::pack_int(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::IntValue};
}

ce::CE * CP::pack_lambda_icl(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::LambdaValue};
}

ce::CE * CP::pack_lambda_js(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::JavaScriptLambdaValue};
}

ce::CE * CP::pack_lambda_sql(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::SqlLambdaValue};
}

ce::CE * CP::pack_list(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::ListValue};
}

ce::CE * CP::pack_listener(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::ListenerValue};
}

ce::CE * CP::pack_object(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::ObjectValue};
}

ce::CE * CP::pack_query(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::QueryValue};
}

ce::CE * CP::pack_set(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::SetValue};
}

ce::CE * CP::pack_session(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::SessionValue};
}

ce::CE * CP::pack_string(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::StringValue};
}

ce::CE * CP::pack_tab(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::TabValue};
}

ce::CE * CP::pack_void(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::VoidValue};
}

ce::CE * CP::pack_window(const FlyResult & /*result*/) {
    return new ce::TypeToken{il, Type::WindowValue};
}

ce::CE * CP::pack_system_type(const FlyResult & result) {
    il->vm->cp_sig(
      flyer.issueWithPath(result.begin) %
      "Using of system types is restricted. Detected: " % result.key);
    return nullptr;
}

ce::CE * CP::pack_bool_literal(const FlyResult & result) {
    return new ce::Bool{il, result.key};
}

ce::CE * CP::pack_int_literal(const FlyResult & result) {
    return new ce::Int{il, result.key};
}

ce::CE * CP::pack_double_literal(const FlyResult & result) {
    return new ce::Double{il, result.key};
}

ce::CE * CP::pack_string_literal(const FlyResult & result) {
    return new ce::String{il, result.data["content"]};
}

ce::CE * CP::pack_js_value(const FlyResult & result) {
    il::CodeFragment getter, setter;

    getter.source = code.source;
    getter.begin  = result.data["getter-begin"];
    getter.end    = result.data["getter-end"];

    if (result.data.contains("setter-begin")) {
        setter.source = code.source;
        setter.begin  = result.data["setter-begin"];
        setter.end    = result.data["setter-end"];

        return service::Factory::fromValue(
          il, getter.getCode(), setter.getCode());
    }

    return service::Factory::fromValue(il, getter.getCode(), {});
}

ce::CE * CP::pack_js_file(const FlyResult & result) {
    return new ce::JsFile(il, result.data["lit-str"]);
}

ce::CE * CP::pack_js_code(const FlyResult & result) {
    il::JsLambda value;

    value.target         = std::make_shared<il::CodeFragment>();
    value.target->source = code.source;
    value.target->begin  = result.data["begin"];
    value.target->end    = result.data["end"];
    value.data            = std::make_shared<il::TargetData>();
    value.data->sessionId = result.data["target-session"];
    value.data->windowId  = result.data["target-window"];

    return new ce::LambdaJsValue{il, value};
}

ce::CE * CP::pack_js_literal(const FlyResult & result) {
    FlyResult res = result;

    flyer.flyJs(res);

    switch (res.type) {
    case ResultType::JsCode:
        return pack_js_code(res);

    case ResultType::JsFile:
        return pack_js_file(res);

    case ResultType::JsValue:
        return pack_js_value(res);

    default:
        assert(false);
        return nullptr;
    }
}

ce::CE * CP::pack_element_literal(const FlyResult & result) {
    icString                pattern;
    icString                searchMode = result.key;
    icStringList            modifiers;
    memory::DataContainer * container;
    icString                varName;
    il::CodeFragment        code;

    auto copy = result;
    flyer.flyElement(copy);

    if (il->vm->hasOkState()) {
        code.source = this->code.source;
        code.begin  = copy.data["code-begin"];
        code.end    = copy.data["code-end"];
        pattern     = code.getCode();
        modifiers   = copy.data["modifiers"];

        icString scope = copy.data["scope"];

        if (scope.isEmpty()) {
            container = nullptr;
        }
        else if (scope.length() == 1) {
            varName   = scope;
            container = il->mem->stackIt().getContainerForVariable("@");
        }
        else {
            varName = scope.mid(1, -1);

            if (scope[0] == '#') {
                container = il->mem->stateIt().state();
            }
            else {
                container = il->mem->stackIt().getContainerForVariable(varName);
            }
        }
    }
    else {
        container = nullptr;
    }

    return new ce::Element{il,        pattern, searchMode, modifiers,
                           container, varName, code};
}

ce::CE * CP::pack_css_literal(const FlyResult & result) {
    return pack_element_literal(result);
}

ce::CE * CP::pack_xpath_literal(const FlyResult & result) {
    return pack_element_literal(result);
}

ce::CE * CP::pack_link_literal(const FlyResult & result) {
    return pack_element_literal(result);
}

ce::CE * CP::pack_links_literal(const FlyResult & result) {
    return pack_element_literal(result);
}

ce::CE * CP::pack_tag_literal(const FlyResult & result) {
    return pack_element_literal(result);
}

ce::CE * CP::pack_tags_literal(const FlyResult & result) {
    return pack_element_literal(result);
}

ce::CE * CP::pack_input_literal(const FlyResult & result) {
    return pack_element_literal(result);
}

ce::CE * CP::pack_field_literal(const FlyResult & result) {
    return pack_element_literal(result);
}

ce::CE * CP::pack_regex_literal(const FlyResult & result) {
    return new ce::RegEx{il, result.data["regex"], result.data["mods"]};
}

ce::CE * CP::pack_And(const FlyResult & /*result*/) {
    return new ce::Conjunction(il);
}

ce::CE * CP::pack_Bg(const FlyResult & /*result*/) {
    return new ce::Bigger(il);
}

ce::CE * CP::pack_BgEq(const FlyResult & /*result*/) {
    return new ce::BiggerEqual(il);
}

ce::CE * CP::pack_BgEqSm(const FlyResult & /*result*/) {
    return new ce::BiggerEqualSmaller(il);
}

ce::CE * CP::pack_BgSm(const FlyResult & /*result*/) {
    return new ce::BiggerSmaller(il);
}

ce::CE * CP::pack_Bs(const FlyResult & /*result*/) {
    return new ce::Remainder(il);
}

ce::CE * CP::pack_Cl(const FlyResult & /*result*/) {
    return new ce::Cast{il};
}

ce::CE * CP::pack_ClAs(const FlyResult & /*result*/) {
    return new ce::ErrorLessCast{il};
}

ce::CE * CP::pack_ClCl(const FlyResult & /*result*/) {
    return new ce::TypeOf{il};
}

ce::CE * CP::pack_ClSt(const FlyResult & /*result*/) {
    return new ce::CanCast{il};
}

ce::CE * CP::pack_ClEq(const FlyResult & /*result*/) {
    return new ce::Overload{il};
}

ce::CE * CP::pack_Eq(const FlyResult & /*result*/) {
    return new ce::Assign{il};
}

ce::CE * CP::pack_EqEq(const FlyResult & /*result*/) {
    return new ce::Equality(il);
}

ce::CE * CP::pack_Mn(const FlyResult & /*result*/) {
    return new ce::Subtraction(il);
}

ce::CE * CP::pack_Nt(const FlyResult & /*result*/) {
    return new ce::Negation(il);
}

ce::CE * CP::pack_NtEq(const FlyResult & /*result*/) {
    return new ce::Inequality(il);
}

ce::CE * CP::pack_NtSm(const FlyResult & /*result*/) {
    return new ce::NotContains(il);
}

ce::CE * CP::pack_NtSt(const FlyResult & /*result*/) {
    return new ce::NotContainsTemplate(il);
}

ce::CE * CP::pack_Or(const FlyResult & /*result*/) {
    return new ce::Disjunction(il);
}

ce::CE * CP::pack_Or2(const FlyResult & /*result*/) {
    return new ce::ExclusiveDisjunction(il);
}

ce::CE * CP::pack_Pc(const FlyResult & /*result*/) {
    return new ce::SelectiveSelect(il);
}

ce::CE * CP::pack_Pl(const FlyResult & /*result*/) {
    return new ce::Addition(il);
}

ce::CE * CP::pack_Sm(const FlyResult & /*result*/) {
    return new ce::Smaller(il);
}

ce::CE * CP::pack_SmBg(const FlyResult & /*result*/) {
    return new ce::SmallerBigger(il);
}

ce::CE * CP::pack_SmEq(const FlyResult & /*result*/) {
    return new ce::SmallerEqual(il);
}

ce::CE * CP::pack_SmEqBg(const FlyResult & /*result*/) {
    return new ce::SmallerEqualBigger(il);
}

ce::CE * CP::pack_SmSm(const FlyResult & /*result*/) {
    return new ce::Contains(il);
}

ce::CE * CP::pack_SmSt(const FlyResult & /*result*/) {
    return new ce::ContainsTemplate(il);
}

ce::CE * CP::pack_Sl(const FlyResult & /*result*/) {
    return new ce::Division(il);
}

ce::CE * CP::pack_SlAp(const FlyResult & /*result*/) {
    return new ce::Root(il);
}

ce::CE * CP::pack_St(const FlyResult & /*result*/) {
    return new ce::Multiplication(il);
}

ce::CE * CP::pack_StSt(const FlyResult & /*result*/) {
    return new ce::Exponentiation(il);
}

ce::CE * CP::pack_Tl(const FlyResult & /*result*/) {
    return new ce::Biconditional(il);
}

ce::CE * CP::pack_RunCx(const FlyResult & result) {
    return new ce::RunContext(il, toCodeF(result), result.data["name"]);
}

ce::CE * CP::pack_LimitedCx(const FlyResult & result) {
    return new ce::LimitedContext(il, toCodeF(result));
}

ce::CE * CP::pack_ValueCx(const FlyResult & result) {
    return new ce::ValueContext(il, toCodeF(result));
}

ce::CE * CP::pack_comma(const FlyResult & /*result*/) {
    return new ce::Comma(il);
}

ce::CE * CP::pack_CommandEnd(const FlyResult & /*result*/) {
    return nullptr;
}

ce::CE * CP::pack_unexpected(const FlyResult & result) {
    il->vm->cp_sig("Unexpected token: " + result.key);
    return nullptr;
}

void CP::newSignal(const icString & name) {}

int CP::getSignal(const icString & name) {
    return 0;
}

icList<il::CodeFragment> CP::splitCommands(const il::CodeFragment & code) {
    Flyer fly{il, code};

    icList<il::CodeFragment> commands;
    il::CodeFragment         current;

    current.source     = code.source;
    current.begin.line = -1;

    while (true) {
        FlyResult next = fly.flyNext(true);

        if (next.type == ResultType::CommandEnd) {
            if (current.begin.line == -1) {
                break;
            }

            current.end = next.end;
            commands.append(current);
            current.begin.line = -1;
        }
        else if (current.begin.line == -1) {
            current.begin = next.begin;
        }
    }

    return commands;
}

icString CP::lastTokenPosition() {
    return flyer.issueWithPath(m_lastToken.begin).remove(il->vms->getRootDir());
}

}  // namespace icL::cp
