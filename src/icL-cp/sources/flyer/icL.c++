#include "icL.h++"

#include "structs.h++"

#include <icL-types/replaces/ic-char.h++>
#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/auxiliary/source-of-code.h++>
#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-memory/state/memory.h++>

//#include <iostream>



namespace icL::cp {

FlyResult icL::flyNext(bool packContexts, bool skipErrors) {
    static const icObject<icChar, void (icL::*)(FlyResult &)> proxies{
      {'`', &icL::flyComment},   {'@', &icL::flyLocal},
      {'#', &icL::flyGlobal},    {'\'', &icL::flyProperty},
      {'.', &icL::flyMethod},    {'$', &icL::flyFunction},
      {'0', &icL::flyNumber},    {'1', &icL::flyNumber},
      {'2', &icL::flyNumber},    {'3', &icL::flyNumber},
      {'4', &icL::flyNumber},    {'5', &icL::flyNumber},
      {'6', &icL::flyNumber},    {'7', &icL::flyNumber},
      {'8', &icL::flyNumber},    {'9', &icL::flyNumber},
      {'"', &icL::flyString},    {'&', &icL::flyAnd},
      {'>', &icL::flyBg},        {'\\', &icL::flyBs},
      {':', &icL::flyCl},        {'=', &icL::flyEq},
      {'-', &icL::flyMn},        {'!', &icL::flyNt},
      {'|', &icL::flyOr},        {'^', &icL::flyOr2},
      {'%', &icL::flyPc},        {'+', &icL::flyPl},
      {'<', &icL::flySm},        {'/', &icL::flySl},
      {'*', &icL::flySt},        {'~', &icL::flyTl},
      {'{', &icL::flyRunOp},     {'[', &icL::flyLimitedOp},
      {'(', &icL::flyValueOp},   {'}', &icL::flyRunCl},
      {']', &icL::flyLimitedCl}, {')', &icL::flyValueCl},
      {',', &icL::flyComma},     {';', &icL::flyCommandEnd}};

    source()->seek(source()->getPosition());

    FlyResult result;
    icChar    firstChar = flyToNext();
    auto      proxy     = proxies.find(firstChar);

    result.begin = source()->getPosition();
    result.begin.relative -= 1;
    result.begin.absolute -= 1;
    result.begin.byte -= 1;

    if (proxy != proxies.end()) {
        (this->**proxy)(result);
    }
    else if (firstChar.isLetter()) {
        flyKey(result);
    }
    else if (firstChar == '\0') {
        flyCommandEnd(result);
    }
    else {
        result.type = ResultType::CommandEnd;
        if (!skipErrors) {
            _il()->vm->cp_sig(
              issueWithPath(result.begin) % ": Unexpected token: " % firstChar);
        }
    }

    if (packContexts) {
        if (
          result.type == ResultType::RunOp ||
          result.type == ResultType::LimitedOp ||
          result.type == ResultType::ValueOp) {
            finishContext(result);
        }
    }

    result.end = source()->getPosition();

    return result;
}

void icL::finishContext(FlyResult & result) {
    struct Match
    {
        ResultType   expected;
        icChar       ch;
        il::Position position;
    };

    icList<Match> matches;
    FlyResult     current = result;

    result.data.insert("begin", source()->getPosition());

    while (true) {
        switch (current.type) {
        case ResultType::RunOp:
            matches.append({ResultType::RunCl, '{', current.begin});
            break;

        case ResultType::LimitedOp:
            matches.append({ResultType::LimitedCl, '[', current.begin});
            break;

        case ResultType::ValueOp:
            matches.append({ResultType::ValueCl, '(', current.begin});
            break;

        case ResultType::RunCl:
        case ResultType::LimitedCl:
        case ResultType::ValueCl:
            if (matches.last().expected == current.type) {
                matches.removeLast();
            }
            else {
                auto & last = matches.last();
                _il()->vm->cp_sig(
                  source()->getFilePath() % ": Missmatched brackets: `" %
                  last.ch % "`(" % issue(last.position) % ") & `" %
                  current.key % "`(" % issue(source()->getPosition()) % ')');
            }
            break;

        default: /* do nothing */;
        }

        if (matches.isEmpty() || source()->atEnd()) {
            break;
        }

        //        std::cout << "Key ready: " << current.key.toString() <<
        //        std::endl;
        current = flyNext(false, true);
    }

    if (!matches.isEmpty()) {
        _il()->vm->cp_sig(
          issueWithPath(matches.first().position) % ": Bracket pair not found");
    }

    result.data.insert("end", current.begin);
}

icStringList icL::getModifiers() {
    source()->next();

    icStringList modifiers;
    while (source()->current() == '-') {
        icString modifier;

        while (source()->next().isLetterOrDigit()) {
            modifier += source()->current();
        }

        if (modifier.isEmpty()) {
            _il()->vm->cp_sig(
              issueWithPath(source()->getPosition()) %
              "Modifier name can not be empty");
            break;
        }

        modifiers.append(modifier);
    }

    source()->prev();

    return modifiers;
}

icStringList icL::getModifiersWithNotSupport() {
    if (source()->next() == '!') {
        return icStringList{"not"};
    }

    source()->prev();
    return getModifiers();
}

icChar icL::flyToNext() {
    icChar next = '\0';

    if (source()->atEnd()) {
        return next;
    }

    do {
        next = source()->next();
    } while (next.isWhite() && !source()->atEnd());

    return next.isWhite() ? icChar{'\0'} : next;
}

icString icL::flyName() {
    icString ret;

    while (source()->next().isLetterOrDigit() || source()->current() == '_') {
        ret += source()->current();
    }

    source()->prev();

    return ret;
}

void icL::flyLiteral(FlyResult & result) {
    int foldLevel = 0;

    source()->next();
    result.data.insert("lit-begin", source()->getPosition());
    source()->prev();

    icString litStr;

    while (!source()->atEnd()) {
        icChar next = source()->next();

        if (next == '[') {
            foldLevel++;
        }
        else if (next == ']') {
            if (foldLevel == 0) {
                break;
            }

            foldLevel--;
        }
        else if (next == '\\') {
            litStr += source()->next();
        }
        else {
            litStr += next;
        }
    }

    if (source()->current() != ']') {
        _il()->vm->cp_sig(
          issueWithPath(result.begin) % "Failed to find end of literal");
    }

    result.data.insert("lit-end", source()->getPosition());
    result.data.insert("lit-str", litStr);
}

void icL::flyComment(FlyResult & result) {
    icChar next = source()->next();
    // The position where the comment end
    il::Position end = source()->getPosition();

    result.type = ResultType::Comment;

    if (next == '`') {
        next = source()->next();

        if (next == '`') {
            next = source()->next();

            result.data["begin"] = source()->getPosition();
            result.data["type"]  = 3;

            auto currc = next, prevc = currc;  // previous chars
            auto currp = end, prevp = currp;   // previous positions

            while (next != '\0' &&
                   !(next == '`' && currc == '`' && prevc == '`')) {
                // clang-format off
                prevc = currc; currc = next;  next  = source()->next();
                end   = prevp; prevp = currp; currp = source()->getPosition();
                // clang-format on
            }

            if (next != '`') {
                _il()->vm->cp_sig(
                  issueWithPath(result.begin) %
                  "Multiline comment not finished");
            }
        }
        else {
            result.data["begin"] = source()->getPosition();
            result.data["type"]  = 2;

            while (next != '\n' && next != '\0') {
                next = source()->next();
                end  = source()->getPosition();
            }
        }
    }
    else {
        result.data["begin"] = source()->getPosition();
        result.data["type"]  = 1;

        while (next != '\n' && next != '`' && next != '\0') {
            next = source()->next();
            end  = source()->getPosition();
        }

        if (next != '`') {
            _il()->vm->cp_sig(
              issueWithPath(result.begin) % "Inline comment not finished");
        }
    }
    result.data["end"] = end;
}

void icL::flyLocal(FlyResult & result) {
    icString name = flyName();
    result.key    = "@" % name;

    if (name.isEmpty()) {
        name = "@";
    }

    result.data.insert("name", name);
    result.type = ResultType::Local;
}

void icL::flyGlobal(FlyResult & result) {
    icString name = flyName();
    result.key    = "#" % name;

    if (name.isEmpty()) {
        name        = "#";
        result.type = ResultType::Local;
    }
    else {
        result.type = ResultType::Global;
    }

    result.data.insert("name", name);
}

void icL::flyFunction(FlyResult & result) {
    icString name = flyName();
    result.key    = "$" % name;
    result.type   = ResultType::Function;

    result.data.insert("name", name);
}

void icL::flyProperty(FlyResult & result) {
    static const icSet<icString> prefixes{"attr", "attrs", "css", "prop",
                                          "props"};

    icString name = flyName();

    if (source()->next() == '-' && prefixes.contains(name)) {
        result.data["prefix"] = name;
        name                  = flyName();
    }
    else {
        source()->prev();
    }

    result.type = ResultType::Property;
    result.key  = '\'' + name;
    // send the name of property as meta data
    result.data.insert("name", name);

    if (name.isEmpty()) {
        _il()->vm->cp_sig(
          issueWithPath(result.begin) % "Property has empty name");
    }
}

void icL::flyMethod(FlyResult & result) {
    icString name = flyName();

    result.type = ResultType::Method;
    result.key  = '.' + name;
    // send the name of method as meta data
    result.data["name"] = name;

    if (name.isEmpty()) {
        _il()->vm->cp_sig(issueWithPath(result.begin) % "Method name is empty");
    }
}

void icL::flyKey(FlyResult & result) {
    source()->prev();

    result.key  = flyName();
    result.type = ResultType::Key;
}

void icL::flyDouble(FlyResult & result) {
    result.type = ResultType::Double;
}

void icL::flyElement(FlyResult & result) {
    result.type              = ResultType::Element;
    result.data["modifiers"] = getModifiers();

    icChar sibling = source()->next();

    if (sibling == '#' || sibling == '@') {
        auto name = flyName();

        result.data["scope"] = sibling % name;

        sibling = source()->next();
    }
    else {
        result.data["scope"] = icString{};
    }

    if (sibling == '[') {
        auto beginPos = source()->getPosition();

        FlyResult next = flyNext();
        while (next.type != ResultType::LimitedCl && !source()->atEnd() &&
               _il()->vm->hasOkState()) {
            next = flyNext();
        }

        source()->prev();
        auto endPos = source()->getPosition();
        source()->next();

        if (beginPos.line != endPos.line) {
            _il()->vm->syssig(
              "Unexpected new line character in element literal");
        }
        else {
            result.data.insert("code-begin", beginPos);
            result.data.insert("code-end", endPos);
        }
    }
    else {
        _il()->vm->syssig(
          "Unexpected `" % sibling % "` character, expected `[`");
    }

    result.end = source()->getPosition();
}

void icL::flyInt(FlyResult & result) {
    result.type = ResultType::Int;
}

void icL::flyJs(FlyResult & result) {
    icChar sibling = source()->next();

    if (sibling == '-') {
        il::Position modPos = source()->getPosition();
        icString     mod    = flyName();

        if (mod == "value") {
            flyJsValue(result);
        }
        else if (mod == "file") {
            flyJsFile(result);
        }
        else {
            _il()->vm->cp_sig(
              issueWithPath(modPos) % "Unknown modifier `" % mod %
              "`, expected `value` or `file`");
        }
    }
    else if (sibling == '{') {
        flyJsCode(result, _il()->server->getCurrentTarget());
    }
    else if (sibling == '@' || sibling == '#') {
        icString name = sibling == '#' ? "#" : flyName();

        if (name.isEmpty()) {
            name = "@";
        }

        if (source()->next() == '{') {
            flyJsCode(
              result, _il()
                        ->mem->stackIt()
                        .getContainerForVariable(name)
                        ->getValue(name)
                        .toTarget());
        }
        else {
            _il()->vm->cp_sig(
              issueWithPath(source()->getPosition()) % "Unexpected `" %
              source()->current() % "`, expected '{'");
        }
    }
    else {
        _il()->vm->cp_sig(
          issueWithPath(source()->getPosition()) % "Unexpected `" % sibling %
          "`, expected `-` or `{`");
        source()->prev();
    }
}

void icL::flyJsValue(FlyResult & result) {
    icChar       sibling = source()->next();
    il::Position start   = source()->getPosition();

    if (sibling == '{') {
        result.data.insert("getter-begin", source()->getPosition() + 1);

        JsFlyResult current;

        while ((current = jsFlyNext()).type != JsResultType::CurlyCl &&
               !source()->atEnd() && _il()->vm->hasOkState()) {
            if (current.type == JsResultType::CommandEnd) {
                if (result.data.contains("setter-begin")) {
                    _il()->vm->cp_sig(
                      issueWithPath(source()->getPosition()) %
                      "JavaScript value can contains just a getter and maybe a "
                      "setter");
                }
                else {
                    result.data.insert("getter-end", source()->getPosition());
                    result.data.insert(
                      "setter-begin", source()->getPosition() + 1);
                }
            }
        }

        if (current.type != JsResultType::CurlyCl) {
            _il()->vm->cp_sig(
              issueWithPath(start) % "End of js:value was not found");
        }

        if (result.data.contains("setter-begin")) {
            result.data.insert("setter-end", source()->getPosition());
        }
        else {
            result.data.insert("getter-end", source()->getPosition());
        }

        result.type = ResultType::JsValue;
    }
    else {
        _il()->vm->cp_sig(
          issueWithPath(source()->getPosition()) % "Unexpected `" %
          source()->current() % "`, expected `{`");
    }
}

void icL::flyJsCode(FlyResult & result, const il::TargetData & target) {
    result.type = ResultType::JsCode;

    if (target.sessionId.isEmpty()) {
        _il()->vm->cp_sig(
          issueWithPath(result.begin) %
          "Wrong target, a compatible target is a document, tab, window or "
          "session");
    }
    else {
        il::Position curlyOpenBracketPosition = source()->getPosition();

        source()->next();
        result.data.insert("begin", source()->getPosition());
        source()->prev();

        result.data.insert("target-session", target.sessionId);
        result.data.insert("target-window", target.windowId);

        JsFlyResult current;

        while ((current = jsFlyNext()).type != JsResultType::CurlyCl &&
               !source()->atEnd() && _il()->vm->hasOkState())
            ;

        if (current.type != JsResultType::CurlyCl) {
            _il()->vm->cp_sig(
              issueWithPath(curlyOpenBracketPosition) %
              "End of js code was not found");
        }

        source()->prev();
        result.data.insert("end", source()->getPosition());
        source()->next();
    }
}

void icL::flyJsFile(FlyResult & result) {
    result.type = ResultType::JsFile;

    if (source()->next() == '[') {
        flyLiteral(result);
    }
    else {
        _il()->vm->cp_sig(
          issueWithPath(source()->getPosition()) % "Unexpected `" %
          source()->current() % "`, expected `[`");
    }
}

void icL::flyNumber(FlyResult & result) {
    result.key = source()->current();

    while (source()->next().isDigit()) {
        result.key += source()->current();
    }

    if (source()->current() == '.') {
        if (source()->next().isDigit()) {
            result.key += '.';
            result.key += source()->current();

            while (source()->next().isDigit()) {
                result.key += source()->current();
            }

            source()->prev();
            flyDouble(result);
            return;
        }

        source()->prev();
        source()->prev();
    }

    source()->prev();
    flyInt(result);
}

void icL::flyRegEx(FlyResult & result) {
    icChar   delimiter = source()->current();
    auto     backup    = source()->getPosition();
    icChar   last      = '\0';
    icString regex;

    while (!source()->next().isWhite()) {
        if (last == delimiter && source()->current() == '/') {
            break;
        }

        last = source()->current();
        regex += last;
    }

    if (last == delimiter && source()->current() == '/') {
        result.end  = source()->getPosition();
        result.type = ResultType::RegEx;

        regex.remove(regex.length() - 1);
        result.data.insert("regex", regex);

        icString modifiers;

        while (source()->next().isLetter()) {
            modifiers += source()->current();
        }

        result.data.insert("mods", modifiers);
        source()->prev();
    }
    else {
        source()->seek(backup);
        source()->prev();
    }
}

void icL::flyString(FlyResult & result) {
    icString content;

    while (source()->next() != '"' && !source()->atEnd()) {
        content += source()->current();

        if (source()->current() == '\\') {
            content += source()->next();
        }
        else if (source()->current() == '\n') {
            _il()->vm->cp_sig(
              issueWithPath(source()->getPosition()) %
              "Expected \" instead of \\n");
            break;
        }
    }

    if (source()->atEnd() && source()->current() != '"') {
        _il()->vm->cp_sig(
          issueWithPath(result.begin) % "End of string not found");
    }

    result.type = ResultType::String;
    result.key  = '"' % content % '"';
    result.data.insert("content", content);
}

void icL::flyAnd(FlyResult & result) {
    result.key  = "&";
    result.type = ResultType::And;
}

void icL::flyBg(FlyResult & result) {
    if (source()->next() == '=') {
        if (source()->next() == '<') {
            flyBgEqSm(result);
        }
        else {
            source()->prev();
            flyBgEq(result);
        }
    }
    else if (source()->current() == '<') {
        flyBgSm(result);
    }
    else {
        result.key  = ">";
        result.type = ResultType::Bg;
        source()->prev();
    }
}

void icL::flyBgEq(FlyResult & result) {
    result.key  = ">=";
    result.type = ResultType::BgEq;
}

void icL::flyBgEqSm(FlyResult & result) {
    result.key  = ">=<";
    result.type = ResultType::BgEqSm;
}

void icL::flyBgSm(FlyResult & result) {
    result.key  = "><";
    result.type = ResultType::BgSm;
}

void icL::flyBs(FlyResult & result) {
    result.key  = "\\";
    result.type = ResultType::Bs;
}

void icL::flyCl(FlyResult & result) {
    icChar next = source()->next();

    if (next == '?') {
        flyClAs(result);
    }
    else if (next == ':') {
        flyClCl(result);
    }
    else if (next == '*') {
        flyClSt(result);
    }
    else if (next == '=') {
        flyClEq(result);
    }
    else {
        source()->prev();
        result.key  = ":";
        result.type = ResultType::Cl;
    }
}

void icL::flyClAs(FlyResult & result) {}

void icL::flyClCl(FlyResult & result) {}

void icL::flyClSt(FlyResult & result) {}

void icL::flyClEq(FlyResult & result) {
    result.key  = ":=";
    result.type = ResultType::ClEq;
}

void icL::flyEq(FlyResult & result) {
    if (source()->next() == '=') {
        flyEqEq(result);
    }
    else {
        result.key  = "=";
        result.type = ResultType::Eq;
        source()->prev();
    }
}

void icL::flyEqEq(FlyResult & result) {
    result.key  = "==";
    result.type = ResultType::EqEq;
}

void icL::flyMn(FlyResult & result) {
    result.key  = "-";
    result.type = ResultType::Mn;
}

void icL::flyNt(FlyResult & result) {
    if (source()->next() == '=') {
        flyNtEq(result);
    }
    else if (source()->current() == '<') {
        flyNtSm(result);
    }
    else if (source()->current() == '*') {
        flyNtSt(result);
    }
    else {
        result.key  = "!";
        result.type = ResultType::Nt;
        source()->prev();
    }
}

void icL::flyNtEq(FlyResult & result) {
    result.key  = "!=";
    result.type = ResultType::NtEq;
}

void icL::flyNtSm(FlyResult & result) {
    result.key  = "!<";
    result.type = ResultType::NtSm;
}

void icL::flyNtSt(FlyResult & result) {
    result.key  = "!*";
    result.type = ResultType::NtSt;
}

void icL::flyOr(FlyResult & result) {
    result.key  = "|";
    result.type = ResultType::Or;
}

void icL::flyOr2(FlyResult & result) {
    result.key  = "^";
    result.type = ResultType::Or2;
}

void icL::flyPc(FlyResult & result) {
    result.key  = "%";
    result.type = ResultType::Pc;
}

void icL::flyPl(FlyResult & result) {
    result.key  = "+";
    result.type = ResultType::Pl;
}

void icL::flySm(FlyResult & result) {
    if (source()->next() == '>') {
        flySmBg(result);
    }
    else if (source()->current() == '=') {
        if (source()->next() == '>') {
            flySmEqBg(result);
        }
        else {
            flySmEq(result);
            source()->prev();
        }
    }
    else if (source()->current() == '<') {
        flySmSm(result);
    }
    else if (source()->current() == '*') {
        flySmSt(result);
    }
    else {
        result.key  = "<";
        result.type = ResultType::Sm;
        source()->prev();
    }
}

void icL::flySmBg(FlyResult & result) {
    result.key  = "<>";
    result.type = ResultType::SmBg;
}

void icL::flySmEq(FlyResult & result) {
    result.key  = "<=";
    result.type = ResultType::SmEq;
}

void icL::flySmEqBg(FlyResult & result) {
    result.key  = "<=>";
    result.type = ResultType::SmEqBg;
}

void icL::flySmSm(FlyResult & result) {
    result.key  = "<<";
    result.type = ResultType::SmSm;
}

void icL::flySmSt(FlyResult & result) {
    result.key  = "<*";
    result.type = ResultType::SmSt;
}

void icL::flySl(FlyResult & result) {
    icChar next = source()->next();

    if (next == '\'') {
        flySlAp(result);
    }
    else if (!next.isWhite() && !"@#$"_str.contains(next)) {
        flyRegEx(result);
    }
    else {
        result.key  = "/";
        result.type = ResultType::Sl;
        source()->prev();
    }
}

void icL::flySlAp(FlyResult & result) {
    result.key  = "/'";
    result.type = ResultType::SlAp;
}

void icL::flySt(FlyResult & result) {
    if (source()->next() == '*') {
        flyStSt(result);
    }
    else {
        result.key  = "*";
        result.type = ResultType::St;
        source()->prev();
    }
}

void icL::flyStSt(FlyResult & result) {
    result.key  = "**";
    result.type = ResultType::StSt;
}

void icL::flyTl(FlyResult & result) {
    result.key  = "~";
    result.type = ResultType::Tl;
}

void icL::flyRunOp(FlyResult & result) {
    result.key  = "{";
    result.type = ResultType::RunOp;

    if (source()->next() == ':') {
        icString name;

        while (source()->next().isLetterOrDigit()) {
            name += source()->current();
        }

        result.key += ':';
        result.key += name;
        result.data.insert("name", name);
    }
    else {
        result.data.insert("name", icString{});
    }

    source()->prev();
}

void icL::flyRunCl(FlyResult & result) {
    result.key  = "}";
    result.type = ResultType::RunCl;
}

void icL::flyLimitedOp(FlyResult & result) {
    result.key  = "[";
    result.type = ResultType::LimitedOp;
}

void icL::flyLimitedCl(FlyResult & result) {
    result.key  = "]";
    result.type = ResultType::LimitedCl;
}

void icL::flyValueOp(FlyResult & result) {
    result.key  = "(";
    result.type = ResultType::ValueOp;
}

void icL::flyValueCl(FlyResult & result) {
    result.key  = ")";
    result.type = ResultType::ValueCl;
}

void icL::flyComma(FlyResult & result) {
    result.key  = ",";
    result.type = ResultType::Comma;
}

void icL::flyCommandEnd(FlyResult & result) {
    result.type = ResultType::CommandEnd;
}

}  // namespace icL::cp
