#ifndef service_Mouse
#define service_Mouse


namespace icL::service {

class Mouse
{
public:
    Mouse();

    /// `[r/o] Mouse'left : 1`
    int left();

    /// `[r/o] Mouse'middle : 2`
    int middle();

    /// `[r/o] Mouse'right : 3`
    int right();
};

}  // namespace icL::service

#endif  // service_Mouse
