#ifndef service_Key
#define service_Key


namespace icL::service {

class Key
{
public:
    Key();

    // properties level 1

    /// `[r/o] Key'alt : 4`
    int alt();

    /// `[r/o] Key'ctrl : 1`
    int ctrl();

    /// `[r/o] Key'shift : 2`
    int shift();
};

}  // namespace icL::service

#endif  // service_Key
