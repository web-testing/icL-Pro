#ifndef service_Sessions
#define service_Sessions

#include <icL-service-main/values/inode.h++>



namespace icL {

namespace il {
struct Session;
}  // namespace il

namespace service {

class Sessions : virtual public INode
{
public:
    Sessions();

    // properties / methods level 1

    /// `[r/o] Sessions'current : session`
    il::Session current();

    /// `[r/o] Sessions'length : int`
    int length();

    /// `Sessions.closeAll () : void`
    void closeAll();

    /// `Sessions.get (i : int) : session`
    il::Session get(int i);

    /// `Sessions.new () : session`
    il::Session new_();
};

}  // namespace service
}  // namespace icL

#endif  // service_Sessions
