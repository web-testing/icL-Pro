#ifndef service_Windows
#define service_Windows

#include <icL-il/structures/target-data.h++>

#include <icL-service-main/values/inode.h++>



namespace icL::service {

class Windows : virtual public INode
{
public:
    Windows(il::TargetData target);

    // properties level 1

    /// `[r/o] Windows'current : window`
    il::Window current();

    /// `[r/o] Windows'length : int`
    int length();

    /// `[r/o] Windows'session : session`
    il::Session session();

    // methods level 1

    /// `Windows.get (i : int) : Window`
    il::Window get(int i);

protected:
    /// \brief target data about current webview of windows
    il::TargetData target;
};

}  // namespace icL::service

#endif  // service_Windows
