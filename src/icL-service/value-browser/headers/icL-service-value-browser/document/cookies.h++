#ifndef service_Cookies
#define service_Cookies

#include <icL-il/structures/target-data.h++>

#include <icL-service-main/values/inode.h++>



namespace icL {

namespace il {
struct Cookie;
}  // namespace il

namespace service {

class Cookies : virtual public INode
{
public:
    Cookies(il::TargetData target);

    // properties level 1

    /// `cookies'tab : tab`
    il::Tab tab();

    // methods level 1

    /// `Cookies.deleteAll () : void`
    void deleteAll();

    /// `Cookies.get (name : string) : Cookie`
    il::Cookie get(const icString & name);

    /// `Cookies.new () : cookie`
    il::Cookie new_();

protected:
    /// \brief target data about webview of cookie
    il::TargetData target;
};

}  // namespace service
}  // namespace icL

#endif  // service_Cookies
