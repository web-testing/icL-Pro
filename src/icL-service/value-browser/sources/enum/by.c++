#include "by.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using il::Selectors::Selectors;

By::By() = default;

int By::cssSelector() {
    return Selectors::CssSelector;
}

int By::linkText() {
    return Selectors::LinkText;
}

int By::partialLinkText() {
    return Selectors::PartialLinkText;
}

int By::tagName() {
    return Selectors::TagName;
}

int By::xPath() {
    return Selectors::XPath;
}

}  // namespace icL::service
