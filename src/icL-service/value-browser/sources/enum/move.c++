#include "move.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using il::MoveTypes::MoveTypes;

Move::Move() = default;

int Move::bezier() {
    return MoveTypes::Bezier;
}

int Move::cubic() {
    return MoveTypes::Cubic;
}

int Move::linear() {
    return MoveTypes::Linear;
}

int Move::quadratic() {
    return MoveTypes::Quadratic;
}

int Move::teleport() {
    return MoveTypes::Teleport;
}

}  // namespace icL::service
