#include "mouse.h++"

#include <icL-il/main/enums.h++>



namespace icL::service {

using il::MouseButtons::MouseButtons;

Mouse::Mouse() = default;

int Mouse::left() {
    return MouseButtons::Left;
}

int Mouse::middle() {
    return MouseButtons::Middle;
}

int Mouse::right() {
    return MouseButtons::Right;
}

}  // namespace icL::service
