#include "tabs.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>

#include <functional>
#include <utility>



namespace icL::service {

using memory::Signals::Signals;

Tabs::Tabs(il::TargetData target)
    : target(std::move(target)) {}

il::Tab Tabs::current() {
    il::Tab tab;

    tab.data =
      std::make_shared<il::TargetData>(_il()->server->getCurrentTarget());
    return tab;
}

il::Tab Tabs::first() {
    il::Tab                tab;
    icList<il::TargetData> tabs = _il()->server->getWindows();


    tab.data = std::make_shared<il::TargetData>(tabs.first());
    return tab;
}

il::Tab Tabs::last() {
    il::Tab                tab;
    icList<il::TargetData> tabs = _il()->server->getWindows();

    tab.data = std::make_shared<il::TargetData>(tabs.last());
    return tab;
}

int Tabs::length() {
    return _il()->server->getWindows().length();
}

il::Tab Tabs::next() {
    il::Tab                tab;
    icList<il::TargetData> tabs    = _il()->server->getWindows();
    il::TargetData         current = _il()->server->getCurrentTarget();

    int currentPos = tabs.indexOf(current);

    if (currentPos >= tabs.length() - 1 || currentPos == -1) {
        _il()->vm->signal({Signals::NoSuchWindow, {}});
    }
    else {
        tab.data = std::make_shared<il::TargetData>(tabs[currentPos + 1]);
    }

    return tab;
}

il::Tab Tabs::previous() {
    il::Tab                tab;
    icList<il::TargetData> tabs    = _il()->server->getWindows();
    il::TargetData         current = _il()->server->getCurrentTarget();

    int currentPos = tabs.indexOf(current);

    if (currentPos < 1 /*|| currentPos == -1*/) {
        _il()->vm->signal({Signals::NoSuchWindow, {}});
    }
    else {
        tab.data = std::make_shared<il::TargetData>(tabs[currentPos - 1]);
    }

    return tab;
}

il::Session Tabs::session() {
    il::Session session;

    session.data =
      std::make_shared<il::TargetData>(_il()->server->getCurrentTarget());
    return session;
}

int Tabs::close(const icString & template_) {
    return close(icRegEx::fromWildcard(template_));
}

int Tabs::close(const icRegEx & url) {
    return closeBy(url, [this]() -> icString { return _il()->server->url(); });
}

int Tabs::closeByTitle(const icString & template_) {
    return closeByTitle(icRegEx::fromWildcard(template_));
}

int Tabs::closeByTitle(const icRegEx & title) {
    return closeBy(
      title, [this]() -> icString { return _il()->server->title(); });
}

int Tabs::closeOthers() {
    return closeBy(
      [this]() -> bool { return _il()->server->getCurrentTarget() != target; });
}

int Tabs::closeToLeft() {
    icList<il::TargetData> sessions = _il()->server->getSessions();

    int current = sessions.indexOf(target);

    return closeBy([&sessions, &current, this]() -> bool {
        return sessions.indexOf(_il()->server->getCurrentTarget()) < current;
    });
}

int Tabs::closeToRight() {
    icList<il::TargetData> sessions = _il()->server->getSessions();

    int current = sessions.indexOf(target);

    return closeBy([&sessions, &current, this]() -> bool {
        return sessions.indexOf(_il()->server->getCurrentTarget()) > current;
    });
}

il::Tab Tabs::find(const icString & template_) {
    return find(icRegEx::fromWildcard(template_));
}

il::Tab Tabs::find(const icRegEx & url) {
    return findBy(url, [this]() -> icString { return _il()->server->url(); });
}

il::Tab Tabs::findByTitle(const icString & template_) {
    return findByTitle(icRegEx::fromWildcard(template_));
}

il::Tab Tabs::findByTitle(const icRegEx & title) {
    return findBy(title, [this]() { return _il()->server->title(); });
}

il::Tab Tabs::get(int i) {
    icList<il::TargetData> tabs = _il()->server->getWindows();
    il::Tab                tab;

    if (i < 0 || i >= tabs.length()) {
        _il()->vm->signal({Signals::OutOfBounds, {}});
    }
    else {
        tab.data = std::make_shared<il::TargetData>(tabs[i]);
    }

    return tab;
}

il::Tab Tabs::new_() {
    il::Tab tab;
    tab.data = std::make_shared<il::TargetData>(_il()->server->newWindow());
    return tab;
}

int Tabs::closeBy(
  const icRegEx & icRegEx, const std::function<icString()> & getField) {
    return closeBy([&icRegEx, &getField]() -> bool {
        auto match = icRegEx.match(getField());
        return match.hasMatch();
    });
}

int Tabs::closeBy(const std::function<bool()> & cond) {
    il::TargetData         current  = _il()->server->getCurrentTarget();
    icList<il::TargetData> tabs     = _il()->server->getWindows();
    icList<il::TargetData> sessions = _il()->server->getSessions();
    icList<il::TargetData> removed;

    for (auto & tab : tabs) {
        _il()->server->pushTarget(tab);

        if (cond()) {
            _il()->server->closeWindow();
            removed += tab;
        }

        _il()->server->popTarget();
    }

    if (removed.contains(current)) {
        if (removed.length() == tabs.length()) {
            int sessionIndex = sessions.indexOf(current);

            // delete current closed session from sessions
            sessions.removeOne(current);

            if (sessionIndex >= 0 && sessionIndex < sessions.length()) {
                current = sessions[sessionIndex];
            }
            else if (sessionIndex == sessions.length()) {
                current = sessions.last();
            }
            else if (!sessions.isEmpty()) {
                current = sessions[0];
            }
            else {
                current = il::TargetData();
            }
        }
        else {
            for (auto & session : sessions) {
                if (!removed.contains(session)) {
                    current = session;
                    break;
                }
            }
        }

        _il()->server->focusSession(current);
    }

    return removed.length();
}

il::Tab Tabs::findBy(
  const icRegEx & icRegEx, const std::function<icString()> & getField) {
    return findBy([&icRegEx, &getField]() -> bool {
        auto match = icRegEx.match(getField());
        return match.hasMatch();
    });
}

il::Tab Tabs::findBy(const std::function<bool()> & cond) {
    il::TargetData         ret;
    icList<il::TargetData> tabs = _il()->server->getWindows();

    for (auto & tab : tabs) {
        _il()->server->pushTarget(tab);

        if (cond() && tab.sessionId.isEmpty()) {
            ret = tab;
        }

        _il()->server->popTarget();
    }

    il::Tab tab;
    tab.data = std::make_shared<il::TargetData>(ret);
    return tab;
}

}  // namespace icL::service
