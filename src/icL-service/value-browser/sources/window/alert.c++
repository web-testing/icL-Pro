#include "alert.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>



namespace icL::service {

Alert::Alert(il::TargetData target)
    : target(std::move(target)) {}

il::Session Alert::session() {
    il::Session session;

    session.data =
      std::make_shared<il::TargetData>(_il()->server->getCurrentTarget());
    return session;
}

icString Alert::text() {
    return _il()->server->alertText();
}

void Alert::accept() {
    _il()->server->alertAccept();
}

void Alert::dismiss() {
    _il()->server->alertDimiss();
}

void Alert::sendKeys(const icString & text) {
    _il()->server->alertSendText(text);
}

}  // namespace icL::service
