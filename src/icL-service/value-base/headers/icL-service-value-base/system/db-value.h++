#ifndef service_DBValue
#define service_DBValue

#include <icL-service-main/values/inode.h++>
#include <icL-service-main/values/ivalue.h++>



class icString;

namespace icL {

namespace il {
struct Query;
}

namespace service {

class DBValue
    : virtual public INode
    , virtual public IValue
{
public:
    DBValue();

    // methods level 1

    /// `db.close () : void`
    void close();

    /// `db.query (q : Code) : Query`
    il::Query query(const icString & code);
};

}  // namespace service
}  // namespace icL

#endif  // service_DBValue
