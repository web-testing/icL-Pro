#ifndef service_QueryValue
#define service_QueryValue

#include <icL-service-main/values/inode.h++>
#include <icL-service-main/values/ivalue.h++>



class icString;
class icVariant;

namespace icL {

namespace il {
struct Query;
}

namespace service {

class QueryValue
    : virtual public INode
    , virtual public IValue
{
public:
    QueryValue();

    // methods level 1

    /// `query.exec () : bool`
    bool exec();

    /// `query.first () : bool`
    bool first();

    /// `query.get (field : string) : any`
    icVariant get(const icString & field);

    /// `query.getError () : string`
    icString getError();

    /// `query.getLength () : int`
    int getLength();

    /// `query.getRowsAffected () : int`
    int getRowsAffected();

    /// `query.last () : bool`
    bool last();

    /// `query.next () : bool`
    bool next();

    /// `query.previous () : bool`
    bool previous();

    /// `query.seek (i : int, relative = false) : bool`
    bool seek(int i, bool relative = false);

    /// `query.set (field : string, value : any) : void`
    void icSet(const icString & field, const icVariant & value);

protected:
    /**
     * @brief _value return the own value
     * @return the own value as query target
     */
    il::Query _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_QueryValue
