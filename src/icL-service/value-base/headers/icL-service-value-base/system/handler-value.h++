#ifndef service_HandlerValue
#define service_HandlerValue

#include <icL-service-main/values/inode.h++>
#include <icL-service-main/values/ivalue.h++>



namespace icL {

namespace il {
struct Handler;
struct LambdaTarget;
}  // namespace il

namespace service {

/**
 * @brief The HandlerValue class represents a value of type `handler`
 */
class HandlerValue
    : virtual public INode
    , virtual public IValue
{
public:
    /// `handler.setup (code : code-icl) : handler`
    il::Handler setup(const il::LambdaTarget & lambda);

    /// `handler.activate () : handler`
    il::Handler activate();

    /// `handler.deactivate () : handler`
    il::Handler deactivate();

    /// `handler.kill () : void`
    void kill();

protected:
    /**
     * @brief _value returns the own value
     * @return the own value as handler target
     */
    il::Handler _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_HandlerValue
