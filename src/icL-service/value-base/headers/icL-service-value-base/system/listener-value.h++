#ifndef service_ListenerValue
#define service_ListenerValue

#include <icL-service-main/values/inode.h++>
#include <icL-service-main/values/ivalue.h++>



class icString;
template <typename>
class icList;

namespace icL {

namespace il {
struct Handler;
struct Listener;
struct InterLevel;
}  // namespace il

namespace memory {
struct Parameter;
}

namespace service {

/**
 * @brief The ListenerValue class represent a value of `listener` type
 */
class ListenerValue
    : virtual public INode
    , virtual public IValue
{
public:
    /// `listen <serverAddress> : listener`
    static il::Listener create(il::InterLevel * il, const icString & server);

    /// `listener.handle (params ...) : handler`
    il::Handler handle(const icList<icL::memory::Parameter> & params);

protected:
    /**
     * @brief _value returns the own value
     * @return own value as listener target
     */
    il::Listener _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_ListenerValue
