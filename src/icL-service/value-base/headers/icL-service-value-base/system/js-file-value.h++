#ifndef service_JsFileValue
#define service_JsFileValue

#include <icL-service-main/values/inode.h++>
#include <icL-service-main/values/ivalue.h++>


class icString;

template <typename T>
class icList;

using icVariantList = icList<icVariant>;

namespace icL {

namespace il {
struct JsFile;
}

namespace service {

class JsFileValue
    : public virtual IValue
    , public virtual INode
{
public:
    /// `js-file.load (path : string) : js-file`
    il::JsFile load(icString path);

    /// ` js-file.run (args : any ...) : any`
    icVariant run(icVariantList args);

    /// `js-file.runAsync (args : any ...) : void `
    void runAsync(icVariantList args);

    /// ` js-file.setAsUserScript () : js-file`
    il::JsFile setAsUserScript();

    /// `js-file.setAsPersistentUserScript () : js-file`
    il::JsFile setAsPersistentUserScript();

protected:
    virtual il::JsFile _value() = 0;

private:
    icString getFileContent();
};

}  // namespace service
}  // namespace icL

#endif  // service_JsFileValue
