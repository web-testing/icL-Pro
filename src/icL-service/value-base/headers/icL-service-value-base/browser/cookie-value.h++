#ifndef service_CookieValue
#define service_CookieValue

#include <icL-service-main/values/inode.h++>
#include <icL-service-main/values/ivalue.h++>



namespace icL {

namespace il {
struct Cookie;
}

namespace service {

class CookieValue
    : virtual public INode
    , virtual public IValue
{
public:
    CookieValue();
    ~CookieValue() override = default;

    // methods level 1

    /// `cookie.add (years : int, months : int, days : int, hours = 0, minutes =
    /// 0, seconds = 0) : cookie`
    void add(
      int years, int months, int days, int hours = 0, int minutes = 0,
      int seconds = 0);

    /// `cookie.load () : cookie`
    void load();

    /// `cookie.resetTime () : cookie`
    void resetTime();

    /// `cookie.save () : cookie`
    void save();

protected:
    /**
     * @brief _value gets the own value
     * @return the own value as il.CookieRef
     */
    il::Cookie _value();
};

}  // namespace service
}  // namespace icL

#endif  // service_CookieValue
