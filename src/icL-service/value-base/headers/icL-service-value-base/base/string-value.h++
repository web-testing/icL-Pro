#ifndef service_StringValue
#define service_StringValue

#include <icL-service-main/values/inode.h++>
#include <icL-service-main/values/ivalue.h++>



class icString;
class icStringList;
class icRegEx;

namespace icL::service {

class StringValue
    : virtual public IValue
    , virtual public INode
{
public:
    StringValue();
    ~StringValue() override = default;

    // properties level 1

    /// `[r/o] string'empty : bool`
    bool empty();

    /// `[r/o] string'length : int`
    int length();

    /// `[r/o] string'last : string`
    icString last();

    // methods level 1

    /// `string.append (str : string) : string`
    void append(const icString & str);

    /// `string.at (i : int) : string`
    icString at(int i);

    /// `string.beginsWith (str : string) : bool`
    bool beginsWith(const icString & str);

    /// `string.compare (str : string, caseSensitive = true) : bool`
    bool compare(const icString & str, bool caseSensitive = true);

    /// `string.count (str : string) : int`
    int count(const icString & str);

    /// `string.count (re : regex) : int`
    int count(const icRegEx & re);

    /// `string.endWith (str : string) : bool`
    bool endsWith(const icString & str);

    /// `string.indexOf (str : string, startPos = 0) : int`
    int indexOf(const icString & str, int startPos = 0);

    /// `string.indexOf (re : regex, startPos = 0) : int`
    int indexOf(const icRegEx & re, int startPos = 0);

    /// `string.insert (str : string, pos : int) : string`
    void insert(const icString & str, int pos);

    /// `string.lastIndexOf (str : string, startPos = -1) : int`
    int lastIndexOf(const icString & str, int startPos = -1);

    /// `string.lastIndexOf (re : regex, startPos = -1) : int`
    int lastIndexOf(const icRegEx & re, int startPos = -1);

    /// `string.left (int n) : string`
    icString left(int n);

    /// `icString.leftJustified (width : int, fillChar : icString, truncate =
    /// false) : string`
    icString leftJustified(
      int width, const icString & fillChar, bool truncate = false);

    /// `string.midd (pos : int, n = -1) : string`
    icString mid(int pos, int n = -1);

    /// `string.prepend (str : string) : string`
    void prepend(const icString & str);

    /// `string.remove (pos : int, n : int) : string`
    void remove(int pos, int n);

    /// `string.remove (str : string, caseSensitive = true) : string`
    void remove(const icString & str, bool caseSensitive = true);

    /// `string.remove (re : regex) : string`
    void remove(const icRegEx & re);

    /// `string.replace (pos : int, n : int, after : string) : string`
    void replace(int pos, int n, const icString & after);

    /// `string.replace (before : string, after : string) : string`
    void replace(const icString & before, const icString & after);

    /// `string.repalce (re : regex, after : string) : string`
    void replace(const icRegEx & re, const icString & after);

    /// `string.right (n : int) : string`
    icString right(int n);

    /// `icString.rightJustified (width : int, fillChar : icString, truncate =
    /// false) : string`
    icString rightJustified(
      int width, const icString & fillChar, bool truncate = false);

    /// `icString.split (separator : icString, keepEmptyParts = true,
    /// caseSensitive = true) : list`
    icStringList split(
      const icString & separator, bool keepEmptyParts = true,
      bool caseSensitive = true);

    /// `string.split (re : regex, keepEmptyParts = true) : list`
    icStringList split(const icRegEx & re, bool keepEmptyParts = true);

    /// `string.substring (begin : int, end : int) : string`
    icString substring(int begin, int end);

    /// `string.toLowerCase () : string`
    icString toLowerCase();

    /// `string.toUpperCase () : string`
    icString toUpperCase();

    /// `string.trim (justWhitespace = true)`
    icString trim(bool justWhitespace = true);

protected:
    /**
     * @brief _value gets own value
     * @return the own value as icString
     */
    icString _value();
};

}  // namespace icL::service

#endif  // service_StringValue
