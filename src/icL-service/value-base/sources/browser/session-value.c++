#include "session-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>



namespace icL::service {

SessionValue::SessionValue() = default;

icString SessionValue::source() {
    return _il()->server->source();
}

icString SessionValue::title() {
    return _il()->server->title();
}

void SessionValue::back() {
    _il()->server->back();
}

void SessionValue::close() {
    _il()->server->closeSession();
}

void SessionValue::forward() {
    _il()->server->forward();
}

void SessionValue::refresh() {
    _il()->server->refresh();
}

icString SessionValue::screenshot() {
    return _il()->server->screenshot();
}

void SessionValue::switchTo() {
    _il()->server->switchSession();
}

il::Session SessionValue::_value() {
    return getValue();
}

}  // namespace icL::service
