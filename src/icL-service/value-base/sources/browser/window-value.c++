#include "window-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>



namespace icL::service {

WindowValue::WindowValue() = default;

void WindowValue::close() {
    _il()->server->closeWindow();
}

void WindowValue::focus() {
    _il()->server->focusWindow();
}

void WindowValue::fullscreen() {
    _il()->server->fullscreen();
}

void WindowValue::maximize() {
    _il()->server->maximize();
}

void WindowValue::minimize() {
    _il()->server->minimize();
}

void WindowValue::restore() {
    _il()->server->restore();
}

void WindowValue::switchToFrame(int i) {
    _il()->server->switchToFrame(i);
}

void WindowValue::switchToFrame(const il::Element & el) {
    _il()->server->switchToFrame(el);
}

void WindowValue::switchToParent() {
    _il()->server->switchToParent();
}

il::Window WindowValue::_value() {
    return getValue();
}

}  // namespace icL::service
