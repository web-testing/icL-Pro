#include "file-value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/file-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/file-target.h++>

namespace icL::service {

FileValue::FileValue() = default;

int FileValue::format() {
    int ret;

    _il()->file->pushTarget(*_value().target);
    ret = _il()->file->getFormat();
    _il()->file->popTarget();

    return ret;
}

void FileValue::close() {
    _il()->file->pushTarget(*_value().target);
    _il()->file->close();
    _il()->file->popTarget();
}

void FileValue::delete_() {
    _il()->file->pushTarget(*_value().target);
    _il()->file->delete_();
    _il()->file->popTarget();
}

il::File FileValue::_value() {
    return getValue();
}

}  // namespace icL::service
