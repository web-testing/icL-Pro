#include "db-value.h++"

#include <icL-il/main/db-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>

namespace icL::service {

DBValue::DBValue() = default;

void DBValue::close() {
    _il()->db->close();
}

il::Query DBValue::query(const icString & code) {
    il::Query query;

    query.target = std::make_shared<il::DBTarget>(_il()->db->query(code));
    return query;
}

}  // namespace icL::service
