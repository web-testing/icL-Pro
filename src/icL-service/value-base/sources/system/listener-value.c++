#include "listener-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/listen.h++>
#include <icL-il/structures/listen-handler.h++>



namespace icL::service {

il::Listener ListenerValue::create(
  il::InterLevel * il, const icString & server) {
    return il->listen->listen(server);
}

il::Handler ListenerValue::handle(const icList<memory::Parameter> & params) {
    return _il()->listen->handle(_value(), params);
}

il::Listener ListenerValue::_value() {
    return getValue();
}

}  // namespace icL::service
