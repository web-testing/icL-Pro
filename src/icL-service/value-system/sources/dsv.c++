#include "dsv.h++"

#include <icL-il/main/file-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/file-target.h++>

#include <icL-memory/structures/set.h++>



namespace icL::service {

DSV::DSV() = default;

void DSV::append(const il::File & file, const memory::Set & icSet) {
    _il()->file->pushTarget(*file.target);
    _il()->file->append(icSet);
    _il()->file->popTarget();
}

void DSV::append(const il::File & file, const memory::Object & obj) {
    _il()->file->pushTarget(*file.target);
    _il()->file->append(obj);
    _il()->file->popTarget();
}

memory::Set DSV::load(
  const icString & delimiter, const il::File & file, const memory::Set & base) {
    memory::Set ret;

    _il()->file->pushTarget(*file.target);
    ret = _il()->file->load(delimiter, base);
    _il()->file->popTarget();

    return ret;
}

memory::Set DSV::loadCSV(const il::File & file, const memory::Set & base) {
    memory::Set ret;

    _il()->file->pushTarget(*file.target);
    ret = _il()->file->loadCSV(base);
    _il()->file->popTarget();

    return ret;
}

memory::Set DSV::loadTSV(const il::File & file, const memory::Set & base) {
    memory::Set ret;

    _il()->file->pushTarget(*file.target);
    ret = _il()->file->loadTSV(base);
    _il()->file->popTarget();

    return ret;
}

void DSV::sync(const il::File & file, const memory::Set & icSet) {
    _il()->file->pushTarget(*file.target);
    _il()->file->sync(icSet);
    _il()->file->popTarget();
}

void DSV::write(const il::File & file, const memory::Set & icSet) {
    _il()->file->pushTarget(*file.target);
    _il()->file->write(icSet);
    _il()->file->popTarget();
}

}  // namespace icL::service
