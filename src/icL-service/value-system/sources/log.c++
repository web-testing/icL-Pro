#include "log.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>



namespace icL::service {

Log::Log() = default;

void Log::error(const icString & message) {
    _il()->server->logError(message);
}

void Log::info(const icString & message) {
    _il()->server->logInfo(message);
}

void Log::out(const memory::ArgList & args) {
    for (auto & arg : args) {
        info(service::Stringify::arg(arg, arg.container));
    }
}

void Log::stack(const memory::Argument & arg) {
    if (arg.varName.isEmpty()) {
        _il()->vm->signal({memory::Signals::System, "Wrong argument"});
        return;
    }

    memory::StackContainer * it = _il()->mem->stackIt().stack();

    while (it != nullptr) {
        if (it->contains(arg.varName)) {
            info(service::Stringify::arg(arg, it));
        }
    }
}

void Log::state(const memory::Argument & arg) {
    if (arg.varName.isEmpty()) {
        _il()->vm->signal({memory::Signals::System, "Wrong argument"});
        return;
    }

    memory::StateContainer * it = _il()->mem->stateIt().state();

    while (it->getPrev() != nullptr) {
        it = it->getPrev();
    }

    while (it != nullptr) {
        if (it->contains(arg.varName)) {
            info(service::Stringify::arg(arg, it));
        }
    }
}

}  // namespace icL::service
