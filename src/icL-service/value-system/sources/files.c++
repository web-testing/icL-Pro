#include "files.h++"

#include <icL-il/main/file-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/file-target.h++>



namespace icL::service {

Files::Files() = default;

il::File Files::create(const icString & path) {
    il::File ret;

    ret.target = std::make_shared<il::FileTarget>(_il()->file->create(path));
    return ret;
}

void Files::createDir(const icString & path) {
    _il()->file->createDir(path);
}

void Files::createPath(const icString & path) {
    _il()->file->createPath(path);
}

il::File Files::open(const icString & path) {
    il::File ret;

    ret.target = std::make_shared<il::FileTarget>(_il()->file->open(path));
    return ret;
}

}  // namespace icL::service
