#include "datetime.h++"

#include <icL-types/replaces/ic-datetime.h++>



namespace icL::service {

DateTime::DateTime() = default;

icDateTime DateTime::current() {
    return icDateTime::currentDateTime();
}

icDateTime DateTime::currentUTC() {
    return icDateTime::currentDateTimeUtc();
}

}  // namespace icL::service
