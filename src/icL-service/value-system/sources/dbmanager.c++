#include "dbmanager.h++"

#include <icL-il/main/db-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/target-data.h++>



namespace icL::service {

DBManager::DBManager() = default;

il::DB DBManager::connect(const icString & server) {
    il::DB ret;

    ret.target = std::make_shared<il::DBTarget>(_il()->db->connect(server));
    return ret;
}

il::DB DBManager::connect(
  const icString & server, const icString & user, const icString & password) {
    il::DB ret;

    ret.target = std::make_shared<il::DBTarget>(
      _il()->db->connect(server, user, password));
    return ret;
}

il::DB DBManager::openSQLite(const icString & path) {
    il::DB ret;

    ret.target = std::make_shared<il::DBTarget>(_il()->db->openSQLite(path));
    return ret;
}

}  // namespace icL::service
