#include "import.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/source-server.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>



namespace icL::service {

using memory::Type;

Import::Import() = default;

void Import::all(const memory::Object & obj, const icString & path) {
    import(obj, path, [this](memory::Memory * dumpMemory) {
        cloneGlobals(dumpMemory);
        cloneFunctions(dumpMemory);
    });
}

void Import::functions(const memory::Object & obj, const icString & path) {
    import(obj, path, [this](memory::Memory * dumpMemory) {
        cloneGlobals(dumpMemory);
    });
}

void Import::none(const memory::Object & obj, const icString & path) {
    import(obj, path, [](memory::Memory *) {});
}

void Import::run(const icString & path) {
    memory::FunctionCall fcall;

    fcall.code.name   = path;
    fcall.code.source = _il()->source->getSource(path);

    if (fcall.code.source == nullptr) {
        _il()->vm->signal({memory::Signals::System, "File not found"});
    }
    else {
        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != memory::Signals::NoError) {
                _il()->vm->signal(ret.signal);
            }
            return false;
        });
    }
}

memory::ArgValueList Import::mapToArgValueList(const icVariantMap & map) {
    memory::ArgValueList args;

    for (auto it = map.begin(); it != map.end(); it++) {
        memory::ArgValue arg;

        arg.name  = it.key();
        arg.value = it.value();

        args.append(arg);
    }

    return args;
}

void Import::cloneGlobals(memory::Memory * dumpMemory) {
    auto * dumpState      = dumpMemory->stateIt().state();
    auto & dumpGlobalVars = dumpState->getMap();
    auto * curentState    = _il()->mem->stackIt().stack();

    for (auto it = dumpGlobalVars.begin(); it != dumpGlobalVars.end(); it++) {
        Type varType     = curentState->getType(it.key());
        Type dumpVarType = dumpState->getType(it.key());

        if (varType == Type::VoidValue || varType == dumpVarType) {
            curentState->setValue(it.key(), it.value());
        }
    }
}

void Import::cloneFunctions(memory::Memory * dumpMemory) {
    auto & dumpFuncs    = dumpMemory->functions();
    auto & dumpFuncsMap = dumpFuncs.getMap();
    auto   it           = dumpFuncsMap.begin();
    auto & currentFuncs = _il()->mem->functions();

    while (it != dumpFuncsMap.end()) {
        if (!currentFuncs.registerFunction(it.key(), it.value())) {
            _il()->vm->signal(
              {memory::Signals::System, "Try of function redefine"});
            break;
        }
    }
}

void Import::import(
  const memory::Object & obj, const icString & path,
  const std::function<void(memory::Memory *)> & feedback) {
    memory::FunctionCall fcall;

    fcall.isolateGlobalVariables = true;
    fcall.isolateLocalVariables  = true;

    fcall.code.name   = path;
    fcall.code.source = _il()->source->getSource(path);

    if (fcall.code.source == nullptr) {
        _il()->vm->signal({memory::Signals::System, "File not found"});
    }
    else {
        fcall.args = mapToArgValueList(obj.data->getMap());

        _il()->vms->interrupt(fcall, [this, feedback](const il::Return & ret) {
            if (ret.signal.code != memory::Signals::NoError) {
                _il()->vm->signal(ret.signal);
                return false;
            }

            feedback(_il()->mem);
            return false;
        });
    }
}

}  // namespace icL::service
