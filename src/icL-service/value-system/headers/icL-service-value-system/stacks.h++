#ifndef service_Stacks
#define service_Stacks

#include <icL-service-main/values/inode.h++>



class icString;

namespace icL {

namespace memory {
class StackContainer;
}

namespace il {
struct InterLevel;
}

namespace service {

class Stacks : virtual public INode
{
public:
    Stacks();

    // properties level 1

    /// `[r/o] Stacks (name : string) : Stack`
    memory::StackContainer * property(const icString & name);
};

}  // namespace service
}  // namespace icL

#endif  // service_Stacks
