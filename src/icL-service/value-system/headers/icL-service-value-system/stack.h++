#ifndef service_Stack
#define service_Stack


class icString;
class icVariant;

namespace icL {

namespace memory {
class StackContainer;
}

namespace service {

class Stack
{
public:
    Stack(memory::StackContainer * stack);

    // methods level 1

    /// `Stack.addDescription (description : string) : void`
    void addDescription(const icString & description);

    /// `Stack.break () : void`
    void break_();

    /// `Stack.clear () : void`
    void clear();

    /// `Stack.continue () : void`
    void continue_();

    /// `Stack.destroy () : void`
    void destroy();

    /// `Stack.ignore () : Stack`
    void ignore();

    /// `Stack.listen () : Stack`
    void listen();

    /// `Stack.markStep (name : string) : Stack`
    void markStep(const icString & name);

    /// `Stack.markTest (name : string) : Stack`
    void markTest(const icString & name);

    /// `Stack.return (value : any) : void`
    void return_(const icVariant & value);

protected:
    memory::StackContainer * stack = nullptr;
};

}  // namespace service
}  // namespace icL

#endif  // service_Stack
