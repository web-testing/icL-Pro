#ifndef service_DSV
#define service_DSV

#include <icL-service-main/values/inode.h++>



class icString;

namespace icL {

namespace il {
struct File;
struct InterLevel;
}  // namespace il

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace service {

class DSV : virtual public INode
{
public:
    DSV();

    // methods level 1

    /// `DSV.append (f : File, s : set) : File`
    void append(const il::File & file, const memory::Set & icSet);

    /// `DSV.append (f : File, obj : object) : File`
    void append(const il::File & file, const memory::Object & obj);

    /// `DSV.load (delimiter : string, f : File, base : set) : set`
    memory::Set load(
      const icString & delimiter, const il::File & file,
      const memory::Set & base);

    /// `DSV.loadCSV (f : File, base : set) : File`
    memory::Set loadCSV(const il::File & file, const memory::Set & base);

    /// `DSV.loadTSV (f : File, base : set) : File`
    memory::Set loadTSV(const il::File & file, const memory::Set & base);

    /// `DSV.sync (f : File, s : set) : set`
    void sync(const il::File & file, const memory::Set & icSet);

    /// `DSV.write (f : File, s : set) : File`
    void write(const il::File & file, const memory::Set & icSet);
};

}  // namespace service
}  // namespace icL

#endif  // service_DSV
