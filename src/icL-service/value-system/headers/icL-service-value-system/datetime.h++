#ifndef service_Datetime
#define service_Datetime



class icDateTime;

namespace icL::service {

class DateTime
{
public:
    DateTime();

    // methods level 1

    /// `icDateTime.current () : icDateTime`
    icDateTime current();

    /// `icDateTime.currentUTC () : icDateTime`
    icDateTime currentUTC();
};

}  // namespace icL::service

#endif  // service_Datetime
