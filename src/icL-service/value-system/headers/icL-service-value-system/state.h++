#ifndef service_State
#define service_State

#include <icL-service-main/values/inode.h++>



namespace icL {

namespace memory {
struct Object;
}

namespace il {
struct InterLevel;
}

namespace service {

class State : virtual public INode
{
public:
    State();

    // methods level 1

    /// `State.clear () : void`
    void clear();

    /// `State.delete () : void`
    void delete_();

    /// `State.new (data = [=]) : void`
    void new_(const memory::Object & data);

    /// `State.newAtEnd (data = [=]) : void`
    void newAtEnd(const memory::Object & data);

    /// `State.toFirst () : void`
    void toFirst();

    /// `State.toLast () : void`
    void toLast();

    /// `State.toNext () : void`
    void toNext();

    /// `State.toPrev () : void`
    void toPrev();
};

}  // namespace service
}  // namespace icL

#endif  // service_State
