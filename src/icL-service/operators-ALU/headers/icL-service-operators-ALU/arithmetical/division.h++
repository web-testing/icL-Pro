#ifndef service_Division
#define service_Division


namespace icL::service {

class Division
{
public:
    Division();

    // level 1

    /// `int / int : int`
    int intInt(int left, int right);

    /// `double / double : double`
    double doubleDouble(double left, double right);
};

}  // namespace icL::service

#endif  // service_Division
