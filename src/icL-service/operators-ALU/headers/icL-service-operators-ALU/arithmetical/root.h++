#ifndef service_Root
#define service_Root


namespace icL::service {

class Root
{
public:
    Root();

    // level 1

    /// `/' int : int`
    int voidInt(int right);

    /// `/' double : double`
    double voidDouble(double right);

    /// `int /' int : int`
    int intInt(int left, int right);

    /// `int /' double : double`
    double intDouble(int left, double right);

    /// `double /' double : double`
    double doubleDouble(double left, double right);
};

}  // namespace icL::service

#endif  // service_Root
