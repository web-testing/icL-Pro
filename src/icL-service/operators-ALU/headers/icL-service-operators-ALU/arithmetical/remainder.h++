#ifndef service_Remainder
#define service_Remainder

#include <icL-service-main/values/inode.h++>



namespace icL {

namespace memory {
struct Set;
}

namespace service {

class Remainder : virtual public INode
{
public:
    Remainder();

    // level 1

    /// `int \ int : int`
    int intInt(int left, int right);

    /// `set \ set : set`
    memory::Set setSet(const memory::Set & left, const memory::Set & right);
};

}  // namespace service
}  // namespace icL

#endif  // service_Remainder
