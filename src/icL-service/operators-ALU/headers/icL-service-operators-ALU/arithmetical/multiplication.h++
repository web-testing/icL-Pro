#ifndef service_Multiplication
#define service_Multiplication

#include <icL-service-main/values/inode.h++>



class icString;
class icStringList;

namespace icL {

namespace memory {
struct Set;
}

namespace service {

class Multiplication : virtual public INode
{
public:
    Multiplication();

    // level 1

    /// `int * int : int`
    int intInt(int left, int right);

    /// `double * double : double`
    double doubleDouble(double left, double right);

    /// `string * string : bool`
    bool stringString(const icString & left, const icString & right);

    /// `list * string : bool`
    bool listString(const icStringList & left, const icString & right);

    /// `list * list : bool`
    bool listList(const icStringList & left, const icStringList & right);

    /// `set * set : set`
    memory::Set setSet(const memory::Set & left, const memory::Set & right);
};

}  // namespace service
}  // namespace icL

#endif  // service_Multiplication
