#ifndef service_Exponentiation
#define service_Exponentiation

#include <icL-service-main/values/inode.h++>



class icString;
class icStringList;
class icRegEx;

namespace icL {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace service {

class Exponentiation : virtual public INode
{
public:
    Exponentiation();

    /// `int** : int`
    int intVoid(int left);

    /// `double** : double`
    double doubleVoid(double left);

    /// `int ** int : int`
    int intInt(int left, int right);

    /// `double ** int : double`
    double doubleInt(double left, int right);

    /// `double ** double : double`
    double doubleDouble(double left, double right);

    /// `string ** string : double`
    double stringString(const icString & left, const icString & right);

    /// `list ** list : double`
    double listList(const icStringList & left, const icStringList & right);

    /// `set ** set : bool`
    bool setSet(const memory::Set & left, const memory::Set & right);

    /// `string ** regex : object`
    memory::Object stringRegex(const icString & left, const icRegEx & right);
};

}  // namespace service
}  // namespace icL

#endif  // service_Exponentiation
