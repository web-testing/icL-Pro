#include "selective-select.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/factory/factory.h++>
#include <icL-service-main/values/set.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/set.h++>

namespace icL::service {

SelectiveSelect::SelectiveSelect() = default;

int SelectiveSelect::intInt(int left, int right) {
    return left + right;
}

double SelectiveSelect::doubleDouble(double left, double right) {
    return left + right;
}

icStringList SelectiveSelect::stringString(
  const icString & left, const icString & right) {
    return {left, right};
}

icStringList SelectiveSelect::listString(
  const icStringList & left, const icString & right) {
    return {left, right};
}

icStringList SelectiveSelect::stringList(
  const icString & left, const icStringList & right) {
    return {left, right};
}

icStringList SelectiveSelect::listList(
  const icStringList & left, const icStringList & right) {
    return {left, right};
}

memory::Set SelectiveSelect::objectObject(
  const memory::Object & left, const memory::Object & right) {
    memory::Set ret = Set::fromObject(left);

    Set::insert(_il(), ret, right);
    return ret;
}

memory::Set SelectiveSelect::setObject(
  const memory::Set & left, const memory::Object & right) {
    memory::Set ret = Set::clone(left);

    Set::insert(_il(), ret, right);
    return ret;
}

memory::Set SelectiveSelect::objectSet(
  const memory::Object & left, const memory::Set & right) {
    memory::Set ret = Set::clone(right);

    Set::insert(_il(), ret, left);
    return ret;
}

memory::Set SelectiveSelect::setSet(
  const memory::Set & left, const memory::Set & right) {
    memory::Set ret = Set::clone(left);

    Set::insert(_il(), ret, right);
    return ret;
}

}  // namespace icL::service
