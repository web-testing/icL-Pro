#include "root.h++"

#include <icL-types/replaces/ic-math.h++>



namespace icL::service {

Root::Root() = default;

int Root::voidInt(int right) {
    return int(icSqrt(right));
}

double Root::voidDouble(double right) {
    return icSqrt(right);
}

int Root::intInt(int left, int right) {
    return int(icPow(1.0 / double(left), right));
}

double Root::intDouble(int left, double right) {
    return icPow(1.0 / double(left), right);
}

double Root::doubleDouble(double left, double right) {
    return icPow(1.0 / left, right);
}

}  // namespace icL::service
