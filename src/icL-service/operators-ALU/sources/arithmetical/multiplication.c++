#include "multiplication.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-service-main/values/set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::service {

Multiplication::Multiplication() = default;

int Multiplication::intInt(int left, int right) {
    return left * right;
}

double Multiplication::doubleDouble(double left, double right) {
    return left * right;
}

bool Multiplication::stringString(
  const icString & left, const icString & right) {
    icRegEx regex = icRegEx::fromWildcard(right);

    auto match = regex.match(left);
    return match.hasMatch();
}

bool Multiplication::listString(
  const icStringList & left, const icString & right) {
    bool ret = true;

    for (auto & str : left) {
        ret = ret && stringString(str, right);
    }

    return ret;
}

bool Multiplication::listList(
  const icStringList & left, const icStringList & right) {
    bool ret = true;

    for (int i = 0; i < left.length(); i++) {
        ret = ret && stringString(left[i], right[i % right.length()]);
    }

    return ret;
}

memory::Set Multiplication::setSet(
  const memory::Set & left, const memory::Set & right) {
    return service::Set::intersection(_il(), left, right);
}

}  // namespace icL::service
