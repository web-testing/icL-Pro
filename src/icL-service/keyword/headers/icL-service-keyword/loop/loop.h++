#ifndef service_Loop
#define service_Loop

namespace icL::service {

struct Loop
{
    virtual ~Loop() = default;

    virtual void break_()    = 0;
    virtual void continue_() = 0;
};

}  // namespace icL::service

#endif  // service_Loop
