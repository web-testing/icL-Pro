#ifndef service_For
#define service_For

#include "../main/finite-state-machine.h++"
#include "loop.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/structures/code-fragment.h++>

#include <icL-service-main/values/inode.h++>



namespace icL {

namespace il {
struct InterLevel;
}

namespace service {

class Iterator;

class For
    : public FiniteStateMachine
    , public Loop
    , virtual public INode
{
protected:
    /// body is the loop body
    il::CodeFragment body;
    /// maxX is the value of `:maxX` modifier
    int maxX = -1;
    /// number of executed loops
    int executed = 0;
    /// \brief retValue is the console value of last executed loop
    icVariant retValue;
};

}  // namespace service
}  // namespace icL

#endif  // service_For
