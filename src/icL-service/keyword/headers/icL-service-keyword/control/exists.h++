#ifndef service_Exists
#define service_Exists

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/structures/code-fragment.h++>

#include <icL-service-keyword/main/finite-state-machine.h++>
#include <icL-service-main/values/inode.h++>

#include <icL-ce-base/main/ce.h++>  // It's magic



namespace icL::service {

class Exists
    : public FiniteStateMachine
    , public virtual INode
{
private:
    void checkByDefaultCondition();

    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    enum class State {    ///< State of exists FSM
        Initial,          ///< Initial state
        Inited,           ///< valueCode and conditionCode are inited
        ValueCalculed,    ///< value has a assigned value
        ConditionChecked  ///< condition was checked
    } current = State::Initial;

    // padding
    int : 32;

    /// \brief value is the value of exists expression
    icVariant value;

    /// \brief valueCode is the code which must generate a value
    il::CodeFragment valueCode;

    /// \brief conditionCode is the code on condition expression
    il::CodeFragment conditionCode;
};

}  // namespace icL::service

#endif  // service_Exists
