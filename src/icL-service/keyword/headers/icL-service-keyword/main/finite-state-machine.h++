#ifndef service_FiniteStateMachine
#define service_FiniteStateMachine

#include <icL-il/structures/steptype.h++>



class icString;
using icL::il::StepType::StepType;

namespace icL::service {

class FiniteStateMachine
{
public:
    virtual ~FiniteStateMachine() = default;

    /**
     * @brief initialize initializes the finite-state machine
     */
    virtual void initialize() = 0;

    /**
     * @brief transact make a state transation
     */
    virtual StepType transact() = 0;

    /**
     * @brief finalize finalizes the work of finite-state machine
     */
    virtual void finalize() = 0;
};

}  // namespace icL::service

#endif  // service_FiniteStateMachine
