#ifndef service_keyword_global
#define service_keyword_global

#include <QtCore/qglobal.h++>

#if defined(SERVICE_KEYWORD)
#define SERVICE_KEYWORD_EXPORT Q_DECL_EXPORT
#else
#define SERVICE_KEYWORD_EXPORT Q_DECL_IMPORT
#endif

#endif  // service_keyword_global
