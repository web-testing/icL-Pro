#ifndef service_ElementIterator
#define service_ElementIterator

#include "iterator.h++"

#include <icL-il/structures/element.h++>



namespace icL {

namespace il {
struct InterLevel;
}

namespace service {

/**
 * @brief The ElementIterator class describes an iterator for `element` value
 */
class ElementIterator : public Iterator
{
public:
    /**
     * @brief ElementIterator
     * @param element
     */
    ElementIterator(il::Elements element, il::InterLevel * il);

private:
    /**
     * @brief init initializes the class icObject
     */
    void init();

    // Iterator interface
public:
    void toFirst() override;
    void toLast() override;
    void toNext() override;
    void toPrev() override;
    bool atBegin() override;
    bool atEnd() override;
    bool atStop() override;
    void setStopToCurrent() override;
    void setEndToCurrent() override;

    icVariant getCurrent() override;
    int       getSize() override;

private:
    /// pointer to interlevel interface
    il::InterLevel * il;

    /// element is the collection to iterate
    il::Elements element;

    /// current is the index of current item
    int current = 0;
    /// stop is the index of stop item
    int stop = 0;
    /// count is the count of elements in collection
    int count{};
    /// end is the programmable end element
    int end{};
};

}  // namespace service
}  // namespace icL

#endif  // service_ElementIterator
