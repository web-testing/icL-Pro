#ifndef service_SetIterator
#define service_SetIterator

#include "iterator.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-memory/structures/set.h++>



namespace icL::service {

/**
 * @brief The SetIterator class describes a itertor for a icSet
 */
class SetIterator : public Iterator
{
public:
    /**
     * @brief SetIterator builds a new icSet iterator
     * @param icSet
     */
    SetIterator(const memory::Set & set);

private:
    /**
     * @brief init initializes the class icObject
     */
    void init();

    // Iterator interface
public:
    void toFirst() override;
    void toLast() override;
    void toNext() override;
    void toPrev() override;
    bool atBegin() override;
    bool atEnd() override;
    bool atStop() override;
    void setStopToCurrent() override;
    void setEndToCurrent() override;

    icVariant getCurrent() override;
    int       getSize() override;

private:
    /// is the icSet to iterate
    memory::Set set;
    /// current is the icList active iterator
    memory::SetData::Iterator current;
    /// stop is the icList stop iterator
    memory::SetData::Iterator stop;
    /// end is the programmable end of collection
    memory::SetData::Iterator end;
};

}  // namespace icL::service

#endif  // service_SetIterator
