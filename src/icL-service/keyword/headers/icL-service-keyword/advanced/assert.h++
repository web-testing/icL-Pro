#ifndef service_Assert
#define service_Assert

#include "../main/finite-state-machine.h++"

#include <icL-il/structures/code-fragment.h++>

#include <icL-service-main/values/inode.h++>



namespace icL::service {

class Assert
    : public FiniteStateMachine
    , virtual public INode
{
public:
    // FiniteStateMachine interface
public:
    StepType transact() override;

    enum class State {      ///< Finite state machine for `assert`
        Initial,            ///< Initial state of FMS
        ConditionChecking,  ///< Checking assert condition
        ValueExtracting,    ///< extract value of assert string
        End                 ///< The FMS ends here
    } current = State::Initial;

    // padding
    int : 32;

    il::CodeFragment condition;
    il::CodeFragment message;
};

}  // namespace icL::service

#endif  // service_Assert
