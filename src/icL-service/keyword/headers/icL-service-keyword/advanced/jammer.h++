#ifndef service_Jammer
#define service_Jammer

#include "../main/finite-state-machine.h++"

#include <icL-il/structures/code-fragment.h++>

#include <icL-service-main/values/inode.h++>

#include <icL-memory/state/signals.h++>



namespace icL::service {

class Jammer
    : public FiniteStateMachine
    , virtual public INode
{
public:
    // FiniteStateMachine interface
public:
    StepType transact() override;

protected:
    enum class State {  ///< Finite state machine for `jammer`
        Initial,        ///< Initial state of FMS
        Running,        ///< Time to run the code
        End             ///< The FMS ends here
    } current = State::Initial;

    /// \brief errorCode is the code of catched error
    int errorCode = memory::Signals::NoError;

    /// `jammer {<here>}`
    il::CodeFragment code;

    /// `jammer-system` state
    bool catchSystem = false;
};

}  // namespace icL::service

#endif  // service_Jammer
