#include "for-ever.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::service {

using memory::Signals::Signals;

ForEver::ForEver(int xTimes)
    : xTimes(xTimes) {}

void ForEver::break_() {
    current = State::End;
}

void ForEver::continue_() {
    current = State::ConditionChecking;
}

StepType ForEver::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        _il()->vms->pushKeepAliveLayer(il::LayerType::Loop);
        current = State::ConditionChecking;
        break;

    case State::ConditionChecking:
        if (condition.source == nullptr) {
            current = State::CodeExecution;
        }
        else {
            memory::FunctionCall fcall;

            fcall.code        = condition;
            fcall.createLayer = false;
            fcall.contextName = Stringify::alternative(fcall.code.name, "for");
            fcall.args.append({"@", executed});

            _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
                if (ret.signal.code != Signals::NoError) {
                    _il()->vm->signal(ret.signal);
                }
                else if (ret.consoleValue.type() != icType::Bool) {
                    _il()->vm->signal(
                      {Signals::System,
                       "For: condition must return a bool value"});
                }
                else {
                    if (ret.consoleValue.toBool()) {
                        current = State::CodeExecution;
                    }
                    else {
                        current = State::End;
                    }
                }
                return false;
            });
            retValue = executed;
        }
        break;

    case State::CodeExecution: {
        memory::FunctionCall fcall;

        if (xTimes > 0 && executed >= xTimes) {
            current = State::End;
            break;
        }

        fcall.code        = body;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "for");
        fcall.args.append({"@", executed});

        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                _il()->vm->signal(ret.signal);
            }
            else {
                current = State::ConditionChecking;
            }
            return false;
        });

        retValue = ++executed;
        ret      = StepType::CommandIn;
        break;
    }
    case State::End:
        finalize();
        _il()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

}  // namespace icL::service
