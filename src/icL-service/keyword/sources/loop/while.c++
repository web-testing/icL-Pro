#include "while.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::service {

using memory::Signals::Signals;

void While::break_() {
    current = State::End;
}

void While::continue_() {
    current = State::CondtitionChecking;
}

StepType While::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        _il()->vms->pushKeepAliveLayer(il::LayerType::Loop);
        initialize();
        current =
          doWhile || min > 0 ? State::CodeExecution : State::CondtitionChecking;
        if (max == -1) {
            max = 100;
        }
        break;

    case State::CondtitionChecking: {
        memory::FunctionCall fcall;

        if (executed < max) {
            current = State::End;
        }

        fcall.code        = condition;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "while");
        fcall.args.append({"@", executed <= min});

        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                _il()->vm->signal(ret.signal);
            }
            else if (ret.consoleValue.type() != icType::Bool) {
                _il()->vm->signal(
                  {Signals::System,
                   "While: condition must return a bool value"});
            }
            else {
                current = ret.consoleValue.toBool() != notMod
                            ? State::CodeExecution
                            : State::End;
            }
            return false;
        });
        ret = StepType::CommandIn;
        break;
    }
    case State::CodeExecution: {
        memory::FunctionCall fcall;

        fcall.code        = loopBody;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "while");

        _il()->vms->interrupt(fcall, [this](const il::Return & ret) {
            if (ret.signal.code != Signals::NoError) {
                _il()->vm->signal(ret.signal);
            }
            else {
                current = State::CondtitionChecking;
            }
            return false;
        });

        executed++;
        ret = StepType::CommandIn;
        break;
    }
    case State::End:
        _il()->vms->popKeepAliveLayer();
        finalize();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

}  // namespace icL::service
