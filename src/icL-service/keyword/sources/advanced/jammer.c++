#include "jammer.h++"

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>



namespace icL::service {

StepType Jammer::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        current = State::Running;
        break;

    case State::Running: {
        memory::FunctionCall fcall;

        fcall.code        = code;
        fcall.contextName = Stringify::alternative(fcall.code.name, "jammer");

        _il()->vms->interrupt(fcall, [this](const il::Return & result) {
            if (result.signal.code == memory::Signals::System && !catchSystem) {
                _il()->vm->signal(result.signal);
            }
            else {
                errorCode = result.signal.code;
            }
            return false;
        });

        current = State::End;
        ret     = StepType::CommandIn;
        break;
    }

    case State::End:
        finalize();
        ret = StepType::CommandEnd;
        break;
    }

    return ret;
}

}  // namespace icL::service
