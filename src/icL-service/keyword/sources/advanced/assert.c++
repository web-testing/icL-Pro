#include "assert.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/function-call.h++>


namespace icL::service {

StepType icL::service::Assert::transact() {
    StepType ret = StepType::MiniStep;

    switch (current) {
    case State::Initial:
        initialize();
        _il()->vms->pushKeepAliveLayer(il::LayerType::Control);
        current = State::ConditionChecking;
        break;

    case State::ConditionChecking: {
        memory::FunctionCall fcall;

        fcall.code        = condition;
        fcall.createLayer = false;
        fcall.contextName = Stringify::alternative(fcall.code.name, "assert");

        _il()->vms->interrupt(fcall, [this](const il::Return result) {
            if (result.signal.code != memory::Signals::NoError) {
                _il()->vm->signal(result.signal);
            }
            else if (result.consoleValue.isBool()) {
                current = result.consoleValue.toBool() ? State::End
                                                       : State::ValueExtracting;
            }
            else {
                _il()->vm->syssig("assert: Condition must return a bool value");
            }
            return false;
        });

        ret = StepType::CommandIn;
        break;
    }
    case State::ValueExtracting:
        if (message.source == nullptr) {
            _il()->vm->sendAssert(condition.getCode().replace("\n", " "));
            current = State::End;
        }
        else {
            memory::FunctionCall fcall;

            fcall.code        = message;
            fcall.createLayer = false;
            fcall.contextName =
              Stringify::alternative(fcall.code.name, "assert");

            _il()->vms->interrupt(fcall, [this](const il::Return & result) {
                if (result.signal.code != memory::Signals::NoError) {
                    _il()->vm->signal(result.signal);
                }
                else if (result.consoleValue.isString()) {
                    _il()->vm->sendAssert(result.consoleValue.toString());
                    current = State::End;
                }
                else {
                    _il()->vm->syssig(
                      "assert: message code must return a string value");
                }
                return false;
            });

            ret = StepType::CommandIn;
        }
        break;

    case State::End:
        finalize();
        _il()->vms->popKeepAliveLayer();
        ret = StepType::CommandEnd;
    }

    return ret;
}

}  // namespace icL::service
