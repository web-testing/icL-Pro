#include "set.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-cast/base/void-cast.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>

#include <functional>



// Fix compile error
// uint qhash(const icVariant & var) {
//    uint ret = 0;

//    switch (var.type()) {
//    case icType::Int:
//        ret = icObject(var.toInt());
//        break;

//    case icType::Bool:
//        ret = icObject(var.toUInt());
//        break;

//    case icType::Double:
//        ret = icObject(var.toUInt());
//        break;

//    case icType::String:
//        ret = icObject(var.toString());
//        break;

//    case icType::StringList:
//        ret = icObject(var.toString());
//        break;

//    default:
//        ret = 0xFFFFFFFF;
//    }

//    return ret;
//}

namespace icL::service {

using memory::Type;
using memory::Signals::Signals;

icVariant Set::castStringTo(memory::Type type, const icString & str) {
    icVariant ret;
    bool      ok = false;

    switch (type) {
    case Type::BoolValue:
        if (str == "false") {
            ret = false;
            ok  = true;
        }
        else if (str == "true") {
            ret = true;
            ok  = true;
        }
        break;

    case Type::DatetimeValue:
        ret = icDateTime::fromString(str);
        ok  = ret.toDateTime().isValid();
        break;

    case Type::DoubleValue:
        ret = str.toDouble(ok);
        break;

    case Type::IntValue:
        ret = str.toInt(ok);
        break;

    case Type::ListValue:
        ret = icStringList{str};
        break;

    case Type::StringValue:
        ret = str;
        break;

    default:
        ok = false;
    }

    if (!ok) {
        ret = {};
    }

    return ret;
}

int Set::getFieldIndex(const memory::Set & icSet, const icString & name) {
    auto & header = *icSet.header.get();

    for (int i = 0; i < header.length(); i++) {
        if (header[i].name == name) {
            return i;
        }
    }

    return -1;
}

bool Set::checkObject(
  il::InterLevel * il, const memory::Set & value, const memory::Object & obj) {

    if (value.header->length() != obj.data->countVariables()) {
        il->vm->signal({Signals::IncompatibleData, {}});
        return false;
    }

    for (auto & column : *value.header) {
        if (!obj.data->checkType(column.name, column.type)) {
            il->vm->signal({Signals::IncompatibleData, {}});
            return false;
        }
    }

    return true;
}

icList<int> Set::checkSubObject(
  il::InterLevel * il, const memory::Set & value, const memory::Object & obj) {
    const auto & map = obj.data->getMap();
    icList<int>  ret;

    for (auto it = map.begin(); it != map.end(); it++) {
        // Attention: indexOf will not check the type of column
        int column = value.header->indexOf({Type::VoidValue, it.key()});

        if (
          column == -1 ||
          !obj.data->checkType(it.key(), (*value.header)[column].type)) {
            il->vm->signal({Signals::IncompatibleData, {}});
            return ret;
        }

        ret.append(column);
    }

    return ret;
}

icList<int> Set::checkSet(
  il::InterLevel * il, const memory::Set & s1, const memory::Set & s2) {
    icList<int> ret;

    for (auto & column : *s1.header) {
        int index = getFieldIndex(s2, column.name);

        if (index < 0 && column.type == (*s1.header)[index].type) {
            il->vm->signal({memory::Signals::System, {}});
            break;
        }

        ret.append(index);
    }

    return ret;
}

icVariantList Set::argsToList(const memory::ArgList & args) {
    icVariantList args_values;

    for (auto & arg : args) {
        args_values.append(arg.value);
    }

    return args_values;
}

memory::Set Set::fromObject(const memory::Object & obj) {
    memory::Set ret = service::VoidCast::toSet();

    auto & dataMap = obj.data->getMap();
    auto   objItem = icVariantList{};

    for (auto it = dataMap.begin(); it != dataMap.end(); it++) {
        ret.header->append({obj.data->getType(it.key()), it.key()});
        objItem.append(it.value());
    }

    ret.setData->insert(objItem);

    return ret;
}

bool Set::isFullMatch(const icList<int> & indices) {
    bool matches = true;

    for (int i = 0; i < indices.length(); i++) {
        matches = matches && i == indices[i];
    }

    return matches;
}

void Set::forEach(
  const memory::Set &                                 icSet,
  const std::function<void(const memory::Object &)> & run) {
    memory::Object obj = VoidCast::toObject();

    for (auto & varList : *icSet.setData) {
        for (int i = 0; i < icSet.header->length(); i++) {
            auto & column = (*icSet.header)[i];

            obj.data->setValue(column.name, varList[i]);
        }

        run(obj);
    }
}

icVariantList Set::objectToList(
  const memory::Set & s, const memory::Object & obj) {
    icVariantList data;

    for (auto & column : *s.header) {
        data.append(obj.data->getValue(column.name));
    }

    return data;
}

memory::Object Set::rawToObject(
  const memory::Set & icSet, const icVariantList & raw) {
    memory::Object obj = VoidCast::toObject();

    for (int i = 0; i < icSet.header->length(); i++) {
        auto & column = (*icSet.header)[i];

        obj.data->setValue(column.name, raw[i]);
    }

    return obj;
}

void Set::applicate(
  il::InterLevel * il, const memory::Set & value,
  const icList<icStringList> & data) {

    if (data.length() != value.header->length()) {
        il->vm->signal({Signals::IncompatibleData, {}});
        return;
    }

    int size = 0;

    for (auto & icList : data) {
        if (icList.length() > size) {
            size = icList.length();
        }
    }

    for (int i = 0; i < size; i++) {
        icVariantList objdata;

        auto & columns = *value.header;

        for (int j = 0; j < columns.length(); j++) {
            icVariant cell =
              castStringTo(columns[j].type, data[j][i % data[j].length()]);

            if (cell.isNull()) {
                il->vm->signal({Signals::InvalidArgument, {}});
                return;
            }

            objdata.append(cell);
        }

        insert(il, value, objdata);
    }
}

memory::Set Set::clone(const memory::Set & value) {
    memory::Set clone;


    clone.header  = std::make_shared<memory::Columns>(*value.header);
    clone.setData = std::make_shared<memory::SetData>(*value.setData);

    return clone;
}

icStringList Set::getField(
  il::InterLevel * il, const memory::Set & value, const icString & name) {
    icStringList ret;

    int index = getFieldIndex(value, name);

    if (index < 0) {
        il->vm->signal({Signals::FieldNotFound, {}});
        return {};
    }

    for (auto && row : static_cast<const memory::SetData &>(*value.setData)) {
        ret.append(row[index].toString());
    }

    return ret;
}

void Set::insert(
  il::InterLevel * il, const memory::Set & value, const icVariantList & data) {
    if (data.length() != value.header->length()) {
        il->vm->signal({Signals::IncompatibleData, {}});
        return;
    }

    for (int i = 0; i < data.length(); i++) {
        if (memory::variantToType(data[i]) != value.header->at(i).type) {
            il->vm->signal({Signals::IncompatibleData, {}});
            return;
        }
    }

    value.setData->insert(data);
}

void Set::insert(
  il::InterLevel * il, const memory::Set & value, const memory::Object & obj) {
    if (!checkObject(il, value, obj)) {
        return;
    }

    value.setData->insert(objectToList(value, obj));
}

void Set::insert(
  il::InterLevel * il, const memory::Set & where, const memory::Set & what) {
    icList<int> indices = checkSet(il, where, what);

    if (indices.length() != where.header->length()) {
        return;
    }

    for (auto & row : *what.setData) {
        icVariantList newRow;

        for (int i = 0; i < row.length(); i++) {
            newRow.append(row[indices[i]]);
        }

        where.setData->insert(newRow);
    }
}

memory::Set Set::insertField(
  il::InterLevel * il, const memory::Set & value, const icString & name,
  const icStringList & values, memory::Type type) {
    if (getFieldIndex(value, name) >= 0) {
        il->vm->signal({Signals::FieldAlreadyExists, {}});
        return {};
    }

    icVariantList valuesAsVar;

    for (const auto & str : values) {
        icVariant var = castStringTo(type, str);

        if (var.isNull()) {
            il->vm->signal({Signals::IncompatibleData, {}});
            return {};
        }
    }

    memory::Set newSet;

    newSet.header  = std::make_shared<memory::Columns>(*value.header);
    newSet.setData = std::make_shared<memory::SetData>(memory::SetData());

    newSet.header->append({type, name});

    int i = 0;

    for (auto && row : static_cast<const memory::SetData &>(*value.setData)) {
        icVariantList rowCopy = row;

        rowCopy.append(valuesAsVar[i++ % valuesAsVar.length()]);
        newSet.setData->insert(rowCopy);
    }

    return newSet;
}

memory::Set Set::insertField(
  il::InterLevel * il, const memory::Set & value, const icString & name,
  const icVariant & var, memory::Type type) {
    if (getFieldIndex(value, name) >= 0) {
        il->vm->signal({Signals::FieldAlreadyExists, {}});
        return {};
    }

    if (memory::variantToType(var) != type) {
        il->vm->signal({Signals::IncompatibleData, {}});
        return {};
    }

    memory::Set newSet;

    newSet.header  = std::make_shared<memory::Columns>(*value.header);
    newSet.setData = std::make_shared<memory::SetData>(memory::SetData());

    newSet.header->append({type, name});

    for (auto && row : static_cast<const memory::SetData &>(*value.setData)) {
        icVariantList rowCopy = row;

        rowCopy.append(var);
        newSet.setData->insert(rowCopy);
    }

    return newSet;
}

void Set::remove(
  il::InterLevel * il, const memory::Set & value, const memory::Object & obj) {
    if (!checkObject(il, value, obj)) {
        return;
    }

    value.setData->remove(objectToList(value, obj));
}

void Set::remove(
  il::InterLevel * il, const memory::Set & from, const memory::Set & what) {
    icList<int> indices = checkSet(il, from, what);

    if (indices.length() != from.header->length()) {
        return;
    }

    for (auto & row : *what.setData) {
        icVariantList newRow;

        for (int i = 0; i < row.length(); i++) {
            newRow.append(row[indices[i]]);
        }

        from.setData->remove(newRow);
    }
}

void Set::remove(const memory::Set & value, const icVariantList & data) {
    value.setData->remove(data);
}

memory::Set Set::removeField(
  il::InterLevel * il, const memory::Set & value, const icString & name) {
    int index = getFieldIndex(value, name);

    if (index < 0) {
        il->vm->signal({Signals::FieldNotFound, {}});
        return {};
    }

    memory::Set newSet;

    newSet.header  = std::make_shared<memory::Columns>(*value.header);
    newSet.setData = std::make_shared<memory::SetData>(memory::SetData());

    newSet.header->removeAt(index);

    for (auto && row : static_cast<const memory::SetData &>(*value.setData)) {
        icVariantList rowCopy = row;

        rowCopy.removeAt(index);
        newSet.setData->insert(rowCopy);
    }

    return newSet;
}

memory::Set Set::unite(
  il::InterLevel * il, const memory::Set & s1, const memory::Set & s2) {
    memory::Set ret     = s1;
    icList<int> indices = checkSet(il, s1, s2);

    if (indices.length() != s1.header->length()) {
        return ret;
    }

    if (isFullMatch(indices)) {
        ret.setData =
          std::make_shared<memory::SetData>(*s1.setData + *s2.setData);
    }
    else {
        ret = clone(s1);
        insert(il, ret, s2);
    }

    return ret;
}

memory::Set Set::complement(
  il::InterLevel * il, const memory::Set & s1, const memory::Set & s2) {
    memory::Set ret     = s1;
    icList<int> indices = checkSet(il, s1, s2);

    if (indices.length() != s1.header->length()) {
        return ret;
    }

    if (isFullMatch(indices)) {
        ret.setData =
          std::make_shared<memory::SetData>(*s1.setData - *s2.setData);
    }
    else {
        ret = clone(s1);
        remove(il, ret, s2);
    }

    return ret;
}

memory::Set Set::symmetricDifference(
  il::InterLevel * il, const memory::Set & s1, const memory::Set & s2) {
    // A - B = (A \ B) + (B \ A)
    return unite(il, complement(il, s1, s2), complement(il, s2, s1));
}

memory::Set Set::intersection(
  il::InterLevel * il, const memory::Set & s1, const memory::Set & s2) {
    memory::Set ret     = s1;
    icList<int> indices = checkSet(il, s1, s2);

    if (indices.length() != s1.header->length()) {
        return ret;
    }

    if (isFullMatch(indices)) {
        ret.setData =
          std::make_shared<memory::SetData>(*s1.setData * *s2.setData);
    }
    else {
        // A * B = (A + B) \ (A - B)
        ret =
          complement(il, unite(il, s1, s2), symmetricDifference(il, s1, s2));
    }

    return ret;
}

bool Set::intersects(
  il::InterLevel * il, const memory::Set & s1, const memory::Set & s2) {
    icList<int> indices = checkSet(il, s1, s2);

    if (indices.length() != s1.header->length()) {
        return false;
    }

    if (isFullMatch(indices)) {
        return (*s1.setData).intersects(*s2.setData);
    }

    return !intersection(il, s1, s2).setData->empty();
}

bool Set::contains(
  il::InterLevel * il, const memory::Set & s1, const memory::Set & s2) {
    icList<int> indices = checkSet(il, s1, s2);

    if (indices.length() != s1.header->length()) {
        return false;
    }

    if (isFullMatch(indices)) {
        return (*s1.setData).contains(*s2.setData);
    }

    return intersection(il, s1, s2).setData->size() == s2.setData->size();
}

bool Set::contains(
  il::InterLevel * il, const memory::Set & s1, const memory::Object & o1) {
    if (!checkObject(il, s1, o1)) {
        return false;
    }

    return s1.setData->contains(objectToList(s1, o1));
}

bool Set::containsTemplate(
  il::InterLevel * il, const memory::Set & s1, const memory::Object & o1) {
    bool ret = false;

    icList<int> indices = checkSubObject(il, s1, o1);

    if (indices.length() != o1.data->countVariables()) {
        return ret;
    }

    auto & o1map = o1.data->getMap();

    forEach(s1, [&ret, &o1map](const memory::Object & obj) {
        bool match = true;

        for (auto it = o1map.begin(); it != o1map.end(); it++) {
            match = match && obj.data->getValue(it.key()) == it.value();
        }

        ret = ret || match;
    });

    return ret;
}

bool Set::containsTemplate(
  il::InterLevel * il, const memory::Object & obj,
  const memory::Object & subobj) {

    auto & subobjMap = subobj.data->getMap();
    bool   ret       = true;

    for (auto it = subobjMap.begin(); it != subobjMap.end(); it++) {
        if (obj.data->contains(it.key())) {
            if (obj.data->checkType(it.key(), subobj.data->getType(it.key()))) {
                ret = ret && obj.data->getValue(it.key()) == it.value();
            }
            else {
                il->vm->signal({memory::Signals::IncompatibleData, ""});
            }
        }
        else {
            il->vm->signal({memory::Signals::IncompatibleData, ""});
        }
    }

    return ret;
}

bool Set::compare(
  il::InterLevel * il, const memory::Set & s1, const memory::Set & s2) {
    icList<int> indices = checkSet(il, s1, s2);

    if (indices.length() != s1.header->length()) {
        return false;
    }

    if (isFullMatch(indices)) {
        return *s1.setData == *s2.setData;
    }

    int size = intersection(il, s1, s2).setData->size();

    return size == s1.setData->size() && size == s2.setData->size();
}

}  // namespace icL::service
