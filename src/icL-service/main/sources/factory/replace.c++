#include "replace.h++"

#include <icL-ce-base/main/ce.h++>



namespace icL::service {

void Replace::linkAfter(ce::CE * item, ce::CE * toLink) {
    toLink->m_next = nullptr;
    toLink->m_prev = item;
    item->m_next   = toLink;
}

void Replace::replace(ce::CE * before, ce::CE * after) {
    after->m_prev = before->m_prev;
    after->m_next = before->m_next;

    if (before->m_prev != nullptr) {
        before->m_prev->m_next = after;
    }
    if (before->m_next != nullptr) {
        before->m_next->m_prev = after;
    }

    delete before;
}

void Replace::replace(
  ce::CE * beforeBegin, ce::CE * beforeEnd, ce::CE * after) {
    if (beforeBegin->m_prev != nullptr) {
        beforeBegin->m_prev->m_next = after;
    }

    if (beforeEnd->m_next != nullptr) {
        beforeEnd->m_next->m_prev = after;
    }

    after->m_next = beforeEnd->m_next;
    after->m_prev = beforeBegin->m_prev;

    beforeEnd->m_next = nullptr;

    while (beforeBegin != nullptr) {
        auto * next = beforeBegin->m_next;

        delete beforeBegin;
        beforeBegin = next;
    }
}


}  // namespace icL::service
