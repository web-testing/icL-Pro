#include "factory.h++"

#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/file-target.h++>

#include <icL-ce-base/value/packed-value.h++>
#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/double-value.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/base/string-value.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-base/browser/element-value.h++>
#include <icL-ce-value-base/browser/elements-value.h++>
#include <icL-ce-value-base/browser/session-value.h++>
#include <icL-ce-value-base/browser/tab-value.h++>
#include <icL-ce-value-base/browser/window-value.h++>
#include <icL-ce-value-base/complex/datetime-value.h++>
#include <icL-ce-value-base/complex/list-value.h++>
#include <icL-ce-value-base/complex/object-value.h++>
#include <icL-ce-value-base/complex/regex-value.h++>
#include <icL-ce-value-base/complex/set-value.h++>
#include <icL-ce-value-base/system/db-value.h++>
#include <icL-ce-value-base/system/file-value.h++>
#include <icL-ce-value-base/system/query-value.h++>

#include <icL-memory/state/datacontainer.h++>
#include <icL-memory/structures/set.h++>



namespace icL::service {

ce::CE * Factory::fromValue(il::InterLevel * il, const icVariant & value) {
    ce::CE * ret = nullptr;

    switch (value.type()) {
    case icType::Bool:
        ret = new ce::BoolValue{il, value};
        break;

    case icType::Int:
        ret = new ce::IntValue{il, value};
        break;

    case icType::Double:
        ret = new ce::DoubleValue{il, value};
        break;

    case icType::String:
        ret = new ce::StringValue{il, value};
        break;

    case icType::StringList:
        ret = new ce::ListValue{il, value};
        break;

    case icType::RegEx:
        ret = new ce::RegexValue{il, value};
        break;

    case icType::DateTime:
        ret = new ce::DatetimeValue{il, value};
        break;

    case icType::Element:
        ret = new ce::ElementValue{il, value};
        break;

    case icType::Elements:
        ret = new ce::ElementsValue(il, value);
        break;

    case icType::Set:
        ret = new ce::SetValue{il, value};
        break;

    case icType::Object:
        ret = new ce::ObjectValue{il, value};
        break;

    case icType::Session:
        ret = new ce::SessionValue{il, value};
        break;

    case icType::Tab:
        ret = new ce::TabValue{il, value};
        break;

    case icType::Window:
        ret = new ce::WindowValue{il, value};
        break;

    case icType::DB:
        ret = new ce::DBValue{il, value};
        break;

    case icType::File:
        ret = new ce::FileValue{il, value};
        break;

    case icType::Query:
        ret = new ce::QueryValue{il, value};
        break;

    case icType::Packed:
        ret = new ce::PackedValue{il, value};
        break;

    case icType::Initial:
        ret = new ce::PackedValue{il, memory::PackedValue{}};
        break;

    default:
        ret = new ce::VoidValue{il, value};
        break;
    }

    return ret;
}

ce::CE * Factory::fromValue(
  il::InterLevel * il, const icString & getter, const icString & setter) {
    icVariant value = il->server->executeSync(getter, {});
    ce::CE *  ret   = nullptr;

    switch (value.type()) {
    case icType::Bool:
        ret = new ce::BoolValue{il, getter, setter};
        break;

    case icType::Int:
        ret = new ce::IntValue{il, getter, setter};
        break;

    case icType::Double:
        ret = new ce::DoubleValue{il, getter, setter};
        break;

    case icType::String:
        ret = new ce::StringValue{il, getter, setter};
        break;

    case icType::StringList:
        ret = new ce::ListValue{il, getter, setter};
        break;

    case icType::RegEx:
        ret = new ce::RegexValue{il, getter, setter};
        break;

    case icType::DateTime:
        ret = new ce::DatetimeValue{il, getter, setter};
        break;

    case icType::Element:
        ret = new ce::ElementValue{il, getter, setter};
        break;

    case icType::Elements:
        ret = new ce::ElementsValue{il, getter, setter};
        break;

    case icType::Set:
        ret = new ce::SetValue{il, getter, setter};
        break;

    case icType::Object:
        ret = new ce::ObjectValue{il, getter, setter};
        break;

    default:
        ret = new ce::VoidValue{il, value};
        break;
    }

    return ret;
}

ce::CE * Factory::fromValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & var) {
    ce::CE * ret = nullptr;

    switch (container->getType(var)) {
    case memory::Type::BoolValue:
        ret = new ce::BoolValue{il, container, var};
        break;

    case memory::Type::IntValue:
        ret = new ce::IntValue{il, container, var};
        break;

    case memory::Type::DoubleValue:
        ret = new ce::DoubleValue{il, container, var};
        break;

    case memory::Type::StringValue:
        ret = new ce::StringValue{il, container, var};
        break;

    case memory::Type::ListValue:
        ret = new ce::ListValue{il, container, var};
        break;

    case memory::Type::RegexValue:
        ret = new ce::RegexValue{il, container, var};
        break;

    case memory::Type::DatetimeValue:
        ret = new ce::DatetimeValue{il, container, var};
        break;

    case memory::Type::ElementValue:
        ret = new ce::ElementValue{il, container, var};
        break;

    case memory::Type::ElementsValue:
        ret = new ce::ElementsValue{il, container, var};
        break;

    case memory::Type::SetValue:
        ret = new ce::SetValue{il, container, var};
        break;

    case memory::Type::ObjectValue:
        ret = new ce::ObjectValue{il, container, var};
        break;

    case memory::Type::SessionValue:
        ret = new ce::SessionValue{il, container, var};
        break;

    case memory::Type::TabValue:
        ret = new ce::TabValue{il, container, var};
        break;

    case memory::Type::WindowValue:
        ret = new ce::WindowValue{il, container, var};
        break;

    case memory::Type::DatabaseValue:
        ret = new ce::DBValue{il, container, var};
        break;

    case memory::Type::FileValue:
        ret = new ce::FileValue{il, container, var};
        break;

    case memory::Type::Query:
        ret = new ce::QueryValue{il, container, var};
        break;

    default:
        ret = new ce::VoidValue{il, container, var};
        break;
    }

    return ret;
}

}  // namespace icL::service
