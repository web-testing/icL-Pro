#ifndef service_INode
#define service_INode



namespace icL {

namespace il {
struct InterLevel;
}

namespace service {

/**
 * @brief The INode class represent an interface for a node
 *
 * This interface is special for classes which will become nodes later and the
 * inherance of Node twince will be a serious problem
 */
class INode
{
public:
    virtual ~INode() = default;

protected:
    virtual il::InterLevel * _il() = 0;
};

}  // namespace service
}  // namespace icL

#endif  // service_INode
