#ifndef service_IValue
#define service_IValue



class icVariant;

namespace icL::service {

class IValue
{
public:
    virtual ~IValue() = default;

    /**
     * @brief getValue return the contained value
     * @return the contained value
     */
    virtual icVariant getValue(bool excludeFunctional = false) = 0;

    /**
     * @brief setValue sets the contained value
     * @param value is the value to icSet
     */
    virtual void setValue(
      const icVariant & value, bool excludeFunctional = false) = 0;
};

}  // namespace icL::service

#endif  // service_IValue
