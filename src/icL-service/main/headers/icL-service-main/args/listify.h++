#ifndef service_Listify
#define service_Listify

template <typename>
class icList;
class icVariant;

namespace icL {

namespace memory {
struct Argument;
using ArgList = icList<Argument>;
struct Parameter;
using ParamList = icList<Parameter>;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace ce {
class Value;
}

namespace service {

class Listify
{
public:
    /**
     * @brief argsToList cast each argument to `T`
     * @param args is the icList of afguemnts
     * @return a icList of casted values
     */
    template <typename T>
    static icList<T> fromArgs(const memory::ArgList & args);

    /**
     * @brief toArgList cast a value to a args list
     * @param il is the inter-level node
     * @param value is the value to cast
     * @return an arg icList created from given value
     */
    static memory::ArgList toArgList(il::InterLevel * il, ce::Value * value);

    /**
     * @brief toArgList cast a value to a args list
     * @param il is the inter-level node
     * @param value is the value to cast
     * @return an args list created from given value
     */
    static memory::ArgList toArgList(
      il::InterLevel * il, const icVariant & value);

    /**
     * @brief toParamList casts a value to a params list
     * @param il is the inter-level node
     * @param value is the value to cast
     * @return a params list created from given value
     */
    static memory::ParamList toParamList(
      il::InterLevel * il, const icVariant & value);

    /**
     * @brief isArgList checks if the value can be an arguments list
     * @param value is the value to check
     * @return true if it can be casted to arguments list, otherwise false
     */
    static bool isArgList(ce::Value * value);

    /**
     * @brief isParamList checks if the value can be a parameters list
     * @param value is the value to check in
     * @return true if it can be casted to parameters lists, otherwise false
     */
    static bool isParamList(ce::Value * value);
};

}  // namespace service
}  // namespace icL

#endif  // service_Listify
