#ifndef service_Listify_argsToList
#define service_Listify_argsToList

#include "listify.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::service {

template <typename T>
icList<T> Listify::fromArgs(const memory::ArgList & args) {
    icList<T> ret;

    for (auto & arg : args) {
        ret.append(arg);
    }

    return ret;
}

}  // namespace icL::service

#endif  // service_Listify_argsToList
