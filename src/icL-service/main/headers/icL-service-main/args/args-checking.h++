#ifndef service_ArgsChecking
#define service_ArgsChecking

#include <icL-memory/structures/type.h++>



template <typename>
class icList;

using icL::memory::Type;

namespace icL {

namespace memory {
struct Argument;
using ArgList = icList<Argument>;
}  // namespace memory

namespace service {

class ArgsChecking
{
public:
    /**
     * @brief checkArg check the compability of arg with type
     * @param arg is the argument to check
     * @param type is the type to match
     * @return true if arg martches the type, otherwise false
     */
    static bool checkArg(const memory::Argument & arg, Type type);

    /**
     * @brief checksArg checks the compability of args with standard
     * @param args are the arguments of method call
     * @param standard is the wanted arguments icList
     * @return true if compatible, otherwise false
     */
    static bool checkArgs(
      const memory::ArgList & args, const icList<Type> & standard);

    /**
     * @brief checksArg checks the compability of each arg with type
     * @param args are the arguments of method call
     * @param type is the wanted arguments type
     * @return true if compatible, otherwise false
     */
    static bool checkArgs(Type type, const memory::ArgList & args);

    /**
     * @brief checkArgs check if all aguments are valid
     * @param args are the arguments of method call
     * @return true if valid, otherwise false
     */
    static bool checkArgs(const memory::ArgList & args);
};

}  // namespace service
}  // namespace icL

#endif  // service_ArgsChecking
