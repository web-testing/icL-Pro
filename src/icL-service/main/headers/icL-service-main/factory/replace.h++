#ifndef service_Replace
#define service_Replace



namespace icL {

namespace ce {
class CE;
}

namespace service {

class Replace
{
public:
    /**
     * @brief linkAfter adds `item` after `toLink` in 2-sided linked icList
     * @param item is the item to append
     * @param toLink is the item to append after it
     */
    static void linkAfter(ce::CE * item, ce::CE * toLink);

    /**
     * @brief replace replaces a node in linked icList
     * @param before is the node to be replaced
     * @param after is the node which will replace
     */
    static void replace(ce::CE * before, ce::CE * after);

    /**
     * @brief replace repalces an interval with a node
     * @param beforeBegin is the begin of interval which will be repalced
     * @param beforeEnd is the end of interval which will be repalced
     * @param after is the node which will replace
     */
    static void replace(
      ce::CE * beforeBegin, ce::CE * beforeEnd, ce::CE * after);
};

}  // namespace service
}  // namespace icL

#endif  // service_Replace
