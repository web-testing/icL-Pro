#ifndef service_Factory
#define service_Factory

class icVariant;
class icString;

namespace icL {

namespace memory {
class DataContainer;
}

namespace il {
struct InterLevel;
}

namespace ce {
class CE;
}

namespace service {

class Factory
{
public:
    /**
     * @brief fromValue creates a new CE node from a icVariant
     * @param il is the inter-level node
     * @param value is the value of new CE node
     * @return a pointer to the new created node
     */
    static ce::CE * fromValue(il::InterLevel * il, const icVariant & value);

    /**
     * @brief fromValue creates a new CE node from a [getter,setter] pair
     * @param il is the inter-level node
     * @param getter is the js code to get the value
     * @param setter is the js code to icSet the value
     * @return a pointer to the new created node
     */
    static ce::CE * fromValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /**
     * @brief fromValue creates a new CE node from a container variable
     * @param il is the inter-level node
     * @param container is the container of variable
     * @param var is the name of variable in container
     * @return a pointer to the new created node
     */
    static ce::CE * fromValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & var);
};

}  // namespace service
}  // namespace icL

#endif  // service_Factory
