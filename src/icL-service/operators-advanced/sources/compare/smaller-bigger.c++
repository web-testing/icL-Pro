#include "smaller-bigger.h++"



namespace icL::service {

bool SmallerBigger::intIntInt(int left, int begin, int end) {
    return left < begin || left > end;
}

bool SmallerBigger::doubleDoubleDouble(double left, double begin, double end) {
    return left < begin || left > end;
}

}  // namespace icL::service
