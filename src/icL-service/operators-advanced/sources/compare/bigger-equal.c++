#include "bigger-equal.h++"



namespace icL::service {

bool BiggerEqual::intInt(int left, int right) {
    return left >= right;
}

bool BiggerEqual::doubleDouble(double left, double right) {
    return left >= right;
}

}  // namespace icL::service
