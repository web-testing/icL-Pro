#ifndef il_BiggerSmaller
#define il_BiggerSmaller


namespace icL::service {

class BiggerSmaller
{
public:
    /// `int >< (int, int) : bool`
    bool intIntInt(int left, int begin, int end);

    /// `double >< (double, double) : bool`
    bool doubleDoubleDouble(double left, double begin, double end);
};

}  // namespace icL::service

#endif  // il.BiggerSmaller
