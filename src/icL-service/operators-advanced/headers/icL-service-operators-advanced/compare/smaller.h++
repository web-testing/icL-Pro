#ifndef service_Smaller
#define service_Smaller


namespace icL::service {

class Smaller
{
public:
    /// `int < int : bool`
    bool intInt(int left, int right);

    /// `double < double : bool`
    bool doubleDouble(double left, double right);
};

}  // namespace icL::service

#endif  // service_Smaller
