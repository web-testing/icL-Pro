#ifndef service_SmallerEqual
#define service_SmallerEqual


namespace icL::service {

class SmallerEqual
{
public:
    /// `int <= int : bool`
    bool intInt(int left, int right);

    /// `double <= double : bool`
    bool doubleDouble(double left, double right);
};

}  // namespace icL::service

#endif  // service_SmallerEqual
