#ifndef service_Bigger
#define service_Bigger


namespace icL::service {

class Bigger
{
public:
    /// `int > int : bool`
    bool intInt(int left, int right);

    /// `double > double : bool`
    bool doubleDouble(double left, double right);
};

}  // namespace icL::service

#endif  // service_Bigger
