#ifndef service_BiggerEqual
#define service_BiggerEqual


namespace icL::service {

class BiggerEqual
{
public:
    /// `int >= int : bool`
    bool intInt(int left, int right);

    /// `double >= double : bool`
    bool doubleDouble(double left, double right);
};

}  // namespace icL::service

#endif  // service_BiggerEqual
