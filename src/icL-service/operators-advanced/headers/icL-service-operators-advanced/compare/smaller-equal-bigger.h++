#ifndef service_SmallerEqualBigger
#define service_SmallerEqualBigger


namespace icL::service {

class SmallerEqualBigger
{
public:
    /// `int <=> (int, int) : bool`
    bool intIntInt(int left, int begin, int end);

    /// `double <=> (double, double) : bool`
    bool doubleDoubleDouble(double left, double begin, double end);
};

}  // namespace icL::service

#endif  // service_SmallerEqualBigger
