#ifndef service_BiggerEqualSmaller
#define service_BiggerEqualSmaller


namespace icL::service {

class BiggerEqualSmaller
{
public:
    /// `int >=< (int, int) : bool`
    bool intIntInt(int left, int begin, int end);

    /// `double >=< (double, double) : bool`
    bool doubleDoubleDouble(double left, double begin, double end);
};

}  // namespace icL::service

#endif  // service_BiggerEqualSmaller
