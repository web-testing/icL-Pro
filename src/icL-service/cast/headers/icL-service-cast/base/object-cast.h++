#ifndef service_ObjectCast
#define service_ObjectCast



class icString;
class icStringList;

namespace icL {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class ObjectCast
{
public:
    /// `object : bool`
    static bool toBool(const memory::Object & value);

    /// `object : string`
    static icString toString(const memory::Object & value);
};

}  // namespace service
}  // namespace icL

#endif  // service_ObjectCast
