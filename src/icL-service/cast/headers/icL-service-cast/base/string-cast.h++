#ifndef service_StringCast
#define service_StringCast

#include <icL-types/json/js-object.h++>



class icString;
class icStringList;

namespace jsonxx {
class icObject;
}

namespace icL {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace il {
struct InterLevel;
}

namespace service {

class StringCast
{
public:
    /// `string : bool`
    static bool toBool(const icString & value);

    /// `string : int`
    static int toInt(il::InterLevel * il, const icString & value);

    /// `string : double`
    static double toDouble(il::InterLevel * il, const icString & value);

    /// `string : list`
    static icStringList toList(const icString & value);

    /**
     * @brief toObject creates a icObject from a JSON-icObject
     * @param obj is the icObject to cast to icL icObject
     * @return a icL icObject with the same field as JSON-icObject
     */
    static memory::Object toObject(il::InterLevel * il, jsObject obj);

    /// `string : object`
    static memory::Object toObject(il::InterLevel * il, const icString & value);

    /// `string : set`
    static memory::Set toSet(il::InterLevel * il, const icString & value);
};

}  // namespace service
}  // namespace icL

#endif  // service_StringCast
