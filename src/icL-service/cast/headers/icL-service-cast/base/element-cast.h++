#ifndef service_ElementCast
#define service_ElementCast



class icString;

namespace icL {

namespace il {
struct Element;
struct Elements;
}  // namespace il

namespace service {

class ElementCast
{
private:
    ElementCast() = default;

public:
    /// `element : bool`
    static bool toBool(const icL::il::Element & value);

    /// `element : string`
    static icString toString(const icL::il::Element & value);
};

}  // namespace service
}  // namespace icL

#endif  // service_ElementCast
