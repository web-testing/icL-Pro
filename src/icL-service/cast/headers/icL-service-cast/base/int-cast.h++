#ifndef service_IntCast
#define service_IntCast



class icString;

namespace icL::service {

class IntCast
{
public:
    /// `int : bool`
    static bool toBool(int value);

    /// `int : double`
    static double toDouble(int value);

    /// `int : string`
    static icString toString(int value);
};

}  // namespace icL::service

#endif  // service_IntCast
