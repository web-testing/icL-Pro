#ifndef service_DatetimeCast
#define service_DatetimeCast



class icDateTime;
class icString;

namespace icL::service {

class DatetimeCast
{
public:
    /// `icDateTime : string`
    static icString toString(const icDateTime & value);

    /// `icDateTime : (string, format)`
    static icString toString(const icDateTime & value, const icString & format);

    /// `string : icDateTime`
    static icDateTime toDatetime(const icString & value);

    /// `string : (icDateTime, format)`
    static icDateTime toDatetime(
      const icString & value, const icString & format);
};

}  // namespace icL::service

#endif  // service_DatetimeCast
