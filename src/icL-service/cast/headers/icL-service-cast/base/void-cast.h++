#ifndef service_VoidCast
#define service_VoidCast

#include <icL-memory/structures/type.h++>



class icString;
class icStringList;
class icVariant;

namespace icL {

namespace memory {
struct Object;
struct Set;
}  // namespace memory

namespace il {
struct Element;
}

namespace service {

class VoidCast
{
public:
    /// `void : bool`
    static bool toBool();

    /// `void : int`
    static int toInt();

    /// `void : double`
    static double toDouble();

    /// `void : string`
    static icString toString();

    /// `void : list`
    static icStringList toList();

    /// `void : object`
    static memory::Object toObject();

    /// `void : set`
    static memory::Set toSet();

    /// `void : element`
    static il::Element toElement();

    /// `new var : type`
    static icVariant fromType(memory::Type type);
};

}  // namespace service
}  // namespace icL

#endif  // service_VoidCast
