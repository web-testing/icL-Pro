#include "int-cast.h++"

#include <icL-types/replaces/ic-string.h++>

namespace icL::service {

bool IntCast::toBool(int value) {
    return value != 0;
}

double IntCast::toDouble(int value) {
    return double(value);
}

icString IntCast::toString(int value) {
    return icString::number(value);
}

}  // namespace icL::service
