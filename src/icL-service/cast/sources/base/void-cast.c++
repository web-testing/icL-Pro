#include "void-cast.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/structures/cookie-data.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/file-target.h++>
#include <icL-il/structures/listen-handler.h++>

#include <icL-memory/structures/set.h++>



namespace icL::service {

bool VoidCast::toBool() {
    return false;
}

int VoidCast::toInt() {
    return 0;
}

double VoidCast::toDouble() {
    return 0.0;
}

icString VoidCast::toString() {
    return "";
}

icStringList VoidCast::toList() {
    return {};
}

memory::Object VoidCast::toObject() {
    memory::Object ret;

    ret.data = std::make_shared<memory::DataContainer>();
    return ret;
}

memory::Set VoidCast::toSet() {
    memory::Set ret;

    ret.header  = std::make_shared<memory::Columns>();
    ret.setData = std::make_shared<memory::SetData>();
    return ret;
}

il::Element VoidCast::toElement() {
    return {};
}

icVariant VoidCast::fromType(memory::Type type) {
    switch (type) {
    case memory::Type::BoolValue:
        return false;

    case memory::Type::DatetimeValue:
        return icDateTime::currentDateTime();

    case memory::Type::DoubleValue:
        return 0.0;

    case memory::Type::ElementValue:
        return il::Element{};

    case memory::Type::IntValue:
        return 0;

    case memory::Type::ListValue:
        return icStringList{};

    case memory::Type::ObjectValue:
        return toObject();

    case memory::Type::RegexValue:
        return icRegEx{};

    case memory::Type::SetValue:
        return toSet();

    case memory::Type::StringValue:
        return toString();

    case memory::Type::VoidValue:
        return icVariant::makeVoid();

    case memory::Type::CookieValue:
        return il::Cookie{};

    case memory::Type::SessionValue:
        return il::Session{};

    case memory::Type::TabValue:
        return il::Tab{};

    case memory::Type::WindowValue:
        return il::Window{};

    case memory::Type::DatabaseValue:
        return il::DB{};

    case memory::Type::QueryValue:
        return il::Query{};

    case memory::Type::FileValue:
        return il::File{};

    case memory::Type::ListenerValue:
        return il::Listener{};

    case memory::Type::HandlerValue:
        return il::Handler{};

    default:
        return {};
    }
}


}  // namespace icL::service
