#include "listen-handler.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::il {

Listener::Listener(const icVariant & value) {}

bool Listener::operator==(Listener & other) {}

icL::il::Listener::operator icVariant() const {}

Handler::Handler(const icVariant & value) {}

bool Handler::operator==(Handler & other) {}

icL::il::Handler::operator icVariant() const {}


}  // namespace icL::il
