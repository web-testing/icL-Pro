#include "signal.h++"

#include <utility>

namespace icL::il {

Signal::Signal(int code, icString message)
    : code(code)
    , message(std::move(message)) {}

}  // namespace icL::il
