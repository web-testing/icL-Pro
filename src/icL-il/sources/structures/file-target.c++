#include "file-target.h++"

#include <icL-types/replaces/ic-variant.h++>

namespace icL::il {

File::operator icVariant() const {
    return icVariant::fromValue(*this);
}

File::File(const icVariant & value) {
    *this = value.toFile();
}

bool File::operator==(const File & other) const {
    return target == other.target;
}

JsFile::JsFile(const icVariant & value) {
    *this = value.toJsFile();
}

bool JsFile::operator==(const JsFile & other) const {
    return target == other.target;
}

icL::il::JsFile::operator icVariant() const {
    return icVariant::fromValue(*this);
}

}  // namespace icL::il
