#ifndef il_FileTarget
#define il_FileTarget

#include <icL-types/replaces/ic-string.h++>

#include <memory>

class icVariant;

namespace icL::il {

/**
 * @brief The FileTarget struct describes a file target
 */
struct FileTarget
{
    icString fileId;  ///< file identifier
};

/**
 * @brief The File struct represents a file value
 */
struct File
{
    std::shared_ptr<FileTarget> target;  ///< file target

    File() = default;
    File(const icVariant & value);

    /**
     * @brief operator icVariant casts the file value to icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == check if 2 files has the same target
     * @param other is the file to compare with
     * @return true if they contain the same target, otherwise false
     */
    bool operator==(const File & other) const;
};

/**
 * @brief The ResourceFile struct represents a link to a resource
 */
struct ResourceFile {
    icString fileName; //< file name
};

struct JsFile {
    std::shared_ptr<ResourceFile> target; ///< path target

    JsFile() = default;
    JsFile(const icVariant & value);

    /**
     * @brief operator icVariant casts the js file value to icVariant
     */
    operator icVariant() const;

    /**
     * @brief operator == check if 2 js files has the same target
     * @param other is the file to compare with
     * @return true if they contain the same target, otherwise false
     */
    bool operator==(const JsFile & other) const;
};

}  // namespace icL::il

#endif  // il_FileTarget
