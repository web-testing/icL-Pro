#ifndef il_MouseData
#define il_MouseData

#include "../main/enums.h++"

class icString;

namespace icL {

namespace memory {
struct Object;
enum class Type;
}  // namespace memory

namespace il {

/**
 * @brief The MouseData struct contians mouse event data
 */
struct MouseData
{
    virtual ~MouseData() = default;

    double rx = 0.5;  ///< relative x coordinate
    double ry = 0.5;  ///< relative y coordinate

    int ax = -1;  ///< absolute x coordinate
    int ay = -1;  ///< absolute y coordinate

    int button = MouseButtons::Left;  ///< mouse button

    // padding
    int : 32;

    /**
     * @brief loadDictionary loads data from dictionary
     * @param dic is the dictionary to load data
     */
    virtual void loadDictionary(const memory::Object & dic);

protected:
    /**
     * @brief checkType checks the type of a dictionaty object
     * @param dic is the dictionary to load the field
     * @param name is the name of the field
     * @param type is the expected type
     * @return true if type matches, otherwise false
     */
    static bool checkType(
      const memory::Object & dic, const icString & name, memory::Type type);

    /**
     * @brief checkAndAssign checks the type and assigns on success
     * @param dic is the dictionary to load the field
     * @param name is the name of the field (must be an int)
     * @param field is the variable which will get the value from dictionary
     */
    static void checkAndAssign(
      const memory::Object & dic, const icString & name, int & field);

    /**
     * @brief checkAndAssign checks the type and assigns on success
     * @param dic is the dictionary to load the field
     * @param name is the name of the field (must be a double)
     * @param field is the variable which will get the value from dictionary
     */
    static void checkAndAssign(
      const memory::Object & dic, const icString & name, double & field);
};

/**
 * @brief The ClickData struct contains mouse click data
 */
struct ClickData : public MouseData
{
    int delay = -1;  ///< time interval between mouse down and up events (in ms)
    int count = 1;   ///< the count of clicks

    void loadDictionary(const icL::memory::Object & dic) override;
};

/**
 * @brief The HoverData struct contains mouse move data
 */
struct HoverData : public MouseData
{
    double p1x = -1.0;  ///< P1.x of cubic Bezier function
    double p1y = -1.0;  ///< P1.y of cubic Bezier function
    double p2x = -1.0;  ///< P2.x of cubic Bezier function
    double p2y = -1.0;  ///< P2.y of cubic Bezier function

    int moveTime     = -1;  ///< time of moving by 100px (in ms)
    int moveFunction = 1;   ///< is the function of move path

    void loadDictionary(const memory::Object & dic) override;
};

}  // namespace il
}  // namespace icL

#endif  // il_MouseData
