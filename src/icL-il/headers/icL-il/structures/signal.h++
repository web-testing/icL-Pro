#ifndef il_Signal
#define il_Signal

#include <icL-types/replaces/ic-string.h++>

#include <icL-memory/state/signals.h++>

namespace icL::il {

/**
 * @brief The Signal class is an icL specific signal
 */
class Signal
{
public:
    Signal() = default;

    /**
     * @brief Signal create an inited signal
     * @param code is the code of signal
     * @param message is the message of signal
     */
    Signal(int code, icString message);

    /// \brief code is the integer code of signal
    int code = memory::Signals::NoError;

    /// \brief message is the message of signal
    icString message;

    /// \brief printed elude repeat stack trace
    bool printed = false;
};

}  // namespace icL::il

#endif  // il_Signal
