#ifndef il_CodeFragment
#define il_CodeFragment

#include "position.h++"

#include <icL-types/replaces/ic-string.h++>

#include <memory>



class icString;

namespace icL::il {

class SourceOfCode;

/**
 * @brief The CodeFragment struct is the description of code in {} brackets
 */
struct CodeFragment
{
    /// \brief name is the name of code fragment
    icString name;

    /// \brief begin is the absolute position of first char after }
    Position begin = {0, 0, 1, 1};

    /// \brief end is the absolute position of }
    /// -1 means end of input
    Position end = {-1, -1, -1, -1};

    /// \brief source is the source of code
    std::shared_ptr<SourceOfCode> source;

    icString getCode() const;

    CodeFragment & operator=(const CodeFragment & other);
};

}  // namespace icL::il

#endif  // il_CodeFragment
