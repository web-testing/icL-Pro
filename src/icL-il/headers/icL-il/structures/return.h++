#ifndef il_Return
#define il_Return

#include "signal.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-memory/state/signals.h++>



namespace icL::il {

/**
 * @brief The Return class is a result of code executing
 */
struct Return
{
    Return() = default;

    /// \brief returnValue is the value returned by function
    icVariant returnValue;

    /// \brief consoleValue is the value returned by last line of code
    icVariant consoleValue;

    /// \brief signal is the genereted error
    Signal signal = {memory::Signals::NoError, {}};
};

}  // namespace icL::il

#endif  // il_Return
