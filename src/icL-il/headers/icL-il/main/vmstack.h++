#ifndef il_VMStack
#define il_VMStack

#include "vmlayer.h++"

#include <functional>
#include <memory>

class icString;
class icStringList;

namespace icL {

namespace memory {
struct FunctionCall;
}

namespace il {

struct Return;
class Signal;
struct Position;

enum class SelectionColor {  ///< color for current code highlighting
    Parsing,                 ///< parsed token highlight
    Executing,               ///< executed token highlight
    NewStack,                ///< new stack code highlight
    Destroying,              ///< closed stack highlight
    Error                    ///< code which failed
};

enum class LayerType {  ///< It the type of VMLayer
    Control,            ///< `if` / `else` / `switch` / `case`
    Loop,               ///< `for` / `filter` / `range` / `while`
    Function            ///< is a function call
};

/**
 * @brief The VMStack class represents a stack of virtual machines lnked to
 * different source files and fragments of code
 */
class VMStack
{
public:
    virtual ~VMStack() = default;

    /**
     * @brief interrupt interrups commnad processor to execute a fragment of
     * code
     * @param fcall is call of function with data about the code to execute
     * @param feedback is feedback function to process the result of code
     */
    virtual void interrupt(
      const memory::FunctionCall &        fcall,
      std::function<bool(const Return &)> feedback) = 0;

    /**
     * @brief pushKeepAliveLayer pushes a layer which will be used multiple
     * times
     */
    virtual VMLayer * pushKeepAliveLayer(LayerType type) = 0;

    /**
     * @brief popKeepAliveLayer pops the last keep-alive layer
     */
    virtual void popKeepAliveLayer() = 0;

    /**
     * @brief getRootDir get the rott directory of projects
     * @return the root directory of projects
     */
    virtual icString getRootDir() = 0;

    /**
     * @brief getResourceDir get directory path with resources
     * @return an absolute path to resource directory
     */
    virtual icString getResourceDir(const icString & projectName) = 0;

    /**
     * @brief getLibraryDir get directory path with libraries
     * @param projectName is the name of project to extract lib, use empty
     * string for current project
     * @return an absolute path to libs directory
     */
    virtual icString getLibraryDir(const icString & projectName) = 0;

    /**
     * @brief highlight hightlight a code fragment in text editor
     * @param pos1 is the begin position of hightlight
     * @param pos2 is the end position of hightlight
     */
    virtual void highlight(
      const il::Position & pos1, const il::Position & pos2) = 0;

    /**
     * @brief setSColor sets the hightlight color of hightlighted fragment
     * @param scolor is the needed color
     */
    virtual void setSColor(SelectionColor scolor) = 0;

    /**
     * @brief getCurrentLayer gets the current virtual layer
     * @return a pointer to the current virtual layer
     */
    virtual VMLayer * getCurrentLayer() = 0;

    /**
     * @brief init initializes the VM stack
     * @param path is the path to the project
     */
    virtual void init(const icString & path) = 0;

    /**
     * @brief run runs the code from begin to end
     * @return true if no errors excepted, otherwise false
     */
    virtual bool run() = 0;

    /**
     * @brief debug runs the code in debug mode, pause on breakpoints
     * @return true if no errors excepted, otherwise false
     */
    virtual bool debug() = 0;

    /**
     * @brief stepInto steps into the function
     * @return true if no errors execpted, ohterwise false
     */
    virtual bool stepInto() = 0;

    /**
     * @brief stepOver steps over the current command
     * @return true if no errors execpted, ohterwise false
     */
    virtual bool stepOver() = 0;

    /**
     * @brief stepOut steps out from function body
     * @return true if no errors execpted, ohterwise false
     */
    virtual bool stepOut() = 0;

    /**
     * @brief stop reset the vm stack to initial state
     */
    virtual void stop() = 0;

    /**
     * @brief removeLayers remove the last `count` layers
     * @param last is the last layer to remove !! not null
     */
    virtual void removeLayers(VMLayer * last) = 0;

    /**
     * @brief stackTrace gets the stack trace
     * @return the stack trace
     */
    virtual icStringList stackTrace() = 0;
};


}  // namespace il
}  // namespace icL

#endif  // il_VMStack
