#ifndef il_Listen
#define il_Listen



class icString;
template <typename>
class icList;

namespace icL {

namespace memory {
struct Parameter;
}

namespace il {

struct CodeFragment;
struct Listener;
struct Handler;

/**
 * @brief The Listen class is a server of syncronization with external web
 * applications
 */
class Listen
{
public:
    virtual ~Listen() = default;

    /**
     * @brief listen create a listener
     * @param server is the ip address of server
     * @return a UUID for created listener
     */
    virtual Listener listen(const icString & server) = 0;

    /**
     * @brief handle creates a handler
     * @param listener is the UUID of an existing listener
     * @param params are the list of params for handler
     * @return a UUID for created handler
     */
    virtual Handler handle(
      const Listener & listener, const icList<memory::Parameter> & params) = 0;

    /**
     * @brief setupHandler sets up a handler by code
     * @param handler is the UUID of needed handler
     * @param code is the code fragment to run
     */
    virtual void setupHandler(
      const Handler & handler, const CodeFragment & code) = 0;

    /**
     * @brief activateHandler activates a passive handler
     * @param handler is the UUID of needed handler
     */
    virtual void activateHandler(const Handler & handler) = 0;

    /**
     * @brief deactivateHandler deactivates an active handler
     * @param handler is the UUID of needed handler
     */
    virtual void deactivateHandler(const Handler & handler) = 0;

    /**
     * @brief killHandler kills the handler and the listen too, if this is the
     * last handler
     * @param handler is the UUID of needed handler
     */
    virtual void killHandler(const Handler & handler) = 0;
};

}  // namespace il
}  // namespace icL

#endif  // il_Listen
