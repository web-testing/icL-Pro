#ifndef il_CE
#define il_CE

#include <icL-il/structures/steptype.h++>



using icL::il::StepType::StepType;
class icString;
class icVariant;

namespace icL {

namespace cp {
struct FlyResult;
}

namespace il {

struct CodeFragment;

class CE
{
public:
    virtual ~CE() = default;

    /**
     * @brief checkPrev checks the previous entity for compability
     * @param ce is the contextual entity to check
     * @return true if compatible, otherwise false
     */
    virtual bool checkPrev(CE * ce) = 0;

    /**
     * @brief checkNext checks the next entity for compability
     * @param ce is the contextual entity to check
     * @return true if compatible, otherwise false
     */
    virtual bool checkNext(CE * ce) = 0;

    /**
     * @brief currentRunRank is the current rank of entity in current context
     * @param rtl is true if the check is right-to-left, otherwise false
     * @return the rank of entity for copatible context, or -1
     */
    virtual int currentRunRank(bool rtl) = 0;

    /**
     * @brief runNow runs the entity function right now
     * @return the type of the current executed command
     */
    virtual enum StepType runNow() = 0;

    /**
     * @brief release replace the nodes in linked list
     */
    virtual void release(CE *& lastCE) = 0;

    /**
     * @brief append appends a new node after this
     * @param ce is the node to append
     */
    virtual void linkAfter(CE * ce) = 0;

    /**
     * @brief prev gets a pointer to previous contextual entity
     * @return a pointer to previous contextual entity
     */
    virtual CE * prev() = 0;

    /**
     * @brief next gets a pointer to next contextual entity
     * @return a pointer to next contextual entity
     */
    virtual CE * next() = 0;

    /**
     * @brief fragmentData gets the data of token in code
     * @return the data about position of token in code
     */
    virtual const CodeFragment & fragmentData() = 0;

    /**
     * @brief setFragmentData sets data about positionof token in code
     * @param fragmentData is new data about position in code
     */
    virtual CE * link(
      const cp::FlyResult & token, const CodeFragment & origin) = 0;

    /**
     * @brief hasValue checks if that type of token contains a value
     * @return true if this type of token contains a value, otherwise false
     */
    virtual bool hasValue() = 0;

    /**
     * @brief getValue gets the contextual entity value
     * @return the contextual entity value (void if is not a value type)
     */
    virtual icVariant getValue(bool excludeFunctional = false) = 0;

    /**
     * @brief toString generate a string to create a token like this
     * @return a string to create a token like this
     */
    virtual icString toString() = 0;
};

}  // namespace il
}  // namespace icL

#endif  // il_CE
