#ifndef il_Node
#define il_Node

namespace icL::il {

struct InterLevel;

/**
 * @brief The Node class is a unit of contextual system
 */
class Node
{
public:
    Node(InterLevel * il)
        : il(il) {}

protected:
    /// \brief il is a pointer to inter-level interface
    InterLevel * il;
};

}  // namespace icL::il

#endif  // il_Node
