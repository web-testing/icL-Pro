#include "type.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-variant.h++>



namespace icL::memory {

Type variantToType(const icVariant & var) {
    static const icObject<icType, Type> map{
      {icType::Void, Type::VoidValue},
      {icType::Bool, Type::BoolValue},
      {icType::Int, Type::IntValue},
      {icType::Double, Type::DoubleValue},
      {icType::String, Type::StringValue},
      {icType::Cookie, Type::CookieValue},
      {icType::DB, Type::DatabaseValue},
      {icType::Query, Type::QueryValue},
      {icType::Element, Type::ElementValue},
      {icType::Elements, Type::ElementsValue},
      {icType::Session, Type::SessionValue},
      {icType::Window, Type::WindowValue},
      {icType::Tab, Type::TabValue},
      {icType::DateTime, Type::DatetimeValue},
      {icType::StringList, Type::ListValue},
      {icType::RegEx, Type::RegexValue},
      {icType::Object, Type::ObjectValue},
      {icType::Set, Type::SetValue},
      {icType::Packed, Type::PackedValue},
      {icType::File, Type::FileValue}};

    return map.value(var.type(), Type::VoidValue);
}

icType typeToVarType(const Type type) {
    static const icObject<Type, icType> map{
      {Type::VoidValue, icType::Void},
      {Type::BoolValue, icType::Bool},
      {Type::IntValue, icType::Int},
      {Type::DoubleValue, icType::Double},
      {Type::StringValue, icType::String},
      {Type::CookieValue, icType::Cookie},
      {Type::DatabaseValue, icType::DB},
      {Type::QueryValue, icType::Query},
      {Type::ElementValue, icType::Element},
      {Type::ElementsValue, icType::Elements},
      {Type::SessionValue, icType::Session},
      {Type::WindowValue, icType::Window},
      {Type::TabValue, icType::Tab},
      {Type::DatetimeValue, icType::DateTime},
      {Type::ListValue, icType::StringList},
      {Type::RegexValue, icType::RegEx},
      {Type::ObjectValue, icType::Object},
      {Type::SetValue, icType::Set},
      {Type::PackedValue, icType::Packed},
      {Type::FileValue, icType::File}};

    return map.value(type, icType::Initial);
}

icString typeToString(const Type & type) {
    static const icObject<Type, icString> names{
      {Type::BoolValue, "bool"},
      {Type::DatetimeValue, "datetime"},
      {Type::DoubleValue, "double"},
      {Type::ElementValue, "element"},
      {Type::IntValue, "int"},
      {Type::ListValue, "list"},
      {Type::ObjectValue, "object"},
      {Type::RegexValue, "regex"},
      {Type::SetValue, "set"},
      {Type::StringValue, "string"},
      {Type::VoidValue, "void"},
      {Type::CookieValue, "cookie"},
      {Type::SessionValue, "session"},
      {Type::TabValue, "tab"},
      {Type::WindowValue, "window"},
      {Type::DatabaseValue, "database"},
      {Type::QueryValue, "query"},
      {Type::FileValue, "file"},
      {Type::PackedValue, "packed"},
      {Type::ListenerValue, "listener"},
      {Type::HandlerValue, "handler"},
      {Type::LambdaValue, "lambda-icl"},
      {Type::JavaScriptLambdaValue, "lambda-js"},
      {Type::SqlLambdaValue, "lambda-sql"}};

    return names.value(type, "unknown");
}

}  // namespace icL::memory
