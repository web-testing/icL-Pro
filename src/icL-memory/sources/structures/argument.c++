#include "argument.h++"

#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-il/structures/cookie-data.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/file-target.h++>
#include <icL-il/structures/lambda-target.h++>

#include <set.h++>



namespace icL::memory {

icL::memory::Argument::operator bool() const {
    return value.toBool();
}

icL::memory::Argument::operator int() const {
    return value.toInt();
}

icL::memory::Argument::operator double() const {
    return value.toDouble();
}

icL::memory::Argument::operator icString() const {
    return value.toString();
}

icL::memory::Argument::operator icDateTime() const {
    return value.toDateTime();
}

icL::memory::Argument::operator il::Element() const {
    return value.toElement();
}

icL::memory::Argument::operator il::Elements() const {
    return value.toElements();
}

icL::memory::Argument::operator icStringList() const {
    return value.toStringList();
}

icL::memory::Argument::operator Object() const {
    return value.toObject();
}

icL::memory::Argument::operator Set() const {
    return value.toSet();
}

icL::memory::Argument::operator icRegEx() const {
    return value.toRegEx();
}

icL::memory::Argument::operator il::File() const {
    return value.toFile();
}

icL::memory::Argument::operator il::Cookie() const {
    return value.toCookie();
}

icL::memory::Argument::operator il::Session() const {
    return value.toSession();
}

icL::memory::Argument::operator il::Tab() const {
    return value.toTab();
}

icL::memory::Argument::operator il::Window() const {
    return value.toWindow();
}

icL::memory::Argument::operator il::DB() const {
    return value.toDB();
}

icL::memory::Argument::operator il::Query() const {
    return value.toQuery();
}

icL::memory::Argument::operator il::LambdaTarget() const {
    return value.toLambda();
}

icL::memory::Argument::operator Type() const {
    return type;
}

}  // namespace icL::memory
