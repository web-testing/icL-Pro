#include "parameter.h++"

#include <utility>

icL::memory::Parameter::Parameter(icString name, icL::memory::Type type)
    : name(std::move(name))
    , type(type) {}

icL::memory::Parameter::Parameter(icString name, icVariant value)
    : name(std::move(name))
    , value(std::move(value)) {}

bool icL::memory::Parameter::operator==(
  const icL::memory::Parameter & other) const {
    return name == other.name;
}
