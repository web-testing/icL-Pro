#include "datacontainer.h++"

#include "../structures/set.h++"

#include <icL-il/structures/element.h++>



namespace icL::memory {

void DataContainer::setValue(const icString & name, const icVariant & value) {
    dataMap.insert(name, value);
}

bool DataContainer::contains(const icString & name) {
    return dataMap.contains(name);
}

Type DataContainer::getType(const icString & name) const {
    return variantToType(dataMap[name]);
}

bool DataContainer::checkType(const icString & name, const Type & type) {
    bool ret;

    switch (type) {
    case Type::BoolValue:
        ret = dataMap[name].isBool();
        break;

    case Type::IntValue:
        ret = dataMap[name].isInt();
        break;

    case Type::DoubleValue:
        ret = dataMap[name].isDouble();
        break;

    case Type::StringValue:
        ret = dataMap[name].isString();
        break;

    case Type::ListValue:
        ret = dataMap[name].isStringList();
        break;

    case Type::RegexValue:
        ret = dataMap[name].isRegEx();
        break;

    case Type::DatetimeValue:
        ret = dataMap[name].isDateTime();
        break;

    case Type::ObjectValue:
        ret = dataMap[name].isObject();
        break;

    case Type::SetValue:
        ret = dataMap[name].isSet();
        break;

    case Type::ElementValue:
        ret = dataMap[name].isElement();
        break;

    case Type::VoidValue:
        ret = !dataMap.contains(name);
        break;

    default:
        ret = false;
    }

    return ret;
}

icVariant DataContainer::getValue(const icString & name) const {
    return dataMap.value(name, icVariant::makeVoid());
}

int DataContainer::countVariables() {
    return dataMap.count();
}

void DataContainer::cloneDataTo(DataContainer * dc) {
    for (auto it = dataMap.begin(); it != dataMap.end(); it++) {
        dc->dataMap.insert(it.key(), it.value());
    }
}

const icVariantMap & DataContainer::getMap() {
    return dataMap;
}

ContainerType DataContainer::getContainerType() const {
    return ContainerType::Default;
}

bool DataContainer::operator==(const DataContainer & other) const {
    return dataMap == other.dataMap;
}

}  // namespace icL::memory
