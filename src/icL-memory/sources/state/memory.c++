#include "memory.h++"

namespace icL::memory {

StateIt & Memory::stateIt() {
    return m_stateIt;
}

StackIt & Memory::stackIt() {
    return m_stackIt;
}

FunctionContainer & Memory::functions() {
    return m_functions;
}

}  // namespace icL::memory
