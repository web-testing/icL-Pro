#ifndef memory_StackState
#define memory_StackState

#include "datacontainer.h++"

#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>


namespace icL::memory {

/**
 * @brief The StackContainer class is container of stack data
 */
class StackContainer : public DataContainer
{
public:
    /**
     * @brief StackContainer creates a new stack container
     * @param prev the previous stack container
     */
    StackContainer(StackContainer * prev, il::VMLayer * vm, bool isContainer);

    /**
     * @brief getPrev returns prev container
     * @return the previous container
     */
    StackContainer * getPrev();

    /**
     * @brief isLast is the last container
     * @return the last container
     */
    bool isLast();

    /**
     * @brief getStackValue returns @ value
     * @return value of @ variable
     */
    icVariant getStackValue();

    /**
     * @brief isContainer detects if this stack can contains value
     * @return true if it can contains values, otherwise false
     */
    bool isContainer() const;

    /**
     * @brief makeFinally make this layer as finally
     */
    void makeFinally();

    /**
     * @brief isFinally checks if this layer is finally
     * @return true if is finally, otherwise false
     */
    bool isFinally() const;

    /**
     * @brief isNamed checks the name of stack container
     * @param name is the expected name
     * @return true if name == this.name, otherwise false
     */
    bool isNamed(const icString & name);

    /**
     * @brief setName sets the name of stack container
     * @param name is a new name for stack container
     */
    void setName(const icString & name);

    /**
     * @brief getName gets the name of stack container
     * @return the name of the stack container
     */
    icString getName() const;

    /// \brief `Stack.addDescription (description : string) : void`
    void addDescription(const icString & description);

    /// \brief `Stack.break () : void`
    void break_();

    /// \brief `Stack.clear () : void`
    void clear();

    /// \brief `Stack.continue () : void`
    void continue_();

    /// \brief `Stack.destroy () : void`
    void destroy();

    /// \brief `Stack.ignore () : void`
    void ignore();

    /// \brief `Stack.listen () : void`
    void listen();

    /// \brief `Stack.markStep () : void`
    void markStep(const icString & name);

    /// \brief `Stack.markTest () : void`
    void markTest(const icString & name);

    /// \brief `Stack.return () : void`
    void return_(const icVariant & value);

    // DataContainer interface
public:
    ContainerType getContainerType() const override;

private:
    /// \brief m_prev is the prev container in stack
    StackContainer * m_prev = nullptr;

    /// \brief m_name is the name of stack container
    icString m_name;

    /**
     * @brief vm is a ponter to conformance vm layer
     *
     * Each VM layer has a stack container, each stack container is for a VM
     * single layer
     */
    il::VMLayer * m_vm = nullptr;

    /// \brief m_isContainer confirms that this stack can contains values
    bool m_isContainer;

    /// \brief m_isFinally blocks the reading of variables in top layers
    bool m_isFinally = false;

    long : 48;  // padding
};

/**
 * @brief The StackIt class is stack containers iterator
 */
class StackIt
{
public:
    StackIt() = default;
    ~StackIt();

    /**
     * @brief stack gets the last stack which is the current
     * @return the current stack
     */
    StackContainer * stack();

    /**
     * @brief openNewStack opens a new stack
     */
    void openNewStack(il::VMLayer * vm, bool isContainer);

    /**
     * @brief closeStack close the last stack
     */
    void closeStack();

    /**
     * @brief getContainerForVariable gets the container which contains variable
     * `name`
     * @param name is the name of needed variable
     * @return the container which contains the variable `name` if so exists,
     * otherwise the current stack
     */
    StackContainer * getContainerForVariable(const icString & name);

    /**
     * @brief getContainerByName return a container named `name`
     * @param name is the name of needed stack
     * @return a pointer to a stack container named `name`
     */
    StackContainer * getContainerByName(const icString & name);

    /**
     * @brief clear removes all stacks
     */
    void clear();

private:
    /**
     * @brief m_stack is a pointer to current stack
     */
    StackContainer * m_stack = nullptr;
};

}  // namespace icL::memory

#endif  // memory_StackState
