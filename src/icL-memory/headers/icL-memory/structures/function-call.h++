#ifndef memory_FunctionCall
#define memory_FunctionCall

#include "arg-value.h++"

#include <icL-il/structures/code-fragment.h++>



namespace icL::memory {

enum class ContextType : char {  ///< types of context
    Limited,                     ///< limited use context `[]`
    Run,                         ///< full runnable context `{}`
    Value                        ///< value generating context `()`
};

/**
 * @brief The FunctionCall struct is a call to a function
 */
struct FunctionCall
{
    /// \brief args is the list of local variables to create in stack container
    ArgValueList args;

    /// \brief code is the code to run on
    il::CodeFragment code;

    /// \brief isolateLocalVariables is true on fuction call, false on code
    /// fragment call (round or curly brackets)
    bool isolateLocalVariables = false;

    /// \brief isolateGlobalVariables is true on external file execute,
    /// otherwise is false
    bool isolateGlobalVariables = false;

    /// \brief createLayer is true if a new layer must be created
    bool createLayer = true;

    /// \brief contextType is the type of context which will be generated
    ContextType contextType = ContextType::Run;

    /// \brief returnType is the return type of function
    icType returnType = icType::Initial;

    /// \brief contextName is the name of a run context `{:name code }`
    icString contextName;
};

}  // namespace icL::memory

#endif  // memory_FunctionCall
