#ifndef memory_ArgValue
#define memory_ArgValue

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>


namespace icL::memory {

/**
 * @brief The ArgValue struct is a value of a argument, with a name of a field,
 * which will create a local variable in function stack container
 */
struct ArgValue
{
    /// \brief name is the name of argument
    icString name;

    /// \brief value is the value of argument
    icVariant value;
};

/**
 * ArgValueList is a list of arguments values
 */
using ArgValueList = icList<ArgValue>;

}  // namespace icL::memory

#endif  // memory_ArgValue
