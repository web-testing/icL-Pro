#ifndef ce_PackedValueItem
#define ce_PackedValueItem

#include "type.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <memory>



namespace icL::memory {

class DataContainer;

enum class PackedValueType {  ///< Types of item in a pack
    Value,                    ///< it's a value (value, name & container)
    Parameter,                ///< it's a parameter (name & type)
    DefaultParameter,         ///< it's a parameter (name, type & value)
    Field,                    ///< it's a field (name & value)
    Column,                   ///< it's a set column (name & type)
    Type                      ///< it's a type (type only)
};

/**
 * @brief The PackedValueItem struct represent a item in a packet value set
 */
struct PackedValueItem
{
    /// \brief itemType is the type of packed value item
    PackedValueType itemType;
    /// \brief type is the type of contained value
    memory::Type type;
    /// \brief name is the name of contained value
    icString name;
    /// \brief value is the value of contained value
    icVariant value;
    /// \brief container is the continer of the contined value
    memory::DataContainer * container{};
};

using PackedItems = icList<PackedValueItem>;

/**
 * @brief The PacketValuePtr struct it a pointer
 */
struct PackedValue
{
    std::shared_ptr<PackedItems> data;

    PackedValue() = default;

    /**
     * @brief PacketValuePtr casts a icVariant to a packed value
     * @param var is the icVariant to cast
     */
    PackedValue(const icVariant & var);

    /**
     * @brief operator icVariant casts the packed value pointer to a icVariant
     */
    operator icVariant();

    /**
     * @brief operator == checks if packed values are equivalent
     * @param other is the packed value to compare with
     * @return true if they are equivalent, otherwise false
     */
    bool operator==(const PackedValue & other) const;
};

}  // namespace icL::memory

#endif  // ce_PackedValueItem
