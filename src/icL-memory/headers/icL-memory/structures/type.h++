#ifndef memory_Type
#define memory_Type

enum class icType;
class icString;
class icVariant;

namespace icL::memory {

enum class Type {
    // base types
    BoolValue,
    DatetimeValue,
    DoubleValue,
    ElementValue,
    ElementsValue,
    IntValue,
    ListValue,
    ObjectValue,
    RegexValue,
    SetValue,
    StringValue,
    VoidValue,

    // additional values

    CookieValue,
    SessionValue,
    TabValue,
    WindowValue,
    DocumentValue,
    DatabaseValue,
    QueryValue,
    FileValue,
    PackedValue,
    ListenerValue,
    HandlerValue,
    LambdaValue,
    JavaScriptLambdaValue,
    SqlLambdaValue,
    JsFileValue,

    // browser types
    Alert,
    By,
    Cookie,
    Cookies,
    Document,
    icL,
    Key,
    Mouse,
    Move,
    Session,
    Sessions,
    Tab,
    Tabs,
    Window,
    Windows,

    // system types
    Datetime,
    DB,
    DBManager,
    DSV,
    File,
    Files,
    Import,
    Log,
    Make,
    Math,
    Numbers,
    Query,
    Request,
    Signal,
    Stack,
    Stacks,
    State,
    Types,

    // ArgumentList additional
    Identifier,
    Type,
    AnyValue
};

/**
 * @brief variantToType gets type of icVariant value
 * @param var is the variant to extract type
 * @return icL type of icVariant
 */
Type variantToType(const icVariant & var);

/**
 * @brief typeToVarType converts a type to a icType
 * @param type is the type to convert
 * @return the icType equivalent
 */
icType typeToVarType(const Type type);

/**
 * @brief typeToString convert a value type to a string
 * @param type is the type to convert to string
 * @return the type name using icL specification
 */
icString typeToString(const Type & type);

}  // namespace icL::memory

#endif  // memory_Type
