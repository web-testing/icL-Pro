cmake_minimum_required(VERSION 3.8.0)

project(-icL-memory LANGUAGES CXX)

file(GLOB HEADERS "${CMAKE_CURRENT_SOURCE_DIR}/headers/icL-memory/*/*.h++")
file(GLOB SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/sources/*/*.c++")

add_library(${PROJECT_NAME} STATIC
  ${HEADERS}
  ${SOURCES}
)

target_include_directories(${PROJECT_NAME}
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/headers/icL-memory/state
	${CMAKE_CURRENT_SOURCE_DIR}/headers/icL-memory/structures
  PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/headers
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
	-icL-il
	)
