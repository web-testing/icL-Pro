#include "stateful-server.h++"

#include <icL-memory/state/signals.h++>


namespace icL::shared {

using memory::Signals::Signals;

StatefulServer::StatefulServer()
    : map({{"System", Signals::System},
           {"Exit", Signals::Exit},
           {"NoSessions", Signals::NoSessions},
           {"NoSuchWindow", Signals::NoSuchWindow},
           {"NoSuchElement", Signals::NoSuchElement},
           {"NoSuchFrame", Signals::NoSuchFrame},
           {"NoSuchCookie", Signals::NoSuchCookie},
           {"NoSuchAlert", Signals::NoSuchAlert},
           {"NoSuchPlaceholder", Signals::NoSuchPlaceholder},
           {"NoSuchDatabase", Signals::NoSuchDatabase},
           {"NoSuchServer", Signals::NoSuchServer},
           {"WrongUserPassword", Signals::WrongUserPassword},
           {"StaleElementReference", Signals::StaleElementReference},
           {"FolderNotFound", Signals::FolderNotFound},
           {"FileNotFound", Signals::FileNotFound},
           {"FieldNotFound", Signals::FieldNotFound},
           {"FieldAlreadyExists", Signals::FieldAlreadyExists},
           {"OutOfBounds", Signals::OutOfBounds},
           {"UnsupportedOperation", Signals::UnsupportedOperation},
           {"EmptyString", Signals::EmptyString},
           {"EmptyList", Signals::EmptyList},
           {"MultiList", Signals::MultiList},
           {"EmptyElement", Signals::EmptyElement},
           {"MultiElement", Signals::MultiElement},
           {"EmptySet", Signals::EmptySet},
           {"MultiSet", Signals::MultiSet},
           {"InvalidArgument", Signals::InvalidArgument},
           {"InvalidSelector", Signals::InvalidSelector},
           {"InvalidElementState", Signals::InvalidElementState},
           {"InvalidElement", Signals::InvalidElement},
           {"IncompatibleRoot", Signals::IncompatibleRoot},
           {"IncompatibleData", Signals::IncompatibleData},
           {"IncompatibleObject", Signals::IncompatibleObject},
           {"InvalidSessionId", Signals::InvalidSessionId},
           {"InvalidCookieDomain", Signals::InvalidCookieDomain},
           {"InsecureCertificate", Signals::InsecureCertificate},
           {"UnexpectedAlertOpen", Signals::UnexpectedAlertOpen},
           {"UnrealCast", Signals::UnrealCast},
           {"ParsingFailed", Signals::ParsingFailed},
           {"WrongDelimiter", Signals::WrongDelimiter},
           {"ComplexField", Signals::ComplexField},
           {"ElementNotInteractable", Signals::ElementNotInteractable},
           {"ElementClickIntercepted", Signals::ElementClickIntercepted},
           {"MoveTargetOutOfBounds", Signals::MoveTargetOutOfBounds},
           {"UnableToSetCookie", Signals::UnableToSetCookie},
           {"UnableToCaptureScreen", Signals::UnableToCaptureScreen},
           {"JavaScriptError", Signals::JavaScriptError},
           {"ScriptTimeout", Signals::ScriptTimeout},
           {"Timeout", Signals::Timeout},
           {"SessionNotCreated", Signals::SessionNotCreated},
           {"QueryNotExecutedYet", Signals::QueryNotExecutedYet},
           {"UnknownCommand", Signals::UnknownCommand},
           {"UnknownError", Signals::UnknownError},
           {"UnknownMethod", Signals::UnknownMethod}})
    , lastSignalCode(Signals::User) {}

bool StatefulServer::registerSignal(const icString & name) {
    if (map.contains(name)) {
        return false;
    }

    map.insert(name, lastSignalCode++);
    return true;
}

int StatefulServer::getSignal(const icString & name) {
    return map.value(name, Signals::System - 1);
}

}  // namespace icL::shared
