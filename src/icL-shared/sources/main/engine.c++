#include "engine.h++"

#include <icL-cp/cp/cp.h++>
#include <icL-cp/source/source-server.h++>
#include <icL-memory/state/memory.h++>
#include <icL-vm/vmstack.h++>

#include <frontend.h++>
#include <request.h++>
#include <stateful-server.h++>



namespace icL::shared {

void Engine::setMemory(memory::Memory * mem) {
    m_il.mem = mem;
}

void Engine::setVMStack(il::VMStack * vms) {
    m_il.vms = vms;
}

void Engine::setFrontEndServer(il::FrontEnd * server) {
    m_il.server = server;
}

void Engine::setDbServer(il::DBServer * db) {
    m_il.db = db;
}

void Engine::setFileServer(il::FileServer * file) {
    m_il.file = file;
}

void Engine::setSourceServer(il::SourceServer * source) {
    m_il.source = source;
}

void Engine::setStatefulServer(il::StatefulServer * stateful) {
    m_il.stateful = stateful;
}

void Engine::setListenService(il::Listen * listen) {
    m_il.listen = listen;
}

void Engine::setRequestService(il::Request * request) {
    m_il.request = request;
}

void Engine::finalize() {
    if (m_il.source == nullptr) {
        m_il.source = new cp::SourceServer{il()};
    }
    if (m_il.mem == nullptr) {
        m_il.mem = new memory::Memory{};
    }
    if (m_il.server == nullptr) {
        m_il.server = new FrontEnd{};
    }
    if (m_il.vms == nullptr) {
        m_il.vms = new vm::VMStack{il()};
    }
    if (m_il.request == nullptr) {
        m_il.request = new Request{};
    }
    if (m_il.stateful == nullptr) {
        m_il.stateful = new StatefulServer{};
    }
}

il::InterLevel * Engine::il() {
    return &m_il;
}

}  // namespace icL::shared
