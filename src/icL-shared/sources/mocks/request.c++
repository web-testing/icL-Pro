#include "request.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/structures/target-data.h++>

#include <iostream>



namespace icL::shared {

Request::Request() {}

void Request::confirm(const icString & /*text*/) {
    std::cout << "request.confirm called" << std::endl;
    return;
}

bool Request::ask(const icString & /*question*/) {
    std::cout << "request.ask called" << std::endl;
    return false;
}

int Request::int_(const icString & /*text*/) {
    std::cout << "request.int called" << std::endl;
    return 0;
}

double Request::double_(const icString & /*text*/) {
    std::cout << "request.double called" << std::endl;
    return 0.0;
}

icString Request::string(const icString & /*text*/) {
    std::cout << "request.string called" << std::endl;
    return {"test"};
}

icStringList Request::list(const icString & /*text*/) {
    std::cout << "request.list called" << std::endl;
    return {"test"};
}

il::Tab Request::tab(const icString & /*text*/) {
    std::cout << "request.tab called" << std::endl;
    return {};
}

void Request::notify(int /*secunds*/, const icString & /*message*/) {
    std::cout << "notify called" << std::endl;
    return;
}

}  // namespace icL::shared
