#ifndef shared_StatefulServer
#define shared_StatefulServer

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/main/stateful-server.h++>



namespace icL::shared {

class StatefulServer : public il::StatefulServer
{
    /// \brief map is used to store signals
    icObject<icString, int> map;

    /// \brief lastSignalCode gets a new code for each new signal
    int lastSignalCode;

public:
    StatefulServer();

    // StatefulServer interface
public:
    bool registerSignal(const icString & name) override;
    int  getSignal(const icString & name) override;
};

}  // namespace icL::shared

#endif  // shared_StatefulServer
