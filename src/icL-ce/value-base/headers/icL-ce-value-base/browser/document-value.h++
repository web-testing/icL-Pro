#ifndef ce_DocumentValue
#define ce_DocumentValue

#include <icL-service-value-base/browser/document-value.h++>

#include <icL-ce-base/value/base-value.h++>



namespace icL {

namespace il {
struct TargetData;
}

namespace ce {

/**
 * @brief The Document class represents a `Document` token
 */
class DocumentValue
    : public BaseValue
    , public service::DocumentValue
{
public:
    /// @brief SessionValue calls BaseValue(il, container, varName, readonly)
    DocumentValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief SessionValue calls BaseValue(il, rvalue)
    DocumentValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief SessionValue calls BaseValue(il, getter, setter)
    DocumentValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief SessionValue calls BaseValue(value)
    DocumentValue(BaseValue * value);

    // properties level 2

    /// `document'tab`
    void runTab();

    // methods level 2

    /// `document.click`
    void runClick(const memory::ArgList & args);

    /// `document.hover`
    void runHover(const memory::ArgList & args);

    /// `document.mouseDown`
    void runMouseDown(const memory::ArgList & args);

    /// `document.mouseUp`
    void runMouseUp(const memory::ArgList & args);

    /// `document.type`
    void runType(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_DocumentValue
