#ifndef ce_TabValue
#define ce_TabValue

#include <icL-service-value-base/browser/tab-value.h++>

#include <icL-ce-base/value/base-value.h++>



namespace icL {

namespace il {
struct Tab;
}

namespace ce {

class TabValue
    : public BaseValue
    , public service::TabValue
{
public:
    /// @brief TabValue calls BaseValue(il, container, varName, readonly)
    TabValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief TabValue calls BaseValue(il, rvalue)
    TabValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief TabValue calls BaseValue(il, getter, setter)
    TabValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief TabValue calls BaseValue(value)
    TabValue(BaseValue * value);

    // properties level 1

    /// `tab'cookies : Cookies`
    CE * cookies();

    /// `tab'doc : Doc`
    CE * doc();

    /// `session'tabs : Tabs`
    CE * tabs();

    /// `[r/w] tab'url : string`
    CE * url();

    /// `session'windows : Windows`
    CE * window();

    // properties level 2

    /// `tab'cookies`
    void runCookies();

    /// `tab'doc`
    void runDoc();

    /// `tab'screenshot`
    void runScreenshot();

    /// `tab'source`
    void runSource();

    /// `tab'title`
    void runTitle();

    /// `tab'tabs`
    void runTabs();

    /// `tab'url`
    void runUrl();

    /// `tab'window`
    void runWindow();

    // methods level 2

    /// `tab.back`
    void runBack(const memory::ArgList & args);

    /// `tab.close`
    void runClose(const memory::ArgList & args);

    /// `tab.focus`
    void runFocus(const memory::ArgList & args);

    /// `tab.forward`
    void runForward(const memory::ArgList & args);

    /// `tab.get`
    void runGet(const memory::ArgList & args);

    /// `tab.load`
    void runLoad(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_TabValue
