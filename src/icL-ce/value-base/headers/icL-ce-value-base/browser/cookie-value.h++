#ifndef ce_CookieValue
#define ce_CookieValue

#include <icL-service-value-base/browser/cookie-value.h++>

#include <icL-ce-base/value/base-value.h++>

namespace icL {

namespace il {
struct CookieData;
struct Cookie;
}  // namespace il

namespace ce {

/**
 * @brief The CookieValue class represents a `cookie` value
 */
class CookieValue
    : public BaseValue
    , public service::CookieValue
{
public:
    /// @brief CookieValue calls BaseValue(il, container, varName, readonly)
    CookieValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief CookieValue calls BaseValue(il, rvalue)
    CookieValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief CookieValue calls BaseValue(il, getter, setter)
    CookieValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief CookieValue calls BaseValue(value)
    CookieValue(BaseValue * value);

private:
    /**
     * @brief runCookieProperty runs a cookie specified property
     * @param getter is the getter of property value
     * @param setter is the setter of property value
     * @return a editable contextual entity value
     */
    template <typename RetValueType>
    CE * runCookieProperty(
      const std::function<icVariant(const il::CookieData &)> & getter,
      const std::function<void(il::CookieData &, icVariant)> & setter);

public:
    // properties level 1

    /// `[r/o] cookie'cookies : Cookies`
    CE * cookies();

    /// `[r/w] cookie'domain : string`
    CE * domain();

    /// `[r/w] cookie'expiry : int`
    CE * expiry();

    /// `[r/w] cookie'httpOnly : bool`
    CE * httpOnly();

    /// `[r/w] cookie'name : string`
    CE * name();

    /// `[r/w] cookie'path : string`
    CE * path();

    /// `[r/w] cookie'secure : bool`
    CE * secure();

    /// `[r/w] cookie'value : string`
    CE * value();

    // properties level 2

    /// `cookie'cookies`
    void runCookies();

    /// `cookie'domain`
    void runDomain();

    /// `cookie'expiry`
    void runExpiry();

    /// `cookie'httpOnly`
    void runHttpOnly();

    /// `cookie'name`
    void runName();

    /// `cookie'path`
    void runPath();

    /// `cookie'secure`
    void runSecure();

    /// `cookie'value`
    void runValue();

    /// `cookie.add`
    void runAdd(const memory::ArgList & args);

    /// `cookie.load`
    void runLoad(const memory::ArgList & args);

    /// `cookie.resetTime`
    void runResetTime(const memory::ArgList & args);

    /// `cookie.save`
    void runSave(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_CookieValue
