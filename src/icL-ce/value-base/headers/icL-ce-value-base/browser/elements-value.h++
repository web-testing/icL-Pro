#ifndef ce_ElementsValue
#define ce_ElementsValue

#include <icL-service-value-base/browser/elements-value.h++>

#include <icL-ce-base/value/base-value.h++>


namespace icL {

namespace il {
struct Element;
struct Elements;
}  // namespace il

namespace ce {

/**
 * @brief The ElementsValue class represents an `element` value
 */
class ElementsValue
    : public BaseValue
    , public service::ElementsValue
{
public:
    /// @brief ElementsValue calls BaseValue(il, container, varName, readonly)
    ElementsValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief ElementsValue calls BaseValue(il, rvalue)
    ElementsValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief ElementsValue calls BaseValue(il, getter, setter)
    ElementsValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief ElementsValue calls BaseValue(value)
    ElementsValue(BaseValue * value);

    // properties level 2

    /// `elements'attr-*`
    void runAttr(const icString & name);

    /// `elements'empty`
    void runEmpty();

    /// `elements'length`
    void runLength();

    /// `elements'prop-*`
    void runProp(const icString & name);

    /// `elements'rects`
    void runRects();

    /// `elements'tags`
    void runTags();

    /// `element'texts`
    void runTexts();

    // methods level 2

    /// `elements.add`
    void runAdd(const memory::ArgList & args);

    /// `elements.copy`
    void runCopy(const memory::ArgList & args);

    /// `elements.filter`
    void runFilter(const memory::ArgList & args);

    /// `elements.get`
    void runGet(const memory::ArgList & args);

private:
    /**
     * @brief runOwnProperty runs a property with `None` prefix
     * @param name is the name of property
     */
    void runOwnProperty(const icString & name);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
    void runMethod(
      const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_ElementsValue
