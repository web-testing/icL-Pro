#ifndef ce_WindowValue
#define ce_WindowValue

#include <icL-service-value-base/browser/window-value.h++>

#include <icL-ce-base/value/base-value.h++>



class icRect;

namespace icL {

namespace il {
struct Window;
struct Element;
}  // namespace il

namespace ce {

class WindowValue
    : public BaseValue
    , public service::WindowValue
{
public:
    /// @brief WindowValue calls BaseValue(il, container, varName, readonly)
    WindowValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief WindowValue calls BaseValue(il, rvalue)
    WindowValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief WindowValue calls BaseValue(il, getter, setter)
    WindowValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief WindowValue calls BaseValue(value)
    WindowValue(BaseValue * value);

private:
    /**
     * @brief runIntProperty runs a int property
     * @return a contextual entity for needed int propeperty
     */
    CE * runRectProperty(
      double (icRect::*getter)() const, void (icRect::*setter)(double));

public:
    // properties level 1

    /// `[r/w] window'height : int`
    CE * height();

    /// `[r/o] window'tab : tab`
    CE * tab();

    /// `[r/w] window'width : int`
    CE * width();

    /// `[r/o] window'windows : Windows`
    CE * windows();

    /// `[r/w] window'x : int`
    CE * x();

    /// `[r/w] window'y : int`
    CE * y();

    // properties level 2

    /// `window'height`
    void runHeight();

    /// `window'tab`
    void runTab();

    /// `window'width`
    void runWidth();

    /// `window'windows`
    void runWindows();

    /// `window'x`
    void runX();

    /// `window'y`
    void runY();

    // methods level 2

    /// `window.close`
    void runClose(const memory::ArgList & args);

    /// `window.focus`
    void runFocus(const memory::ArgList & args);

    /// `window.fullscreen`
    void runFullscreen(const memory::ArgList & args);

    /// `window.maximize`
    void runMaximize(const memory::ArgList & args);

    /// `window.minimize`
    void runMinimize(const memory::ArgList & args);

    /// `window.restore`
    void runRestore(const memory::ArgList & args);

    /// `window.switchToFrame`
    void runSwitchToFrame(const memory::ArgList & args);

    /// `window.switchToParent`
    void runSwitchToParent(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_WindowValue
