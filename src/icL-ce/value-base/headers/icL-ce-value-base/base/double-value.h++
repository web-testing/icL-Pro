#ifndef ce_DoubleValue
#define ce_DoubleValue

#include <icL-ce-base/value/base-value.h++>



namespace icL::ce {

/**
 * @brief The DoubleValue class represents a `double` value
 */
class DoubleValue : public BaseValue
{
public:
    /// @brief DoubleValue calls BaseValue(il, container, varName, readonly)
    DoubleValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief DoubleValue calls BaseValue(il, rvalue)
    DoubleValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief DoubleValue calls BaseValue(il, getter, setter)
    DoubleValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief DoubleValue calls BaseValue(value)
    DoubleValue(BaseValue * value);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_DoubleValue
