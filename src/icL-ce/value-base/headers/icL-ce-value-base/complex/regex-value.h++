#ifndef ce_RegexValue
#define ce_RegexValue

#include <icL-ce-base/value/base-value.h++>



namespace icL::ce {

/**
 * @brief The RegexValue class represents a `regex` value
 */
class RegexValue : public BaseValue
{
public:
    /// @brief RegexValue calls BaseValue(il, container, varName, readonly)
    RegexValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief RegexValue calls BaseValue(il, rvalue)
    RegexValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief RegexValue calls BaseValue(il, getter, setter)
    RegexValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief RegexValue calls BaseValue(value)
    RegexValue(BaseValue * value);


    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_RegexValue
