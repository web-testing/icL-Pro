#ifndef ce_JsFileValue
#define ce_JsFileValue

#include <icL-service-value-base/system/js-file-value.h++>

#include <icL-ce-base/value/base-value.h++>



namespace icL::ce {

class JsFileValue
    : public BaseValue
    , public service::JsFileValue
{
public:
    /// @brief JsFileValue calls BaseValue(il, container, varName, readonly)
    JsFileValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief JsFileValue calls BaseValue(il, rvalue)
    JsFileValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief JsFileValue calls BaseValue(value)
    JsFileValue(BaseValue * value);

    // methods level 2

    /// `js-file.load`
    void runLoad(const memory::ArgList & args);

    /// `js-file.run`
    void runRun(const memory::ArgList & args);

    /// `js-file.runAsync`
    void runRunAsync(const memory::ArgList & args);

    /// `js-file.setAsUserScript`
    void runSetAsUserScript(const memory::ArgList & args);

    /// `js-file.setAsPersistentUserScript`
    void runSetAsPersistentUserScript(const memory::ArgList & args);

    // Value interface
public:
    memory::Type type() const;
    icString     typeName();
    void         runMethod(const icString & name, const memory::ArgList & args);

    // JsFileValue interface
protected:
    il::JsFile _value();
};

}  // namespace icL::ce

#endif  // ce_JsFileValue
