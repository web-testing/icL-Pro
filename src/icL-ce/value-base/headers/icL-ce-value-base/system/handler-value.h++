#ifndef ce_HandlerValue
#define ce_HandlerValue

#include <icL-service-value-base/system/handler-value.h++>

#include <icL-ce-base/value/base-value.h++>



namespace icL::ce {

class HandlerValue
    : public BaseValue
    , public service::HandlerValue
{
public:
    /// @brief QueryValue calls BaseValue(il, container, varName, readonly)
    HandlerValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief QueryValue calls BaseValue(il, rvalue)
    HandlerValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief QueryValue calls BaseValue(il, getter, setter)
    HandlerValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief QueryValue calls BaseValue(value)
    HandlerValue(BaseValue * value);

    // methods level 2

    /// `handler.setup (code : lambda-icl) : handler`
    void runSetup(const memory::ArgList & args);

    /// `handler.activate () : handler`
    void runActivate(const memory::ArgList & args);

    /// `handler.deactivate () : handler`
    void runDeactivate(const memory::ArgList & args);

    /// `handler.kill () : void`
    void runKill(const memory::ArgList & args);

    // Value interface
public:
    memory::Type type() const override;
    icString     typeName() override;
    void         runMethod(
              const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_HandlerValue
