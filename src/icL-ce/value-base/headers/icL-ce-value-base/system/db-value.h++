#ifndef ce_DBValue
#define ce_DBValue

#include <icL-service-value-base/system/db-value.h++>

#include <icL-ce-base/value/base-value.h++>

namespace icL {

namespace il {
struct Query;
}

namespace ce {

class DBValue
    : public BaseValue
    , public service::DBValue
{
public:
    /// @brief DBValue calls BaseValue(il, container, varName, readonly)
    DBValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief DBValue calls BaseValue(il, rvalue)
    DBValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief DBValue calls BaseValue(il, getter, setter)
    DBValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief DBValue calls BaseValue(value)
    DBValue(BaseValue * value);

    // methods level 2

    /// `db.close`
    void runClose(const memory::ArgList & args);

    /// `db.query`
    void runQuery(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_DBValue
