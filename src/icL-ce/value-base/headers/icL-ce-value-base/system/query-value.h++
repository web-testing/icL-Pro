#ifndef ce_QueryValue
#define ce_QueryValue

#include <icL-service-value-base/system/query-value.h++>

#include <icL-ce-base/value/base-value.h++>



namespace icL {

namespace il {
struct Query;
}

namespace ce {

class QueryValue
    : public BaseValue
    , public service::QueryValue
{
public:
    /// @brief QueryValue calls BaseValue(il, container, varName, readonly)
    QueryValue(
      il::InterLevel * il, memory::DataContainer * container,
      const icString & varName, bool readonly = false);

    /// @brief QueryValue calls BaseValue(il, rvalue)
    QueryValue(il::InterLevel * il, const icVariant & rvalue);

    /// @brief QueryValue calls BaseValue(il, getter, setter)
    QueryValue(
      il::InterLevel * il, const icString & getter, const icString & setter);

    /// @brief QueryValue calls BaseValue(value)
    QueryValue(BaseValue * value);

    // properties level 1

    /// `[r/o] query'(name : string) : any`
    CE * roProperty(const icString & name);

    /// `[w/o] query'(name : string) : any`
    CE * woProperty(const icString & name);

    /// `query'(name : string) : any`
    CE * property(const icString & name);

    // properties level 2

    /// `query'(string)`
    void runPropertyLevel2(const icString & name);

    // methods level 2

    /// `query.exec`
    void runExec(const memory::ArgList & args);

    /// `query.first`
    void runFirst(const memory::ArgList & args);

    /// `query.get`
    void runGet(const memory::ArgList & args);

    /// `query.getError`
    void runGetError(const memory::ArgList & args);

    /// `query.getLength`
    void runGetLength(const memory::ArgList & args);

    /// `query.getRowsAffected`
    void runGetRowsAffected(const memory::ArgList & args);

    /// `query.last`
    void runLast(const memory::ArgList & args);

    /// `query.next`
    void runNext(const memory::ArgList & args);

    /// `query.previous`
    void runPrevious(const memory::ArgList & args);

    /// `query.seek`
    void runSeek(const memory::ArgList & args);

    /// `query.set`
    void runSet(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_QueryValue
