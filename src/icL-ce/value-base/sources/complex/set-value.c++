#include "set-value.h++"

#include "bool-value.h++"
#include "int-value.h++"
#include "list-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-rect.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/values/set.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

using memory::Signals::Signals;

SetValue::SetValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

SetValue::SetValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

SetValue::SetValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

SetValue::SetValue(BaseValue * value)
    : BaseValue(value) {}

SetValue::SetValue(il::InterLevel * il, const icList<icRect> & rects)
    : BaseValue(il, icVariant{}) {
    memory::Set     set;
    memory::Columns columns{{Type::IntValue, "x"},
                            {Type::IntValue, "y"},
                            {Type::IntValue, "width"},
                            {Type::IntValue, "height"}};
    memory::SetData data;

    for (auto & rect : rects) {
        data.insert({rect.x(), rect.y(), rect.width(), rect.height()});
    }

    set.header  = std::make_shared<memory::Columns>(columns);
    set.setData = std::make_shared<memory::SetData>(data);

    setValue(set);
}

void SetValue::runCapacity() {
    m_newContext = new IntValue{il, capacity()};
}

void SetValue::runEmpty() {
    m_newContext = new BoolValue{il, empty()};
}

void SetValue::runApplicate(const memory::ArgList & args) {
    bool eachArgIsList = true;

    for (auto & arg : args) {
        eachArgIsList =
          eachArgIsList && !arg.value.isNull() && arg.type == Type::ListValue;
    }

    if (eachArgIsList) {
        icList<icStringList> args_values;

        for (const auto & arg : args) {
            args_values.append(arg);
        }

        applicate(args_values);
        m_newContext = new SetValue{this};
    }
    else {
        il->vm->signal({Signals::IncompatibleData, {}});
    }
}

void SetValue::runClear(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        clear();
        m_newContext = new SetValue{this};
    }
}

void SetValue::runClone(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new SetValue{il, clone()};
    }
}

void SetValue::runGetField(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new ListValue{il, getField(args[0])};
    }
}

void SetValue::runHasField(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new BoolValue{il, hasField(args[0])};
    }
}

void SetValue::runInsert(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        insert(args[0]);
        m_newContext = new SetValue{this};
    }
    else if (checkArgs(args)) {
        insert(service::Set::argsToList(args));
        m_newContext = new SetValue{this};
    }
}

void SetValue::runInsertField(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::ListValue})) {
        insertField(args[0], args[1]);
        m_newContext = new SetValue{this};
    }
    else if (checkArgs(
               args, {Type::StringValue, Type::ListValue, Type::Type})) {
        insertField(args[0], args[1], args[2].type);
        m_newContext = new SetValue{this};
    }
    else if (checkArgs(args, {Type::StringValue, Type::AnyValue})) {
        insertField(args[0], args[1].value);
        m_newContext = new SetValue{this};
    }
    else if (checkArgs(args, {Type::StringValue, Type::AnyValue, Type::Type})) {
        insertField(args[0], args[1].value, args[2].type);
        m_newContext = new SetValue{this};
    }
}

void SetValue::runRemove(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        remove(args[0]);
        m_newContext = new SetValue{this};
    }
    else if (checkArgs(args)) {
        remove(service::Set::argsToList(args));
        m_newContext = new SetValue{this};
    }
}

void SetValue::runRemoveField(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        removeField(args[0]);
        m_newContext = new SetValue{this};
    }
}

Type SetValue::type() const {
    return Type::SetValue;
}

icString SetValue::typeName() {
    return "icSet";
}

void SetValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (SetValue::*)()> properties{
      {"capacity", &SetValue::runCapacity}, {"empty", &SetValue::runEmpty}};

    runPropertyWithPrefixCheck<SetValue, BaseValue>(properties, prefix, name);
}

void SetValue::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (SetValue::*)(const memory::ArgList &)>
      methods{{"applicate", &SetValue::runApplicate},
              {"clear", &SetValue::runClear},
              {"clone", &SetValue::runClone},
              {"getField", &SetValue::runGetField},
              {"hasField", &SetValue::runHasField},
              {"insert", &SetValue::runInsert},
              {"insertField", &SetValue::runInsertField},
              {"remove", &SetValue::runRemove},
              {"removeField", &SetValue::runRemoveField}};

    runMethodNow<SetValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
