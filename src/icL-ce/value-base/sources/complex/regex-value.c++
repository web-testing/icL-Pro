#include "regex-value.h++"

namespace icL::ce {

RegexValue::RegexValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

RegexValue::RegexValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

RegexValue::RegexValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

RegexValue::RegexValue(BaseValue * value)
    : BaseValue(value) {}

Type RegexValue::type() const {
    return Type::RegexValue;
}

icString RegexValue::typeName() {
    return "icRegEx";
}

}  // namespace icL::ce
