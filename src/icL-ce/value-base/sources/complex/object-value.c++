#include "object-value.h++"

#include <icL-types/replaces/ic-rect.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

ObjectValue::ObjectValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

ObjectValue::ObjectValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

ObjectValue::ObjectValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

ObjectValue::ObjectValue(il::InterLevel * il, const icRect & rect)
    : BaseValue(il, icVariant{}) {
    memory::Object obj;

    obj.data = std::make_shared<memory::DataContainer>();
    obj.data->setValue("x", rect.x());
    obj.data->setValue("y", rect.y());
    obj.data->setValue("width", rect.width());
    obj.data->setValue("height", rect.height());

    setValue(obj);
}

ObjectValue::ObjectValue(BaseValue * value)
    : BaseValue(value) {}

void ObjectValue::runGet(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        memory::Object object = getValue();

        m_newContext =
          service::Factory::fromValue(il, object.data.get(), args[0]);
    }
}

Type ObjectValue::type() const {
    return Type::ObjectValue;
}

icString ObjectValue::typeName() {
    return "icObject";
}

void ObjectValue::runProperty(Prefix prefix, const icString & name) {
    if (prefix == Prefix::None) {
        static icSet<icString> predefined = {"rValue", "readOnly", "lValue",
                                             "link",   "typeId",   "typeName"};

        if (predefined.contains(name)) {
            BaseValue::runProperty(prefix, name);
        }
        else {
            memory::Object icObject = getValue();

            m_newContext =
              service::Factory::fromValue(il, icObject.data.get(), name);
        }
    }
    else {
        BaseValue::runProperty(prefix, name);
    }
}

void ObjectValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (ObjectValue::*)(const memory::ArgList &)>
      methods{{"get", &ObjectValue::runGet}};

    runMethodNow<ObjectValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
