#include "elements-value.h++"

#include "bool-value.h++"
#include "document-value.h++"
#include "int-value.h++"
#include "object-value.h++"
#include "string-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-rect.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>

#include <element-value.h++>
#include <list-value.h++>
#include <set-value.h++>



namespace icL::ce {

ElementsValue::ElementsValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

ElementsValue::ElementsValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

ElementsValue::ElementsValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

ElementsValue::ElementsValue(BaseValue * value)
    : BaseValue(value) {}

void ElementsValue::runAttr(const icString & name) {
    m_newContext = new StringValue{il, attr(name)};
}

void ElementsValue::runEmpty() {
    m_newContext = new BoolValue{il, empty()};
}

void ElementsValue::runLength() {
    m_newContext = new IntValue{il, length()};
}

void ElementsValue::runProp(const icString & name) {
    m_newContext = service::Factory::fromValue(il, prop(name));
}

void ElementsValue::runRects() {
    m_newContext = new SetValue{il, rects()};
}

void ElementsValue::runTags() {
    m_newContext = new ListValue{il, tags()};
}

void ElementsValue::runTexts() {
    m_newContext = new ListValue{il, texts()};
}

void ElementsValue::runAdd(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ElementValue})) {
        m_newContext = new ElementsValue{il, add_m1(args[0])};
    }
    else if (checkArgs(args, {Type::ElementsValue})) {
        m_newContext = new ElementsValue{il, add_mm(args[0])};
    }
}

void ElementsValue::runCopy(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new ElementsValue{il, copy()};
    }
}

void ElementsValue::runFilter(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new ElementsValue{il, filter(args[0])};
    }
}

void ElementsValue::runGet(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = new ElementsValue{il, get(args[0])};
    }
}

void ElementsValue::runOwnProperty(const icString & name) {
    static icObject<icString, void (ElementsValue::*)()> properties{
      {"empty", &ElementsValue::runEmpty},
      {"length", &ElementsValue::runLength},
      {"rects", &ElementsValue::runRects},
      {"tags", &ElementsValue::runTags},
      {"texts", &ElementsValue::runTexts}};

    bool isNumber;
    int  number = name.toInt(isNumber);

    if (isNumber) {
        m_newContext = new ElementValue{il, get(number)};
    }
    else {
        Prefix prefix = Prefix::None;
        runPropertyNow<ElementsValue, BaseValue>(properties, prefix, name);
    }
}

Type ElementsValue::type() const {
    return Type::ElementsValue;
}

icString ElementsValue::typeName() {
    return "element";
}

void ElementsValue::runProperty(Prefix prefix, const icString & name) {
    switch (prefix) {
    case Prefix::Attr:
        runAttr(name);
        break;

    case Prefix::Prop:
        runProp(name);
        break;

    case Prefix::CSS:
        break;

    case Prefix::None:
        runOwnProperty(name);
        break;
    }
}

void ElementsValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (ElementsValue::*)(const memory::ArgList &)>
      methods{{"add", &ElementsValue::runAdd},
              {"filter", &ElementsValue::runFilter},
              {"get", &ElementsValue::runGet}};

    runMethodNow<ElementsValue, BaseValue>(methods, name, args);
}


}  // namespace icL::ce
