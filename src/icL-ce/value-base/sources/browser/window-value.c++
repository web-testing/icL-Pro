#include "window-value.h++"

#include "int-value.h++"
#include "tab-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-rect.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-browser/window/windows.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

WindowValue::WindowValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

WindowValue::WindowValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

WindowValue::WindowValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

WindowValue::WindowValue(BaseValue * value)
    : BaseValue(value) {}

CE * WindowValue::runRectProperty(
  double (icRect::*getter)() const, void (icRect::*setter)(double)) {
    auto * ret = new IntValue{this};
    auto * il  = this->il;

    ret->makeFunctional(
      [il, getter](BaseValue * window) -> icVariant {
          il::Window windowPtr = window->getValue(true);

          double value;

          il->server->pushTarget(*windowPtr.data);
          value = (il->server->getWindowRect().*getter)();
          il->server->popTarget();

          return value;
      },
      [il, setter](BaseValue * window, const icVariant & value) {
          il::Window windowPtr = window->getValue(true);
          icRect     rect;

          il->server->pushTarget(*windowPtr.data);
          rect = il->server->getWindowRect();
          (rect.*setter)(value.toInt());
          il->server->setWindowRect(rect);
          il->server->popTarget();
      });

    return ret;
}

CE * WindowValue::height() {
    return runRectProperty(&icRect::height, &icRect::setHeight);
}

CE * WindowValue::tab() {
    il::Tab tab;

    tab.data = _value().data;
    return new TabValue{il, tab};
}

CE * WindowValue::width() {
    return runRectProperty(&icRect::width, &icRect::setWidth);
}

CE * WindowValue::windows() {
    return new Windows{il, *_value().data};
}

CE * WindowValue::x() {
    return runRectProperty(&icRect::x, &icRect::setX);
}

CE * WindowValue::y() {
    return runRectProperty(&icRect::y, &icRect::setY);
}

void WindowValue::runHeight() {
    m_newContext = height();
}

void WindowValue::runTab() {
    m_newContext = tab();
}

void WindowValue::runWidth() {
    m_newContext = width();
}

void WindowValue::runWindows() {
    m_newContext = windows();
}

void WindowValue::runX() {
    m_newContext = x();
}

void WindowValue::runY() {
    m_newContext = y();
}

void WindowValue::runClose(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = new VoidValue{il};
    }
}

void WindowValue::runFocus(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        focus();
        m_newContext = new WindowValue{this};
    }
}

void WindowValue::runFullscreen(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        fullscreen();
        m_newContext = new WindowValue{this};
    }
}

void WindowValue::runMaximize(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        maximize();
        m_newContext = new WindowValue{this};
    }
}

void WindowValue::runMinimize(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        minimize();
        m_newContext = new WindowValue{this};
    }
}

void WindowValue::runRestore(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        restore();
        m_newContext = new WindowValue{this};
    }
}

void WindowValue::runSwitchToFrame(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        switchToFrame(int(args[0]));
        m_newContext = new WindowValue{this};
    }
    else if (checkArgs(args, {Type::ElementValue})) {
        switchToFrame(il::Element(args[0]));
        m_newContext = new WindowValue{this};
    }
}

void WindowValue::runSwitchToParent(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        switchToParent();
        m_newContext = new WindowValue{this};
    }
}

Type WindowValue::type() const {
    return Type::WindowValue;
}

icString WindowValue::typeName() {
    return "window";
}

void WindowValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (WindowValue::*)()> properties{
      {"height", &WindowValue::runHeight},
      {"tab", &WindowValue::runTab},
      {"width", &WindowValue::runWidth},
      {"windows", &WindowValue::runWindows},
      {"x", &WindowValue::runX},
      {"y", &WindowValue::runY}};

    il->server->pushTarget(*_value().data);
    runPropertyWithPrefixCheck<WindowValue, BaseValue>(
      properties, prefix, name);
    il->server->popTarget();
}

void WindowValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (WindowValue::*)(const memory::ArgList &)>
      methods{{"close", &WindowValue::runClose},
              {"focus", &WindowValue::runFocus},
              {"fullscreen", &WindowValue::runFullscreen},
              {"maximize", &WindowValue::runMaximize},
              {"minimize", &WindowValue::runMinimize},
              {"restore", &WindowValue::runRestore},
              {"switchToFrame", &WindowValue::runSwitchToFrame},
              {"switchToParent", &WindowValue::runSwitchToParent}};

    il->server->pushTarget(*_value().data);
    runMethodNow<WindowValue, BaseValue>(methods, name, args);
    il->server->popTarget();
}

}  // namespace icL::ce
