#include "tab-value.h++"

#include "document-value.h++"
#include "string-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-browser/document/cookies.h++>
#include <icL-ce-value-browser/window/tabs.h++>
#include <icL-ce-value-browser/window/windows.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>



namespace icL::ce {

TabValue::TabValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

TabValue::TabValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

TabValue::TabValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

TabValue::TabValue(BaseValue * value)
    : BaseValue(value) {}

CE * TabValue::cookies() {
    return new Cookies{il, *_value().data};
}

CE * TabValue::doc() {
    il::Document document;
    document.data = std::make_shared<il::TargetData>(*_value().data);
    return new DocumentValue{il, document};
}

CE * TabValue::tabs() {
    return new Tabs{il, *_value().data};
}

CE * TabValue::url() {
    BaseValue *      ret = new StringValue{this};
    il::InterLevel * il  = this->il;

    ret->makeFunctional(
      [il](BaseValue * tab) -> icVariant {
          il::Tab   tabPtr = tab->getValue(true);
          icVariant ret;

          il->server->pushTarget(*tabPtr.data);
          ret = il->server->url();
          il->server->popTarget();

          return ret;
      },
      [il](BaseValue * tab, const icVariant & value) {
          il::Tab tabPtr = tab->getValue(true);

          il->server->pushTarget(*tabPtr.data);
          il->server->load(value.toString());
          il->server->popTarget();
      });

    return ret;
}

CE * TabValue::window() {
    return new Windows{il, *_value().data};
}

void TabValue::runCookies() {
    m_newContext = cookies();
}

void TabValue::runDoc() {
    m_newContext = doc();
}

void TabValue::runScreenshot() {
    m_newContext = new StringValue{il, screenshot()};
}

void TabValue::runSource() {
    m_newContext = new StringValue{il, source()};
}

void TabValue::runTitle() {
    m_newContext = new StringValue{il, title()};
}

void TabValue::runTabs() {
    m_newContext = tabs();
}

void TabValue::runUrl() {
    m_newContext = url();
}

void TabValue::runWindow() {
    m_newContext = window();
}

void TabValue::runBack(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        back();
        m_newContext = new TabValue{this};
    }
}

void TabValue::runClose(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = new VoidValue{il};
    }
}

void TabValue::runFocus(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        focus();
        m_newContext = new TabValue{this};
    }
}

void TabValue::runForward(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        forward();
        m_newContext = new TabValue{this};
    }
}

void TabValue::runGet(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new BoolValue{il, get(args[0])};
    }
}

void TabValue::runLoad(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        load(args[0]);
        m_newContext = new VoidValue{il};
    }
}

Type TabValue::type() const {
    return Type::TabValue;
}

icString TabValue::typeName() {
    return "tab";
}

void TabValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (TabValue::*)()> properties{
      {"cookies", &TabValue::runCookies},
      {"doc", &TabValue::runDoc},
      {"screenshot", &TabValue::runScreenshot},
      {"source", &TabValue::runSource},
      {"title", &TabValue::runTitle},
      {"tabs", &TabValue::runTabs},
      {"url", &TabValue::runUrl},
      {"window", &TabValue::runWindow}};

    il->server->pushTarget(*_value().data);
    runPropertyWithPrefixCheck<TabValue, BaseValue>(properties, prefix, name);
    il->server->popTarget();
}

void TabValue::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (TabValue::*)(const memory::ArgList &)>
      methods{
        {"back", &TabValue::runBack},   {"close", &TabValue::runClose},
        {"focus", &TabValue::runFocus}, {"forward", &TabValue::runForward},
        {"get", &TabValue::runGet},     {"load", &TabValue::runLoad}};

    il->server->pushTarget(*_value().data);
    runMethodNow<TabValue, BaseValue>(methods, name, args);
    il->server->popTarget();
}

}  // namespace icL::ce
