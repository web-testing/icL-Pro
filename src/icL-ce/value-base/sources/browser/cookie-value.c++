#include "cookie-value.h++"

#include "bool-value.h++"
#include "int-value.h++"
#include "string-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/structures/cookie-data.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-browser/document/cookies.h++>

#include <icL-memory/structures/argument.h++>

#include <utility>

namespace icL::ce {

CookieValue::CookieValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

CookieValue::CookieValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

CookieValue::CookieValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

CookieValue::CookieValue(BaseValue * value)
    : BaseValue(value) {}

CE * CookieValue::cookies() {
    return new Cookies{il, _value().data->target};
}

template <typename RetValueType>
CE * CookieValue::runCookieProperty(
  const std::function<icVariant(const il::CookieData &)> & getter,
  const std::function<void(il::CookieData &, icVariant)> & setter) {
    auto * ret = new RetValueType{this};

    ret->makeFunctional(
      [getter](BaseValue * cookieValue) -> icVariant {
          return getter(*il::Cookie(cookieValue->getValue(true)).data);
      },
      [setter](BaseValue * cookieValue, icVariant property) {
          setter(
            *il::Cookie(cookieValue->getValue(true)).data, std::move(property));
      });

    return ret;
}

CE * CookieValue::domain() {
    return runCookieProperty<StringValue>(
      [](const il::CookieData & data) -> icVariant { return data.domain; },
      [](il::CookieData & data, const icVariant & value) {
          data.domain = value.toString();
      });
}

CE * CookieValue::expiry() {
    return runCookieProperty<IntValue>(
      [](const il::CookieData & data) -> icVariant { return data.expiry; },
      [](il::CookieData & data, const icVariant & value) {
          data.expiry = value.toInt();
      });
}

CE * CookieValue::httpOnly() {
    return runCookieProperty<BoolValue>(
      [](const il::CookieData & data) -> icVariant { return data.httpOnly; },
      [](il::CookieData & data, const icVariant & value) {
          data.httpOnly = value.toBool();
      });
}

CE * CookieValue::name() {
    return runCookieProperty<StringValue>(
      [](const il::CookieData & data) -> icVariant { return data.name; },
      [](il::CookieData & data, const icVariant & value) {
          data.name = value.toString();
      });
}

CE * CookieValue::path() {
    return runCookieProperty<StringValue>(
      [](const il::CookieData & data) -> icVariant { return data.path; },
      [](il::CookieData & data, const icVariant & value) {
          data.path = value.toString();
      });
}

CE * CookieValue::secure() {
    return runCookieProperty<BoolValue>(
      [](const il::CookieData & data) -> icVariant { return data.secure; },
      [](il::CookieData & data, const icVariant & value) {
          data.secure = value.toBool();
      });
}

CE * CookieValue::value() {
    return runCookieProperty<StringValue>(
      [](const il::CookieData & data) -> icVariant { return data.value; },
      [](il::CookieData & data, const icVariant & value) {
          data.value = value.toString();
      });
}

void CookieValue::runCookies() {
    m_newContext = cookies();
}

void CookieValue::runDomain() {
    m_newContext = domain();
}

void CookieValue::runExpiry() {
    m_newContext = expiry();
}

void CookieValue::runHttpOnly() {
    m_newContext = httpOnly();
}

void CookieValue::runName() {
    m_newContext = name();
}

void CookieValue::runPath() {
    m_newContext = path();
}

void CookieValue::runSecure() {
    m_newContext = secure();
}

void CookieValue::runValue() {
    m_newContext = value();
}

void CookieValue::runAdd(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue, Type::IntValue, Type::IntValue})) {
        add(args[0], args[1], args[2]);
        m_newContext = new CookieValue{this};
    }
    else if (checkArgs(
               args, {Type::IntValue, Type::IntValue, Type::IntValue,
                      Type::IntValue})) {
        add(args[0], args[1], args[2], args[3]);
        m_newContext = new CookieValue{this};
    }
    else if (checkArgs(
               args, {Type::IntValue, Type::IntValue, Type::IntValue,
                      Type::IntValue, Type::IntValue})) {
        add(args[0], args[1], args[2], args[3], args[4]);
        m_newContext = new CookieValue{this};
    }
    else if (checkArgs(
               args, {Type::IntValue, Type::IntValue, Type::IntValue,
                      Type::IntValue, Type::IntValue, Type::IntValue})) {
        add(args[0], args[1], args[2], args[3], args[4], args[5]);
        m_newContext = new CookieValue{this};
    }
}

void CookieValue::runLoad(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        load();
        m_newContext = new CookieValue{this};
    }
}

void CookieValue::runResetTime(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        resetTime();
        m_newContext = new CookieValue{this};
    }
}

void CookieValue::runSave(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        save();
        m_newContext = new CookieValue{this};
    }
}

Type CookieValue::type() const {
    return Type::CookieValue;
}

icString CookieValue::typeName() {
    return "cookie";
}

void CookieValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (CookieValue::*)()> properties{
      {"cookies", &CookieValue::runCookies},
      {"domain", &CookieValue::runDomain},
      {"expiry", &CookieValue::runExpiry},
      {"httpOnly", &CookieValue::runHttpOnly},
      {"name", &CookieValue::runName},
      {"path", &CookieValue::runPath},
      {"secure", &CookieValue::runSecure},
      {"value", &CookieValue::runValue}};

    runPropertyWithPrefixCheck<CookieValue, BaseValue>(
      properties, prefix, name);
}

void CookieValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (CookieValue::*)(const memory::ArgList &)>
      methods{{"add", &CookieValue::runAdd},
              {"load", &CookieValue::runLoad},
              {"resetTime", &CookieValue::runResetTime},
              {"save", &CookieValue::runSave}};

    runMethodNow<CookieValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
