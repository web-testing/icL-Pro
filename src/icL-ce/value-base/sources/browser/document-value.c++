#include "document-value.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/mouse-data.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-base/browser/element-value.h++>
#include <icL-ce-value-base/browser/tab-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

DocumentValue::DocumentValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

DocumentValue::DocumentValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

DocumentValue::DocumentValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

DocumentValue::DocumentValue(BaseValue * value)
    : BaseValue(value) {}

void DocumentValue::runTab() {
    m_newContext = new TabValue{il, tab()};
}

void DocumentValue::runClick(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::ClickData data;

        data.loadDictionary(args[0]);
        click(data);

        m_newContext = new DocumentValue{this};
    }
}

void DocumentValue::runHover(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::HoverData data;

        data.loadDictionary(args[0]);
        hover(data);

        m_newContext = new DocumentValue{this};
    }
}

void DocumentValue::runMouseDown(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::MouseData data;

        data.loadDictionary(args[0]);
        mouseDown(data);

        m_newContext = new DocumentValue{this};
    }
}

void DocumentValue::runMouseUp(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        il::MouseData data;

        data.loadDictionary(args[0]);
        mouseUp(data);

        m_newContext = new DocumentValue{this};
    }
}

void DocumentValue::runType(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        typeMethod(args[0]);
        m_newContext = new VoidValue{il};
    }
}

Type DocumentValue::type() const {
    return Type::DocumentValue;
}

icString DocumentValue::typeName() {
    return "Doc";
}

void DocumentValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (DocumentValue::*)()> properties{
      {"tab", &DocumentValue::runTab}};

    il->server->pushTarget(*_value().data);
    runPropertyWithPrefixCheck<DocumentValue, BaseValue>(
      properties, prefix, name);
    il->server->popTarget();
}

void DocumentValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (DocumentValue::*)(const memory::ArgList &)>
      methods{{"click", &DocumentValue::runClick},
              {"hover", &DocumentValue::runHover},
              {"mouseDown", &DocumentValue::runMouseDown},
              {"mouseup", &DocumentValue::runMouseUp},
              {"type", &DocumentValue::runType}};

    il->server->pushTarget(*_value().data);
    runMethodNow<DocumentValue, BaseValue>(methods, name, args);
    il->server->popTarget();
}

}  // namespace icL::ce
