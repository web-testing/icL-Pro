#include "db-value.h++"

#include "query-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/structures/db-target.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-value-base/base/void-value.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

DBValue::DBValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

DBValue::DBValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

DBValue::DBValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

DBValue::DBValue(BaseValue * value)
    : BaseValue(value) {}

void DBValue::runClose(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = new VoidValue{il};
    }
}

void DBValue::runQuery(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new QueryValue{il, query(args[0])};
    }
}

Type DBValue::type() const {
    return Type::DatabaseValue;
}

icString DBValue::typeName() {
    return "db";
}

void DBValue::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (DBValue::*)(const memory::ArgList &)>
      methods{{"close", &DBValue::runClose}, {"query", &DBValue::runQuery}};

    runMethodNow<DBValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
