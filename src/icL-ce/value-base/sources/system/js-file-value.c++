#include "js-file-value.h++"

#include "../base/void-value.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-il/structures/file-target.h++>

#include <icL-service-main/values/set.h++>

#include <icL-ce-base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

JsFileValue::JsFileValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

JsFileValue::JsFileValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

JsFileValue::JsFileValue(BaseValue * value)
    : BaseValue(value) {}

void JsFileValue::runLoad(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new JsFileValue{il, load(args[0])};
    }
}

void JsFileValue::runRun(const memory::ArgList & args) {
    if (checkArgs(args)) {
        m_newContext = new JsFileValue{il, run(service::Set::argsToList(args))};
    }
}

void JsFileValue::runRunAsync(const memory::ArgList & args) {
    if (checkArgs(args)) {
        runAsync(service::Set::argsToList(args));
        m_newContext = new VoidValue{il};
    }
}

void JsFileValue::runSetAsUserScript(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new JsFileValue{il, setAsUserScript()};
    }
}

void JsFileValue::runSetAsPersistentUserScript(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new JsFileValue{il, setAsPersistentUserScript()};
    }
}

memory::Type JsFileValue::type() const {
    return Type::JsFileValue;
}

icString JsFileValue::typeName() {
    return "js-file";
}

void JsFileValue::runMethod(
  const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (JsFileValue::*)(const memory::ArgList &)>
      methods{{"load", &JsFileValue::runLoad},
              {"run", &JsFileValue::runRun},
              {"runAsync", &JsFileValue::runRunAsync},
              {"setAsUserScript", &JsFileValue::runSetAsUserScript},
              {"setAsPersistentUserScript",
               &JsFileValue::runSetAsPersistentUserScript}};

    runMethodNow<JsFileValue, BaseValue>(methods, name, args);
}

il::JsFile JsFileValue::_value() {
    return getValue();
}

}  // namespace icL::ce
