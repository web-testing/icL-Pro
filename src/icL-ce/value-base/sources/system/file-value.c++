#include "file-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/structures/file-target.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/base/void-value.h++>



namespace icL::ce {

FileValue::FileValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

FileValue::FileValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

FileValue::FileValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

FileValue::FileValue(BaseValue * value)
    : BaseValue(value) {}

void FileValue::runFormat() {
    m_newContext = new IntValue{il, format()};
}

void FileValue::runClose(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        close();
        m_newContext = new VoidValue{il};
    }
}

void FileValue::runDelete(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        delete_();
        m_newContext = new VoidValue{il};
    }
}

Type FileValue::type() const {
    return Type::FileValue;
}

icString FileValue::typeName() {
    return "file";
}

void FileValue::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (FileValue::*)()> properties{
      {"format", &FileValue::runFormat}};

    runPropertyWithPrefixCheck<FileValue, BaseValue>(properties, prefix, name);
}

void FileValue::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (FileValue::*)(const memory::ArgList &)>
      methods{{"close", &FileValue::runClose},
              {"delete", &FileValue::runDelete}};

    runMethodNow<FileValue, BaseValue>(methods, name, args);
}

}  // namespace icL::ce
