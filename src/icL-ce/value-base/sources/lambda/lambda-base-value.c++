#include "lambda-base-value.h++"

namespace icL::ce {

LambdaValueBase::LambdaValueBase(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

LambdaValueBase::LambdaValueBase(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

LambdaValueBase::LambdaValueBase(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

LambdaValueBase::LambdaValueBase(BaseValue * value)
    : BaseValue(value) {}

}  // namespace icL::ce
