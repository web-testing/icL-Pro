#include "bool-value.h++"

namespace icL::ce {

BoolValue::BoolValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

BoolValue::BoolValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

BoolValue::BoolValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

BoolValue::BoolValue(BaseValue * value)
    : BaseValue(value) {}

Type BoolValue::type() const {
    return Type::BoolValue;
}

icString BoolValue::typeName() {
    return "bool";
}

}  // namespace icL::ce
