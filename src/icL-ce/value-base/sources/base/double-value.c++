#include "double-value.h++"

namespace icL::ce {

DoubleValue::DoubleValue(
  il::InterLevel * il, memory::DataContainer * container,
  const icString & varName, bool readonly)
    : BaseValue(il, container, varName, readonly) {}

DoubleValue::DoubleValue(il::InterLevel * il, const icVariant & rvalue)
    : BaseValue(il, rvalue) {}

DoubleValue::DoubleValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : BaseValue(il, getter, setter) {}

DoubleValue::DoubleValue(BaseValue * value)
    : BaseValue(value) {}

Type DoubleValue::type() const {
    return Type::DoubleValue;
}

icString DoubleValue::typeName() {
    return "double";
}

}  // namespace icL::ce
