#include "limited-context.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/return.h++>

#include <icL-ce-value-base/browser/element-value.h++>
#include <icL-ce-value-base/complex/list-value.h++>
#include <icL-ce-value-base/complex/object-value.h++>
#include <icL-ce-value-base/complex/set-value.h++>

#include <icL-memory/structures/function-call.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

using memory::PackedValueType;

LimitedContext::LimitedContext(
  il::InterLevel * il, const il::CodeFragment & code)
    : Context(il, code) {}

void LimitedContext::tryToMakeSomething(const memory::PackedItems & items) {
    if (checkItems(items, PackedValueType::Value)) {
        if (checkItems(items, Type::StringValue, Type::ListValue)) {
            m_newContext = new ListValue{il, makeList(items)};
        }
        else if (checkItems(items, Type::ObjectValue, Type::SetValue)) {
            m_newContext = new SetValue{il, makeSet(items)};
        }
        else if (checkItems(items, Type::ElementValue, Type::ElementsValue)) {
            m_newContext = new ElementValue{il, makeElement(items)};
        }
    }
    else if (checkItems(items, PackedValueType::Field)) {
        m_newContext = new ObjectValue{il, makeObject(items)};
    }
    else if (checkItems(items, PackedValueType::Column)) {
        m_newContext = new SetValue{il, makeEmptySet(items)};
    }
    else if (items.isEmpty()) {
        m_newContext = new ListValue{il, icStringList{}};
    }
}

icString LimitedContext::toString() {
    return "[..]";
}

StepType LimitedContext::runNow() {
    if (executed) {
        return StepType::CommandEnd;
    }

    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.contextType = memory::ContextType::Limited;

    il->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code != memory::Signals::NoError) {
            il->vm->signal(ret.signal);
            return false;
        }

        memory::PackedValue packed;

        if (ret.consoleValue.isPacked()) {
            packed = ret.consoleValue;
        }
        else {
            packed.data = std::make_shared<memory::PackedItems>();

            if (!ret.consoleValue.isNull()) {
                memory::PackedValueItem packedItem;

                packedItem.itemType = PackedValueType::Value;
                packedItem.value    = ret.consoleValue;
                packedItem.type     = memory::variantToType(ret.consoleValue);

                packed.data->append(packedItem);
            }
        }

        tryToMakeSomething(*packed.data);

        if (m_newContext == nullptr) {
            il->vm->signal({memory::Signals::System, "No such operator []"});
        }
        return false;
    });

    executed = true;
    return StepType::CommandIn;
}

Role LimitedContext::role() {
    return Role::LimitedContext;
}

const icSet<Role> & LimitedContext::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole,   Role::Any,    Role::Switch,
                                   Role::Case,     Role::For,    Role::Filter,
                                   Role::Range,    Role::Assign, Role::Comma,
                                   Role::Operator, Role::Method};
    return roles;
}

const icSet<Role> & LimitedContext::acceptedNexts() {
    static const icSet<Role> roles{
      Role::NoRole,     Role::Method, Role::Property, Role::Comma,
      Role::RunContext, Role::Cast,   Role::Operator};
    return roles;
}

}  // namespace icL::ce
