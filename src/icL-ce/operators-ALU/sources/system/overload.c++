#include "overload.h++"

#include "run-context.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>

#include <icL-service-main/args/listify.h++>

#include <icL-ce-base/value/base-value.h++>
#include <icL-ce-literal/functional/function.h++>
#include <icL-ce-literal/static/type.h++>
#include <icL-ce-operators-advanced/cast/cast.h++>
#include <icL-ce-value-base/base/void-value.h++>

#include <icL-memory/state/memory.h++>
#include <icL-memory/structures/function.h++>

namespace icL::ce {

Overload::Overload(il::InterLevel * il)
    : SystemOperator(il) {}

int Overload::currentRunRank(bool rtl) {
    return m_next->role() == Role::Value && rtl ? 9 : -1;
}

icString Overload::toString() {
    return ":=";
}

StepType Overload::runNow() {
    memory::Function func;

    auto * next_next = m_next->next();

    if (next_next == nullptr) {
        il->vm->syssig("function: body cannot be skipped");
    }
    else {
        auto nn_body = dynamic_cast<RunContext *>(next_next);
        auto nn_cast = dynamic_cast<Cast *>(next_next);

        if (nn_body != nullptr) {
            func.body       = nn_body->getCode();
            func.returnType = memory::Type::VoidValue;
        }
        else if (nn_cast != nullptr) {
            auto nnn_type = dynamic_cast<TypeToken *>(nn_cast->next());

            if (nnn_type == nullptr) {
                il->vm->syssig("function: a type must go after colon token");
            }
            else {
                auto nnnn_body = dynamic_cast<RunContext *>(nnn_type->next());


                if (nnnn_body == nullptr) {
                    il->vm->syssig(
                      "function: a script must go after return type");
                }
                else {
                    func.body       = nnnn_body->getCode();
                    func.returnType = nnn_type->getType();
                }
            }
        }
    }

    auto n_value = dynamic_cast<BaseValue *>(m_next);

    if (n_value->getValue().isPacked()) {
        func.paramList = service::Listify::toParamList(il, n_value->getValue());
    }
    else {
        il->vm->syssig(
          "function: value context must contains a list of parameter or be "
          "empty");
    }

    il->mem->functions().registerFunction(
      dynamic_cast<Function *>(m_prev)->getName(), func);

    m_newContext = new VoidValue{il};

    return StepType::CommandEnd;
}

Role Overload::role() {
    return Role::Overload;
}

CE * Overload::firstToReplace() {
    return m_prev;
}

CE * Overload::lastToReplace() {
    CE * node = m_next;

    while (node->role() != Role::RunContext) {
        node = dynamic_cast<CE *>(node->next());
    }

    return node;
}

const icSet<Role> & Overload::acceptedPrevs() {
    static const icSet<Role> roles{Role::Function};
    return roles;
}

const icSet<Role> & Overload::acceptedNexts() {
    static const icSet<Role> roles{Role::Value, Role::ValueContext};
    return roles;
}

void Overload::run(
  const memory::ArgList & /*left*/, const memory::ArgList & /*right*/) {
    assert(false);
}

}  // namespace icL::ce
