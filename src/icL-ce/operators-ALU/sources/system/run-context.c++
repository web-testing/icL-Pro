#include "run-context.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-memory/structures/function-call.h++>

#include <utility>

namespace icL::ce {

RunContext::RunContext(
  il::InterLevel * il, const il::CodeFragment & code, const icString & name)
    : Context(il, code)
    , name(name) {}

icString RunContext::toString() {
    return "{..}";
}

int RunContext::currentRunRank(bool rtl) {
    bool runnable = m_next == nullptr && m_prev == nullptr;
    return runnable && rtl ? 1 : -1;
}

StepType RunContext::runNow() {
    if (executed) {
        return StepType::CommandEnd;
    }

    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.contextName = name;
    fcall.contextType = memory::ContextType::Run;

    il->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code != memory::Signals::NoError) {
            il->vm->signal(ret.signal);
            return false;
        }

        m_newContext = service::Factory::fromValue(il, ret.returnValue);
        return false;
    });

    executed = true;
    return StepType::CommandIn;
}

Role RunContext::role() {
    return Role::RunContext;
}

const icSet<Role> & RunContext::acceptedPrevs() {
    static const icSet<Role> roles{
      Role::NoRole, Role::Value,   Role::Else,           Role::Slot,
      Role::Wait,   Role::Do,      Role::LimitedContext, Role::ValueContext,
      Role::Type,   Role::Emitter, Role::Jammer};
    return roles;
}

const icSet<Role> & RunContext::acceptedNexts() {
    static const icSet<Role> roles{Role::NoRole, Role::Case, Role::Else,
                                   Role::Slot, Role::While};
    return roles;
}

}  // namespace icL::ce
