#include "value-context.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmstack.h++>
#include <icL-il/structures/return.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-memory/structures/function-call.h++>

namespace icL::ce {

ValueContext::ValueContext(il::InterLevel * il, const il::CodeFragment & code)
    : Context(il, code) {}

icString ValueContext::toString() {
    return "(..)";
}

StepType ValueContext::runNow() {
    if (executed) {
        return StepType::CommandEnd;
    }

    memory::FunctionCall fcall;

    fcall.code        = code;
    fcall.contextType = memory::ContextType::Value;

    il->vms->interrupt(fcall, [this](const il::Return & ret) {
        if (ret.signal.code != memory::Signals::NoError) {
            il->vm->signal(ret.signal);
            return false;
        }

        m_newContext = service::Factory::fromValue(il, ret.consoleValue);
        return false;
    });

    executed = true;
    return StepType::CommandIn;
}

Role ValueContext::role() {
    return Role::ValueContext;
}

const icSet<Role> & ValueContext::acceptedPrevs() {
    static const icSet<Role> roles{
      Role::NoRole,  Role::Any,    Role::Case,  Role::Exists,   Role::If,
      Role::Switch,  Role::Assert, Role::Emit,  Role::Listen,   Role::Filter,
      Role::For,     Role::Range,  Role::While, Role::Method,   Role::Function,
      Role::Assign,  Role::Comma,  Role::Cast,  Role::Operator, Role::JsFile,
      Role::Overload};
    return roles;
}

const icSet<Role> & ValueContext::acceptedNexts() {
    static const icSet<Role> roles{
      Role::NoRole,     Role::Method, Role::Property, Role::Assign, Role::Comma,
      Role::RunContext, Role::Cast,   Role::Operator, Role::Case};
    return roles;
}

}  // namespace icL::ce
