#include "context.h++"

#include <utility>

namespace icL::ce {

Context::Context(il::InterLevel * il, const il::CodeFragment & code)
    : SystemOperator(il)
    , code(code) {}

const il::CodeFragment & Context::getCode() {
    return code;
}

int Context::currentRunRank(bool rtl) {
    return !rtl ? 8 : -1;
}

void Context::run(const memory::ArgList &, const memory::ArgList &) {
    assert(false);
}

CE * Context::firstToReplace() {
    return this;
}

CE * Context::lastToReplace() {
    return this;
}

}  // namespace icL::ce
