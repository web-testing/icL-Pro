#include "selective-select.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-value-base/base/double-value.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/browser/element-value.h++>
#include <icL-ce-value-base/complex/list-value.h++>
#include <icL-ce-value-base/complex/set-value.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

SelectiveSelect::SelectiveSelect(il::InterLevel * il)
    : LogicOperator(il) {}

icString SelectiveSelect::toString() {
    return "%";
}

void SelectiveSelect::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    if (left[0].type == Type::VoidValue) {
        m_newContext = service::Factory::fromValue(il, right[0].value);
    }
    if (right[0].type == Type::VoidValue) {
        m_newContext = service::Factory::fromValue(il, left[0].value);
    }
    else {
        switch (left[0].type) {
        case Type::IntValue:
            if (right[0].type == Type::IntValue) {
                m_newContext = new IntValue{il, intInt(left[0], right[0])};
            }
            break;

        case Type::DoubleValue:
            if (right[0].type == Type::DoubleValue) {
                m_newContext =
                  new DoubleValue{il, doubleDouble(left[0], right[0])};
            }
            break;

        case Type::StringValue:
            if (right[0].type == Type::StringValue) {
                m_newContext =
                  new ListValue{il, stringString(left[0], right[0])};
            }
            else if (right[0].type == Type::ListValue) {
                m_newContext = new ListValue{il, stringList(left[0], right[0])};
            }
            break;

        case Type::ListValue:
            if (right[0].type == Type::StringValue) {
                m_newContext = new ListValue{il, listString(left[0], right[0])};
            }
            else if (right[0].type == Type::ListValue) {
                m_newContext = new ListValue{il, listList(left[0], right[0])};
            }
            break;

        case Type::SetValue:
            if (right[0].type == Type::SetValue) {
                m_newContext = new SetValue{il, setSet(left[0], right[0])};
            }
            else if (right[0].type == Type::ObjectValue) {
                m_newContext = new SetValue{il, setObject(left[0], right[0])};
            }
            break;

        case Type::ObjectValue:
            if (right[0].type == Type::SetValue) {
                m_newContext = new SetValue{il, objectSet(left[0], right[0])};
            }
            else if (right[0].type == Type::ObjectValue) {
                m_newContext =
                  new SetValue{il, objectObject(left[0], right[0])};
            }
            break;

        case Type::ElementValue:
            if (right[0].type == Type::ElementValue) {
                m_newContext =
                  new ElementValue{il, il->server->add_11(left[0], right[0])};
            }
            break;

        default:
            void();  // Elude warning
        }
    }

    if (m_newContext == nullptr) {
        il->vm->signal({memory::Signals::System, "No such operator"});
    }
}

}  // namespace icL::ce
