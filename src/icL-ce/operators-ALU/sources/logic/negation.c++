#include "negation.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>

#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

Negation::Negation(il::InterLevel * il)
    : LogicOperator(il) {}

int Negation::currentRunRank(bool rtl) {
    bool runnable =
      m_next->hasValue() &&
      dynamic_cast<BaseValue *>(m_next)->type() == Type::BoolValue;
    return runnable && rtl ? 7 : -1;
}

CE * Negation::firstToReplace() {
    return this;
}

const icSet<Role> & Negation::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole, Role::Function,
                                   Role::Method, Role::Assign,
                                   Role::Comma,  Role::Operator};
    return roles;
}

icString Negation::toString() {
    return "!";
}

void Negation::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    if (left.isEmpty()) {
        m_newContext = new BoolValue{il, !right[0].value};
    }
}



}  // namespace icL::ce
