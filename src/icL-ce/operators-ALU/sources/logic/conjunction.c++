#include "conjunction.h++"

#include <icL-types/replaces/ic-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/void-value.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Conjunction::Conjunction(il::InterLevel * il)
    : LogicOperator(il) {}

bool Conjunction::run(bool left, bool right) {
    return left && right;
}

icString Conjunction::toString() {
    return "&";
}

void Conjunction::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    if (left[0].type == Type::BoolValue && right[0].type == Type::BoolValue) {
        m_newContext = new BoolValue{il, run(left[0], right[0])};
    }
    else if (
      left[0].type == Type::VoidValue || right[0].type == Type::VoidValue) {
        m_newContext = new VoidValue{il};
    }
    else if (left[0].type == right[0].type) {
        m_newContext = service::Factory::fromValue(il, left[0].value);
    }
    else {
        il->vm->signal({memory::Signals::System, "No such operator"});
    }
}

}  // namespace icL::ce
