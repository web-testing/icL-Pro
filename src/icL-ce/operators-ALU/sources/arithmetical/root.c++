#include "root.h++"

#include <icL-types/replaces/ic-math.h++>

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/double-value.h++>
#include <icL-ce-value-base/base/int-value.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

Root::Root(il::InterLevel * il)
    : AmbiguousArithmeticalOperator(il) {}

void Root::runInt(
  const memory::ArgList & /*left*/, const memory::ArgList & right) {
    m_newContext = new IntValue{il, voidInt(right[0])};
}

void Root::runDouble(
  const memory::ArgList & /*left*/, const memory::ArgList & right) {
    m_newContext = new DoubleValue{il, voidDouble(right[0])};
}

void Root::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new IntValue{il, intInt(left[0], right[0])};
}

void Root::runIntDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new DoubleValue{il, intDouble(left[0], right[0])};
}

void Root::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new DoubleValue{il, doubleDouble(left[0], right[0])};
}

icString Root::toString() {
    return "/'";
}

void Root::run(const memory::ArgList & left, const memory::ArgList & right) {
    using memory::Type;

    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (Root::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{}, {Type::IntValue}}, &Root::runInt},
        {{{}, {Type::DoubleValue}}, &Root::runDouble},
        {{{Type::IntValue}, {Type::IntValue}}, &Root::runIntInt},
        {{{Type::IntValue}, {Type::DoubleValue}}, &Root::runIntDouble},
        {{{Type::DoubleValue}, {Type::DoubleValue}}, &Root::runDoubleDouble}};

    runNow<Root>(operators, left, right);
}

int Root::runRank() {
    return 5;
}

int Root::runAbmiguousRank() {
    return 5;
}

}  // namespace icL::ce
