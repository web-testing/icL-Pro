#include "remainder.h++"

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/complex/set-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>

namespace icL::ce {

Remainder::Remainder(il::InterLevel * il)
    : ArithmeticalOperator(il) {}

void Remainder::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new IntValue{il, intInt(left[0], right[0])};
}

void Remainder::runSetSet(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new SetValue{il, setSet(left[0], right[0])};
}

icString Remainder::toString() {
    return "\\";
}

void Remainder::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    using memory::Type;

    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (Remainder::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue}}, &Remainder::runIntInt},
                {{{Type::SetValue}, {Type::SetValue}}, &Remainder::runSetSet}};

    runNow<Remainder>(operators, left, right);
}

int Remainder::runRank() {
    return 5;
}

}  // namespace icL::ce
