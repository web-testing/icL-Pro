#include "subtraction.h++"

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/double-value.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/complex/set-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>

namespace icL::ce {

Subtraction::Subtraction(il::InterLevel * il)
    : AmbiguousArithmeticalOperator(il) {}

void Subtraction::runInt(
  const memory::ArgList & /*left*/, const memory::ArgList & right) {
    m_newContext = new IntValue{il, voidInt(right[0])};
}

void Subtraction::runDouble(
  const memory::ArgList & /*left*/, const memory::ArgList & right) {
    m_newContext = new DoubleValue{il, voidDouble(right[0])};
}

void Subtraction::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new IntValue{il, intInt(left[0], right[0])};
}

void Subtraction::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new DoubleValue{il, doubleDouble(left[0], right[0])};
}

void Subtraction::runSetSet(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new SetValue{il, setSet(left[0], right[0])};
}

icString Subtraction::toString() {
    return "-";
}

void Subtraction::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    using memory::Type;

    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (Subtraction::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{}, {Type::IntValue}}, &Subtraction::runInt},
        {{{}, {Type::DoubleValue}}, &Subtraction::runDouble},
        {{{Type::IntValue}, {Type::IntValue}}, &Subtraction::runIntInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}},
         &Subtraction::runDoubleDouble},
        {{{Type::SetValue}, {Type::SetValue}}, &Subtraction::runSetSet}};

    runNow<Subtraction>(operators, left, right);
}

int Subtraction::runRank() {
    return 4;
}

int Subtraction::runAbmiguousRank() {
    return 7;
}

}  // namespace icL::ce
