#include "multiplication.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/double-value.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/complex/set-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

Multiplication::Multiplication(il::InterLevel * il)
    : ArithmeticalOperator(il) {}

void Multiplication::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new IntValue{il, intInt(left[0], right[0])};
}

void Multiplication::runDoulbleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new DoubleValue{il, doubleDouble(left[0], right[0])};
}

void Multiplication::runStringString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, stringString(left[0], right[0])};
}

void Multiplication::runListString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, listString(left[0], right[0])};
}

void Multiplication::runListList(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, listList(left[0], right[0])};
}

void Multiplication::runSetSet(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new SetValue{il, setSet(left[0], right[0])};
}

icString Multiplication::toString() {
    return "*";
}

void Multiplication::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    using memory::Type;

    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (Multiplication::*)(
        const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{Type::IntValue}, {Type::IntValue}}, &Multiplication::runIntInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}},
         &Multiplication::runDoulbleDouble},
        {{{Type::StringValue}, {Type::StringValue}},
         &Multiplication::runStringString},
        {{{Type::ListValue}, {Type::StringValue}},
         &Multiplication::runListString},
        {{{Type::ListValue}, {Type::ListValue}}, &Multiplication::runListList},
        {{{Type::SetValue}, {Type::SetValue}}, &Multiplication::runSetSet}};

    runNow<Multiplication>(operators, left, right);
}

int Multiplication::runRank() {
    return 5;
}

}  // namespace icL::ce
