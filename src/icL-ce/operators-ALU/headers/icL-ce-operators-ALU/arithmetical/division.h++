#ifndef ce_Division
#define ce_Division

#include <icL-service-operators-ALU/arithmetical/division.h++>

#include <icL-ce-base/alu-operator/arithmetical-operator.h++>



namespace icL::ce {

class Division
    : public ArithmeticalOperator
    , public service::Division
{
public:
    Division(il::InterLevel * il);

    // level 2

    /// `int / int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double / double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;
};

}  // namespace icL::ce

#endif  // ce_Division
