#ifndef ce_Remainder
#define ce_Remainder

#include <icL-service-operators-ALU/arithmetical/remainder.h++>

#include <icL-ce-base/alu-operator/arithmetical-operator.h++>



namespace icL {

namespace memory {
struct Set;
}

namespace ce {

class Remainder
    : public ArithmeticalOperator
    , public service::Remainder
{
public:
    Remainder(il::InterLevel * il);

    // level 2

    /// `int \ int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `set \ set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Remainder
