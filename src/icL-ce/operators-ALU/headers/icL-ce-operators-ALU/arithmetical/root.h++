#ifndef ce_Root
#define ce_Root

#include <icL-service-operators-ALU/arithmetical/root.h++>

#include <icL-ce-base/alu-operator/ambiguous-arithmetical-operator.h++>



namespace icL::ce {

class Root
    : public AmbiguousArithmeticalOperator
    , public service::Root
{
public:
    Root(il::InterLevel * il);

    // level 2

    /// `/' int`
    void runInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `/' double`
    void runDouble(const memory::ArgList & left, const memory::ArgList & right);

    /// `int /' int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `int /' double`
    void runIntDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `double /' double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;

    // ArithmeticalOperator interface
protected:
    int runRank() override;

    // AmbiguousArithmeticalOperator interface
protected:
    int runAbmiguousRank() override;
};

}  // namespace icL::ce

#endif  // ce_Root
