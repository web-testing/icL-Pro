#ifndef ce_Context
#define ce_Context

#include <icL-il/structures/code-fragment.h++>

#include <icL-ce-base/alu-operator/system-operator.h++>



namespace icL::ce {

/**
 * @brief The Context class represent any bracket-isolated code `[] () {}`
 */
class Context : public SystemOperator
{
public:
    Context(il::InterLevel * il, const il::CodeFragment &code);

    /**
     * @brief getCode gets the code fragment of context
     * @return the code fragment of context
     */
    const il::CodeFragment & getCode();

    // CE interface
public:
    int currentRunRank(bool rtl) override;

    // Operator interface
public:
    void run(const memory::ArgList &, const memory::ArgList &) override;

    // fields
protected:
    /// \brief code is the fragment of code isolated in brackets
    il::CodeFragment code;
    /// \brief executed is true if the code was executed, otherwise false
    bool executed = false;

    // padding
    long : 56;

    // CE interface
public:
    CE * firstToReplace() override;
    CE * lastToReplace() override;
};

}  // namespace icL::ce

#endif  // ce_Context
