#ifndef ce_Overload
#define ce_Overload

#include <icL-ce-base/alu-operator/system-operator.h++>



namespace icL::ce {

/**
 * @brief The Overload class represent `:=` token
 */
class Overload : public SystemOperator
{
public:
    Overload(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    icString toString() override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    CE * firstToReplace() override;
    CE * lastToReplace() override;

    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_Overload
