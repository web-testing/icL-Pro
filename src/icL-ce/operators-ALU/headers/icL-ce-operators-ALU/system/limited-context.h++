#ifndef ce_LimitedContext
#define ce_LimitedContext

#include "context.h++"

#include <icL-service-operators-ALU/system/limited-context.h++>



namespace icL::ce {

/**
 * @brief The LimitedContext class represents a limited context `[]`
 */
class LimitedContext
    : public Context
    , public service::LimitedContext
{
public:
    LimitedContext(il::InterLevel * il, const il::CodeFragment & code);

    /**
     * @brief tryToMakeSomething tries to create a value from packed items
     * @param items are the result of parsing the inner context
     */
    void tryToMakeSomething(const memory::PackedItems & items);

    // CE interface
public:
    icString toString() override;
    StepType runNow() override;
    Role     role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;
};

}  // namespace icL::ce

#endif  // ce_LimitedContext
