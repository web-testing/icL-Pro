#ifndef ce_ValueContext
#define ce_ValueContext

#include "context.h++"



namespace icL::ce {

class ValueContext : public Context
{
public:
    ValueContext(il::InterLevel * il, const il::CodeFragment & code);

    // CE interface
public:
    icString toString() override;
    StepType runNow() override;
    Role     role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;
};

}  // namespace icL::ce

#endif  // ce_ValueContext
