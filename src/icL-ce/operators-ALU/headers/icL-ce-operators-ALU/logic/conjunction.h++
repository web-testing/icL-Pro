#ifndef ce_Conjuction
#define ce_Conjuction

#include <icL-ce-base/alu-operator/logic-operator.h++>



class icVariant;

namespace icL::ce {

/**
 * @brief The Conjunction class represents conjunction operator token `&`
 */
class Conjunction : public LogicOperator
{
public:
    Conjunction(il::InterLevel * il);

    /**
     * @brief run applicates the logic operation conjuction
     * @param left is the left operand
     * @param right is the right operand
     * @return true if both the left and the right are true, otherwise false
     */
    bool run(bool left, bool right);

    // CE interface
public:
    icString toString() override;

    // Operator interface

    /**
     * @brief run applicates primary select operation
     */
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_Conjuction
