#ifndef ce_Disjunction
#define ce_Disjunction

#include <icL-ce-base/alu-operator/logic-operator.h++>



class icVariant;

namespace icL::ce {

/**
 * @brief The Disjunction class represents a disjynction operator token `|`
 */
class Disjunction : public LogicOperator
{
public:
    Disjunction(il::InterLevel * il);

    /**
     * @brief run applicates the logical operation disjunction
     * @param left is the left operand
     * @param right is the right operand
     * @return true if any of operands is true, otherwise false
     */
    bool run(bool left, bool right);

    // CE interface
public:
    icString toString() override;

    // Operator interface

    /**
     * @brief run applicates the alternative select operation
     */
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_Disjunction
