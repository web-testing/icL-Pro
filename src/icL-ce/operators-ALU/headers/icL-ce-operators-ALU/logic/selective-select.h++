#ifndef ce_SelectiveSelect
#define ce_SelectiveSelect

#include <icL-service-operators-ALU/logic/selective-select.h++>

#include <icL-ce-base/alu-operator/logic-operator.h++>



class icString;
class icStringList;

namespace icL {

namespace memory {
struct Set;
struct Object;
}  // namespace memory

namespace ce {

class SelectiveSelect
    : public LogicOperator
    , public service::SelectiveSelect
{
public:
    SelectiveSelect(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    /**
     * @brief run applicates selective select operation
     */
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_SelectiveSelect
