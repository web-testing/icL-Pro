#ifndef ce_ExclusiveDisjunction
#define ce_ExclusiveDisjunction

#include <icL-ce-base/alu-operator/logic-operator.h++>



class icVariant;

namespace icL::ce {

/**
 * @brief The ExclusiveDisjunction class represent a excl. disjuction `^` token
 */
class ExclusiveDisjunction : public LogicOperator
{
public:
    ExclusiveDisjunction(il::InterLevel * il);

    /**
     * @brief run applicates the logical exclusive disjunction operation
     * @param left is the left operand
     * @param right is the right operand
     * @return true if just one of operands is true, otherwise false
     */
    bool run(bool left, bool right);

    // CE interface
public:
    icString toString() override;

    // Operator interface

    /**
     * @brief run applicates the exclusive select operation
     */
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_ExclusiveDisjunction
