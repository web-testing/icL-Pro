#ifndef ce_FunctionalJsLiteral
#define ce_FunctionalJsLiteral

#include "../main/literal.h++"



namespace icL::ce {

class FunctionalJsLiteral : public Literal
{
public:
    FunctionalJsLiteral(il::InterLevel * il);

    // CE interface
protected:
    const icSet<Role> &acceptedPrevs();
    const icSet<Role> &acceptedNexts();
};

}  // namespace icL::ce

#endif  // ce_FunctionalJsLiteral
