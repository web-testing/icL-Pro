#ifndef ce_StaticLiteral
#define ce_StaticLiteral

#include "../main/literal.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-memory/structures/packed-value-item.h++>



namespace icL::ce {

class StaticLiteral : public Literal
{
public:
    StaticLiteral(il::InterLevel * il)
        : Literal(il) {}

    virtual ~StaticLiteral() override = default;

    // CE interface
public:
    int currentRunRank(bool /*rtl*/) override {
        return -1;
    }
    StepType runNow() override {
        return StepType::None;
    }
    bool hasValue() override {
        return true;
    }

    icVariant getValue(bool /*excludeFunctional*/ = false) override {
        memory::PackedValue value;

        value.data = std::make_shared<memory::PackedItems>();
        value.data->append(packNow());

        return value;
    }

protected:
    const icSet<Role> & acceptedPrevs() override {
        static const icSet<Role> accepted{Role::NoRole, Role::Comma,
                                          Role::Cast};
        return accepted;
    }
    const icSet<Role> & acceptedNexts() override {
        static const icSet<Role> accepted{Role::NoRole, Role::RunContext,
                                          Role::Comma};
        return accepted;
    }
};



}  // namespace icL::ce

#endif  // ce_StaticLiteral
