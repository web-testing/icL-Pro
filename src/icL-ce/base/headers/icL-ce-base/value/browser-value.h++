#ifndef ce_BrowserValue
#define ce_BrowserValue

#include "../main/value.h++"



namespace icL::ce {

/**
 * @brief The BrowserValue class as system value which manipulates a browser
 */
class BrowserValue : public Value
{
public:
    BrowserValue(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
};

}  // namespace icL::ce

#endif  // ce_BrowserValue
