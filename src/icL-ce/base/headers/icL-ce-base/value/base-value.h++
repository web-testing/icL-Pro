#ifndef ce_BaseValue
#define ce_BaseValue

#include "../main/value.h++"

#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-service-main/values/ivalue.h++>

#include <functional>

namespace icL {

namespace memory {
class DataContainer;
}

namespace ce {

/**
 * @brief The BaseValue class contains all commom posibilities of values
 */
class BaseValue
    : public Value
    , virtual public service::IValue
{
public:
    /**
     * @brief BaseValue constructs a L-Value
     * @param container is a DataState pointer
     * @param varName is the name of var in container
     * @param readonly if true restricts assigns
     */
    BaseValue(il::InterLevel * il, memory::DataContainer * container, const icString &varName,
      bool readonly = false);

    /**
     * @brief BaseValue constucts a R-Value
     * @param rvalue is the value of Value
     */
    BaseValue(il::InterLevel * il, icVariant rvalue);

    /**
     * @brief BaseValue constructs a JS-Value
     * @param getter is a getter icString, eg. window.height
     * @param setter is a setter icString, eg. window.height = ${value}
     */
    BaseValue(il::InterLevel * il, const icString &getter, const icString &setter);

    /**
     * @brief BaseValue create a copy of
     * @param value is the value to copy
     */
    BaseValue(BaseValue * value);

    /**
     * @brief makeFunctional makes this value icL value
     * @param fgetter is the functional getter of value
     * @param fsetter is the functional setter of value
     */
    void makeFunctional(
      const std::function<icVariant(BaseValue *)> &       fgetter,
      const std::function<void(BaseValue *, icVariant)> & fsetter);

    /**
     * @brief isLValue checks if this is a left value
     * @return true if is a lvalue, otherwise false
     */
    bool isLValue() const;

    /**
     * @brief isRValue checks if this is a right value
     * @return true if is a rvalue, otherwise false
     */
    bool isRValue() const;

    /**
     * @brief rebind lvalue to the last stack
     */
    void rebind();

    // methods level 1

    /**
     * @brief ensureRValue copies the value from the container
     * @return the new created icL Value
     */
    CE * ensureRValue();

    // methods level 2

    /// \brief runTypeId `.ensureRValue` level 2
    void runEnsureRValue(const memory::ArgList & args);

    // CE interface
public:
    bool hasValue() override;

    // CE interface
public:
    icString toString() override;
    Role     role() override;

    memory::PackedValueItem packNow() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // IValue interface
public:
    icVariant getValue(bool excludeFunctional = false) override;
    void      setValue(
           const icVariant & value, bool excludeFunctional = false) override;

    // Value interface
public:
    void runMethod(
      const icString & name, const memory::ArgList & args) override;

    // class fields

protected:
    // L-Value
    /// \brief container is the container of value
    memory::DataContainer * container = nullptr;
    /// \brief varName is the name of varaible in continer
    icString varName;

    // JS-Value
    /// \brief setter is the setter of JS-Value ("" for readonly)
    icString setter;
    /// \brief getter is the getter of JS-Value
    icString getter;

    // R-Value
    /// \brief rvalue is the value of a R-Value or last cached JS-Value
    icVariant rvalue;

    // Functional value
    /// \brief fgetter is the functional getter
    std::function<icVariant(BaseValue *)> fgetter;
    /// \brief fsetter is the functional setter
    std::function<void(BaseValue *, icVariant)> fsetter;

    /**
     * @brief The Value enum decribes the type of a value
     */
    enum class ValueType {
        L,  ///< L-Value
        R,  ///< R-Value
        Js  ///< JS-Value
    };

    ValueType valuetype;  ///< The type of contained value

    /// \brief readonly if true will generate an exception on assign
    bool readonly = false;

    // Fix size warning
    int : 24;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_BaseValue
