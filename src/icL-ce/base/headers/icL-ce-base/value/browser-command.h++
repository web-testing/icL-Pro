#ifndef ce_BrowserCommand
#define ce_BrowserCommand

#include "browser-value.h++"



namespace icL::ce {

class BrowserCommand : public BrowserValue
{
public:
    BrowserCommand(il::InterLevel * il);

    // CE interface
protected:
    const icSet<Role> & acceptedNexts();
};

}  // namespace icL::ce

#endif  // ce_BrowserCommand
