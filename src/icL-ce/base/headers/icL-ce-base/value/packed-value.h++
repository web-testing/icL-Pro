#ifndef ce_PackedValue
#define ce_PackedValue

#include "base-value.h++"

#include <icL-memory/structures/packed-value-item.h++>


namespace icL::ce {

/**
 * @brief The PackedValue class represent a icL packed value (read standard)
 */
class PackedValue : public BaseValue
{
public:
    /**
     * @brief PackedValue constructs a packed value on values merge
     * @param il is the inter-level node
     * @param first is the left comma operand
     * @param second is the right comma operand
     */
    PackedValue(
      il::InterLevel * il, CE * first = nullptr, CE * second = nullptr);

    /**
     * @brief PackedValue copy a packed value
     * @param il is the interl-level node
     * @param other is the pack to copy
     */
    PackedValue(il::InterLevel * il, const memory::PackedValue & other);

    /**
     * @brief PackedValue create a copy of
     * @param value is the value to copy
     */
    PackedValue(BaseValue * value);

    /**
     * @brief getValues gets the packed values
     * @return the packed values
     */
    const memory::PackedValue & getValues();

protected:
    /**
     * @brief extractDataFrom extracts data from a CE node
     * @param ce is the node to extract data of
     */
    void extractDataFrom(CE * ce);

    // CE interface
protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // Value interface
public:
    memory::Type type() const override;
    icString     typeName() override;

    // BaseValue interface
public:
    icVariant getValue(bool excludeFunctional) override;

    // fields
private:
    memory::PackedValue values;
};

}  // namespace icL::ce

#endif  // ce_PackedValue
