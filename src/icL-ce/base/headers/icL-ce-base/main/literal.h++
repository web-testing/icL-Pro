#ifndef ce_Literal
#define ce_Literal

#include "ce.h++"



namespace icL::ce {

class Literal : public CE
{
public:
    Literal(il::InterLevel * il);
};

}  // namespace icL::ce

#endif  // ce_Literal
