#ifndef ce_Operator_runNow
#define ce_Operator_runNow

#include "operator.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/args/listify-args-to-list.h++>
#include <icL-service-main/args/listify.h++>

#include <icL-memory/state/signals.h++>

namespace icL::memory {

}  // namespace icL::memory

namespace icL::ce {

template <typename This>
void Operator::runNow(
  const icObject<
    icPair<icList<memory::Type>, icList<memory::Type>>,
    void (This::*)(const memory::ArgList &, const memory::ArgList &)> &
                          operators,
  const memory::ArgList & left, const memory::ArgList & right) {

    auto this_ = dynamic_cast<This *>(this);

    icList<memory::Type> leftTypes =
      service::Listify::fromArgs<memory::Type>(left);
    icList<memory::Type> rightTypes =
      service::Listify::fromArgs<memory::Type>(right);

    auto it = operators.find({leftTypes, rightTypes});

    if (it == operators.end()) {
        il->vm->signal({memory::Signals::System, "No such operator"});
    }
    else {
        (this_->*it.value())(left, right);
    }
}

}  // namespace icL::ce

#endif  // ce_Operator_runNow
