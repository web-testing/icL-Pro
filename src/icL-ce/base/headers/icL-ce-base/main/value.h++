#ifndef ce_Value
#define ce_Value

#include "ce.h++"

#include <icL-memory/structures/parameter.h++>

namespace std {
template <typename>
class function;
}

template <typename, typename>
class icObject;

namespace icL::ce {

/**
 * @brief The Prefix enum describes aviables prefixes for properties
 */
enum class Prefix {  ///< Possible prefixes of a property
    Attr,            ///< `attr-*`
    CSS,             ///< `css-*`
    Prop,            ///< `prop-*`
    None             ///< `*`
};

/**
 * @brief The BaseValue class contains all commom posibilities of values
 */
class Value : public CE
{

public:
    Value(il::InterLevel * il);

    /**
     * @brief type returs the type of contained value
     * @return the default value (memory.Type.Void)
     */
    virtual Type type() const = 0;

    // properties level 1

    /**
     * @brief typeId gets the indentifier of type
     * @return the indentifier of type as int
     */
    virtual int typeId();

    /**
     * @brief typeName gets the name of type
     * @return the name of type
     */
    virtual icString typeName() = 0;

    // properties level 2

    /// \brief `any'typeId`
    void runTypeId();

    /// \brief `any'typeName`
    void runTypeName();

    // methods/properties level 3

    /**
     * @brief runProperty returns the value of property, a part of high-level
     * interface
     * @param prefix is the prefix of property, web elements has prefixed
     * properties
     * @param name is the name of requested property
     */
    virtual void runProperty(Prefix prefix, const icString & name);

    /**
     * @brief runMethod runs a method of object, a part of high-level
     * interface
     * @param name the name of property to run
     * @param args is the arguments of method
     */
    virtual void runMethod(const icString & name, const memory::ArgList & args);

    /**
     * @brief runMeta runs a meta method of object, a part of high-level
     * interface
     * @param name is the name of meta-method
     * @param params is the parameters list of meta-method
     */
    virtual void runMeta(
      const icString & name, const memory::ParamList & params);

protected:
    /**
     * @brief sendNoSuchProperty send a signal with error
     * @param prefix is the prefix of unexisting property
     * @param name is the name of unexisting property
     */
    void sendNoSuchProperty(Prefix prefix, const icString & name);

    /**
     * @brief sendnoSuchMethod send a signal with error
     * @param name is the name of the method
     * @param args is the argument of call
     */
    void sendNoSuchMethod(const icString & name, const memory::ArgList & args);

    /**
     * @brief sendNoSuchMeta send a signal with error
     * @param name is the name of meta-method
     */
    void sendNoSuchMeta(const icString & name);

protected:
    /**
     * @brief runMethod runs a method from hash map
     * @param methods are the hash map of methods
     * @param name is the name of needed method
     * @param args are the argument icList for method
     */
    template <typename This, typename ParentClass>
    void runMethodNow(
      const icObject<icString, void (This::*)(const memory::ArgList &)> &
                       methods,
      const icString & name, const memory::ArgList & args);

    /**
     * @brief runProperty runs a property from hash map
     * @param properties is the hash map of properties
     * @param prefix is the prefix of property
     * @param name is the name of property
     */
    template <typename This, typename ParentClass>
    void runPropertyNow(
      const icObject<icString, void (This::*)()> & properties, Prefix prefix,
      const icString & name);

    /**
     * @brief runPropertyWithPrefixCheck runs a property and checks if prefix is
     * none, if property is not none, the parent handler will be called
     * @param properties is the hash map of properties
     * @param prefix is the prefix of property
     * @param name is the name of property
     */
    template <typename This, typename ParentClass>
    void runPropertyWithPrefixCheck(
      const icObject<icString, void (This::*)()> & properties, Prefix prefix,
      const icString & name);

    /**
     * @brief runOwnPropertyWithPrefixCheck runs a own property and checks is
     * prefix is none, if property is not none, the parent handler wll be called
     * @param prefix is the prefix of property
     * @param name is the name of property
     */
    template <typename This, typename ParentClass>
    void runOwnPropertyWithPrefixCheck(
      void (This::*property)(const icString &), Prefix prefix,
      const icString & name);

    /**
     * @brief runPropertyWithIndexCheck runs a property with '(i : int) support
     * @param properties is the hash map of properties
     * @param prefix is the prefix of property
     * @param name is the name of property
     */
    template <typename This, typename ParentClass, typename ReturnClass>
    void runPropertyWithIndexCheck(
      const icObject<icString, void (This::*)()> & properties, Prefix prefix,
      const icString & name, const std::function<icVariant(int)> & at);

    // CE interface
public:
    int      currentRunRank(bool) override;
    Role     role() override;
    StepType runNow() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;
};

}  // namespace icL::ce

#endif  // ce_Value
