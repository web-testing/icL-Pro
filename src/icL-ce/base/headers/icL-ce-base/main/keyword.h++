#ifndef ce_Keyword
#define ce_Keyword

#include "ce.h++"

class icStringList;



namespace icL::ce {

class Keyword : public CE
{
public:
    Keyword(il::InterLevel * il);

    /**
     * @brief giveModifiers gives modifiers from tokenizer `:mod :[mod1,mod1]`
     * @param modifiers is a icList of modifiers as strings
     */
    virtual void giveModifiers(const icStringList & modifiers);
};

}  // namespace icL::ce

#endif  // ce_Keyword
