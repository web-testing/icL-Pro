#ifndef ce_Value_runPropertyWithIndexCheck
#define ce_Value_runPropertyWithIndexCheck

#include "value.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <functional>

namespace icL::ce {

template <typename This, typename ParentClass, typename ReturnClass>
void Value::runPropertyWithIndexCheck(
  const icObject<icString, void (This::*)()> & properties, Prefix prefix,
  const icString & name, const std::function<icVariant(int)> & at) {
    auto this_ = dynamic_cast<This *>(this);

    if (prefix == Prefix::None) {
        bool isInt;
        int  nameAsInt = name.toInt(isInt);

        if (isInt) {
            m_newContext = new ReturnClass{il, at(nameAsInt)};
        }
        else {
            auto it = properties.find(name);

            if (it != properties.end()) {
                (this_->*it.value())();
            }
            else {
                this_->ParentClass::runProperty(prefix, name);
            }
        }
    }
    else {
        this_->ParentClass::runProperty(prefix, name);
    }
}

}  // namespace icL::ce

#endif  // ce_Value_runPropertyWithIndexCheck
