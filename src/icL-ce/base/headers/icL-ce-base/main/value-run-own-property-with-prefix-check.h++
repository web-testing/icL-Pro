#ifndef ce_Value_runOwnPropertyWithPrefixcheck
#define ce_Value_runOwnPropertyWithPrefixcheck

#include "value.h++"

namespace icL::ce {

template <typename This, typename ParentClass>
void Value::runOwnPropertyWithPrefixCheck(
  void (This::*property)(const icString &), Prefix prefix,
  const icString & name) {
    auto this_ = dynamic_cast<This *>(this);

    if (prefix == Prefix::None) {
        (this_->*property)(name);
    }
    else {
        this_->ParentClass::runProperty(prefix, name);
    }
}

}  // namespace icL::ce

#endif  // ce_Value_runOwnPropertyWithPrefixcheck
