#ifndef ce_Operator
#define ce_Operator

#include "ce.h++"



class icVariant;
template <typename, typename>
class icObject;
template <typename, typename>
struct icPair;

namespace icL::ce {

/**
 * @brief The Operator class represent an operator token (abstract)
 */
class Operator : public CE
{
public:
    Operator(il::InterLevel * il);

    /**
     * @brief run runs the operator now
     * @param left is the left operand
     * @param right is the right operand
     */
    virtual void run(
      const memory::ArgList & left, const memory::ArgList & right) = 0;

    /**
     * @brief runOperator runs a operator form hash map
     * @param operators are the hash map of operators
     * @param left is the left operand
     * @param right is the right operand
     */
    template <typename This>
    void runNow(
      const icObject<
        icPair<icList<memory::Type>, icList<memory::Type>>,
        void (This::*)(const memory::ArgList &, const memory::ArgList &)> &
                              operators,
      const memory::ArgList & left, const memory::ArgList & right);

protected:
    /**
     * @brief byContext selects a roles set by context
     * @param run returns it if current context is of run type
     * @param limited returns it if current context is of limited type
     * @param value returns it if current context is of value type
     * @return one of given contextes
     */
    const icSet<Role> & byContext(
      const icSet<Role> & run, const icSet<Role> & limited,
      const icSet<Role> & value);

    // CE interface
public:
    Role     role() override;
    StepType runNow() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    CE * firstToReplace() override;
    CE * lastToReplace() override;
};

}  // namespace icL::ce

// uint qhash(const icL::memory::Type & type);

#endif  // ce_Operator
