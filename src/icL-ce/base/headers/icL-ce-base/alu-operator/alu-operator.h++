#ifndef ce_AluOperator
#define ce_AluOperator

#include "../main/operator.h++"



namespace icL::ce {

class AluOperator : public Operator
{
public:
    AluOperator(il::InterLevel * il);
};

}  // namespace icL::ce

#endif  // ce_AluOperator
