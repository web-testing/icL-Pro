#ifndef ce_SystemOperator
#define ce_SystemOperator

#include "alu-operator.h++"



namespace icL::ce {

class SystemOperator : public AluOperator
{
public:
    SystemOperator(il::InterLevel * il);
};

}  // namespace icL::ce

#endif  // ce_SystemOperator
