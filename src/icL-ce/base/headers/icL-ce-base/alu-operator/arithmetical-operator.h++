#ifndef ce_ArithmeticalOperator
#define ce_ArithmeticalOperator

#include "alu-operator.h++"



namespace icL::ce {

class ArithmeticalOperator : public AluOperator
{
public:
    ArithmeticalOperator(il::InterLevel * il);

protected:
    /**
     * @brief runRank returns the run rank of this operator
     * @return the run rank of this operator
     */
    virtual int runRank() = 0;

    // CE interface
public:
    int currentRunRank(bool rtl) override;
};

}  // namespace icL::ce

#endif  // ce_ArithmeticalOperator
