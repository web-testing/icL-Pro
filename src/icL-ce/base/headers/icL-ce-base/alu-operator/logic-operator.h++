#ifndef ce_LogicOperator
#define ce_LogicOperator

#include "alu-operator.h++"



namespace icL::ce {

/**
 * @brief The LogicOperator class represents a logic operator `&, |, ~, ^, %, !`
 */
class LogicOperator : public AluOperator
{
public:
    LogicOperator(il::InterLevel * il);

    // CE interface
public:
    int currentRunRank(bool rtl) override;
};

}  // namespace icL::ce

#endif  // ce_LogicOperator
