#ifndef ce_AmbiguousArithmeticalOperator
#define ce_AmbiguousArithmeticalOperator

#include "arithmetical-operator.h++"



namespace icL::ce {

class AmbiguousArithmeticalOperator : public ArithmeticalOperator
{
public:
    AmbiguousArithmeticalOperator(il::InterLevel * il);

protected:
    /**
     * @brief runAbmiguousRank gets the ambiguous run rank
     * @return the ambiguous run rank
     * @note normal run rank is for call of type `operand operator operand`
     * @note ambiguous run rank is for call of type `operator operand`
     */
    virtual int runAbmiguousRank() = 0;

    // CE interface
public:
    int  currentRunRank(bool rtl) override;
    CE * firstToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;

    // fields
protected:
    bool asAmbiguous = false;

    // padding
    long : 56;
};

}  // namespace icL::ce

#endif  // ce_AmbiguousArithmeticalOperator
