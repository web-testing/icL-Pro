#ifndef ce_AdvancedKeyword
#define ce_AdvancedKeyword

#include "../main/keyword.h++"



namespace icL::ce {

class AdvancedKeyword : public Keyword
{
public:
    AdvancedKeyword(il::InterLevel * il);
};

}  // namespace icL::ce

#endif  // ce_AdvancedKeyword
