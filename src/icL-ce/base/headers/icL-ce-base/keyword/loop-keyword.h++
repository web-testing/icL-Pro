#ifndef ce_LoopKeyword
#define ce_LoopKeyword

#include "../main/keyword.h++"



namespace icL::ce {

class LoopKeyword : public Keyword
{
public:
    LoopKeyword(il::InterLevel * il);

    // CE interface
protected:
    CE * lastToReplace() override;
};

}  // namespace icL::ce

#endif  // ce_LoopKeyword
