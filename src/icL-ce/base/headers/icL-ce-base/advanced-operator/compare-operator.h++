#ifndef ce_CompareOperator
#define ce_CompareOperator

#include "advanced-operator.h++"



namespace icL::ce {

class CompareOperator : public AdvancedOperator
{
public:
    CompareOperator(il::InterLevel * il);

    // CE interface
public:
    int currentRunRank(bool rtl) override;
};

}  // namespace icL::ce

#endif  // ce_CompareOperator
