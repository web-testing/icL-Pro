#ifndef ce_AdvancedOperator
#define ce_AdvancedOperator

#include "../main/operator.h++"



namespace icL::ce {

class AdvancedOperator : public Operator
{
public:
    AdvancedOperator(il::InterLevel * il);
};

}  // namespace icL::ce

#endif  // ce_AdvancedOperator
