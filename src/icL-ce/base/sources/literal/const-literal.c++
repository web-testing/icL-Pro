#include "const-literal.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-service-main/factory/factory.h++>

#include <utility>

namespace icL::ce {

ConstLiteral::ConstLiteral(il::InterLevel * il, const icString & pattern)
    : Literal(il)
    , pattern(pattern) {}

icString ConstLiteral::toString() {
    return pattern;
}

int ConstLiteral::currentRunRank(bool rtl) {
    return rtl ? -1 : 10;
}

StepType ConstLiteral::runNow() {
    m_newContext = service::Factory::fromValue(il, getValueOf());
    return StepType::CommandEnd;
}

Role ConstLiteral::role() {
    return Role::Value;
}

const icSet<Role> & ConstLiteral::acceptedPrevs() {
    static icSet<Role> roles = {
      Role::NoRole,   Role::Any,    Role::If,       Role::For,    Role::Range,
      Role::Filter,   Role::Method, Role::Function, Role::Assign, Role::Comma,
      Role::Operator, Role::JsRun,  Role::Emit};
    return roles;
}

const icSet<Role> & ConstLiteral::acceptedNexts() {
    static icSet<Role> roles = {Role::NoRole,     Role::Assign, Role::Comma,
                                Role::RunContext, Role::Cast,   Role::Operator,
                                Role::Property,   Role::Method};
    return roles;
}

}  // namespace icL::ce
