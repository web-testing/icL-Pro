#include "arithmetical-operator.h++"

namespace icL::ce {

ArithmeticalOperator::ArithmeticalOperator(il::InterLevel * il)
    : AluOperator(il) {}

int ArithmeticalOperator::currentRunRank(bool rtl) {
    bool runnable =
      m_prev->role() == Role::Value && m_next->role() == Role::Value;

    return runnable && !rtl ? runRank() : -1;
}

}  // namespace icL::ce
