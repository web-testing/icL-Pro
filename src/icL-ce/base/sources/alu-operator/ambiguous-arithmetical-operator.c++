#include "ambiguous-arithmetical-operator.h++"

#include <icL-types/replaces/ic-set.h++>

namespace icL::ce {

AmbiguousArithmeticalOperator::AmbiguousArithmeticalOperator(
  il::InterLevel * il)
    : ArithmeticalOperator(il) {}

int AmbiguousArithmeticalOperator::currentRunRank(bool rtl) {
    bool leftIsValue  = m_prev != nullptr && m_prev->role() == Role::Value;
    bool rightIsValue = m_next->role() == Role::Value;

    asAmbiguous = rightIsValue && !leftIsValue;

    return asAmbiguous && !rtl
             ? runAbmiguousRank()
             : rightIsValue && leftIsValue && rtl ? runRank() : -1;
}

CE * AmbiguousArithmeticalOperator::firstToReplace() {
    return asAmbiguous ? this : m_prev;
}

const icSet<Role> & AmbiguousArithmeticalOperator::acceptedPrevs() {
    static const icSet<Role> roles{ArithmeticalOperator::acceptedPrevs(),
                                   {Role::NoRole, Role::Function, Role::Method,
                                    Role::Assign, Role::Comma, Role::Operator}};
    return roles;
}

}  // namespace icL::ce
