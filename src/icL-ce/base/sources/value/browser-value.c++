#include "browser-value.h++"

namespace icL::ce {

BrowserValue::BrowserValue(il::InterLevel * il)
    : Value(il) {}

icString BrowserValue::toString() {
    return typeName();
}

}  // namespace icL::ce
