#include "system-value.h++"

namespace icL::ce {

SystemValue::SystemValue(il::InterLevel * il)
    : Value(il) {}

icString SystemValue::toString() {
    return typeName();
}

}  // namespace icL::ce
