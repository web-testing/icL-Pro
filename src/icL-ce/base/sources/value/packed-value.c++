#include "packed-value.h++"

#include <icL-types/replaces/ic-set.h++>

#include <icL-memory/state/datacontainer.h++>



namespace icL::ce {

PackedValue::PackedValue(il::InterLevel * il, CE * first, CE * second)
    : BaseValue(il, {}) {
    if (first != nullptr) {
        extractDataFrom(first);
    }
    if (second != nullptr) {
        extractDataFrom(second);
    }
}

PackedValue::PackedValue(il::InterLevel * il, const memory::PackedValue & other)
    : BaseValue(il, {}) {
    if (other.data != nullptr) {
        values.data = other.data;
    }
    else {
        values.data = std::make_shared<memory::PackedItems>();
    }
}

PackedValue::PackedValue(BaseValue * value)
    : BaseValue(value) {
    values = dynamic_cast<PackedValue *>(value)->values;
}

const memory::PackedValue & PackedValue::getValues() {
    return values;
}

void PackedValue::extractDataFrom(CE * ce) {
    auto * packedValue = dynamic_cast<PackedValue *>(ce);

    if (packedValue != nullptr) {
        if (values.data == nullptr) {
            values.data = packedValue->values.data;
        }
        else {
            values.data->append(*packedValue->values.data);
        }
    }
    else {
        if (values.data == nullptr) {
            values.data = std::make_shared<icList<memory::PackedValueItem>>();
        }

        values.data->append(ce->packNow());
    }
}

const icSet<Role> & PackedValue::acceptedPrevs() {
    static icSet<Role> roles{Role::NoRole,   Role::Case,     Role::Assign,
                             Role::Switch,   Role::Comma,    Role::Cast,
                             Role::Operator, Role::Function, Role::Method};
    return roles;
}

const icSet<Role> & PackedValue::acceptedNexts() {
    static icSet<Role> roles{Role::NoRole, Role::Method, Role::Property,
                             Role::Assign, Role::Comma,  Role::RunContext};
    return roles;
}

Type PackedValue::type() const {
    return Type::PackedValue;
}

icString PackedValue::typeName() {
    return "packed";
}

icVariant PackedValue::getValue(bool /*excludeFunctional*/) {
    // the values get out of context, destroy containers links
    //    for (auto & value : *values.data) {
    //        if (value.container != nullptr) {
    //            value.container = nullptr;
    //            value.name.clear();
    //        }
    //    }

    return values;
}

}  // namespace icL::ce
