#include "value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/printing/stringify.h++>

#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/base/string-value.h++>
#include <icL-ce-value-base/base/void-value.h++>

#include <icL-memory/state/signals.h++>
#include <icL-memory/structures/argument.h++>

namespace icL::ce {

Value::Value(il::InterLevel * il)
    : CE(il) {}

int Value::typeId() {
    return static_cast<int>(type());
}

void Value::runTypeId() {
    m_newContext = new IntValue(il, typeId());
}

void Value::runTypeName() {
    m_newContext = new StringValue(il, typeName());
}

void Value::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Value::*)()> properties{
      {"typeId", &Value::runTypeId}, {"typeName", &Value::runTypeName}};


    if (prefix != Prefix::None) {
        sendNoSuchProperty(prefix, name);
    }
    else {
        auto it = properties.find(name);

        if (it != properties.end()) {
            (this->*it.value())();
        }
        else {
            sendNoSuchProperty(prefix, name);
        }
    }
}

void Value::runMethod(const icString & name, const memory::ArgList & args) {
    if (args.length() == 1 && args.at(0).type == Type::VoidValue) {
        m_newContext = new VoidValue(il);
    }
    else {
        sendNoSuchMethod(name, args);
    }
}

void Value::runMeta(
  const icString & name, const memory::ParamList & /*params*/) {
    sendNoSuchMeta(name);
}

void Value::sendNoSuchProperty(Prefix prefix, const icString & name) {
    icString propertyPrefix = prefix == Prefix::CSS
                                ? "css-"
                                : prefix == Prefix::Attr
                                    ? "attr-"
                                    : prefix == Prefix::Prop ? "prop-" : "";
    icString propertyName = propertyPrefix + name;

    il->vm->signal({memory::Signals::System,
                    "No such property: " % typeName() % '\'' % propertyName});
}

void Value::sendNoSuchMethod(
  const icString & name, const memory::ArgList & args) {
    icString argsStr = service::Stringify::fromArgs(args);
    icString message =
      "No such method: " % typeName() % '.' % name % '(' % argsStr % ')';

    il->vm->signal({memory::Signals::System, message});
}

void Value::sendNoSuchMeta(const icString & name) {
    il->vm->syssig("No such meta-method: " % name);
}

int Value::currentRunRank(bool) {
    // A value is not executable
    return -1;
}

Role Value::role() {
    return Role::SystemValue;
}

StepType Value::runNow() {
    return il::StepType::None;
}

const icSet<Role> & Value::acceptedPrevs() {
    static icSet<Role> roles{Role::NoRole, Role::Method, Role::Function,
                             Role::Assign, Role::Comma,  Role::Operator,
                             Role::JsRun,  Role::Now,    Role::Emit};
    return roles;
}

const icSet<Role> & Value::acceptedNexts() {
    static icSet<Role> roles{Role::Method, Role::Property};
    return roles;
}


}  // namespace icL::ce
