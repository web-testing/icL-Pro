#include "cast-operator.h++"

#include "base-value.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-service-main/args/listify.h++>

#include <icL-ce-literal/static/identifier.h++>
#include <icL-ce-literal/static/type.h++>

#include <icL-memory/structures/argument.h++>

#include <cassert>



namespace icL::ce {

using service::Listify;

CastOperator::CastOperator(il::InterLevel * il)
    : AdvancedOperator(il) {}

int CastOperator::runRank() {
    return 4;
}

int CastOperator::currentRunRank(bool rtl) {
    bool runnable =
      (m_prev->role() == Role::Value || m_prev->role() == Role::Identifier) &&
      (m_next->role() == Role::Value || m_next->role() == Role::Type);

    return rtl && runnable ? runRank() : -1;
}

StepType CastOperator::runNow() {
    memory::ArgList left, right;

    auto * asValue      = dynamic_cast<BaseValue *>(m_prev);
    auto * asIdentifier = dynamic_cast<Identifier *>(m_prev);

    if (asValue != nullptr) {
        left = Listify::toArgList(il, asValue);
    }
    else if (asIdentifier != nullptr) {
        memory::Argument arg;
        arg.varName = asIdentifier->getName();
        arg.type    = memory::Type::Identifier;
        left.append(arg);
    }
    else {
        assert(false);
    }

    if (m_next->role() == Role::Type) {
        memory::Argument arg;

        arg.type = dynamic_cast<ce::TypeToken *>(m_next)->getType();
        right.append(arg);
    }
    else {
        right = Listify::toArgList(il, dynamic_cast<BaseValue *>(m_next));
    }

    if (checkArgs(left)) {
        if (checkArgs(memory::Type::Type, right)) {
            if (left.length() == 1) {
                for (auto & arg : right) {
                    runCast(left[0], arg.type);
                }
            }
            else if (right.length() == 1) {
                for (auto & arg : left) {
                    runCast(arg, right[0].type);
                }
            }
            else if (left.length() == right.length()) {
                for (int i = 0; i < left.length(); i++) {
                    runCast(left[i], right[i].type);
                }
            }
            else {
                runCast(left, right);
            }
        }
        else {
            runCast(left, right);
        }
    }
    else if (
      checkArgs(left, {memory::Type::Identifier}) &&
      checkArgs(right, {memory::Type::Type})) {
        runCast(left[0], right[0]);
    }

    if (m_newContext == nullptr) {
        il->vm->syssig("No such casting operator");
    }

    return StepType::CommandEnd;
}

const icSet<Role> & CastOperator::acceptedNexts() {
    static icSet<Role> roles{Role::ValueContext, Role::Type};
    return roles;
}

void CastOperator::run(
  const memory::ArgList & /*left*/, const memory::ArgList & /*right*/) {
    assert(false);  // must be never called
}

}  // namespace icL::ce
