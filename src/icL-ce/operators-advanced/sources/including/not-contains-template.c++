#include "not-contains-template.h++"

#include <icL-ce-value-base/base/bool-value.h++>

namespace icL::ce {

NotContainsTemplate::NotContainsTemplate(il::InterLevel * il)
    : ContainsTemplate(il) {}

icString NotContainsTemplate::toString() {
    return "!*";
}

void NotContainsTemplate::run(
  const memory::ArgList & left, const memory::ArgList & right) {

    ContainsTemplate::run(left, right);

    auto * asBool = dynamic_cast<BoolValue *>(m_newContext);

    if (asBool == nullptr) {
        delete m_newContext;
        m_newContext = nullptr;
        return;
    }


    asBool->setValue(!asBool->getValue().toBool());
}

}  // namespace icL::ce
