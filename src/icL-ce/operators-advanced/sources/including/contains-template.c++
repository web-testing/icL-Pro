#include "contains-template.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/complex/list-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

ContainsTemplate::ContainsTemplate(il::InterLevel * il)
    : IncludingOperator(il) {}

void ContainsTemplate::runListString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, listString(left[0], right[0])};
}

void ContainsTemplate::runSetObject(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, setObject(left[0], right[0])};
}

void ContainsTemplate::runObjectObject(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, objectObject(left[0], right[0])};
}

void ContainsTemplate::runStringRegex(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new ListValue{il, stringRegex(left[0], right[0])};
}

icString ContainsTemplate::toString() {
    return "<*";
}

void ContainsTemplate::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (ContainsTemplate::*)(
        const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::ListValue}, {Type::StringValue}},
                 &ContainsTemplate::runListString},
                {{{Type::SetValue}, {Type::ObjectValue}},
                 &ContainsTemplate::runSetObject},
                {{{Type::ObjectValue}, {Type::ObjectValue}},
                 &ContainsTemplate::runObjectObject},
                {{{Type::StringValue}, {Type::RegexValue}},
                 &ContainsTemplate::runStringRegex}};

    runNow<ContainsTemplate>(operators, left, right);
}

}  // namespace icL::ce
