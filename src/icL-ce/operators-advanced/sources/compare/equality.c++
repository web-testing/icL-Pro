#include "equality.h++"

#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/structures/target-data.h++>

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

Equality::Equality(il::InterLevel * il)
    : CompareOperator(il) {}

void Equality::runBoolBool(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, boolBool(left[0], right[0])};
}

void Equality::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, intInt(left[0], right[0])};
}

void Equality::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, doubleDouble(left[0], right[0])};
}

void Equality::runStringString(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, stringString(left[0], right[0])};
}

void Equality::runListList(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, listList(left[0], right[0])};
}

void Equality::runObjectObject(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, objectObject(left[0], right[0])};
}

void Equality::runSetSet(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, setSet(left[0], right[0])};
}

void Equality::runSessionSession(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, sessionSession(left[0], right[0])};
}

void Equality::runTabTab(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, tabTab(left[0], right[0])};
}

void Equality::runWindowWindow(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, windowWindow(left[0], right[0])};
}

icString Equality::toString() {
    return "==";
}

void Equality::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (Equality::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{Type::BoolValue}, {Type::BoolValue}}, &Equality::runBoolBool},
        {{{Type::IntValue}, {Type::IntValue}}, &Equality::runIntInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}},
         &Equality::runDoubleDouble},
        {{{Type::StringValue}, {Type::StringValue}},
         &Equality::runStringString},
        {{{Type::ListValue}, {Type::ListValue}}, &Equality::runListList},
        {{{Type::ObjectValue}, {Type::ObjectValue}},
         &Equality::runObjectObject},
        {{{Type::SetValue}, {Type::SetValue}}, &Equality::runSetSet},
        {{{Type::SessionValue}, {Type::SessionValue}},
         &Equality::runSessionSession},
        {{{Type::TabValue}, {Type::TabValue}}, &Equality::runTabTab},
        {{{Type::WindowValue}, {Type::WindowValue}},
         &Equality::runWindowWindow}};

    runNow<Equality>(operators, left, right);
}

}  // namespace icL::ce
