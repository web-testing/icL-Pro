#include "smaller-bigger.h++"

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

SmallerBigger::SmallerBigger(il::InterLevel * il)
    : CompareOperator(il) {}

void SmallerBigger::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, intIntInt(left[0], right[0], right[1])};
}

void SmallerBigger::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext =
      new BoolValue{il, doubleDoubleDouble(left[0], right[0], right[1])};
}

icString SmallerBigger::toString() {
    return "<>";
}

void SmallerBigger::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (SmallerBigger::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue, Type::IntValue}},
                 &SmallerBigger::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue, Type::DoubleValue}},
                 &SmallerBigger::runDoubleDouble}};

    runNow<SmallerBigger>(operators, left, right);
}

}  // namespace icL::ce
