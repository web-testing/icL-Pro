#include "inequality.h++"

#include <icL-ce-base/value/base-value.h++>

namespace icL::ce {

Inequality::Inequality(il::InterLevel * il)
    : Equality(il) {}

icString Inequality::toString() {
    return "!=";
}

void Inequality::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    Equality::run(left, right);

    if (m_newContext == nullptr) {
        return;
    }

    auto * newContext = dynamic_cast<BaseValue *>(m_newContext);

    newContext->setValue(!newContext->getValue().toBool());
}

}  // namespace icL::ce
