#include "bigger.h++"

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

Bigger::Bigger(il::InterLevel * il)
    : CompareOperator(il) {}

void Bigger::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, intInt(left[0], right[0])};
}

void Bigger::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, doubleDouble(left[0], right[0])};
}

icString Bigger::toString() {
    return ">";
}

void Bigger::run(const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (Bigger::*)(const memory::ArgList &, const memory::ArgList &)>
      operators{
        {{{Type::IntValue}, {Type::IntValue}}, &Bigger::runIntInt},
        {{{Type::DoubleValue}, {Type::DoubleValue}}, &Bigger::runDoubleDouble}};

    runNow<Bigger>(operators, left, right);
}

}  // namespace icL::ce
