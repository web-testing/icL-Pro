#include "smaller-equal-bigger.h++"

#include <icL-ce-base/main/operator-run-now.h++>
#include <icL-ce-value-base/base/bool-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

SmallerEqualBigger::SmallerEqualBigger(il::InterLevel * il)
    : CompareOperator(il) {}

void SmallerEqualBigger::runIntInt(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext = new BoolValue{il, intIntInt(left[0], right[0], right[1])};
}

void SmallerEqualBigger::runDoubleDouble(
  const memory::ArgList & left, const memory::ArgList & right) {
    m_newContext =
      new BoolValue{il, doubleDoubleDouble(left[0], right[0], right[1])};
}

icString SmallerEqualBigger::toString() {
    return "<=>";
}

void SmallerEqualBigger::run(
  const memory::ArgList & left, const memory::ArgList & right) {
    static icObject<
      icPair<icList<memory::Type>, icList<memory::Type>>,
      void (SmallerEqualBigger::*)(
        const memory::ArgList &, const memory::ArgList &)>
      operators{{{{Type::IntValue}, {Type::IntValue, Type::IntValue}},
                 &SmallerEqualBigger::runIntInt},
                {{{Type::DoubleValue}, {Type::DoubleValue, Type::DoubleValue}},
                 &SmallerEqualBigger::runDoubleDouble}};

    runNow<SmallerEqualBigger>(operators, left, right);
}

}  // namespace icL::ce
