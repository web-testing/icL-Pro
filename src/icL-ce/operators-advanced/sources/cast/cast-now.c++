#include "cast-now.h++"

namespace icL::ce {

CastNow::CastNow(il::InterLevel * il)
    : Cast(il) {}

icString CastNow::toString() {
    return ":!";
}

int CastNow::runRank() {
    return 7;
}

}  // namespace icL::ce
