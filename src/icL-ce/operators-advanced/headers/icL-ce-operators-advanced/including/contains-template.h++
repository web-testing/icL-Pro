#ifndef ce_ContainsTemplate
#define ce_ContainsTemplate

#include <icL-service-operators-advanced/including/contains-template.h++>

#include <icL-ce-base/advanced-operator/including-operator.h++>



namespace icL::ce {

class ContainsTemplate
    : public IncludingOperator
    , public service::ContainsTemplate
{
public:
    ContainsTemplate(il::InterLevel * il);

    // level 2

    /// `list <* string`
    void runListString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set <* object`
    void runSetObject(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `object <* object`
    void runObjectObject(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `string <* regex`
    void runStringRegex(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_ContainsTemplate
