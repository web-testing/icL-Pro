#ifndef ce_NotContainsTemplate
#define ce_NotContainsTemplate

#include "contains-template.h++"



namespace icL::ce {

class NotContainsTemplate : public ContainsTemplate
{
public:
    NotContainsTemplate(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_NotContainsTemplate
