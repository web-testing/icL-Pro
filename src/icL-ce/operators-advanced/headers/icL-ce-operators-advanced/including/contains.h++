#ifndef ce_Contains
#define ce_Contains

#include <icL-service-operators-advanced/including/contains.h++>

#include <icL-ce-base/advanced-operator/including-operator.h++>



namespace icL::ce {

class Contains
    : public IncludingOperator
    , public service::Contains
{
public:
    Contains(il::InterLevel * il);

    // level 2

    /// `list << string`
    void runListString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `string << string`
    void runStringString(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set << object`
    void runSetObject(
      const memory::ArgList & left, const memory::ArgList & right);

    /// `set << set`
    void runSetSet(const memory::ArgList & left, const memory::ArgList & right);

    /// `string << regex`
    void runStringRegex(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_Contains
