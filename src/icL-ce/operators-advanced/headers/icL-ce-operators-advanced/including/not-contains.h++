#ifndef ce_NotContains
#define ce_NotContains

#include "contains.h++"



namespace icL::ce {

class NotContains : public Contains
{
public:
    NotContains(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_NotContains
