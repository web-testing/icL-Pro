#ifndef ce_BiggerSmaller
#define ce_BiggerSmaller

#include <icL-service-operators-advanced/compare/bigger-smaller.h++>

#include <icL-ce-base/advanced-operator/compare-operator.h++>



namespace icL::ce {

class BiggerSmaller
    : public CompareOperator
    , public service::BiggerSmaller
{
public:
    BiggerSmaller(il::InterLevel * il);

    /// `int >< int`
    void runIntInt(const memory::ArgList & left, const memory::ArgList & right);

    /// `double >< double`
    void runDoubleDouble(
      const memory::ArgList & left, const memory::ArgList & right);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_BiggerSmaller
