#ifndef ce_Inequality
#define ce_Inequality

#include "equality.h++"



namespace icL::ce {

class Inequality : public Equality
{
public:
    Inequality(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // Operator interface
public:
    void run(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace icL::ce

#endif  // ce_Inequality
