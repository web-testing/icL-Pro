#ifndef ce_CastNow
#define ce_CastNow

#include "cast.h++"



namespace icL::ce {

class CastNow : public Cast
{
public:
    CastNow(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // CastOperator interface
public:
    int runRank() override;
};

}  // namespace icL::ce

#endif  // ce_CastNow
