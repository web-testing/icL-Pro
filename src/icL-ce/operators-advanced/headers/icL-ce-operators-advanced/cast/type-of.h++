#ifndef ce_TypeOf
#define ce_TypeOf

#include <icL-ce-base/advanced-operator/cast-operator.h++>



namespace icL {

namespace memory {
struct Argument;
}

namespace ce {

class TypeOf : public CastOperator
{
public:
    TypeOf(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;

    // CastOperator interface
public:
    void runCast(const memory::Argument & left, memory::Type right) override;
    void runCast(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_TypeOf
