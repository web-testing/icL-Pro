#ifndef ce_ErrorLessCast
#define ce_ErrorLessCast

#include "cast.h++"



namespace icL::ce {

class ErrorLessCast : public Cast
{
public:
    ErrorLessCast(il::InterLevel * il);

private:
    template <typename ValueType, typename ReturnType, typename ArgType>
    void runAny(
      ReturnType (*func)(il::InterLevel *, const ArgType &),
      const memory::Argument & left);

public:
    /// all unreal casts
    void runAlwaysVoid(const memory::Argument & left);

    // CE interface
public:
    icString toString() override;

    // Cast interface
public:
    void runUnhandled(const memory::Argument & left) override;
    void runStringInt(const memory::Argument & left) override;
    void runStringDouble(const memory::Argument & left) override;
    void runStringObject(const memory::Argument & left) override;
    void runStringSet(const memory::Argument & left) override;
    void runListString(const memory::Argument & left) override;
    void runListSet(const memory::Argument & left) override;
    void runSetObject(const memory::Argument & left) override;
};

}  // namespace icL::ce

#endif  // ce_ErrorLessCast
