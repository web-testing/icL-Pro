#ifndef ce_Cast
#define ce_Cast

#include <icL-ce-base/advanced-operator/cast-operator.h++>



namespace icL {

namespace memory {
struct Argument;
}

namespace ce {

class Cast : public CastOperator
{
public:
    Cast(il::InterLevel * il);

    /// run a unhandled cast
    virtual void runUnhandled(const memory::Argument & left);

    /// `void : bool`
    void runVoidBool(const memory::Argument & left);

    /// `void : int`
    void runVoidInt(const memory::Argument & left);

    /// `void : double`
    void runVoidDouble(const memory::Argument & left);

    /// `void : string`
    void runVoidString(const memory::Argument & left);

    /// `void : list`
    void runVoidList(const memory::Argument & left);

    /// `void : object`
    void runVoidObject(const memory::Argument & left);

    /// `void : set`
    void runVoidSet(const memory::Argument & left);

    /// `bool : int`
    void runBoolInt(const memory::Argument & left);

    /// `bool : double`
    void runBoolDouble(const memory::Argument & left);

    /// `bool : string`
    void runBoolString(const memory::Argument & left);

    /// `int : bool`
    void runIntBool(const memory::Argument & left);

    /// `int : double`
    void runIntDouble(const memory::Argument & left);

    /// `int : string`
    void runIntString(const memory::Argument & left);

    /// `double : bool`
    void runDoubleBool(const memory::Argument & left);

    /// `double : int`
    void runDoubleInt(const memory::Argument & left);

    /// `double : string`
    void runDoubleString(const memory::Argument & left);

    /// `string : bool`
    void runStringBool(const memory::Argument & left);

    /// `string : int`
    virtual void runStringInt(const memory::Argument & left);

    /// `string : double`
    virtual void runStringDouble(const memory::Argument & left);

    /// `string : list`
    void runStringList(const memory::Argument & left);

    /// `string : object`
    virtual void runStringObject(const memory::Argument & left);

    /// `string : set`
    virtual void runStringSet(const memory::Argument & left);

    /// `list : bool`
    void runListBool(const memory::Argument & left);

    /// `list : string`
    virtual void runListString(const memory::Argument & left);

    /// `list : set`
    virtual void runListSet(const memory::Argument & left);

    /// `object : bool`
    void runObjectBool(const memory::Argument & left);

    /// `object : string`
    void runObjectString(const memory::Argument & left);

    /// `set : bool`
    void runSetBool(const memory::Argument & left);

    /// `set : string`
    void runSetString(const memory::Argument & left);

    /// `set : list`
    void runSetList(const memory::Argument & left);

    /// `set : object`
    virtual void runSetObject(const memory::Argument & left);

    /// `element : bool`
    void runElementBool(const memory::Argument & left);

    /// `element : string`
    void runElementString(const memory::Argument & left);

    /// `string : icDateTime`
    void runStringDatetime(const memory::Argument & left);

    /// `icDateTime : string`
    void runDatetimeString(const memory::Argument & left);

    /// `string : (..) -> string : (icDateTime, format)`
    void runStringPack(
      const memory::Argument & left, const memory::Argument & right);

    /// `icDateTime : (..) -> icDateTime : (string, format)`
    void runDatetimePack(
      const memory::Argument & left, const memory::Argument & right);

    /// `identifier : type : column`
    void runIdentifierType(const memory::Argument & left, memory::Type right);

private:
    /// `now var : type`
    CE * declareCast(const memory::Argument & left, memory::Type right);

    // CE interface
public:
    Role     role() override;
    icString toString() override;

    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    CE * firstToReplace() override;
    CE * lastToReplace() override;

    // CastOperator interface
public:
    void runCast(const memory::Argument & left, memory::Type right) override;
    void runCast(
      const memory::ArgList & left, const memory::ArgList & right) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Cast
