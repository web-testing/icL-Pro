#include "window.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce-value-base/browser/window-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Window::Window(il::InterLevel * il)
    : BrowserCommand(il) {}

int Window::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Window::runNow() {
    il::Window window;

    window.data =
      std::make_shared<il::TargetData>(il->server->getCurrentTarget());
    m_newContext = new WindowValue{il, window};

    return il::StepType::CommandEnd;
}

Type Window::type() const {
    return Type::Window;
}

icString Window::typeName() {
    return "Window";
}

}  // namespace icL::ce
