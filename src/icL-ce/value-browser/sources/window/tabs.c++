#include "tabs.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-index-check.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/browser/session-value.h++>
#include <icL-ce-value-base/browser/tab-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Tabs::Tabs(il::InterLevel * il, il::TargetData target)
    : BrowserValue(il)
    , service::Tabs(std::move(target)) {
    if (this->target.sessionId.isEmpty()) {
        this->target = il->server->getCurrentTarget();
    }
}

void Tabs::runCurrent() {
    m_newContext = new TabValue{il, current()};
}

void Tabs::runFirst() {
    m_newContext = new TabValue{il, first()};
}

void Tabs::runLast() {
    m_newContext = new TabValue{il, last()};
}

void Tabs::runLength() {
    m_newContext = new IntValue{il, length()};
}

void Tabs::runNext() {
    m_newContext = new TabValue{il, service::Tabs::next()};
}

void Tabs::runPrevious() {
    m_newContext = new TabValue{il, previous()};
}

void Tabs::runSession() {
    m_newContext = new SessionValue{il, session()};
}

void Tabs::runClose(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new IntValue{il, close(icString(args[0]))};
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = new IntValue{il, close(icRegEx(args[0]))};
    }
}

void Tabs::runCloseByTitle(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new IntValue{il, closeByTitle(icString(args[0]))};
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = new IntValue{il, closeByTitle(icRegEx(args[0]))};
    }
}

void Tabs::runCloseOthers(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new IntValue{il, closeOthers()};
    }
}

void Tabs::runCloseToLeft(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new IntValue{il, closeToLeft()};
    }
}

void Tabs::runCloseToRight(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new IntValue{il, closeToRight()};
    }
}

void Tabs::runFind(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new TabValue{il, find(icString(args[0]))};
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = new TabValue{il, find(icRegEx(args[0]))};
    }
}

void Tabs::runFindByTitle(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new TabValue{il, findByTitle(icString(args[0]))};
    }
    else if (checkArgs(args, {Type::RegexValue})) {
        m_newContext = new TabValue{il, findByTitle(icRegEx(args[0]))};
    }
}

void Tabs::runGet(const memory::ArgList & args) {
    if (checkArgs(args, {Type::IntValue})) {
        m_newContext = new TabValue{il, get(args[0])};
    }
}

void Tabs::runNew(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new TabValue{il, new_()};
    }
}

Type Tabs::type() const {
    return Type::Tabs;
}

icString Tabs::typeName() {
    return "Tabs";
}

void Tabs::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Tabs::*)()> properties{
      {"current", &Tabs::runCurrent}, {"first", &Tabs::runFirst},
      {"last", &Tabs::runLast},       {"length", &Tabs::runLength},
      {"next", &Tabs::runNext},       {"previous", &Tabs::runPrevious},
      {"session", &Tabs::runSession}};

    il->server->pushTarget(target);
    runPropertyWithIndexCheck<Tabs, BrowserValue, TabValue>(
      properties, prefix, name, [this](int i) -> icVariant { return get(i); });
    il->server->popTarget();
}

void Tabs::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Tabs::*)(const memory::ArgList &)> methods{
      {"close", &Tabs::runClose},
      {"closeByTitle", &Tabs::runCloseByTitle},
      {"closeOthers", &Tabs::runCloseOthers},
      {"closeToLeft", &Tabs::runCloseToLeft},
      {"closeToRight", &Tabs::runCloseToRight},
      {"find", &Tabs::runFind},
      {"findByTitle", &Tabs::runFindByTitle},
      {"get", &Tabs::runGet},
      {"new", &Tabs::runNew}};

    il->server->pushTarget(target);
    runMethodNow<Tabs, BrowserValue>(methods, name, args);
    il->server->popTarget();
}

}  // namespace icL::ce
