#include "session.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce-value-base/browser/session-value.h++>



namespace icL::ce {

Session::Session(il::InterLevel * il)
    : BrowserCommand(il) {}

int Session::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Session::runNow() {
    il::Session session;

    session.data =
      std::make_shared<il::TargetData>(il->server->getCurrentTarget());
    m_newContext = new SessionValue{il, session};

    return il::StepType::CommandEnd;
}

Type Session::type() const {
    return Type::Session;
}

icString Session::typeName() {
    return "Session";
}

}  // namespace icL::ce
