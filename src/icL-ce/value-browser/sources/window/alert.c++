#include "alert.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/string-value.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-base/browser/session-value.h++>

#include <icL-memory/structures/argument.h++>

namespace icL::ce {

Alert::Alert(il::InterLevel * il, il::TargetData target)
    : BrowserCommand(il)
    , service::Alert(std::move(target)) {
    if (this->target.sessionId.isEmpty()) {
        this->target = il->server->getCurrentTarget();
    }
}

void Alert::runSession() {
    m_newContext = new SessionValue{il, session()};
}

void Alert::runText() {
    m_newContext = new StringValue{il, text()};
}

void Alert::runAccept(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        accept();
        m_newContext = new VoidValue{il};
    }
}

void Alert::runDismiss(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        dismiss();
        m_newContext = new VoidValue{il};
    }
}

void Alert::runSendKeys(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        sendKeys(args[0]);
        m_newContext = new VoidValue{il};
    }
}

Type Alert::type() const {
    return Type::Alert;
}

icString Alert::typeName() {
    return "Alert";
}

void Alert::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Alert::*)()> properties{
      {"session", &Alert::runSession}, {"text", &Alert::runText}};

    il->server->pushTarget(target);
    runPropertyWithPrefixCheck<Alert, BrowserValue>(properties, prefix, name);
    il->server->popTarget();
}

void Alert::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Alert::*)(const memory::ArgList &)> methods{
      {"accept", &Alert::runAccept},
      {"dismiss", &Alert::runDismiss},
      {"sendKeys", &Alert::runSendKeys}};

    il->server->pushTarget(target);
    runMethodNow<Alert, BrowserValue>(methods, name, args);
    il->server->popTarget();
}

}  // namespace icL::ce
