#include "icl.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>

#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/int-value.h++>



namespace icL::ce {

icL::icL(il::InterLevel * il)
    : BrowserValue(il) {}

CE * icL::runIntProperty(
  int (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(int)) {
    auto * ret = new IntValue{il, {}};
    auto   il  = this->il;

    ret->makeFunctional(
      [il, getter](BaseValue *) -> icVariant {
          return (il->server->*getter)();
      },
      [il, setter](BaseValue *, const icVariant & var) {
          (il->server->*setter)(var.toInt());
      });

    return ret;
}

CE * icL::runBoolProperty(
  bool (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(bool)) {
    auto * ret = new BoolValue{il, {}};
    auto   il  = this->il;

    ret->makeFunctional(
      [il, getter](BaseValue *) -> icVariant {
          return (il->server->*getter)();
      },
      [il, setter](BaseValue *, const icVariant & var) {
          (il->server->*setter)(var.toBool());
      });

    return ret;
}

CE * icL::clickTime() {
    return runIntProperty(
      &il::FrontEnd::getClickTime, &il::FrontEnd::setClickTime);
}

CE * icL::flashMode() {
    return runBoolProperty(
      &il::FrontEnd::getFlashMode, &il::FrontEnd::setFlashMode);
}

CE * icL::humanMode() {
    return runBoolProperty(
      &il::FrontEnd::getHumanMode, &il::FrontEnd::setHumanMode);
}

CE * icL::moveTime() {
    return runIntProperty(
      &il::FrontEnd::getMoveTime, &il::FrontEnd::setMoveTime);
}

CE * icL::pressTime() {
    return runIntProperty(
      &il::FrontEnd::getPressTime, &il::FrontEnd::setPressTime);
}

CE * icL::silentMode() {
    return runBoolProperty(
      &il::FrontEnd::getSilentMode, &il::FrontEnd::setSilentMode);
}

void icL::runClickTime() {
    m_newContext = clickTime();
}

void icL::runFlashMode() {
    m_newContext = flashMode();
}

void icL::runHumanMode() {
    m_newContext = humanMode();
}

void icL::runMoveTime() {
    m_newContext = moveTime();
}

void icL::runPressTime() {
    m_newContext = pressTime();
}

void icL::runSilentMode() {
    m_newContext = silentMode();
}

Type icL::type() const {
    return Type::icL;
}

icString icL::typeName() {
    return "icL";
}

void icL::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (icL::*)()> properties{
      {"clickTime", &icL::runClickTime}, {"flashMode", &icL::runFlashMode},
      {"humanMode", &icL::runHumanMode}, {"moveTime", &icL::runMoveTime},
      {"pressTime", &icL::runPressTime}, {"silentMode", &icL::runSilentMode}};

    runPropertyWithPrefixCheck<icL, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
