#include "mouse.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/int-value.h++>

namespace icL::ce {

Mouse::Mouse(il::InterLevel * il)
    : BrowserValue(il) {}

void Mouse::runLeft() {
    m_newContext = new IntValue{il, left()};
}

void Mouse::runMiddle() {
    m_newContext = new IntValue{il, middle()};
}

void Mouse::runRight() {
    m_newContext = new IntValue{il, right()};
}

Type Mouse::type() const {
    return Type::Mouse;
}

icString Mouse::typeName() {
    return "Mouse";
}

void Mouse::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Mouse::*)()> properties{
      {"left", &Mouse::runLeft},
      {"middle", &Mouse::runMiddle},
      {"right", &Mouse::runRight}};

    runPropertyWithPrefixCheck<Mouse, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
