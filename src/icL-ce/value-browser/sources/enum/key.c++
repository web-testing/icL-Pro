#include "key.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/int-value.h++>

namespace icL::ce {

Key::Key(il::InterLevel * il)
    : BrowserValue(il) {}

void Key::runAlt() {
    m_newContext = new IntValue{il, alt()};
}

void Key::runCtrl() {
    m_newContext = new IntValue{il, ctrl()};
}

void Key::runShift() {
    m_newContext = new IntValue{il, shift()};
}

Type Key::type() const {
    return Type::Key;
}

icString Key::typeName() {
    return "Key";
}

void Key::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Key::*)()> properties{
      {"alt", &Key::runAlt},
      {"ctrl", &Key::runCtrl},
      {"shift", &Key::runShift}};

    runPropertyWithPrefixCheck<Key, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
