#include "move.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/int-value.h++>

namespace icL::ce {

Move::Move(il::InterLevel * il)
    : BrowserValue(il) {}

void Move::runBezier() {
    m_newContext = new IntValue{il, bezier()};
}

void Move::runCubic() {
    m_newContext = new IntValue{il, cubic()};
}

void Move::runLinear() {
    m_newContext = new IntValue{il, linear()};
}

void Move::runQuadratic() {
    m_newContext = new IntValue{il, quadratic()};
}

void Move::runTeleport() {
    m_newContext = new IntValue{il, teleport()};
}

Type Move::type() const {
    return Type::Move;
}

icString Move::typeName() {
    return "Move";
}

void Move::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Move::*)()> properties{
      {"bezier", &Move::runBezier},
      {"cubic", &Move::runCubic},
      {"linear", &Move::runLinear},
      {"quadratic", &Move::runQuadratic},
      {"teleport", &Move::runTeleport}};

    runPropertyWithPrefixCheck<Move, BrowserValue>(properties, prefix, name);
}

}  // namespace icL::ce
