#include "cookies.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/cookie-data.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-base/browser/cookie-value.h++>
#include <icL-ce-value-base/browser/tab-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Cookies::Cookies(il::InterLevel * il, il::TargetData target)
    : BrowserValue(il)
    , service::Cookies(std::move(target)) {
    if (this->target.sessionId.isEmpty()) {
        this->target = il->server->getCurrentTarget();
    }
}

void Cookies::runTab() {
    m_newContext = new TabValue{il, tab()};
}

void Cookies::runDeleteAll(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        deleteAll();
        m_newContext = new VoidValue{il};
    }
}

void Cookies::runGet(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new CookieValue{il, get(args[0])};
    }
}

void Cookies::runNew(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        m_newContext = new CookieValue{il, new_()};
    }
}

Type Cookies::type() const {
    return Type::Cookies;
}

icString Cookies::typeName() {
    return "Cookies";
}

void Cookies::runProperty(Prefix prefix, const icString & name) {
    static icObject<icString, void (Cookies::*)()> properties{
      {"tab", &Cookies::runTab}};

    il->server->pushTarget(target);
    runPropertyWithPrefixCheck<Cookies, BrowserValue>(properties, prefix, name);
    il->server->popTarget();
}

void Cookies::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Cookies::*)(const memory::ArgList &)>
      methods{{"deleteAll", &Cookies::runDeleteAll},
              {"get", &Cookies::runGet},
              {"new", &Cookies::runNew}};

    il->server->pushTarget(target);
    runMethodNow<Cookies, BrowserValue>(methods, name, args);
    il->server->popTarget();
}

}  // namespace icL::ce
