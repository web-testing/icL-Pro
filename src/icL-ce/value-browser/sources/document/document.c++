#include "document.h++"

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/target-data.h++>

#include <icL-ce-value-base/browser/document-value.h++>



namespace icL::ce {

Document::Document(il::InterLevel * il)
    : BrowserCommand(il) {}

int Document::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Document::runNow() {
    il::Document document;

    document.data =
      std::make_shared<il::TargetData>(il->server->getCurrentTarget());
    m_newContext = new DocumentValue{il, document};

    return il::StepType::CommandEnd;
}

memory::Type Document::type() const {
    return Type::DocumentValue;
}

icString Document::typeName() {
    return "Document";
}

}  // namespace icL::ce
