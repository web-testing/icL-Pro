#include "cookie.h++"

#include <icL-il/structures/cookie-data.h++>

#include <icL-ce-value-base/browser/cookie-value.h++>



namespace icL::ce {

Cookie::Cookie(il::InterLevel * il)
    : BrowserCommand(il) {}

int Cookie::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType Cookie::runNow() {
    il::Cookie ref;
    ref.data     = std::make_shared<il::CookieData>();
    m_newContext = new CookieValue{il, ref};

    return il::StepType::CommandEnd;
}

Type Cookie::type() const {
    return Type::Cookie;
}

icString Cookie::typeName() {
    return "Cookie";
}

}  // namespace icL::ce
