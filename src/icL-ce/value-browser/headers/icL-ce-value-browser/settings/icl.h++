#ifndef ce_icL
#define ce_icL

#include <icL-ce-base/value/browser-value.h++>



namespace icL {

namespace memory {
class Memory;
}

namespace il {
class FrontEnd;
}

namespace ce {

/**
 * @brief The icL class represents an `icL` token
 */
class icL : public BrowserValue
{
public:
    icL(il::InterLevel * il);

private:
    /**
     * @brief runIntProperty runs a int property
     * @return a contextual entity for needed int propeperty
     */
    CE * runIntProperty(
      int (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(int));

    /**
     * @brief runBoolProperty runs a bool property
     * @return a contextual entity for needed bool property
     */
    CE * runBoolProperty(
      bool (il::FrontEnd::*getter)(), void (il::FrontEnd::*setter)(bool));

public:
    // properties level 1

    /// `[r/w] icL'clickTime : int`
    CE * clickTime();

    /// `[r/w] icL'flashMode : bool`
    CE * flashMode();

    /// `[r/w] icL'humanMode : bool`
    CE * humanMode();

    /// `[r/w] icL'moveTime : int`
    CE * moveTime();

    /// `[r/w] icL'pressTime : int`
    CE * pressTime();

    /// `[r/w] icL'silentMode : bool`
    CE * silentMode();

    // properties level 2

    /// `icL'clickTime`
    void runClickTime();

    /// `icL'flashMode`
    void runFlashMode();

    /// `icL'humanMode`
    void runHumanMode();

    /// `icL'moveTime`
    void runMoveTime();

    /// `icL'pressTime`
    void runPressTime();

    /// `icL'silentMode`
    void runSilentMode();

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_icL
