#ifndef ce_Window
#define ce_Window

#include <icL-ce-base/value/browser-command.h++>



namespace icL::ce {

class Window : public BrowserCommand
{
public:
    Window(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_Window
