#ifndef ce_Cookies
#define ce_Cookies

#include <icL-service-value-browser/document/cookies.h++>

#include <icL-ce-base/value/browser-value.h++>



namespace icL {

namespace il {
struct TargetData;
}

namespace ce {

/**
 * @brief The Cookies class represents a `Cookies` token
 */
class Cookies
    : public BrowserValue
    , public service::Cookies
{
public:
    Cookies(il::InterLevel * il, il::TargetData target = {});

    // properties level 2

    /// `Cookies'tab`
    void runTab();

    // methods level 2

    /// `Cookies.deleteAll`
    void runDeleteAll(const memory::ArgList & args);

    /// `Cookies.get`
    void runGet(const memory::ArgList & args);

    /// `Cookies.new`
    void runNew(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace ce
}  // namespace icL

#endif  // ce_Cookies
