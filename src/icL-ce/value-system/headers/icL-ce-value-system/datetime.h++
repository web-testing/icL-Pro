#ifndef ce_DateTime
#define ce_DateTime

#include <icL-service-value-system/datetime.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

/**
 * @brief The icDateTime class respresent a `icDateTime` token
 */
class icDateTime
    : public SystemValue
    , public service::DateTime
{
public:
    icDateTime(il::InterLevel * il);

    // methods level 2

    /// `icDateTime.current`
    void runCurrent(const memory::ArgList & args);

    /// `icDateTime.currentUTC`
    void runCurrentUTC(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_DateTime
