#ifndef ce_Types
#define ce_Types

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

/**
 * @brief The Types class represents `Types` token
 */
class Types : public SystemValue
{
public:
    Types(il::InterLevel * il);

    // properties level 2

    /// `Types'bool`
    void runBool();

    /// `Types'cookie`
    void runCookie();

    /// `Types'database`
    void runDatabase();

    /// `Types'document`
    void runDocument();

    /// `Types'double`
    void runDouble();

    /// `Types'element`
    void runElement();

    /// `Types'file`
    void runFile();

    /// `Types'handler`
    void runHandler();

    /// `Types'int`
    void runInt();

    /// `Types'lambda-icl`
    void runLambda_icL();

    /// `Types'lambda-js`
    void runLambdaJs();

    /// `Types'lambda-sql`
    void runLambdaSql();

    /// `Types'list`
    void runList();

    /// `Types'listener`
    void runListener();

    /// `Types'object`
    void runObject();

    /// `Types'query`
    void runQuery();

    /// `Types'set`
    void runSet();

    /// `Types'session`
    void runSession();

    /// `Types'string`
    void runString();

    /// `Types'tab`
    void runTab();

    /// `Types'void`
    void runVoid();

    /// `Types'window`
    void runWindow();

    // Value interface
public:
    memory::Type type() const override;
    icString     typeName() override;

    void runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::ce

#endif  // ce_Types
