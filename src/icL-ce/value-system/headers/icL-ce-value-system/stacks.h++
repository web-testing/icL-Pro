#ifndef ce_Stacks
#define ce_Stacks

#include <icL-service-value-system/stacks.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class Stack;

class Stacks
    : public SystemValue
    , public service::Stacks
{
public:
    Stacks(il::InterLevel * il);

    // properties level 2

    /// `Stacks'(string)`
    void runPropertyLevel2(const icString & name);

    // Value interface
public:
    memory::Type type() const override;
    icString     typeName() override;
    void         runProperty(Prefix prefix, const icString & name) override;
};

}  // namespace icL::ce

#endif  // ce_Stacks
