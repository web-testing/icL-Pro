#ifndef ce_Math
#define ce_Math

#include <icL-service-value-system/math.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class Math
    : public SystemValue
    , public service::Math
{
public:
    Math(il::InterLevel * il);

    // properties level 2

    /// `Math'1divPi`
    void run1divPi();

    /// `Math'1divSqrt2`
    void run1divSqrt2();

    /// `Math'2divPI`
    void run2divPi();

    /// `Math'2divSqrtPi`
    void run2divSqrtPi();

    /// `Math'e`
    void runE();

    /// `Math'ln2`
    void runLn2();

    /// `Math'ln10`
    void runLn10();

    /// `Math'log2e`
    void runLog2e();

    /// `Math'log10e`
    void runLog10e();

    /// `Math'pi`
    void runPi();

    /// `Math'piDiv2`
    void runPiDiv2();

    /// `Math'piDiv4`
    void runPiDiv4();

    /// `Math'sqrt2`
    void runSqrt2();

    // methods level 2

    /// `Math.acos`
    void runAcos(const memory::ArgList & args);

    /// `Math.asin`
    void runAsin(const memory::ArgList & args);

    /// `Math.atan`
    void runAtan(const memory::ArgList & args);

    /// `Math.ceil`
    void runCeil(const memory::ArgList & args);

    /// `Math.cos`
    void runCos(const memory::ArgList & args);

    /// `Math.degreesToRadians`
    void runDegreesToRadians(const memory::ArgList & args);

    /// `Math.exp`
    void runExp(const memory::ArgList & args);

    /// `Math.floor`
    void runFloor(const memory::ArgList & args);

    /// `Math.ln`
    void runLn(const memory::ArgList & args);

    /// `Math.max`
    void runMax(const memory::ArgList & args);

    /// `Math.min`
    void runMin(const memory::ArgList & args);

    /// `Math.radiansToDegrees`
    void runRadiansToDegrees(const memory::ArgList & args);

    /// `Math.round`
    void runRound(const memory::ArgList & args);

    /// `Math.sin`
    void runSin(const memory::ArgList & args);

    /// `Math.tan`
    void runTan(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_Math
