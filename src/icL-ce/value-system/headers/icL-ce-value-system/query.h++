#ifndef ce_Query
#define ce_Query

#include <icL-il/structures/db-target.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class Query : public SystemValue
{
public:
    Query(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_Query
