#ifndef ce_Stack
#define ce_Stack

#include <icL-service-value-system/stack.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class Stack
    : public SystemValue
    , public service::Stack
{
public:
    Stack(il::InterLevel * il, memory::StackContainer * stack);

    // properties level 1

    /// `[r/w] Stack'(name : string) : any`
    CE * property(const icString & name);

    // properties level 2

    /// `Stack'(string)`
    void runPropertyLevel2(const icString & name);

    // methods level 2

    /// `Stack.addDescription`
    void runAddDescription(const memory::ArgList & args);

    /// `Stack.break`
    void runBreak(const memory::ArgList & args);

    /// `Stack.clear`
    void runClear(const memory::ArgList & args);

    /// `Stack.continue`
    void runContinue(const memory::ArgList & args);

    /// `Stack.destroy`
    void runDestroy(const memory::ArgList & args);

    /// `Stack.ignore`
    void runIgnore(const memory::ArgList & args);

    /// `Stack.listen`
    void runListen(const memory::ArgList & args);

    /// `Stack.markStep`
    void runMarkStep(const memory::ArgList & args);

    /// `Stack.markTest`
    void runMarkTest(const memory::ArgList & args);

    /// `Stack.return`
    void runReturn(const memory::ArgList & args);

    // Value interface
public:
    memory::Type type() const override;
    icString     typeName() override;
    void         runProperty(Prefix prefix, const icString & name) override;
    void         runMethod(
              const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_Stack
