#ifndef ce_Signal
#define ce_Signal

#include <icL-service-value-system/signal.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class Signal
    : public SystemValue
    , public service::Signal
{
public:
    Signal(il::InterLevel * il);

    // properties level 2

    /// `Signal'(string)`
    void runPropertyLevel2(const icString & name);

    // methods level 2

    /// `Signal.add`
    void runAdd(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runProperty(Prefix prefix, const icString & name) override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_Signal
