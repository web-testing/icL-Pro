#ifndef ce_DBManager
#define ce_DBManager

#include <icL-service-value-system/dbmanager.h++>

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class DBManager
    : public SystemValue
    , public service::DBManager
{
public:
    DBManager(il::InterLevel * il);

    // methods level 2

    /// `DBManager.connect`
    void runConnect(const memory::ArgList & args);

    /// `DBManager.openSQLite`
    void runOpenSQLite(const memory::ArgList & args);

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
    void     runMethod(
          const icString & name, const memory::ArgList & args) override;
};

}  // namespace icL::ce

#endif  // ce_DBManager
