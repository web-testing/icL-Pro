#ifndef ce_DB
#define ce_DB

#include <icL-ce-base/value/system-value.h++>



namespace icL::ce {

class DB : public SystemValue
{
public:
    DB(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // Value interface
public:
    Type     type() const override;
    icString typeName() override;
};

}  // namespace icL::ce

#endif  // ce_DB
