#include "types.h++"

#include <icL-types/replaces/ic-object.h++>

#include <icL-ce-base/main/value-run-property-with-prefix-check.h++>
#include <icL-ce-value-base/base/int-value.h++>

namespace icL::ce {

Types::Types(il::InterLevel * il)
    : SystemValue(il) {}

void Types::runBool() {
    m_newContext = new IntValue{il, int(Type::BoolValue)};
}

void Types::runCookie() {
    m_newContext = new IntValue{il, int(Type::CookieValue)};
}

void Types::runDatabase() {
    m_newContext = new IntValue{il, int(Type::DatabaseValue)};
}

void Types::runDocument() {
    m_newContext = new IntValue{il, int(Type::DocumentValue)};
}

void Types::runDouble() {
    m_newContext = new IntValue{il, int(Type::DoubleValue)};
}

void Types::runElement() {
    m_newContext = new IntValue{il, int(Type::ElementValue)};
}

void Types::runFile() {
    m_newContext = new IntValue{il, int(Type::FileValue)};
}

void Types::runHandler() {
    m_newContext = new IntValue{il, int(Type::HandlerValue)};
}

void Types::runInt() {
    m_newContext = new IntValue{il, int(Type::IntValue)};
}

void Types::runLambda_icL() {
    m_newContext = new IntValue{il, int(Type::LambdaValue)};
}

void Types::runLambdaJs() {
    m_newContext = new IntValue{il, int(Type::JavaScriptLambdaValue)};
}

void Types::runLambdaSql() {
    m_newContext = new IntValue{il, int(Type::SqlLambdaValue)};
}

void Types::runList() {
    m_newContext = new IntValue{il, int(Type::ListValue)};
}

void Types::runListener() {
    m_newContext = new IntValue{il, int(Type::ListenerValue)};
}

void Types::runObject() {
    m_newContext = new IntValue{il, int(Type::ObjectValue)};
}

void Types::runQuery() {
    m_newContext = new IntValue{il, int(Type::QueryValue)};
}

void Types::runSet() {
    m_newContext = new IntValue{il, int(Type::SetValue)};
}

void Types::runSession() {
    m_newContext = new IntValue{il, int(Type::SessionValue)};
}

void Types::runString() {
    m_newContext = new IntValue{il, int(Type::StringValue)};
}

void Types::runTab() {
    m_newContext = new IntValue{il, int(Type::TabValue)};
}

void Types::runVoid() {
    m_newContext = new IntValue{il, int(Type::VoidValue)};
}

void Types::runWindow() {
    m_newContext = new IntValue{il, int(Type::WindowValue)};
}

memory::Type Types::type() const {
    return Type::Types;
}

icString Types::typeName() {
    return "Types";
}

void Types::runProperty(Prefix prefix, const icString & name) {
    static const icObject<icString, void (Types::*)()> properties{
      {"bool", &Types::runBool},         {"cookie", &Types::runCookie},
      {"database", &Types::runDatabase}, {"double", &Types::runDouble},
      {"element", &Types::runElement},   {"file", &Types::runFile},
      {"handler", &Types::runHandler},   {"int", &Types::runInt},
      {"list", &Types::runList},         {"listener", &Types::runListener},
      {"object", &Types::runObject},     {"query", &Types::runQuery},
      {"set", &Types::runSet},           {"session", &Types::runSession},
      {"string", &Types::runString},     {"tab", &Types::runTab},
      {"void", &Types::runVoid},         {"window", &Types::runWindow}};

    runPropertyWithPrefixCheck<Types, SystemValue>(properties, prefix, name);
}

}  // namespace icL::ce
