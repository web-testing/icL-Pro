#include "dsv.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/structures/file-target.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-base/complex/set-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

DSV::DSV(il::InterLevel * il)
    : SystemValue(il) {}

void DSV::runAppend(const memory::ArgList & args) {
    if (checkArgs(args, {Type::FileValue, Type::SetValue})) {
        append(args[0], memory::Set(args[1]));
        m_newContext = new VoidValue{il};
    }
    else if (checkArgs(args, {Type::FileValue, Type::ObjectValue})) {
        append(args[0], memory::Object(args[1]));
        m_newContext = new VoidValue{il};
    }
}

void DSV::runLoad(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::FileValue, Type::SetValue})) {
        m_newContext = new SetValue{il, load(args[0], args[1], args[2])};
    }
}

void DSV::runLoadCSV(const memory::ArgList & args) {
    if (checkArgs(args, {Type::FileValue, Type::SetValue})) {
        m_newContext = new SetValue{il, loadCSV(args[0], args[1])};
    }
}

void DSV::runLoadTSV(const memory::ArgList & args) {
    if (checkArgs(args, {Type::FileValue, Type::SetValue})) {
        m_newContext = new SetValue{il, loadTSV(args[0], args[1])};
    }
}

void DSV::runSync(const memory::ArgList & args) {
    if (checkArgs(args, {Type::FileValue, Type::SetValue})) {
        sync(args[0], args[1]);
        m_newContext = new VoidValue{il};
    }
}

void DSV::runWrite(const memory::ArgList & args) {
    if (checkArgs(args, {Type::FileValue, Type::SetValue})) {
        write(args[0], args[1]);
        m_newContext = new VoidValue{il};
    }
}

void DSV::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (DSV::*)(const memory::ArgList &)> methods{
      {"append", &DSV::runAppend},   {"load", &DSV::runLoad},
      {"loadCSV", &DSV::runLoadCSV}, {"loadTSV", &DSV::runLoadTSV},
      {"sync", &DSV::runSync},       {"write", &DSV::runWrite}};

    runMethodNow<DSV, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
