#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/structures/target-data.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-value-base/base/bool-value.h++>
#include <icL-ce-value-base/base/double-value.h++>
#include <icL-ce-value-base/base/int-value.h++>
#include <icL-ce-value-base/base/string-value.h++>
#include <icL-ce-value-base/base/void-value.h++>
#include <icL-ce-value-base/browser/tab-value.h++>
#include <icL-ce-value-base/complex/list-value.h++>

#include <icL-memory/structures/argument.h++>

#include <request.h++>



namespace icL::ce {

Request::Request(il::InterLevel * il)
    : SystemValue(il) {}

void Request::runConfirm(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        confirm(args[0]);
        m_newContext = new VoidValue{il};
    }
}

void Request::runAsk(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new BoolValue{il, ask(args[0])};
    }
}

void Request::runInt(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new IntValue{il, int_(args[0])};
    }
}

void Request::runDouble(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new DoubleValue{il, double_(args[0])};
    }
}

void Request::runString(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new StringValue{il, string(args[0])};
    }
}

void Request::runList(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new ListValue{il, list(args[0])};
    }
}

void Request::runTab(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new TabValue{il, tab(args[0])};
    }
}

Type Request::type() const {
    return Type::Request;
}

icString Request::typeName() {
    return "Request";
}

void Request::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Request::*)(const memory::ArgList &)>
      methods{
        {"confirm", &Request::runConfirm}, {"ask", &Request::runAsk},
        {"int", &Request::runInt},         {"double", &Request::runDouble},
        {"string", &Request::runString},   {"list", &Request::runList},
        {"tab", &Request::runTab}};

    runMethodNow<Request, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
