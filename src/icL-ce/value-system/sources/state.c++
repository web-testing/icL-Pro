#include "state.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-value-base/base/void-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

State::State(il::InterLevel * il)
    : SystemValue(il) {}

void State::runClear(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        clear();
        m_newContext = new VoidValue{il};
    }
}

void State::runDelete(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        delete_();
        m_newContext = new VoidValue{il};
    }
}

void State::runNew(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        new_(args[0]);
        m_newContext = new VoidValue{il};
    }
    else if (checkArgs(args, {})) {
        new_({});
        m_newContext = new VoidValue{il};
    }
}

void State::runNewAtEnd(const memory::ArgList & args) {
    if (checkArgs(args, {Type::ObjectValue})) {
        newAtEnd(args[0]);
        m_newContext = new VoidValue{il};
    }
    else if (checkArgs(args, {})) {
        newAtEnd({});
        m_newContext = new VoidValue{il};
    }
}

void State::runToFirst(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        toFirst();
        m_newContext = new VoidValue{il};
    }
}

void State::runToLast(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        toLast();
        m_newContext = new VoidValue{il};
    }
}

void State::runToNext(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        toNext();
        m_newContext = new VoidValue{il};
    }
}

void State::runToPrev(const memory::ArgList & args) {
    if (checkArgs(args, {})) {
        toPrev();
        m_newContext = new VoidValue{il};
    }
}

const icSet<Role> & State::acceptedNexts() {
    static icSet<Role> roles{SystemValue::acceptedNexts(), {Role::NoRole}};
    return roles;
}

Type State::type() const {
    return Type::State;
}

icString State::typeName() {
    return "State";
}

void State::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (State::*)(const memory::ArgList &)> methods{
      {"clear", &State::runClear},     {"delete", &State::runDelete},
      {"new", &State::runNew},         {"newAtEnd", &State::runNewAtEnd},
      {"toFirst", &State::runToFirst}, {"toLast", &State::runToLast},
      {"toNext", &State::runToNext},   {"toPrev", &State::runToPrev}};

    runMethodNow<State, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
