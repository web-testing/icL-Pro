#include "dbmanager.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-il/structures/db-target.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-value-base/system/db-value.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

DBManager::DBManager(il::InterLevel * il)
    : SystemValue(il) {}

void DBManager::runConnect(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new DBValue{il, connect(args[0])};
    }
    else if (checkArgs(
               args,
               {Type::StringValue, Type::StringValue, Type::StringValue})) {
        m_newContext = new DBValue{il, connect(args[0], args[1], args[2])};
    }
}

void DBManager::runOpenSQLite(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        m_newContext = new DBValue{il, openSQLite(args[0])};
    }
}

Type DBManager::type() const {
    return Type::DBManager;
}

icString DBManager::typeName() {
    return "DBManager";
}

void DBManager::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (DBManager::*)(const memory::ArgList &)>
      methods{{"connect", &DBManager::runConnect},
              {"openSQLite", &DBManager::runOpenSQLite}};

    runMethodNow<DBManager, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
