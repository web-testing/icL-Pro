#include "make.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/value-run-method.h++>

#include <icL-memory/structures/argument.h++>



namespace icL::ce {

Make::Make(il::InterLevel * il)
    : SystemValue(il) {}


void Make::runImage(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue, Type::StringValue})) {
        image(args[0], args[1]);
    }
}

Type Make::type() const {
    return Type::Make;
}

icString Make::typeName() {
    return "Make";
}

void Make::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Make::*)(const memory::ArgList &)> methods{
      {"image", &Make::runImage}};

    runMethodNow<Make, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
