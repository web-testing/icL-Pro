#include "import.h++"

#include <icL-types/replaces/ic-object.h++>
#include <icL-types/replaces/ic-pair.h++>

#include <icL-ce-base/main/value-run-method.h++>
#include <icL-ce-value-base/base/void-value.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/set.h++>



namespace icL::ce {

Import::Import(il::InterLevel * il)
    : SystemValue(il) {}

void Import::runAll(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        all({icVariantObject{}}, args[0]);
        m_newContext = new VoidValue{il};
    }
    else if (checkArgs(args, {Type::ObjectValue, Type::StringValue})) {
        all(args[0], args[1]);
        m_newContext = new VoidValue{il};
    }
}

void Import::runFunctions(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        functions({icVariantObject{}}, args[0]);
        m_newContext = new VoidValue{il};
    }
    else if (checkArgs(args, {Type::ObjectValue, Type::StringValue})) {
        functions(args[0], args[1]);
        m_newContext = new VoidValue{il};
    }
}

void Import::runNone(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        none({icVariantObject{}}, args[0]);
        m_newContext = new VoidValue{il};
    }
    else if (checkArgs(args, {Type::ObjectValue, Type::StringValue})) {
        none(args[0], args[1]);
        m_newContext = new VoidValue{il};
    }
}

void Import::runRun(const memory::ArgList & args) {
    if (checkArgs(args, {Type::StringValue})) {
        run(args[0]);
        m_newContext = new VoidValue{il};
    }
}

Type Import::type() const {
    return Type::Import;
}

icString Import::typeName() {
    return "Import";
}

void Import::runMethod(const icString & name, const memory::ArgList & args) {
    static icObject<icString, void (Import::*)(const memory::ArgList &)>
      methods{{"all", &Import::runAll},
              {"functions", &Import::runFunctions},
              {"none", &Import::runNone},
              {"run", &Import::runRun}};

    runMethodNow<Import, SystemValue>(methods, name, args);
}

}  // namespace icL::ce
