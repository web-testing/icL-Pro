#include "db.h++"

#include <icL-il/main/db-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>

#include <icL-ce-value-base/system/db-value.h++>



namespace icL::ce {

DB::DB(il::InterLevel * il)
    : SystemValue(il) {}

int DB::currentRunRank(bool rtl) {
    return rtl ? 8 : 0;
}

StepType DB::runNow() {
    il::DB db;

    db.target    = std::make_shared<il::DBTarget>(il->db->getCurrentTarget());
    m_newContext = new DBValue{il, db};

    return StepType::CommandEnd;
}

Type DB::type() const {
    return Type::DB;
}

icString DB::typeName() {
    return "DB";
}

}  // namespace icL::ce
