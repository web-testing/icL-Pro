#include "query.h++"

#include <icL-il/main/db-server.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/structures/db-target.h++>

#include <icL-ce-value-base/system/query-value.h++>



namespace icL::ce {

Query::Query(il::InterLevel * il)
    : SystemValue(il) {}

int Query::currentRunRank(bool rtl) {
    return rtl ? 8 : 0;
}

StepType Query::runNow() {
    il::Query query;

    query.target = std::make_shared<il::DBTarget>(il->db->getCurrentTarget());
    m_newContext = new QueryValue{il, query};

    return StepType::CommandEnd;
}

Type Query::type() const {
    return Type::Query;
}

icString Query::typeName() {
    return "Query";
}

}  // namespace icL::ce
