#ifndef ce_While
#define ce_While

#include <icL-service-keyword/loop/while.h++>

#include <icL-ce-base/keyword/loop-keyword.h++>



namespace icL::ce {

class While
    : public LoopKeyword
    , public service::While
{
public:
    While(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;
    CE * firstToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;

    // INode interface
protected:
    il::InterLevel * _il() override;
};

}  // namespace icL::ce

#endif  // ce_While
