#ifndef ce_Do
#define ce_Do

#include <icL-ce-base/keyword/loop-keyword.h++>



namespace icL::ce {

class Do : public LoopKeyword
{
public:
    Do(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;
};

}  // namespace icL::ce

#endif  // ce_Do
