#ifndef ce_ForCollection
#define ce_ForCollection

#include "for-base.h++"

#include <icL-service-keyword/loop/for-collection.h++>



namespace icL::ce {

class ForCollection
    : public ForBase
    , public service::ForCollection
{
public:
    ForCollection(il::InterLevel * il, bool reverse, int maxX);

    // CE interface
public:
    StepType runNow() override;
    icString toString() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::ce

#endif  // ce_ForCollection
