#ifndef ce_ForBase
#define ce_ForBase

#include <icL-service-keyword/loop/loop.h++>

#include <icL-ce-base/keyword/loop-keyword.h++>



namespace icL::ce {

class ForBase : public LoopKeyword
{
public:
    ForBase(il::InterLevel * il);

    // il.CE interface
public:
    int currentRunRank(bool rtl) override;

private:
    // ce.CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;
};

}  // namespace icL::ce

#endif  // ce_ForBase
