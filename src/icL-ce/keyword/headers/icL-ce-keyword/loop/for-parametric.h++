#ifndef ce_ForParametric
#define ce_ForParametric

#include "for-base.h++"

#include <icL-service-keyword/loop/for-parametric.h++>



namespace icL::ce {

class ForParametric
    : public ForBase
    , public service::ForParametric
{
public:
    ForParametric(il::InterLevel * il, bool alt);


    // CE interface
public:
    StepType runNow() override;
    icString toString() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::ce

#endif  // ce_ForParametric
