#ifndef ce_Else
#define ce_Else

#include <icL-ce-base/keyword/control-keyword.h++>



namespace icL::ce {

/**
 * @brief The Else class represents an `else` token
 */
class Else : public ControlKeyword
{
public:
    Else(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;
};

}  // namespace icL::ce

#endif  // ce_Else
