#ifndef ce_IfExists
#define ce_IfExists

#include "if-base.h++"

#include <icL-service-keyword/control/if-exists.h++>



namespace icL::ce {

class IfExists
    : public IfBase
    , public service::IfExists
{
public:
    IfExists(il::InterLevel * il, bool notMod);

    // CE interface
public:
    StepType runNow() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // IfBase interface
protected:
    bool hasNotModifier() override;
    void setNotModifier() override;
};

}  // namespace icL::ce

#endif  // ce_IfExists
