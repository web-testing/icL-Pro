#ifndef ce_IfBase
#define ce_IfBase

#include <icL-service-keyword/main/finite-state-machine.h++>

#include <icL-ce-base/keyword/control-keyword.h++>



namespace icL::ce {

class IfBase : public ControlKeyword
{
public:
    IfBase(il::InterLevel * il);

protected:
    virtual bool hasNotModifier() = 0;
    virtual void setNotModifier() = 0;

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    icString toString() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;
};

}  // namespace icL::ce

#endif  // ce_IfBase
