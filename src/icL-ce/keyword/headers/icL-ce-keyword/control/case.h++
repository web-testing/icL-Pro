#ifndef ce_Case
#define ce_Case

#include <icL-ce-base/keyword/control-keyword.h++>



namespace icL::ce {

class Case : public ControlKeyword
{
public:
    Case(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;
};

}  // namespace icL::ce

#endif  // ce_Case
