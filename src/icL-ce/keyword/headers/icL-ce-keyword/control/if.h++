#ifndef ce_If
#define ce_If

#include "if-base.h++"

#include <icL-service-keyword/control/if.h++>



namespace icL::ce {

class If
    : public IfBase
    , public service::If
{
public:
    If(il::InterLevel * il, bool notMod);

    // CE interface
public:
    StepType runNow() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // IfBase interface
protected:
    bool hasNotModifier() override;
    void setNotModifier() override;
};

}  // namespace icL::ce

#endif  // ce_If
