#ifndef ce_Switch
#define ce_Switch

#include <icL-service-keyword/control/switch.h++>

#include <icL-ce-base/keyword/control-keyword.h++>



namespace icL::ce {

class Switch
    : public ControlKeyword
    , public service::Switch
{
public:
    Switch(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::ce

#endif  // ce_Switch
