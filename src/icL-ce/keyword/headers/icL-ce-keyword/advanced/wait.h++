#ifndef ce_Wait
#define ce_Wait

#include <icL-service-keyword/advanced/wait.h++>

#include <icL-ce-base/keyword/advanced-keyword.h++>



namespace icL::ce {

class Wait
    : public AdvancedKeyword
    , public service::Wait
{
public:
    Wait(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    CE * lastToReplace() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;
};

}  // namespace icL::ce

#endif  // ce_Wait
