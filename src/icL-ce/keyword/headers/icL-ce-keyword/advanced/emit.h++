#ifndef ce_Emit
#define ce_Emit

#include <icL-ce-base/keyword/advanced-keyword.h++>



namespace icL::ce {

class Emit : public AdvancedKeyword
{
public:
    Emit(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;

private:
    int errorCode = 0;
};

}  // namespace icL::ce

#endif  // ce_Emit
