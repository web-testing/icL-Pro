#ifndef ce_Now
#define ce_Now

#include <icL-ce-base/keyword/advanced-keyword.h++>



namespace icL::ce {

/**
 * @brief The Now class respresent the `now` keyword
 */
class Now : public AdvancedKeyword
{
public:
    Now(il::InterLevel * il);

    // CE interface
public:
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    icString toString() override;

    // CE interface
public:
    Role role() override;
    CE * lastToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;
};

}  // namespace icL::ce

#endif  // ce_Now
