#ifndef ce_Emitter
#define ce_Emitter

#include <icL-service-keyword/advanced/emitter.h++>

#include <icL-ce-base/keyword/advanced-keyword.h++>



namespace icL::ce {

class Emitter
    : public AdvancedKeyword
    , public service::Emitter
{
public:
    Emitter(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;
    CE * lastToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;
};

}  // namespace icL::ce

#endif  // ce_Emitter
