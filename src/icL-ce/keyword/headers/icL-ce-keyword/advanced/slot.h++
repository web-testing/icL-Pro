#ifndef ce_Slot
#define ce_Slot

#include <icL-types/replaces/ic-list.h++>

#include <icL-service-keyword/advanced/slot.h++>

#include <icL-ce-base/keyword/control-keyword.h++>



namespace icL::ce {

class Slot
    : public ControlKeyword
    , public service::Slot
{
public:
    Slot(il::InterLevel * il);
    ~Slot() override = default;

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;

private:
    icList<int> handleCodes;

    // Slot interface
public:
    bool check(int code) override;
};

}  // namespace icL::ce

#endif  // ce_Slot
