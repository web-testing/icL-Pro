#ifndef ce_Jammer
#define ce_Jammer

#include <icL-service-keyword/advanced/jammer.h++>

#include <icL-ce-base/keyword/advanced-keyword.h++>



namespace icL::ce {

class Jammer
    : public AdvancedKeyword
    , public service::Jammer
{
public:
    Jammer(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;
    CE * lastToReplace() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;

    // Keyword interface
public:
    void giveModifiers(const icStringList & modifiers) override;
};

}  // namespace icL::ce

#endif  // ce_Jammer
