#ifndef ce_Assert
#define ce_Assert

#include <icL-service-keyword/advanced/assert.h++>

#include <icL-ce-base/keyword/advanced-keyword.h++>



namespace icL::ce {

class Assert
    : public AdvancedKeyword
    , public service::Assert
{
public:
    Assert(il::InterLevel * il);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    // CE interface
public:
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    CE * lastToReplace() override;

    // FiniteStateMachine interface
public:
    void initialize() override;
    void finalize() override;
};

}  // namespace icL::ce

#endif  // ce_Assert
