#include "if.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-service-main/factory/factory.h++>

#include <icL-ce-operators-ALU/system/context.h++>


namespace icL::ce {

If::If(il::InterLevel * il, bool notMod)
    : IfBase(il) {
    notModifier = notMod;
}

StepType If::runNow() {
    return transact();
}

void If::initialize() {
    bool ok   = true;
    CE * it   = this;
    Role prev = Role::NoRole;

    while (it != nullptr && ok) {
        Role next = it->next() == nullptr
                      ? Role::NoRole
                      : dynamic_cast<CE *>(it->next())->role();

        switch (it->role()) {
        case Role::If:
            ok = prev == Role::NoRole || prev == Role::Else;
            break;

        case Role::Value:
        case Role::ValueContext:
            ok = prev == Role::If;
            break;

        case Role::RunContext:
            ok = prev == Role::Value || prev == Role::ValueContext ||
                 (prev == Role::Else && next == Role::NoRole);
            break;

        case Role::Else:
            ok = prev == Role::RunContext;
            break;

        default:
            ok = false;
        }

        prev = it->role();
        it   = dynamic_cast<CE *>(it->next());
    }

    if (!ok || prev != Role::RunContext) {
        il->vm->syssig("Syntax error in if-else statement");
        return;
    }

    it = this;

    while (it != nullptr) {
        if (it->role() == Role::If) {
            IfData ifData;

            auto * if_   = dynamic_cast<If *>(it);
            auto * value = dynamic_cast<CE *>(it->next());
            auto * runCx = dynamic_cast<Context *>(value->next());

            ifData.notModifier = if_->notModifier;
            ifData.body        = runCx->getCode();

            if (value->role() == Role::Value) {
                ifData.condition = value->fragmentData();
            }
            else {
                ifData.condition = dynamic_cast<Context *>(value)->getCode();
            }

            ifs.append(ifData);
            it = dynamic_cast<CE *>(runCx->next());
        }
        else /* role == else */ {
            elseCode = dynamic_cast<Context *>(it->next())->getCode();
            it       = nullptr;
        }
    }
}

void If::finalize() {
    m_newContext = service::Factory::fromValue(il, ret);
}

bool If::hasNotModifier() {
    return notModifier;
}

void If::setNotModifier() {
    notModifier = true;
}

}  // namespace icL::ce
