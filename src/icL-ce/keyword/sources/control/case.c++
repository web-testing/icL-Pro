#include "case.h++"

#include <icL-types/replaces/ic-set.h++>

#include <cassert>



namespace icL::ce {

Case::Case(il::InterLevel * il)
    : ControlKeyword(il) {}

icString Case::toString() {
    return "case";
}

int Case::currentRunRank(bool /*rtl*/) {
    return -1;
}

StepType Case::runNow() {
    assert(false);  // to be never called
    return StepType::None;
}

Role Case::role() {
    return Role::Case;
}

const icSet<Role> & Case::acceptedPrevs() {
    static const icSet<Role> roles{Role::ValueContext, Role::Value,
                                   Role::RunContext};
    return roles;
}

const icSet<Role> & Case::acceptedNexts() {
    static const icSet<Role> roles{Role::ValueContext, Role::Value};
    return roles;
}

}  // namespace icL::ce
