#include "if-base.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>



namespace icL::ce {

IfBase::IfBase(il::InterLevel * il)
    : ControlKeyword(il) {}

int IfBase::currentRunRank(bool rtl) {
    bool runnable = m_prev == nullptr;
    return runnable && !rtl ? 9 : -1;
}

icString IfBase::toString() {
    return hasNotModifier() ? "if-not" : "if";
}

Role IfBase::role() {
    return Role::If;
}

const icSet<Role> & IfBase::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole, Role::Else};
    return roles;
}

const icSet<Role> & IfBase::acceptedNexts() {
    static const icSet<Role> roles{Role::Value, Role::ValueContext,
                                   Role::Exists};
    return roles;
}

void IfBase::giveModifiers(const icStringList & modifiers) {
    for (auto & modifier : modifiers) {
        if (modifier == "not") {
            if (hasNotModifier()) {
                il->vm->cpe_sig("if: `not` modifier setted twince");
            }
            else {
                setNotModifier();
                break;
            }
        }
        else {
            il->vm->cpe_sig("if: No such modifier: " + modifier);
            break;
        }
    }
}

}  // namespace icL::ce
