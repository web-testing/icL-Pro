#include "else.h++"

#include <icL-types/replaces/ic-set.h++>

#include <cassert>



namespace icL::ce {

Else::Else(il::InterLevel * il)
    : ControlKeyword(il) {}

icString Else::toString() {
    return "else";
}

int Else::currentRunRank(bool /*rtl*/) {
    return -1;
}

StepType Else::runNow() {
    assert(false);  // to be never called
    return StepType::None;
}

Role Else::role() {
    return Role::Else;
}

const icSet<Role> & Else::acceptedPrevs() {
    static const icSet<Role> roles = {Role::RunContext};
    return roles;
}

const icSet<Role> & Else::acceptedNexts() {
    static const icSet<Role> roles = {Role::If, Role::RunContext};
    return roles;
}

}  // namespace icL::ce
