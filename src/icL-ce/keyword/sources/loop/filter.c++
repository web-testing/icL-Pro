#include "filter.h++"

#include <icL-types/replaces/ic-list.h++>
#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce-operators-ALU/system/context.h++>
#include <icL-ce-value-base/base/int-value.h++>



namespace icL::ce {

using memory::Signals::Signals;

Filter::Filter(il::InterLevel * il)
    : LoopKeyword(il) {}

icString Filter::toString() {
    icString str = "filter";

    if (reverse)
        str += "-reverse";
    if (max != -1)
        str += "-max" % icString::number(max);

    return str;
}

int Filter::currentRunRank(bool /*rtl*/) {
    auto * nextNext = dynamic_cast<CE *>(m_next->next());

    return m_next->role() == Role::ValueContext &&
               nextNext->role() == Role::RunContext
             ? 9
             : -1;
}

StepType Filter::runNow() {
    return transact();
}

Role Filter::role() {
    return Role::Filter;
}

const icSet<Role> & Filter::acceptedPrevs() {
    static icSet<Role> roles{Role::NoRole};
    return roles;
}

const icSet<Role> & Filter::acceptedNexts() {
    static icSet<Role> roles{Role::ValueContext};
    return roles;
}

void Filter::initialize() {
    auto commands =
      il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

    if (commands.length() != 2) {
        il->vm->signal(
          {Signals::System,
           "filter: round brackets content must contians 2 commands"});
    }
    else {
        collectionCode = commands[0];
        conditionCode  = commands[1];
        loopBody       = dynamic_cast<Context *>(m_next->next())->getCode();
    }
}

void Filter::finalize() {
    m_newContext = new IntValue{il, executed};
}

void Filter::giveModifiers(const icStringList & modifiers) {
    static icRegEx max_rx = R"(max(\d+))"_rx;
    icRegExMatch   match;

    for (auto & mod : modifiers) {
        if (mod == "reverse") {
            if (reverse) {
                il->vm->cpe_sig("filter: `reverse` modifier is repeated");
            }
            reverse = true;
        }
        else if ((match = max_rx.match(mod)).hasMatch()) {
            if (max > 0) {
                il->vm->cpe_sig("filter: `maxX` modifier is repeated");
            }
            else {
                max = match.captured(1).toInt();

                if (max < 2) {
                    il->vm->cpe_sig(
                      "filter: `maxX` modifier value must be begger then 1");
                }
            }
        }
    }
}

il::InterLevel * icL::ce::Filter::_il() {
    return il;
}

}  // namespace icL::ce
