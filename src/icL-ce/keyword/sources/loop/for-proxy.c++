#include "for-proxy.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/cp.h++>
#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-ce-operators-ALU/system/context.h++>

#include <for-collection.h++>
#include <for-ever.h++>
#include <for-parametric.h++>



namespace icL::ce {

using memory::Signals::Signals;

ForProxy::ForProxy(il::InterLevel * il)
    : ForBase(il) {}

int ForProxy::currentRunRank(bool rtl) {
    return rtl && m_next->role() != Role::Any ? 9 : -1;
}

StepType ForProxy::runNow() {
    auto commands =
      il->cpu->splitCommands(dynamic_cast<Context *>(m_next)->getCode());

    switch (flags) {
    case Alt:
        m_newContext = new ForParametric(il, true);
        break;

    case None:
        if (commands.length() == 1) {
            m_newContext = new ForCollection(il, false, -1);
        }
        else {
            m_newContext = new ForParametric(il, false);
        }
        break;

    case Ever:
    case XTimes:
        m_newContext = new ForEver(il, xTimesNumber);
        break;

    case MaxX:
    case Reverse:
    case MaxX | Reverse:
        m_newContext =
          new ForCollection(il, (flags & Reverse) != 0, maxXnumber);
        break;

    default:
        il->vm->signal({Signals::System, "for: wrong modifiers"});
    }

    return StepType::CommandEnd;
}

icString ForProxy::toString() {
    icString str = "for";

    if (flags & Alt)
        str += "-alt";
    if (flags & Ever)
        str += "-ever";
    if (flags & XTimes)
        str += '-' % icString::number(xTimesNumber) % "times";
    if (flags & MaxX)
        str += "-max" % icString::number(maxXnumber);
    if (flags & Reverse)
        str += "-reverse";

    return str;
}

CE * ForProxy::firstToReplace() {
    return this;
}

CE * ForProxy::lastToReplace() {
    return this;
}

void ForProxy::giveModifiers(const icStringList & modifiers) {
    static icRegEx xTimes = R"((\d+)times)"_rx;
    static icRegEx maxX   = R"(max(\d+))"_rx;

    icRegExMatch match;

    for (auto & mod : modifiers) {
        if (mod == "alt") {
            checkFlags(AllOn);
            flags = flags | Alt;
        }
        else if (mod == "ever") {
            checkFlags(AllOn);
            flags = flags | Ever;
        }
        else if (mod == "reverse") {
            checkFlags(AllOn ^ MaxX);
            flags = flags | Reverse;
        }
        else if ((match = maxX.match(mod)).hasMatch()) {
            checkFlags(AllOn ^ Reverse);
            flags      = flags | MaxX;
            maxXnumber = match.captured(1).toInt();
        }
        else if ((match = xTimes.match(mod)).hasMatch()) {
            checkFlags(AllOn);
            flags        = flags | XTimes;
            xTimesNumber = match.captured(1).toInt();
        }
        else {
            il->vm->cpe_sig("for: No such modifier: " % mod);
        }
    }
}

void ForProxy::checkFlags(int mask) {
    if ((flags & mask) != None) {
        il->vm->cpe_sig("for: Wrong modifiers set.");
    }
}

}  // namespace icL::ce
