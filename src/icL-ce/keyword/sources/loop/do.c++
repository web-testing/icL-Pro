#include "do.h++"

#include <icL-types/replaces/ic-set.h++>

namespace icL::ce {

Do::Do(il::InterLevel * il)
    : LoopKeyword(il) {}

icString Do::toString() {
    return "do";
}

int Do::currentRunRank(bool) {
    return -1;
}

StepType Do::runNow() {
    return StepType::None;
}

Role Do::role() {
    return Role::Do;
}

const icSet<Role> & Do::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole};
    return roles;
}

const icSet<Role> & Do::acceptedNexts() {
    static const icSet<Role> roles{Role::RunContext};
    return roles;
}

}  // namespace icL::ce
