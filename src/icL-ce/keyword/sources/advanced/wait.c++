#include "wait.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce-operators-ALU/system/context.h++>
#include <icL-ce-value-base/base/void-value.h++>



namespace icL::ce {

Wait::Wait(il::InterLevel * il)
    : AdvancedKeyword(il) {}

icString Wait::toString() {
    icString str = "wait";

    if (ajax)
        str += "-ajax";
    if (ms != -1)
        str += '-' + icString::number(ms) % "ms";
    if (s != -1)
        str += '-' + icString::number(s) % "s";

    return str;
}

int Wait::currentRunRank(bool) {
    return 9;
}

StepType Wait::runNow() {
    return transact();
}

CE * Wait::lastToReplace() {
    return m_next;
}

Role Wait::role() {
    return Role::Wait;
}

const icSet<Role> & Wait::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole};
    return roles;
}

const icSet<Role> & Wait::acceptedNexts() {
    static const icSet<Role> roles{Role::NoRole, Role::RunContext,
                                   Role::ValueContext};
    return roles;
}

void Wait::initialize() {
    if (m_next != nullptr) {
        condition = dynamic_cast<Context *>(m_next)->getCode();
    }
}

void Wait::finalize() {
    m_newContext = new VoidValue{il};
}

void Wait::giveModifiers(const icStringList & modifiers) {
    static icRegEx timeEx = R"(\A(\d+)(m?)s\z)"_rx;
    icRegExMatch   match;

    for (auto & mod : modifiers) {
        if (mod == "ajax") {
            if (ajax) {
                il->vm->cpe_sig("wait: `ajax` modifier is repeated");
            }
            else {
                ajax = true;
            }
        }
        else if (mod == "try") {
            if (try_) {
                il->vm->cpe_sig("wait: `try` modifier is repeated");
            }
            else {
                try_ = true;
            }
        }
        else if ((match = timeEx.match(mod)).hasMatch()) {
            if (ms != -1 || s != -1) {
                il->vm->cpe_sig("wait: Incompatible modifiers set");
            }
            else {
                if (match.captured(2).isEmpty()) {
                    s = match.captured(1).toInt();
                }
                else {
                    ms = match.captured(1).toInt();
                }
            }
        }
        else {
            il->vm->cpe_sig("wait: No such modifier: " % mod);
        }
    }
}

}  // namespace icL::ce
