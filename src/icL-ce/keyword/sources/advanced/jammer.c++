#include "jammer.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-string-list.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>

#include <icL-ce-operators-ALU/system/context.h++>
#include <icL-ce-value-base/base/int-value.h++>

namespace icL::ce {

Jammer::Jammer(il::InterLevel * il)
    : AdvancedKeyword(il) {}

icString Jammer::toString() {
    icString str = "jammer";

    if (catchSystem)
        str += "-system";

    return str;
}

int icL::ce::Jammer::currentRunRank(bool) {
    return m_next->next() == nullptr ? 9 : -1;
}

StepType Jammer::runNow() {
    return transact();
}

Role Jammer::role() {
    return Role::Jammer;
}

const icSet<Role> & Jammer::acceptedPrevs() {
    static const icSet<Role> roles{Role::NoRole};
    return roles;
}

const icSet<Role> & Jammer::acceptedNexts() {
    static const icSet<Role> roles{Role::RunContext, Role::ValueContext};
    return roles;
}

void Jammer::initialize() {
    code = dynamic_cast<Context *>(m_next)->getCode();
}

void Jammer::finalize() {
    m_newContext = new IntValue{il, errorCode};
}

void Jammer::giveModifiers(const icStringList & modifiers) {
    for (auto & mod : modifiers) {
        if (mod == "system") {
            if (catchSystem) {
                il->vm->cpe_sig("jammer: system modifier setted twince");
            }
            else {
                catchSystem = true;
            }
        }
        else {
            il->vm->cpe_sig("jammer: Unknown modifier: " + mod);
        }
    }
}

CE * icL::ce::Jammer::lastToReplace() {
    return m_next;
}

}  // namespace icL::ce
