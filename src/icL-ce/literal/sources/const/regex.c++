#include "regex.h++"

#include <icL-types/replaces/ic-regex.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>

#include <utility>


namespace icL::ce {

RegEx::RegEx(
  il::InterLevel * il, const icString & pattern, const icString & mods)
    : ConstLiteral(il, pattern)
    , mods(mods) {}

icVariant RegEx::getValueOf() {
    icRegEx check = R"(^[fimsux]*$)"_rx;

    auto match = check.match(mods);

    if (!match.hasMatch()) {
        il->vm->signal({memory::Signals::System, "Wrong RegEx modifiers"});
        return {};
    }

    return icRegEx{pattern, mods};
}

}  // namespace icL::ce
