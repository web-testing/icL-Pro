#include "int.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>

namespace icL::ce {

Int::Int(il::InterLevel * il, const icString & pattern)
    : ConstLiteral(il, pattern) {}

icVariant Int::getValueOf() {
    int  ret;
    bool ok;

    ret = pattern.toInt(ok);

    if (!ok) {
        il->vm->signal({memory::Signals::System, "Wrong integer literal"});
    }

    return ret;
}

}  // namespace icL::ce
