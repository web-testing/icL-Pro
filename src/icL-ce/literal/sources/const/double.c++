#include "double.h++"

#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/interlevel.h++>
#include <icL-il/main/vmlayer.h++>
#include <icL-il/structures/signal.h++>

#include <icL-memory/state/signals.h++>


namespace icL::ce {

Double::Double(il::InterLevel * il, const icString & pattern)
    : ConstLiteral(il, pattern) {}

icVariant Double::getValueOf() {
    double ret;
    bool   ok;

    ret = pattern.toDouble(ok);

    if (!ok) {
        il->vm->signal({memory::Signals::System, "Wrong double literal"});
    }

    return ret;
}

}  // namespace icL::ce
