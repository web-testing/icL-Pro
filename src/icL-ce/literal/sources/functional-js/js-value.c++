#include "js-value.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-service-main/factory/factory.h++>

#include <utility>

namespace icL::ce {

JsValue::JsValue(
  il::InterLevel * il, const icString & getter, const icString & setter)
    : FunctionalJsLiteral(il)
    , getter(getter)
    , setter(setter) {}

icString JsValue::toString() {
    return "$value";
}

int JsValue::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType JsValue::runNow() {
    m_newContext = service::Factory::fromValue(il, getter, setter);
    return StepType::CommandEnd;
}

Role JsValue::role() {
    return Role::JsValue;
}

}  // namespace icL::ce
