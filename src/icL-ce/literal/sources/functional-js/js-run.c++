#include "js-run.h++"

#include <icL-types/replaces/ic-set.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-il/main/frontend.h++>
#include <icL-il/main/interlevel.h++>

#include <utility>

namespace icL::ce {

JsRun::JsRun(
  il::InterLevel * il, const icString & code, icString /*sessionId*/,
  icString /*windowsId*/)
    : FunctionalJsLiteral(il)
    , code(code) {}

icString JsRun::toString() {
    return "$run";
}

int JsRun::currentRunRank(bool rtl) {
    return rtl ? 8 : -1;
}

StepType JsRun::runNow() {
    if (async) {
        il->server->executeAsync(code, {});
    }
    else {
        il->server->executeSync(code, {});
    }

    return StepType::CommandEnd;
}

Role JsRun::role() {
    return Role::JsRun;
}

const icSet<Role> & JsRun::acceptedPrevs() {
    static icSet<Role> roles = {Role::NoRole};
    return roles;
}

const icSet<Role> & JsRun::acceptedNexts() {
    static icSet<Role> roles = {Role::NoRole};
    return roles;
}

}  // namespace icL::ce
