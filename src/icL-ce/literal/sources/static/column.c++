#include "column.h++"

#include <icL-memory/structures/packed-value-item.h++>

#include <utility>

namespace icL::ce {

Column::Column(il::InterLevel * il, const icString & name, memory::Type type)
    : StaticLiteral(il)
    , name(name)
    , type(type) {}

const icString & Column::getName() {
    return name;
}

memory::Type Column::getType() {
    return type;
}

icString Column::toString() {
    return "column[" % name % " : " % memory::typeToString(type) % ']';
}

Role Column::role() {
    return Role::Column;
}

memory::PackedValueItem Column::packNow() {
    memory::PackedValueItem ret;

    ret.itemType = memory::PackedValueType::Column;
    ret.name     = name;
    ret.type     = type;

    return ret;
}

}  // namespace icL::ce
