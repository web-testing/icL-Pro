#include "parameter.h++"

#include <icL-memory/structures/packed-value-item.h++>

#include <utility>

namespace icL::ce {

Parameter::Parameter(
  il::InterLevel * il, const icString & name, memory::Type type)
    : StaticLiteral(il)
    , name(name)
    , type(type) {}

const icString & Parameter::getName() {
    return name;
}

memory::Type Parameter::getType() {
    return type;
}

bool Parameter::isDefault() const {
    return false;
}

icString Parameter::toString() {
    return "parameter[" % name % " : " % memory::typeToString(type) % ']';
}

Role Parameter::role() {
    return Role::Parameter;
}

memory::PackedValueItem Parameter::packNow() {
    memory::PackedValueItem ret;

    ret.itemType = memory::PackedValueType::Parameter;
    ret.name     = name;
    ret.type     = type;

    return ret;
}

}  // namespace icL::ce
