#include "default-parameter.h++"

#include <icL-service-main/printing/stringify.h++>

#include <icL-memory/structures/argument.h++>
#include <icL-memory/structures/packed-value-item.h++>

namespace icL::ce {

DefaultParameter::DefaultParameter(
  il::InterLevel * il, const icString & name, const icVariant & value)
    : Parameter(il, name, memory::variantToType(value))
    , defaultValue(value) {}

const icVariant & DefaultParameter::getDefaultValue() {
    return defaultValue;
}

memory::PackedValueItem DefaultParameter::packNow() {
    memory::PackedValueItem ret = Parameter::packNow();

    ret.itemType = memory::PackedValueType::DefaultParameter;
    ret.value    = defaultValue;

    return ret;
}

icString DefaultParameter::toString() {
    memory::Argument arg;
    arg.value   = defaultValue;
    arg.varName = getName();
    arg.type    = getType();

    return "parameter[" % getName() % " = " %
           service::Stringify::argConstructor(arg, nullptr);
}

bool DefaultParameter::isDefault() const {
    return true;
}

}  // namespace icL::ce
