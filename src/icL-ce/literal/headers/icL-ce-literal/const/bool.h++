#ifndef ce_Bool
#define ce_Bool

#include <icL-ce-base/literal/const-literal.h++>



namespace icL::ce {

/**
 * @brief The Bool class represent a bool literal `true` or `false`
 */
class Bool : public ConstLiteral
{
public:
    Bool(il::InterLevel * il, const icString & pattern);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;
};

}  // namespace icL::ce

#endif  // ce_Bool
