#ifndef ce_String
#define ce_String

#include <icL-ce-base/literal/const-literal.h++>



namespace icL::ce {

/**
 * @brief The string class represent a string literal `".*"`
 */
class String : public ConstLiteral
{
public:
    String(il::InterLevel * il, const icString & pattern);

    // CE interface
public:
    icString toString() override;

    // ConstLiteral interface
public:
    icVariant getValueOf() override;
};

}  // namespace icL::ce

#endif  // ce_String
