#ifndef ce_RegEx
#define ce_RegEx

#include <icL-ce-base/literal/const-literal.h++>



namespace icL::ce {

/**
 * @brief The regex class represents a regex token `//pattern//mods`
 */
class RegEx : public ConstLiteral
{
public:
    RegEx(il::InterLevel * il, const icString & pattern, const icString &mods);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;

    // fields
private:
    icString mods;
};

}  // namespace icL::ce



#endif  // ce_RegEx
