#ifndef ce_Int
#define ce_Int

#include <icL-ce-base/literal/const-literal.h++>



namespace icL::ce {

/**
 * @brief The Int class represent and iteger literal `\-?\d+`
 */
class Int : public ConstLiteral
{
public:
    Int(il::InterLevel * il, const icString & pattern);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;
};

}  // namespace icL::ce

#endif  // ce_Int
