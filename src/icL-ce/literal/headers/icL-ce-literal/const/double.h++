#ifndef ce_Double
#define ce_Double

#include <icL-ce-base/literal/const-literal.h++>



namespace icL::ce {

/**
 * @brief The Double class represent a double literal `\-?\d+\.\d+`
 */
class Double : public ConstLiteral
{
public:
    Double(il::InterLevel * il, const icString & pattern);

    // ConstLiteral interface
public:
    icVariant getValueOf() override;
};

}  // namespace icL::ce

#endif  // ce_Double
