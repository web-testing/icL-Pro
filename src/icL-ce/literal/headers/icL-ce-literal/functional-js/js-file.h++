#ifndef ce_JsFile
#define ce_JsFile

#include <icL-ce-base/literal/functional-js-literal.h++>



class icString;

namespace icL::ce {

/**
 * @brief The JsFile class represents a `$file` token
 */
class JsFile : public FunctionalJsLiteral
{
    icString filename;

public:
    JsFile(il::InterLevel * il, const icString & filename);

protected:
    /**
     * @brief getFileContent gets the content of file
     * @return the content of file
     */
    icString getFileContent();

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    Role role() override;
};

}  // namespace icL::ce

#endif  // ce_JsFile
