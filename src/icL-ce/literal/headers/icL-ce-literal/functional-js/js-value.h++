#ifndef ce_JsValue
#define ce_JsValue

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce-base/literal/functional-js-literal.h++>



namespace icL::ce {

class JsValue : public FunctionalJsLiteral
{
public:
    JsValue(il::InterLevel * il, const icString &getter, const icString &setter);

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;
    Role     role() override;

    // fields
private:
    /// \brief getter is the js code to get the value
    icString getter;
    /// \brief setter is the js code to icSet the value
    icString setter;
};

}  // namespace icL::ce

#endif  // ce_JsValue
