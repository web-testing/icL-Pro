#ifndef ce_Function
#define ce_Function

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce-base/literal/functional-literal.h++>



namespace icL::ce {

class Function : public FunctionalLiteral
{
public:
    Function(il::InterLevel * il, const icString &name);

    const icString & getName() const;

    // CE interface
public:
    icString toString() override;
    int      currentRunRank(bool rtl) override;
    StepType runNow() override;

    CE * lastToReplace() override;
    Role role() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

    // field
private:
    /// \brief name is the name of function
    icString name;
    /// \brief executed is true after function executed
    bool executed = false;

    // padding
    long : 56;
};

}  // namespace icL::ce

#endif  // ce_Function
