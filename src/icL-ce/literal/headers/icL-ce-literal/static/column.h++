#ifndef ce_Column
#define ce_Column

#include <icL-types/replaces/ic-string.h++>

#include <icL-ce-base/literal/static-literal.h++>

#include <icL-memory/structures/type.h++>



namespace icL::ce {

/**
 * @brief The Column class represent a column token `name : type`
 */
class Column : public StaticLiteral
{
public:
    Column(il::InterLevel * il, const icString &name, memory::Type type);

    /**
     * @brief getName gets the name of column
     * @return the name of column
     */
    const icString & getName();

    /**
     * @brief getType gets the type of column
     * @return the type of column
     */
    memory::Type getType();

    // CE interface
public:
    icString toString() override;
    Role     role() override;

    memory::PackedValueItem packNow() override;

private:
    /// \brief name is the name of the column
    icString name;
    /// \brief type is the type of the column
    memory::Type type;

    // padding
    int : 32;
};

}  // namespace icL::ce

#endif  // ce_Column
