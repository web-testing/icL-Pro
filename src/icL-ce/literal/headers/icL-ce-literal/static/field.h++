#ifndef ce_Field
#define ce_Field

#include <icL-types/replaces/ic-string.h++>
#include <icL-types/replaces/ic-variant.h++>

#include <icL-ce-base/literal/static-literal.h++>



namespace icL::ce {

/**
 * @brief The Field class represent a field token - `name = value`
 */
class Field : public StaticLiteral
{
public:
    /**
     * @brief Field constructs a inited field
     * @param il is the InterLevel node
     * @param name is the name of field
     * @param type is the type of field
     */
    Field(il::InterLevel * il, const icString & name, const icVariant & value);

    /**
     * @brief getName gets the name of field
     * @return the name of the field
     */
    const icString & getName();

    /**
     * @brief getValue gets the value of field
     * @return the value of the field
     */
    const icVariant & getFieldValue();

    // CE interface
public:
    icString toString() override;
    Role     role() override;

    memory::PackedValueItem packNow() override;

private:
    /// \brief name is the name of the field
    icString name;
    /// \brief value is the value of field
    icVariant value;
};

}  // namespace icL::ce

#endif  // ce_Field
