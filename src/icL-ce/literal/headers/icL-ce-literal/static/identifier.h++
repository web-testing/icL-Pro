#ifndef ce_Identifier
#define ce_Identifier

#include <icL-ce-base/literal/static-literal.h++>


namespace icL::ce {

class Identifier : public StaticLiteral
{
public:
    Identifier(il::InterLevel * il, const icString &name);

    const icString & getName() const;

    // CE interface
public:
    icString toString() override;

    // CE interface
public:
    Role role() override;
    bool hasValue() override;

protected:
    const icSet<Role> & acceptedPrevs() override;
    const icSet<Role> & acceptedNexts() override;

private:
    /// \brief name is the name of identifier
    icString name;
};

}  // namespace icL::ce

#endif  // ce_Identifier
